import json
import mimetypes
import uuid
from copy import deepcopy
from dataclasses import InitVar, asdict, dataclass, field
from datetime import datetime, timedelta, timezone
from enum import Enum
from json import JSONEncoder
from typing import Any, List, Optional

from dateutil import parser
from django.conf import settings
from django.utils.functional import cached_property

from application_registry.__init__ import __version__ as app_version
from apps.applications.models import ApplicationState
from apps.timezone_utils import localtime, safe_parse_and_localtime

from .errors import ValidationError

NIL_UUID = '00000000-0000-0000-0000-000000000000'
NA_DOCUMENT_ID = 'n/a'


@dataclass
class Application:
    raw: dict = field(init=False)
    metadata: '_Metadata' = field(init=False)

    id: str = field(init=False)  # noqa
    tenant_id: str = field(init=False)
    service_name: str = field(init=False)
    service_id: str = field(init=False)
    user: str = field(init=False)
    event_id: str = field(init=False)
    event_version: str = field(init=False)
    status_name: str = field(init=False)
    submitted_at: datetime = field(init=False)
    subject: str = field(init=False)
    flow_changed_at: datetime = field(init=False, default=None)

    applicant: '_Applicant' = field(init=False)
    compiled_module: Optional['_Document'] = field(init=False, default=None)
    attachments: List['_Document'] = field(init=False)

    outcome_file: Optional['_Document'] = field(init=False, default=None)
    outcome_attachments: List['_Document'] = field(init=False)

    outbound_integration_file: Optional['_Document'] = field(init=False, default=None)
    outbound_integration_attachments: List['_Document'] = field(init=False, default_factory=list)
    inbound_integration_file: Optional['_Document'] = field(init=False, default=None)
    inbound_integration_attachments: List['_Document'] = field(init=False, default_factory=list)
    inbound_withdraw_file: Optional['_Document'] = field(init=False, default=None)

    data: InitVar[dict]

    def __post_init__(self, data):
        try:
            self._post_init(data)
        except Exception as e:
            raise ValidationError(e) from e

    def _post_init(self, data):
        self.raw = deepcopy(data)
        self.metadata = _Metadata(
            **data.get(
                settings.REGISTRY_METADATA,
                {'attempts': 0, 'retry_at': None, 'retry_topic': None},
            ),
        )

        self.id = data['id']
        self.tenant_id = data['tenant']
        self.service_name = data['service_name']
        self.service_id = data['service_id']
        self.user = data['user']
        self.event_id = data['event_id'] or NIL_UUID
        self.event_version = data['event_version']
        self.status_name = data['status_name'].lower()
        self.submitted_at = safe_parse_and_localtime(data['submitted_at']) if data['submitted_at'] else localtime()
        self.subject = data['subject']

        if flow_changed_at := data.get('flow_changed_at', None):
            self.flow_changed_at = safe_parse_and_localtime(flow_changed_at)

        self.applicant = _Applicant(data['data'])

        self._init_inbound_documents(data)
        self._init_outbound_documents(data)
        self._init_integration_documents(data)

    def _init_inbound_documents(self, data):
        if compiled_modules := data['compiled_modules']:
            compiled_module = self._select_latest_compiled_module(compiled_modules)
            self.compiled_module = (
                _Document(compiled_module) if compiled_module.get('protocol_required', True) else None
            )
        if withdraw_file := data.get('attachments', None):
            self.inbound_withdraw_file = (
                _Document(withdraw_file[-1]) if withdraw_file[-1].get('protocol_required', True) else None
            )
        self.attachments = [
            _Document(file_data) for file_data in data['attachments'] if file_data.get('protocol_required', True)
        ]

    def _init_outbound_documents(self, data):
        if outcome_file := data.get('outcome_file', None):
            self.outcome_file = _Document(outcome_file) if outcome_file.get('protocol_required', True) else None
        self.outcome_attachments = [
            _Document(file_data)
            for file_data in data['outcome_attachments']
            if file_data.get('protocol_required', True)
        ]

    def _init_integration_documents(self, data):
        if integrations := data.get('integrations', []):
            latest = integrations[-1]
            outbound, inbound = latest[ApplicationState.Direction.OUTBOUND], latest[ApplicationState.Direction.INBOUND]
            self.outbound_integration_file = _Document(outbound) if outbound.get('protocol_required', True) else None
            self.outbound_integration_attachments = [
                _Document(file_data)
                for file_data in outbound['attachments']
                if file_data.get('protocol_required', True)
            ]
            if inbound:
                self.inbound_integration_file = _Document(inbound) if inbound.get('protocol_required', True) else None
                self.inbound_integration_attachments = [
                    _Document(file_data)
                    for file_data in inbound['attachments']
                    if file_data.get('protocol_required', True)
                ]

    @staticmethod
    def _select_latest_compiled_module(compiled_modules):
        if len(compiled_modules) > 1:
            return sorted(
                compiled_modules,
                key=lambda item: parser.isoparse(item['created_at']),
                reverse=True,
            )[0]
        return compiled_modules[0]

    @cached_property
    def _state(self):
        return ApplicationState.objects.get(name=self.status_name)

    @property
    def _is_integration(self):
        return self._state.integrations

    @property
    def direction(self):
        return self._state.direction

    @property
    def is_inbound(self):
        return self.direction == ApplicationState.Direction.INBOUND

    @property
    def is_outbound(self):
        return self.direction == ApplicationState.Direction.OUTBOUND

    @property
    def current_documents(self):
        if self._is_integration and self.is_outbound:
            return [self.outbound_integration_file, *self.outbound_integration_attachments]
        elif self._is_integration and self.is_inbound:
            return [self.inbound_integration_file, *self.inbound_integration_attachments]
        elif self._state.name == 'status_withdraw' and self.is_inbound:
            return [self.inbound_withdraw_file, *[]]
        elif self.is_inbound:
            return [self.compiled_module, *self.attachments]
        return [self.outcome_file, *self.outcome_attachments]

    @property
    def current_document(self):
        document, *_ = self.current_documents
        return document

    @property
    def current_attachments(self):
        _, *attachments = self.current_documents
        return attachments

    @property
    def is_registration_required(self):
        return self.current_document is not None

    @property
    def registration_date(self):
        return self.flow_changed_at or self.submitted_at

    def build_registration_data(self, reg_number, reg_dt, reg_folder=None):
        if not self._state.update_sdc:
            return {}

        if self._is_integration and self.is_outbound:
            return {
                'integration_outbound_protocol_document_id': (
                    self.outbound_integration_file.attributes.remote_id
                    if self.outbound_integration_file.attributes
                    and self.outbound_integration_file.attributes.remote_id
                    else NA_DOCUMENT_ID
                ),
                'integration_outbound_protocol_number': reg_number,
                'integration_outbound_protocolled_at': reg_dt.isoformat(),
            }
        elif self._is_integration and self.is_inbound:
            return {
                'integration_inbound_protocol_document_id': (
                    self.inbound_integration_file.attributes.remote_id
                    if self.inbound_integration_file.attributes and self.inbound_integration_file.attributes.remote_id
                    else NA_DOCUMENT_ID
                ),
                'integration_inbound_protocol_number': reg_number,
                'integration_inbound_protocolled_at': reg_dt.isoformat(),
            }
        elif self.is_inbound:
            return {
                'protocol_document_id': (
                    self.compiled_module.attributes.remote_id
                    if self.compiled_module.attributes and self.compiled_module.attributes.remote_id
                    else NA_DOCUMENT_ID
                ),
                'protocol_number': reg_number,
                'protocolled_at': reg_dt.isoformat(),
                'protocol_folder_code': reg_folder,
            }
        return {
            'outcome_protocol_document_id': (
                self.outcome_file.attributes.remote_id
                if self.outcome_file.attributes and self.outcome_file.attributes.remote_id
                else NA_DOCUMENT_ID
            ),
            'outcome_protocol_number': reg_number,
            'outcome_protocolled_at': reg_dt.isoformat(),
        }

    @property
    def retry_data(self):
        data, metadata = deepcopy(self.raw), deepcopy(self.metadata)
        metadata.incr_attempts()
        data[settings.REGISTRY_METADATA] = metadata.serialize()
        topic, value = metadata.retry_topic, json.dumps(data).encode()
        return topic, value

    @property
    def is_first_attempt(self):
        return self.metadata.attempts == 0

    @property
    def is_last_attempt(self):
        return self.metadata.attempts == settings.MAX_ATTEMPTS - 1

    @property
    def time_since_submission(self):
        return localtime() - self.submitted_at

    @property
    def dead_letter_data(self):
        data = deepcopy(self.raw)
        data.pop(settings.REGISTRY_METADATA, None)
        return data

    @property
    def project_description(self):
        return f'{self.service_name} {self.applicant.full_name} {self.applicant.fiscal_code}'

    @property
    def document_description(self):
        document_description = self.subject
        if self.applicant.name not in self.subject:
            document_description = f'{document_description} {self.applicant.name}'
        if self.applicant.surname not in self.subject:
            document_description = f'{document_description} {self.applicant.surname}'
        if self.applicant.fiscal_code and self.applicant.fiscal_code not in self.subject:
            document_description = f'{document_description} {self.applicant.fiscal_code}'
        if self.id not in self.subject:
            document_description = f'{document_description} - {self.id}'
        return document_description

    def create_document(self, document_id, number, date, folder, status):
        return Document(self, document_id, number, date, folder, status)


@dataclass
class _Metadata:
    attempts: int
    retry_at: Optional[str]
    retry_topic: Optional[str]

    def incr_attempts(self):
        self.attempts += 1
        topic, delay = self._retry_params
        self.retry_topic = topic
        self.retry_at = (datetime.now(timezone.utc) + delay).isoformat()

    @property
    def _retry_params(self):
        if 1 <= self.attempts <= 2:
            return (settings.RETRY_TOPIC_10, timedelta(minutes=10))
        if 3 <= self.attempts <= 5:
            return (settings.RETRY_TOPIC_30, timedelta(minutes=30))
        if 6 <= self.attempts <= 9:
            return (settings.RETRY_TOPIC_180, timedelta(minutes=180))
        return (settings.RETRY_TOPIC_720, timedelta(minutes=720))  # attempts >= 10

    def serialize(self):
        return asdict(self)


@dataclass
class _Document:
    url: str = field(init=False)
    original_name: str = field(init=False)
    description: str = field(init=False)
    mimetype: str = field(init=False)
    attributes: Any = field(init=False, default=None)

    data: InitVar[dict]

    def __post_init__(self, data):
        self.url = data['url']
        self.original_name = data['originalName']
        self.description = data['description']
        mimetype, _ = mimetypes.guess_type(self.original_name)
        self.mimetype = mimetype or 'application/octet-stream'


@dataclass
class _Applicant:
    name: Optional[str] = field(init=False)
    surname: Optional[str] = field(init=False)
    fiscal_code: Optional[str] = field(init=False)
    address: Optional[str] = field(init=False)
    house_number: Optional[str] = field(init=False)
    house_number_digits: Optional[str] = field(init=False)
    house_number_letters: Optional[str] = field(init=False)
    postal_code: Optional[str] = field(init=False)
    municipality: Optional[str] = field(init=False)
    county: Optional[str] = field(init=False)
    email: Optional[str] = field(init=False)

    data: InitVar[dict]

    def __post_init__(self, data):
        data = data or {}
        self.name = data.get('applicant.data.completename.data.name') or None
        self.surname = data.get('applicant.data.completename.data.surname') or None
        self.fiscal_code = data.get('applicant.data.fiscal_code.data.fiscal_code') or None
        self.address = data.get('applicant.data.address.data.address') or None
        house_number = data.get('applicant.data.address.data.house_number') or None
        self.house_number = house_number
        self.house_number_digits, self.house_number_letters = self._split_house_number(house_number)
        self.postal_code = data.get('applicant.data.address.data.postal_code') or None
        self.municipality = data.get('applicant.data.address.data.municipality') or None
        self.county = data.get('applicant.data.address.data.county') or None
        self.email = data.get('applicant.data.email_address') or None

    @staticmethod
    def _split_house_number(house_number):
        if not house_number:
            return None, None
        digits, letters = [], []
        for char in house_number:
            if char.isdigit():
                digits.append(char)
            if char.isalpha():
                letters.append(char)
        digits = ''.join(digits) or None
        letters = ''.join(letters) or None
        return digits, letters

    @property
    def address_with_house_number(self):
        if not self.address:
            return None
        return f"{self.address} {self.house_number or ''}".strip()

    @property
    def full_name(self):
        name = self.name or ''
        surname = self.surname or ''
        return f'{name} {surname}'.strip()

    @property
    def description(self):
        name = self.name or ''
        surname = self.surname or ''
        fiscal_code = self.fiscal_code or ''
        email = self.email or ''
        return f'{surname} {name} {fiscal_code} - {email}'.strip()


@dataclass
class Author:
    type: str = field(init=False)  # noqa
    tax_identification_number: str = field(init=False)
    name: str = field(init=False)
    family_name: str = field(init=False)
    street_name: Optional[str] = field(init=False)
    building_number: Optional[str] = field(init=False)
    postal_code: Optional[str] = field(init=False)
    email: Optional[str] = field(init=False)
    role: str = field(init=False)

    applicant: InitVar[_Applicant]

    def __post_init__(self, applicant: _Applicant):
        self.type = 'human'
        self.tax_identification_number = applicant.fiscal_code
        self.name = applicant.name
        self.family_name = applicant.surname
        self.street_name = applicant.address
        self.building_number = applicant.house_number
        self.postal_code = applicant.postal_code
        self.email = applicant.email
        self.role = 'sender'

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class Folder:
    title: str = field(init=False)
    id: str = field(init=False)  # noqa

    param_title: InitVar[str]
    param_id: InitVar[str]

    def __post_init__(self, param_title: str, param_id: str):
        self.title = param_title
        self.id = param_id

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class File:
    name: str = field(init=False)
    description: str = field(init=False)
    mime_type: str = field(init=False)
    url: str = field(init=False)
    filename: str = field(init=False)
    md5: Optional[str] = field(init=False, default=None)

    current_document: InitVar[_Document]

    def __post_init__(self, current_document: _Document):
        self.name = current_document.original_name
        self.description = current_document.description
        self.mime_type = current_document.mimetype
        self.url = current_document.url
        self.filename = current_document.original_name
        self.md5 = None

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class TransmissionType(Enum):
    INBOUND = 'Inbound'
    OUTBOUND = 'Outbound'

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class DocumentType:
    APPLICATION_REQUEST = 'application-request'
    INTEGRATION_REQUEST = 'integration-request'
    INTEGRATION_RESPONSE = 'integration-response'
    APPLICATION_OUTCOME = 'application-outcome'
    APPLICATION_WITHDRAW = 'application-withdraw'
    APPLICATION_REVOCATION = 'application-revocation'

    def __init__(self, state):
        self._state = state

    @property
    def document_type(self):
        if self._state == 'status_submitted':
            return self.APPLICATION_REQUEST
        elif self._state == 'status_request_integration':
            return self.INTEGRATION_REQUEST
        elif self._state == 'status_submitted_after_integration':
            return self.INTEGRATION_RESPONSE
        elif self._state == 'status_complete':
            return self.APPLICATION_OUTCOME
        elif self._state == 'status_withdraw':
            return self.APPLICATION_WITHDRAW
        elif self._state == 'status_cancelled':
            return self.APPLICATION_REVOCATION

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class RegistrationData:
    transmission_type: TransmissionType = field(init=False)
    date: datetime = field(init=False)
    document_number: str = field(init=False)

    param_transmission_type: InitVar[TransmissionType]
    param_date: InitVar[datetime]
    param_document_number: InitVar[str]

    def __post_init__(self, param_transmission_type, param_date, param_document_number):
        self.transmission_type = param_transmission_type
        self.date = param_date
        self.document_number = param_document_number

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class RemoteCollection:
    id: uuid.UUID = field(init=False)  # noqa
    type: str = field(init=False)  # noqa

    param_id: InitVar[uuid.UUID]
    param_type: InitVar[str]

    def __post_init__(self, param_id, param_type):
        self.id = param_id
        self.type = param_type

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class DocumentTitle:
    APPLICATION_REQUEST = 'Richiesta:'
    INTEGRATION_REQUEST = 'Richiesta integrazione:'
    INTEGRATION_RESPONSE = 'Risposta integrazione:'
    APPLICATION_OUTCOME = 'Accettazione:'
    APPLICATION_WITHDRAW = 'Ritiro:'
    APPLICATION_REVOCATION = 'Rifiuto:'
    APPLICATION_CANCELLATION = 'Annullamento:'

    def __init__(self, state, description):
        self._state = state
        self._description = description

    @property
    def title(self):
        if self._state == 'status_submitted':
            return f'{self.APPLICATION_REQUEST} {self._description}'
        elif self._state == 'status_request_integration':
            return f'{self.INTEGRATION_REQUEST} {self._description}'
        elif self._state == 'status_submitted_after_integration':
            return f'{self.INTEGRATION_RESPONSE} {self._description}'
        elif self._state == 'status_complete':
            return f'{self.APPLICATION_OUTCOME} {self._description}'
        elif self._state == 'status_withdraw':
            return f'{self.APPLICATION_WITHDRAW} {self._description}'
        elif self._state == 'status_cancelled':
            return f'{self.APPLICATION_REVOCATION} {self._description}'
        return self._description

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class DocumentStatus(Enum):
    REGISTRATION_PENDING = 'REGISTRATION_PENDING'
    REGISTRATION_COMPLETE = 'REGISTRATION_COMPLETE'
    PARTIAL_REGISTRATION = 'PARTIAL_REGISTRATION'
    REGISTRATION_FAILED = 'REGISTRATION_FAILED'
    DOCUMENT_CREATED = 'DOCUMENT_CREATED'

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


@dataclass
class Document:
    app_id: str = field(init=False)
    event_created_at: datetime = field(init=False)
    event_id: uuid.UUID = field(init=False)
    title: str = field(init=False)
    registry_search_key: str = field(init=False)
    id: uuid.UUID = field(init=False)  # noqa
    event_version: int = field(init=False)
    status: 'DocumentStatus' = field(init=False)
    registration_data: 'RegistrationData' = field(init=False)
    folder: Folder = field(init=False)
    type: str = field(init=False)  # noqa
    remote_id: uuid.UUID = field(init=False)
    remote_collection: 'RemoteCollection' = field(init=False)
    short_description: str = field(init=False)
    description: str = field(init=False)
    main_document: 'File' = field(init=False)
    attachments: List['File'] = field(init=False)
    tenant_id: uuid.UUID = field(init=False)
    owner_id: uuid.UUID = field(init=False)
    created_at: datetime = field(init=False)
    updated_at: datetime = field(init=False)
    author: List['Author'] = field(init=False)
    source_type: str = field(init=False)
    recipient_type: str = field(init=False)
    topics: Optional[str] = field(init=False, default=None)
    image_gallery: Optional[str] = field(init=False, default=None)
    related_public_services: Optional[str] = field(init=False, default=None)
    normative_requirements: Optional[str] = field(init=False, default=None)
    related_documents: Optional[str] = field(init=False, default=None)
    life_events: Optional[str] = field(init=False, default=None)
    business_events: Optional[str] = field(init=False, default=None)
    allowed_readers: Optional[str] = field(init=False, default=None)

    data: InitVar[Application]
    document_id: InitVar[uuid.UUID]
    number: InitVar[Optional[str]]
    date: InitVar[Optional[datetime]]
    param_folder: InitVar[Optional[str]]
    param_status: InitVar[DocumentStatus]

    def __post_init__(
        self,
        data: Application,
        document_id: uuid.UUID,
        number: Optional[str],
        date: Optional[datetime],
        param_folder: Optional[str],
        param_status: DocumentStatus,
    ):
        self.app_id = f'{settings.APP_NAME}:{app_version}'
        self.event_created_at = datetime.now()
        self.event_id = uuid.uuid4()
        self.title = DocumentTitle(data.status_name, data.document_description).title
        self.registry_search_key = DocumentTitle(data.status_name, data.document_description).title
        self.id = document_id
        self.event_version = 1
        self.status = param_status
        self.registration_data = RegistrationData(
            param_transmission_type=TransmissionType.INBOUND if data.is_inbound else TransmissionType.OUTBOUND,
            param_date=date,
            param_document_number=number,
        )
        self.folder = Folder(param_title=data.project_description, param_id=param_folder)
        self.type = DocumentType(data.status_name).document_type
        self.remote_id = uuid.UUID(data.id)
        self.remote_collection = RemoteCollection(param_id=uuid.UUID(data.service_id), param_type='service')
        self.short_description = (
            data.document_description
            if len(data.document_description) <= 255
            else f'{data.document_description[:252]}...'
        )
        self.description = data.document_description
        self.main_document = File(data.current_document)
        self.attachments = [File(item) for item in data.current_attachments]
        self.tenant_id = uuid.UUID(data.tenant_id)
        self.owner_id = uuid.UUID(data.user)
        self.created_at = datetime.now()
        self.updated_at = datetime.now()
        self.author = [Author(data.applicant)]
        self.source_type = 'user'
        self.recipient_type = 'tenant'
        self.topics = None
        self.image_gallery = None
        self.related_public_services = None
        self.normative_requirements = None
        self.related_documents = None
        self.life_events = None
        self.business_events = None
        self.allowed_readers = None

    def json(self):
        return json.dumps(self, cls=DocumentEncoder)


class DocumentEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.replace(microsecond=0).astimezone().isoformat()
        elif isinstance(o, uuid.UUID):
            return str(o)
        elif isinstance(o, Enum):
            return o.value
        return o.__dict__
