class SDCError(Exception):
    pass


class TransportError(SDCError):
    pass


class ValidationError(SDCError):
    pass
