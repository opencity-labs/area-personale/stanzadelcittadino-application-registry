from dataclasses import asdict, dataclass

import requests
from django.utils.functional import cached_property
from requests.exceptions import RequestException

from .errors import SDCError, TransportError, ValidationError

TIMEOUT = 30


@dataclass
class Credentials:
    username: str
    password: str

    def to_sdc(self):
        return asdict(self)


class Client:
    http_client = requests

    def __init__(self, *, credentials, base_url):
        self._auth = credentials
        self._base_url = base_url

    @cached_property
    def _token(self):
        url = f'{self._base_url}/auth'
        response = self.http_client.post(url, json=self._auth.to_sdc(), timeout=TIMEOUT)
        response.raise_for_status()
        try:
            return response.json()['token']
        except Exception as e:
            raise ValidationError(e) from e

    def get_document(self, url):
        try:
            return self._get_document(url)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SDCError(e) from e

    def _get_document(self, url):
        headers = {'Authorization': f'Bearer {self._token}'}
        response = self.http_client.get(url, headers=headers, timeout=TIMEOUT)
        response.raise_for_status()
        return response.content

    def update_application(self, application_id, data):
        try:
            self._update_application(application_id, data)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SDCError(e) from e

    def _update_application(self, application_id, data):
        url = f'{self._base_url}/applications/{application_id}'
        headers = {'Authorization': f'Bearer {self._token}'}
        response = self.http_client.patch(url, json=data, headers=headers, timeout=TIMEOUT)
        response.raise_for_status()
