import pytz
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers


class UTCDateTimeField(serializers.DateTimeField):
    def __init__(self, *args, **kwargs):
        super().__init__(default_timezone=pytz.utc, *args, **kwargs)


@extend_schema_field(OpenApiTypes.UUID)
class UUIDSlugRelatedField(serializers.SlugRelatedField):
    pass
