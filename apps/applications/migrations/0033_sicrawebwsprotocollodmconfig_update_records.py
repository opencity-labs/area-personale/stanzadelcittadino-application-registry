# Generated by Django 3.2.22 on 2024-03-15 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0032_auto_20231109_1808'),
    ]

    operations = [
        migrations.AddField(
            model_name='sicrawebwsprotocollodmconfig',
            name='update_records',
            field=models.CharField(help_text='One of: N, S or F', max_length=32, null=True, verbose_name='update records'),
        ),
    ]
