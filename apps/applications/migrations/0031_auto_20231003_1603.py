# Generated by Django 3.2.16 on 2023-10-03 14:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0030_auto_20230921_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationevent',
            name='provider',
            field=models.CharField(choices=[('datagraph', 'datagraph'), ('dedagroup', 'dedagroup'), ('halley', 'halley'), ('halley-cloud', 'halley-cloud'), ('maggioli', 'maggioli'), ('pitre', 'pitre'), ('insiel', 'insiel'), ('infor', 'infor'), ('d3', 'd3'), ('hypersic', 'hypersic'), ('tinn', 'tinn'), ('sicraweb-wsprotocollodm', 'sicraweb-wsprotocollodm')], max_length=32, verbose_name='provider'),
        ),
        migrations.AlterField(
            model_name='service',
            name='provider',
            field=models.CharField(choices=[('datagraph', 'datagraph'), ('dedagroup', 'dedagroup'), ('halley', 'halley'), ('halley-cloud', 'halley-cloud'), ('maggioli', 'maggioli'), ('pitre', 'pitre'), ('insiel', 'insiel'), ('infor', 'infor'), ('d3', 'd3'), ('hypersic', 'hypersic'), ('tinn', 'tinn'), ('sicraweb-wsprotocollodm', 'sicraweb-wsprotocollodm')], max_length=32, verbose_name='provider'),
        ),
        migrations.CreateModel(
            name='SicrawebWSProtocolloDMConfig',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('description', models.CharField(blank=True, max_length=128, verbose_name='description')),
                ('is_active', models.BooleanField(verbose_name='active')),
                ('url', models.URLField(verbose_name='URL')),
                ('connection_string', models.CharField(max_length=64, verbose_name='connection string')),
                ('institution_code', models.CharField(max_length=32, null=True, verbose_name='institution code')),
                ('institution_ou', models.CharField(max_length=32, null=True, verbose_name='institution OU')),
                ('institution_aoo_code', models.CharField(max_length=32, verbose_name='AOO code')),
                ('sending_mode', models.CharField(max_length=32, null=True, verbose_name='sending mode')),
                ('internal_sender', models.CharField(max_length=32, null=True, verbose_name='internal sender')),
                ('classification_hierarchy', models.CharField(max_length=16, null=True, verbose_name='classification hierarchy')),
                ('folder_number', models.CharField(max_length=16, null=True, verbose_name='folder number')),
                ('folder_year', models.CharField(max_length=4, null=True, verbose_name='folder year')),
                ('connection_user', models.CharField(max_length=32, null=True, verbose_name='connection user')),
                ('connection_user_role', models.CharField(max_length=32, null=True, verbose_name='connection user role')),
                ('standard_subject', models.CharField(max_length=32, null=True, verbose_name='standard subject')),
                ('document_type', models.CharField(max_length=32, null=True, verbose_name='document type')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sicraweb_wsprotocollodm_configs', to='applications.service', verbose_name='service')),
            ],
        ),
        migrations.AddConstraint(
            model_name='sicrawebwsprotocollodmconfig',
            constraint=models.UniqueConstraint(condition=models.Q(('is_active', True)), fields=('service', 'is_active'), name='applications_sicrawebwsprotocollodmconfig_unique_service_is_active_true'),
        ),
    ]
