# Generated by Django 3.2.13 on 2023-01-02 09:36

from django.db import migrations, models
import django_jsonform.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0021_auto_20221104_1145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='halleycloudconfig',
            name='folder_id',
            field=models.CharField(help_text='The list of folder names is provided to the institution by the protocol manager, if omitted the default folder will be used', max_length=64, null=True, verbose_name='Folder name'),
        ),
        migrations.RenameField(
            model_name='halleycloudconfig',
            old_name='folder_id',
            new_name='folder_name',
        ),
        migrations.AlterField(
            model_name='halleycloudconfig',
            name='classification_category',
            field=models.CharField(help_text='The list of classifications is provided to the institution by the protocol manager, if omitted the default classification will be used', max_length=16, null=True, verbose_name='classification category'),
        ),
        migrations.AlterField(
            model_name='halleycloudconfig',
            name='institution_office_ids',
            field=django_jsonform.models.fields.ArrayField(base_field=models.PositiveIntegerField(), help_text='The list of office ids is provided to the institution by the protocol manager, if omitted the default office will be used', size=1, verbose_name='institution office IDs'),
        ),
        migrations.AlterField(
            model_name='insielconfig',
            name='register_code',
            field=models.CharField(help_text="Right aligned levels: ex: '  1  2  3'", max_length=32, null=True, verbose_name='Register code'),
        ),
        migrations.AlterField(
            model_name='registrationevent',
            name='provider',
            field=models.CharField(choices=[('datagraph', 'datagraph'), ('dedagroup', 'dedagroup'), ('halley', 'halley'), ('halley-cloud', 'halley-cloud'), ('maggioli', 'maggioli'), ('pitre', 'pitre'), ('insiel', 'insiel')], max_length=32, verbose_name='provider'),
        ),
        migrations.AlterField(
            model_name='service',
            name='provider',
            field=models.CharField(choices=[('datagraph', 'datagraph'), ('dedagroup', 'dedagroup'), ('halley', 'halley'), ('halley-cloud', 'halley-cloud'), ('maggioli', 'maggioli'), ('pitre', 'pitre'), ('insiel', 'insiel')], max_length=32, verbose_name='provider'),
        ),
    ]
