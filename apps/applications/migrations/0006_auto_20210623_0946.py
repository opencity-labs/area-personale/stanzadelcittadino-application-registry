# Generated by Django 3.1.12 on 2021-06-23 07:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0005_auto_20210615_0022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tenant',
            name='latest_registration_number_issued_at',
            field=models.DateTimeField(default=django.utils.timezone.localtime, verbose_name='latest registration number issued at'),
        ),
    ]
