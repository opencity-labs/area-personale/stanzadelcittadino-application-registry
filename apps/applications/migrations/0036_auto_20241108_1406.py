# Generated by Django 3.2.22 on 2024-11-08 13:06

import apps.timezone_utils
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0035_auto_20240712_1008'),
    ]

    operations = [
        migrations.AddField(
            model_name='pitreconfig',
            name='transmission_type',
            field=models.CharField(default='T', max_length=128, null=True, verbose_name='transmission_type'),
        ),
        migrations.AlterField(
            model_name='tenant',
            name='latest_registration_number_issued_at',
            field=models.DateTimeField(default=apps.timezone_utils.localtime, verbose_name='latest registration number issued at'),
        ),
    ]
