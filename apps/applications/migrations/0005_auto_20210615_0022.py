# Generated by Django 3.1.12 on 2021-06-14 22:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0004_auto_20210614_2323'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationState',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=32, unique=True, verbose_name='name')),
                ('code', models.CharField(max_length=16, unique=True, verbose_name='code')),
                ('direction', models.CharField(choices=[('inbound', 'inbound'), ('outbound', 'outbound')], max_length=16, verbose_name='direction')),
                ('default', models.BooleanField(verbose_name='default')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='service',
            name='application_states',
            field=models.ManyToManyField(related_name='services', to='applications.ApplicationState', verbose_name='application states'),
        ),
    ]
