from django.conf import settings
from kafka import KafkaProducer


class RetryProducer:
    def __init__(self, *, producer_class=None):
        self._producer_class = producer_class or KafkaProducer

    def __enter__(self):
        self._producer = self._producer_class(
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            client_id=settings.RETRY_PRODUCER_CLIENT_ID,
            security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._producer.close()

    def send(self, topic, value):
        self._producer.send(topic, value)
        self._producer.flush(timeout=10)
