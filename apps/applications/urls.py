from django.urls import path
from rest_framework.routers import SimpleRouter

from apps.applications.views import (
    ApplicationStateViewSet,
    D3ConfigViewSet,
    D3SchemaAPIView,
    D3ServiceViewSet,
    DatagraphConfigViewSet,
    DatagraphSchemaAPIView,
    DatagraphServiceViewSet,
    DeadLetterViewSet,
    DedagroupConfigViewSet,
    DedagroupSchemaAPIView,
    DedagroupServiceViewSet,
    HalleyCloudConfigViewSet,
    HalleyCloudSchemaAPIView,
    HalleyCloudServiceViewSet,
    HalleyConfigViewSet,
    HalleySchemaAPIView,
    HalleyServiceViewSet,
    HypersicConfigViewSet,
    HypersicSchemaAPIView,
    HypersicServiceViewSet,
    InforConfigViewSet,
    InforSchemaAPIView,
    InforServiceViewSet,
    InsielConfigViewSet,
    InsielSchemaAPIView,
    InsielServiceViewSet,
    MaggioliConfigViewSet,
    MaggioliSchemaAPIView,
    MaggioliServiceViewSet,
    PitreConfigViewSet,
    PitreSchemaAPIView,
    PitreServiceViewSet,
    PreRegistrationViewSet,
    ProviderListAPIView,
    RegistrationViewSet,
    ServiceViewSet,
    SicrawebWSProtocolloDMConfigViewSet,
    SicrawebWSProtocolloDMSchemaAPIView,
    SicrawebWSProtocolloDMServiceViewSet,
    SiscomConfigViewSet,
    SiscomSchemaAPIView,
    SiscomServiceViewSet,
    TenantViewSet,
    TinnConfigViewSet,
    TinnSchemaAPIView,
    TinnServiceViewSet,
)

router = SimpleRouter()
router.register(r'services/d3', D3ServiceViewSet, basename='d3-service')
router.register(r'services/datagraph', DatagraphServiceViewSet, basename='datagraph-service')
router.register(r'services/dedagroup', DedagroupServiceViewSet, basename='dedagroup-service')
router.register(r'services/halley', HalleyServiceViewSet, basename='halley-service')
router.register(r'services/halley-cloud', HalleyCloudServiceViewSet, basename='halley-cloud-service')
router.register(r'services/hypersic', HypersicServiceViewSet, basename='hypersic-service')
router.register(r'services/maggioli', MaggioliServiceViewSet, basename='maggioli-service')
router.register(r'services/pitre', PitreServiceViewSet, basename='pitre-service')
router.register(r'services/insiel', InsielServiceViewSet, basename='insiel-service')
router.register(r'services/infor', InforServiceViewSet, basename='infor-service')
router.register(r'services/siscom', SiscomServiceViewSet, basename='siscom-service')
router.register(r'services/tinn', TinnServiceViewSet, basename='tinn-service')
router.register(
    r'services/sicraweb-wsprotocollodm',
    SicrawebWSProtocolloDMServiceViewSet,
    basename='sicraweb-wsprotocollodm-service',
)
router.register(r'services', ServiceViewSet, basename='service')
router.register(r'application-states', ApplicationStateViewSet, basename='application-state')
router.register(r'd3', D3ConfigViewSet, basename='d3-config')
router.register(r'datagraph', DatagraphConfigViewSet, basename='datagraph-config')
router.register(r'dedagroup', DedagroupConfigViewSet, basename='dedagroup-config')
router.register(r'halley', HalleyConfigViewSet, basename='halley-config')
router.register(r'halley-cloud', HalleyCloudConfigViewSet, basename='halley-cloud-config')
router.register(r'hypersic', HypersicConfigViewSet, basename='hypersic-config')
router.register(r'maggioli', MaggioliConfigViewSet, basename='maggioli-config')
router.register(r'pitre', PitreConfigViewSet, basename='pitre-config')
router.register(r'insiel', InsielConfigViewSet, basename='insiel-config')
router.register(r'infor', InforConfigViewSet, basename='infor-config')
router.register(r'siscom', SiscomConfigViewSet, basename='siscom-config')
router.register(r'tinn', TinnConfigViewSet, basename='tinn-config')
router.register(
    r'sicraweb-wsprotocollodm',
    SicrawebWSProtocolloDMConfigViewSet,
    basename='sicraweb-wsprotocollodm-config',
)
router.register(r'registrations', RegistrationViewSet, basename='registration')
router.register(r'pre-registrations', PreRegistrationViewSet, basename='pre-registration')
router.register(r'dead-letters', DeadLetterViewSet, basename='dead-letter')
router.register(r'tenants', TenantViewSet, basename='tenant')

urlpatterns = [
    path('providers/', ProviderListAPIView.as_view(), name='provider'),
    path('services-schema/d3', D3SchemaAPIView.as_view(), name='d3-service-schema'),
    path('services-schema/datagraph', DatagraphSchemaAPIView.as_view(), name='datagraph-service-schema'),
    path('services-schema/dedagroup', DedagroupSchemaAPIView.as_view(), name='dedagroup-service-schema'),
    path('services-schema/halley', HalleySchemaAPIView.as_view(), name='halley-service-schema'),
    path('services-schema/halley-cloud', HalleyCloudSchemaAPIView.as_view(), name='halley-cloud-service-schema'),
    path('services-schema/hypersic', HypersicSchemaAPIView.as_view(), name='hypersic-service-schema'),
    path('services-schema/infor', InforSchemaAPIView.as_view(), name='infor-service-schema'),
    path('services-schema/insiel', InsielSchemaAPIView.as_view(), name='insiel-service-schema'),
    path('services-schema/maggioli', MaggioliSchemaAPIView.as_view(), name='maggioli-service-schema'),
    path('services-schema/pitre', PitreSchemaAPIView.as_view(), name='pitre-service-schema'),
    path('services-schema/siscom', SiscomSchemaAPIView.as_view(), name='siscom-service-schema'),
    path('services-schema/tinn', TinnSchemaAPIView.as_view(), name='tinn-service-schema'),
    path(
        'services-schema/sicraweb-wsprotocollodm',
        SicrawebWSProtocolloDMSchemaAPIView.as_view(),
        name='sicraweb-wsprotocollodm-service-schema',
    ),
    *router.urls,
]
