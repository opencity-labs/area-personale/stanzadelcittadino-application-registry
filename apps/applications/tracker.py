import logging
import sys

import sentry_sdk

from apps.applications.kafka import RetryProducer
from apps.applications.metrics import GravelGatewayClient
from apps.applications.models import DeadLetter, PreRegistration, Registration, RegistrationEvent
from apps.applications.sdc.entities import DocumentStatus
from apps.workers.errors import RegistrationError
from apps.workers.worker import Producer

logger = logging.getLogger('application_registry')


class RegistrationEventTracker:
    def __init__(
        self,
        document_id,
        application,
        config,
        produce_document=False,
        use_preregistration=False,
        *,
        retry_producer=None,
        gravel_client=None,
    ):
        self._document_id = document_id
        self._application = application
        self._config = config
        self._produce_document = produce_document
        self._use_preregistration = use_preregistration
        self._reg_event = RegistrationEvent.objects.create(
            provider=config.provider,
            config_id=config.id,
            document_id=document_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            data=application.raw,
        )
        self._retry_producer = retry_producer or RetryProducer()
        self._gravel_client = gravel_client or GravelGatewayClient()

    def __enter__(self):
        logger.info('Starting job...')
        self._update_latency_metric()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self._on_success()
            return
        self._on_error(exc_type, exc_value, traceback)
        return True  # suppress the exception

    def _on_success(self):
        self._reg_event.complete()
        logger.info('Job completed.')

    def _on_error(self, exc_type, exc_value, traceback):
        self._reg_event.fail(exc_type, exc_value, traceback)
        if self._produce_document:
            self._create_document(number=None, date=None, folder=None)
        error = {
            'error': str(exc_value),
            'provider': self._config.provider.value,
            'tenant': self._application.tenant_id,
            'application': self._application.id,
            'service': self._application.service_id,
        }
        logger.error(error)
        logger.debug('Job failed.')
        self._perform_dead_lettering() if self._application.is_last_attempt else self._retry()
        try:
            raise RegistrationError(error)
        except RegistrationError as re:
            sentry_sdk.capture_exception(re)

    def _create_document(self, number, date, folder=None):
        logger.debug('Producing Document')
        status = (
            DocumentStatus.PARTIAL_REGISTRATION
            if self._is_registration_partial()
            else DocumentStatus.REGISTRATION_FAILED
        )
        document = self._config.tenant.create_document(
            document_id=self._document_id,
            application=self._application,
            number=number,
            date=date,
            folder=folder,
            status=status,
        )
        with Producer() as producer:
            producer.send(
                value=document.json().encode('utf-8'),
                key=str(document.remote_collection.id).encode('utf-8'),
            )

    def _is_registration_partial(self):
        if self._use_preregistration:
            pre_registration_data = PreRegistration.objects.application_pre_registration_data(self._application)
            if pre_registration_data:
                return (
                    len(self._application.current_documents) != len(pre_registration_data['attachments'])
                    and len(pre_registration_data['attachments']) > 0
                )
            return False
        return (
            Registration.objects.filter(
                application_sdc_id=self._application.id,
                application_state_name=self._application.status_name,
                event_sdc_id=self._application.event_id,
            )
            .values_list('registration_data', flat=True)
            .first()
        )

    def _perform_dead_lettering(self):
        DeadLetter.objects.create(
            tenant_sdc_id=self._application.tenant_id,
            service_sdc_id=self._application.service_id,
            application_sdc_id=self._application.id,
            data=self._application.dead_letter_data,
        )
        logger.debug('Application dead lettered.')
        self._update_dead_lettered_metric()

    def _retry(self):
        try:
            self._retry_async()
        except Exception:
            self._on_retry_error()
        finally:
            self._update_retries_metric()

    def _retry_async(self):
        topic, value = self._application.retry_data
        with self._retry_producer as p:
            p.send(topic, value)
        logger.debug('Retry scheduled (topic: %s).', topic)

    def _on_retry_error(self):
        self._reg_event.fail_to_retry(*sys.exc_info())
        logger.debug('Failed to retry.')
        sentry_sdk.capture_exception()

    def _update_latency_metric(self):
        if self._application.is_first_attempt:
            self._gravel_client.set_latency(self._application.time_since_submission)

    def _update_dead_lettered_metric(self):
        self._gravel_client.incr_dead_lettered(self._config.tenant_slug)

    def _update_retries_metric(self):
        self._gravel_client.incr_retries(self._config.tenant_slug)
