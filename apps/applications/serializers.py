from django.db import transaction
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    InsielConfig,
    MaggioliConfig,
    PitreConfig,
    PreRegistration,
    Registration,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)
from apps.applications.serializer_fields import UTCDateTimeField, UUIDSlugRelatedField


class ApplicationStatesSerializerMixin:
    def __init__(self, *args, **kwargs):
        default_application_states = kwargs.pop('default_application_states', False)

        super().__init__(*args, **kwargs)

        kwargs = {
            'slug_field': 'name',
            'queryset': ApplicationState.objects.all(),
            'many': True,
        }

        if default_application_states:
            kwargs['default'] = lambda: list(ApplicationState.objects.default())

        self.fields['application_states'] = serializers.SlugRelatedField(**kwargs)


class TimestampedSerializer(serializers.Serializer):
    created_at = UTCDateTimeField(read_only=True)
    modified_at = UTCDateTimeField(read_only=True)


class ServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'provider',
            'application_states',
            'created_at',
            'modified_at',
        )


class NestedDatagraphConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = DatagraphConfig
        fields = (
            'id',
            'description',
            'is_active',
            'url',
            'codente',
            'username',
            'password',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'classification_institution_code',
            'classification_aoo_code',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'document_format',
            'created_at',
            'modified_at',
        )


class DatagraphServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedDatagraphConfigSerializer(source='datagraph_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.DATAGRAPH
        configs = validated_data.pop('datagraph_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            DatagraphConfig.objects.bulk_create([DatagraphConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.DATAGRAPH
        configs = validated_data.pop('datagraph_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.datagraph_configs.all().delete()
            DatagraphConfig.objects.bulk_create([DatagraphConfig(**config, service=instance) for config in configs])
        return instance


class NestedMaggioliConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = MaggioliConfig
        fields = (
            'id',
            'description',
            'is_active',
            'url',
            'connection_string',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'sending_mode',
            'sending_method',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'document_type',
            'created_at',
            'modified_at',
        )


class MaggioliServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedMaggioliConfigSerializer(source='maggioli_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.MAGGIOLI
        configs = validated_data.pop('maggioli_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            MaggioliConfig.objects.bulk_create([MaggioliConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.MAGGIOLI
        configs = validated_data.pop('maggioli_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.maggioli_configs.all().delete()
            MaggioliConfig.objects.bulk_create([MaggioliConfig(**config, service=instance) for config in configs])
        return instance


class NestedHalleyConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = HalleyConfig
        fields = (
            'id',
            'description',
            'is_active',
            'login_url',
            'registration_url',
            'username',
            'password',
            'institution_name',
            'institution_aoo_id',
            'institution_office_ids',
            'classification_category',
            'classification_class',
            'classification_subclass',
            'folder_id',
            'created_at',
            'modified_at',
        )


class HalleyServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedHalleyConfigSerializer(source='halley_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.HALLEY
        configs = validated_data.pop('halley_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            HalleyConfig.objects.bulk_create([HalleyConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.HALLEY
        configs = validated_data.pop('halley_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.halley_configs.all().delete()
            HalleyConfig.objects.bulk_create([HalleyConfig(**config, service=instance) for config in configs])
        return instance


class NestedHalleyCloudConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = HalleyCloudConfig
        fields = (
            'id',
            'description',
            'is_active',
            'registration_url',
            'username',
            'password',
            'mailbox',
            'institution_office_ids',
            'classification_category',
            'classification_class',
            'classification_subclass',
            'folder_name',
            'created_at',
            'modified_at',
        )


class HalleyCloudServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedHalleyCloudConfigSerializer(source='halley_cloud_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.HALLEY_CLOUD
        configs = validated_data.pop('halley_cloud_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            HalleyCloudConfig.objects.bulk_create([HalleyCloudConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.HALLEY_CLOUD
        configs = validated_data.pop('halley_cloud_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.halley_cloud_configs.all().delete()
            HalleyCloudConfig.objects.bulk_create(
                [HalleyCloudConfig(**config, service=instance) for config in configs],
            )
        return instance


class NestedDedagroupConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = DedagroupConfig
        fields = (
            'id',
            'description',
            'is_active',
            'token_url',
            'base_url',
            'client_id',
            'client_secret',
            'grant_type',
            'resource',
            'registry_id',
            'author_id',
            'organization_chart_level_code',
            'institution_aoo_id',
            'classification_title',
            'classification_class',
            'classification_subclass',
            'folder_code',
            'created_at',
            'modified_at',
        )


class DedagroupServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedDedagroupConfigSerializer(source='dedagroup_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.DEDAGROUP
        configs = validated_data.pop('dedagroup_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            DedagroupConfig.objects.bulk_create([DedagroupConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.DEDAGROUP
        configs = validated_data.pop('dedagroup_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.dedagroup_configs.all().delete()
            DedagroupConfig.objects.bulk_create([DedagroupConfig(**config, service=instance) for config in configs])
        return instance


class NestedPitreConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = PitreConfig
        fields = (
            'id',
            'description',
            'is_active',
            'base_url',
            'certificate',
            'username',
            'code_role',
            'code_application',
            'code_adm',
            'code_node_classification',
            'transmission_reason',
            'transmission_type',
            'receiver_codes',
            'recipient_code',
            'code_register',
            'means_of_sending',
            'code_rf',
            'note',
            'created_at',
            'modified_at',
        )


class PitreServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedPitreConfigSerializer(source='pitre_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.PITRE
        configs = validated_data.pop('pitre_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            PitreConfig.objects.bulk_create([PitreConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.PITRE
        configs = validated_data.pop('pitre_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.pitre_configs.all().delete()
            PitreConfig.objects.bulk_create([PitreConfig(**config, service=instance) for config in configs])
        return instance


class NestedInsielConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = InsielConfig
        fields = (
            'id',
            'description',
            'is_active',
            'login_url',
            'registry_url',
            'file_transfer_url',
            'client_id',
            'client_secret',
            'grant_type',
            'user_code',
            'user_password',
            'operating_office_code',
            'office_code',
            'office_registry',
            'internal_offices_codes',
            'internal_offices_types',
            'register_code',
            'prog_doc',
            'prog_movi',
            'created_at',
            'modified_at',
        )
        extra_kwargs = {'register_code': {'trim_whitespace': False}}


class InsielServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedInsielConfigSerializer(source='insiel_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.INSIEL
        configs = validated_data.pop('insiel_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            InsielConfig.objects.bulk_create([InsielConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.INSIEL
        configs = validated_data.pop('insiel_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.insiel_configs.all().delete()
            InsielConfig.objects.bulk_create([InsielConfig(**config, service=instance) for config in configs])
        return instance


class NestedInforConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = InforConfig
        fields = (
            'id',
            'description',
            'is_active',
            'auth_username',
            'auth_password',
            'username',
            'denomination',
            'email',
            'wsdl_url',
            'registry_url',
            'incoming_document_type',
            'incoming_intermediary',
            'incoming_sorting',
            'incoming_classification',
            'incoming_folder',
            'response_internal_sender',
            'response_document_type',
            'response_intermediary',
            'response_sorting',
            'response_classification',
            'response_folder',
            'created_at',
            'modified_at',
        )


class InforServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedInforConfigSerializer(source='infor_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.INFOR
        configs = validated_data.pop('infor_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            InforConfig.objects.bulk_create([InforConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.INFOR
        configs = validated_data.pop('infor_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.infor_configs.all().delete()
            InforConfig.objects.bulk_create(
                [InforConfig(**config, service=instance) for config in configs],
            )
        return instance


class NestedD3ConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = D3Config
        fields = (
            'id',
            'description',
            'is_active',
            'username',
            'password',
            'registry_url',
            'repository_id',
            'main_source_category',
            'register_attachments',
            'attachment_source_category',
            'document_subtype',
            'classification',
            'folder_name',
            'competent_service',
            'visibility',
            'created_at',
            'modified_at',
        )


class D3ServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedD3ConfigSerializer(source='d3_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.D3
        configs = validated_data.pop('d3_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            D3Config.objects.bulk_create([D3Config(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.D3
        configs = validated_data.pop('d3_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.d3_configs.all().delete()
            D3Config.objects.bulk_create(
                [D3Config(**config, service=instance) for config in configs],
            )
        return instance


class NestedHypersicConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = HypersicConfig
        fields = (
            'id',
            'description',
            'is_active',
            'username',
            'password',
            'registry_url',
            'office_ids',
            'classification',
            'folder_number',
            'folder_year',
            'annotation',
            'created_at',
            'modified_at',
        )


class HypersicServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedHypersicConfigSerializer(source='hypersic_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.HYPERSIC
        configs = validated_data.pop('hypersic_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            HypersicConfig.objects.bulk_create([HypersicConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.HYPERSIC
        configs = validated_data.pop('hypersic_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.hypersic_configs.all().delete()
            HypersicConfig.objects.bulk_create(
                [HypersicConfig(**config, service=instance) for config in configs],
            )
        return instance


class NestedTinnConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = TinnConfig
        fields = (
            'id',
            'description',
            'is_active',
            'url',
            'codente',
            'username',
            'password',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'classification_institution_code',
            'classification_aoo_code',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'created_at',
            'modified_at',
        )


class TinnServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedTinnConfigSerializer(source='tinn_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.TINN
        configs = validated_data.pop('tinn_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            TinnConfig.objects.bulk_create([TinnConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.TINN
        configs = validated_data.pop('tinn_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.tinn_configs.all().delete()
            TinnConfig.objects.bulk_create([TinnConfig(**config, service=instance) for config in configs])
        return instance


class NestedSicrawebWSProtocolloDMConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = SicrawebWSProtocolloDMConfig
        fields = (
            'id',
            'description',
            'is_active',
            'url',
            'connection_string',
            'institution_code',
            'institution_ou',
            'institution_aoo_code',
            'sending_mode',
            'internal_sender',
            'update_records',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'connection_user',
            'connection_user_role',
            'standard_subject',
            'document_type',
            'created_at',
            'modified_at',
        )


class SicrawebWSProtocolloDMServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedSicrawebWSProtocolloDMConfigSerializer(source='sicraweb_wsprotocollodm_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.SICRAWEB_WSPROTOCOLLODM
        configs = validated_data.pop('sicraweb_wsprotocollodm_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            SicrawebWSProtocolloDMConfig.objects.bulk_create(
                [SicrawebWSProtocolloDMConfig(**config, service=service) for config in configs],
            )
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.SICRAWEB_WSPROTOCOLLODM
        configs = validated_data.pop('sicraweb_wsprotocollodm_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.sicraweb_wsprotocollodm_configs.all().delete()
            SicrawebWSProtocolloDMConfig.objects.bulk_create(
                [SicrawebWSProtocolloDMConfig(**config, service=instance) for config in configs],
            )
        return instance


class NestedSiscomConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = SiscomConfig
        fields = (
            'id',
            'description',
            'is_active',
            'url',
            'license_code',
            'operator_denomination',
            'classification_category',
            'classification_class',
            'folder_id',
            'mail_type',
            'office_id',
            'created_at',
            'modified_at',
        )


class SiscomServiceSerializer(
    ApplicationStatesSerializerMixin,
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    tenant_sdc_id = UUIDSlugRelatedField(source='tenant', slug_field='sdc_id', queryset=Tenant.objects.all())
    configs = NestedSiscomConfigSerializer(source='siscom_configs', many=True)

    class Meta:
        model = Service
        fields = (
            'id',
            'description',
            'tenant_sdc_id',
            'sdc_id',
            'application_states',
            'configs',
            'created_at',
            'modified_at',
        )

    def validate_configs(self, value):
        active_configs = [config for config in value if config['is_active']]
        if len(active_configs) > 1:
            raise serializers.ValidationError(_('Only one active configuration is allowed.'))
        return value

    def create(self, validated_data):
        validated_data['provider'] = Service.Provider.SISCOM
        configs = validated_data.pop('siscom_configs')
        with transaction.atomic():
            service = super().create(validated_data)
            SiscomConfig.objects.bulk_create([SiscomConfig(**config, service=service) for config in configs])
        return service

    def update(self, instance, validated_data):
        validated_data['provider'] = Service.Provider.SISCOM
        configs = validated_data.pop('siscom_configs')  # Partial updates are not supported.
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.siscom_configs.all().delete()
            SiscomConfig.objects.bulk_create([SiscomConfig(**config, service=instance) for config in configs])
        return instance


class ProviderSerializer(serializers.Serializer):
    name = serializers.ChoiceField(choices=Service.Provider.choices)
    endpoint = serializers.CharField()


class FormSchemaSerializer(serializers.Serializer):
    display = serializers.CharField()
    components = serializers.ListField()


class ApplicationStateSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = ApplicationState
        fields = (
            'id',
            'name',
            'direction',
            'integrations',
            'default',
            'created_at',
            'modified_at',
        )


class DatagraphConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = DatagraphConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'url',
            'codente',
            'username',
            'password',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'classification_institution_code',
            'classification_aoo_code',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'document_format',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=DatagraphConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class DedagroupConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = DedagroupConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'token_url',
            'base_url',
            'client_id',
            'client_secret',
            'grant_type',
            'resource',
            'registry_id',
            'author_id',
            'organization_chart_level_code',
            'institution_aoo_id',
            'classification_title',
            'classification_class',
            'classification_subclass',
            'folder_code',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=DedagroupConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class HalleyConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = HalleyConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'login_url',
            'registration_url',
            'username',
            'password',
            'institution_name',
            'institution_aoo_id',
            'institution_office_ids',
            'classification_category',
            'classification_class',
            'classification_subclass',
            'folder_id',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=HalleyConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class HalleyCloudConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = HalleyCloudConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'registration_url',
            'username',
            'password',
            'mailbox',
            'institution_office_ids',
            'classification_category',
            'classification_class',
            'classification_subclass',
            'folder_name',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=HalleyCloudConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class MaggioliConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = MaggioliConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'url',
            'connection_string',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'sending_mode',
            'sending_method',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'document_type',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=MaggioliConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class PitreConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = PitreConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'base_url',
            'certificate',
            'username',
            'code_role',
            'code_application',
            'code_adm',
            'code_node_classification',
            'transmission_reason',
            'transmission_type',
            'receiver_codes',
            'recipient_code',
            'code_register',
            'means_of_sending',
            'code_rf',
            'note',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=PitreConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class InsielConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = InsielConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'login_url',
            'registry_url',
            'file_transfer_url',
            'client_id',
            'client_secret',
            'grant_type',
            'user_code',
            'user_password',
            'operating_office_code',
            'office_code',
            'office_registry',
            'internal_offices_codes',
            'internal_offices_types',
            'register_code',
            'prog_doc',
            'prog_movi',
            'created_at',
            'modified_at',
        )
        extra_kwargs = {'register_code': {'trim_whitespace': False}}
        validators = [
            UniqueTogetherValidator(
                queryset=InsielConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class InforConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = InforConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'auth_username',
            'auth_password',
            'username',
            'denomination',
            'email',
            'wsdl_url',
            'registry_url',
            'incoming_document_type',
            'incoming_intermediary',
            'incoming_sorting',
            'incoming_classification',
            'incoming_folder',
            'response_internal_sender',
            'response_document_type',
            'response_intermediary',
            'response_sorting',
            'response_classification',
            'response_folder',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=InforConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class D3ConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = D3Config
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'username',
            'password',
            'registry_url',
            'repository_id',
            'main_source_category',
            'register_attachments',
            'attachment_source_category',
            'document_subtype',
            'classification',
            'folder_name',
            'competent_service',
            'visibility',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=D3Config.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class HypersicConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = HypersicConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'username',
            'password',
            'registry_url',
            'office_ids',
            'classification',
            'folder_number',
            'folder_year',
            'annotation',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=HypersicConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class TinnConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = TinnConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'url',
            'codente',
            'username',
            'password',
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'classification_institution_code',
            'classification_aoo_code',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=TinnConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class SicrawebWSProtocolloDMConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = SicrawebWSProtocolloDMConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'url',
            'connection_string',
            'institution_code',
            'institution_ou',
            'institution_aoo_code',
            'sending_mode',
            'internal_sender',
            'update_records',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'connection_user',
            'connection_user_role',
            'standard_subject',
            'document_type',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=SicrawebWSProtocolloDMConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class SiscomConfigSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    service_sdc_id = UUIDSlugRelatedField(source='service', slug_field='sdc_id', queryset=Service.objects.all())

    class Meta:
        model = SiscomConfig
        fields = (
            'id',
            'description',
            'service_sdc_id',
            'is_active',
            'url',
            'license_code',
            'operator_denomination',
            'classification_category',
            'classification_class',
            'folder_id',
            'mail_type',
            'office_id',
            'created_at',
            'modified_at',
        )
        validators = [
            UniqueTogetherValidator(
                queryset=SiscomConfig.objects.active(),
                fields=['service_sdc_id', 'is_active'],
                message=_('An active configuration already exists for the selected service.'),
            ),
        ]


class RegistrationSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    date = UTCDateTimeField()

    class Meta:
        model = Registration
        fields = (
            'id',
            'number',
            'date',
            'tenant_sdc_id',
            'service_sdc_id',
            'application_sdc_id',
            'application_state_name',
            'event_sdc_id',
            'is_completed',
            'is_updated',
            'registration_data',
            'created_at',
            'modified_at',
        )


class PreRegistrationSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    date = UTCDateTimeField()

    class Meta:
        model = PreRegistration
        fields = (
            'id',
            'folder',
            'number',
            'date',
            'tenant_sdc_id',
            'service_sdc_id',
            'application_sdc_id',
            'application_state_name',
            'event_sdc_id',
            'pre_registration_data',
            'created_at',
            'modified_at',
        )


class DeadLetterSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = DeadLetter
        fields = (
            'id',
            'tenant_sdc_id',
            'service_sdc_id',
            'application_sdc_id',
            'state',
            'created_at',
            'modified_at',
        )


class TenantSerializer(
    TimestampedSerializer,
    serializers.ModelSerializer,
):
    class Meta:
        model = Tenant
        fields = (
            'id',
            'description',
            'slug',
            'sdc_id',
            'sdc_base_url',
            'sdc_username',
            'sdc_password',
            'sdc_institution_code',
            'sdc_aoo_code',
            'latest_registration_number',
            'latest_registration_number_issued_at',
            'register_after_date',
            'created_at',
            'modified_at',
        )
