import json
import logging
import traceback as tb
from datetime import datetime, timezone

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.utils.functional import cached_property
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from django_jsonform.models.fields import ArrayField

from apps.applications import errors
from apps.applications.kafka import RetryProducer
from apps.applications.sdc.client import Client as SDCClient
from apps.applications.sdc.client import Credentials as SDCCredentials
from apps.timezone_utils import localtime, make_aware
from apps.workers.d3.rest import Client as D3Client
from apps.workers.datagraph.soap import Client as DatagraphClient
from apps.workers.datagraph.soap import Credentials as DatagraphCredentials
from apps.workers.dedagroup.rest import Client as DedagroupClient
from apps.workers.halley.soap import Client as HalleyClient
from apps.workers.halley_cloud.soap import Client as HalleyCloudClient
from apps.workers.hypersic.soap import Client as HypersicClient
from apps.workers.infor.soap import Client as InforClient
from apps.workers.insiel.soap import Client as InsielClient
from apps.workers.maggioli.soap import Client as MaggioliClient
from apps.workers.pitre.rest import Client as PitreClient
from apps.workers.sicraweb_wsprotocollodm.soap import Client as SicrawebWSProtocolloDMClient
from apps.workers.siscom.soap import Client as SiscomClient
from apps.workers.tinn.soap import Client as TinnClient

logger = logging.getLogger('application_registry')


class TimestampedModel(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    modified_at = models.DateTimeField(_('modified at'), auto_now=True)

    class Meta:
        abstract = True


class Tenant(TimestampedModel):
    sdc_client_class = SDCClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    slug = models.SlugField(_('slug'), max_length=64, unique=True)
    sdc_id = models.UUIDField(_('SDC ID'), unique=True)
    sdc_base_url = models.URLField(_('SDC base URL'))
    sdc_username = models.CharField(_('SDC username'), max_length=64)
    sdc_password = models.CharField(_('SDC password'), max_length=64)
    sdc_institution_code = models.CharField(_('SDC institution code'), max_length=32)
    sdc_aoo_code = models.CharField(_('SDC AOO code'), max_length=32)
    latest_registration_number = models.PositiveIntegerField(_('latest registration number'), default=0)
    latest_registration_number_issued_at = models.DateTimeField(
        _('latest registration number issued at'),
        default=localtime,
    )
    register_after_date = models.DateTimeField(
        _('register after date'),
        default=make_aware(datetime.min, timezone=timezone.utc),
    )

    def __str__(self):
        return str(self.sdc_id)

    @cached_property
    def _sdc_client(self):
        return self.sdc_client_class(
            credentials=SDCCredentials(
                username=self.sdc_username,
                password=self.sdc_password,
            ),
            base_url=self.sdc_base_url,
        )

    @property
    def next_registration_number(self):
        localnow = localtime()
        with transaction.atomic():
            self = Tenant.objects.select_for_update().get(pk=self.pk)
            is_new_year = localtime(self.latest_registration_number_issued_at).year < localnow.year
            next_number = 1 if is_new_year else self.latest_registration_number + 1
            self.latest_registration_number = next_number
            self.latest_registration_number_issued_at = localnow
            self.save()
        return self.latest_registration_number

    def is_registration_date_allowed(self, registration_date):
        return registration_date > self.register_after_date

    def get_document(self, url):
        return self._sdc_client.get_document(url)

    def update_application(self, application_id, data):
        self._sdc_client.update_application(application_id, data)

        # Retrieve registration number from api data and updated value
        reg_number = (
            data.get('integration_outbound_protocol_number')
            or data.get('integration_inbound_protocol_number')
            or data.get('protocol_number')
            or data.get('outcome_protocol_number')
        )
        Registration.objects.filter(application_sdc_id=application_id, number=reg_number).update(is_updated=True)

    @staticmethod
    def create_document(document_id, application, number, date, folder, status):
        return application.create_document(document_id, number, date, folder, status)


class ServiceManager(models.Manager):
    def datagraph(self):
        return self.filter(provider=Service.Provider.DATAGRAPH)

    def maggioli(self):
        return self.filter(provider=Service.Provider.MAGGIOLI)

    def halley(self):
        return self.filter(provider=Service.Provider.HALLEY)

    def halley_cloud(self):
        return self.filter(provider=Service.Provider.HALLEY_CLOUD)

    def dedagroup(self):
        return self.filter(provider=Service.Provider.DEDAGROUP)

    def pitre(self):
        return self.filter(provider=Service.Provider.PITRE)

    def insiel(self):
        return self.filter(provider=Service.Provider.INSIEL)

    def infor(self):
        return self.filter(provider=Service.Provider.INFOR)

    def d3(self):
        return self.filter(provider=Service.Provider.D3)

    def hypersic(self):
        return self.filter(provider=Service.Provider.HYPERSIC)

    def tinn(self):
        return self.filter(provider=Service.Provider.TINN)

    def sicraweb_wsprotocollodm(self):
        return self.filter(provider=Service.Provider.SICRAWEB_WSPROTOCOLLODM)

    def siscom(self):
        return self.filter(provider=Service.Provider.SISCOM)


class ProviderConfigManager(models.Manager):
    def active(self):
        return self.filter(is_active=True)


class Service(TimestampedModel):
    class Provider(models.TextChoices):
        DATAGRAPH = ('datagraph', 'datagraph')
        DEDAGROUP = ('dedagroup', 'dedagroup')
        HALLEY = ('halley', 'halley')
        HALLEY_CLOUD = ('halley-cloud', 'halley-cloud')
        MAGGIOLI = ('maggioli', 'maggioli')
        PITRE = ('pitre', 'pitre')
        INSIEL = ('insiel', 'insiel')
        INFOR = ('infor', 'infor')
        D3 = ('d3', 'd3')
        HYPERSIC = ('hypersic', 'hypersic')
        TINN = ('tinn', 'tinn')
        SICRAWEB_WSPROTOCOLLODM = ('sicraweb-wsprotocollodm', 'sicraweb-wsprotocollodm')
        SISCOM = ('siscom', 'siscom')

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    tenant = models.ForeignKey(
        'Tenant',
        verbose_name=_('tenant'),
        related_name='services',
        on_delete=models.PROTECT,
    )
    sdc_id = models.UUIDField(_('SDC ID'), unique=True)
    provider = models.CharField(_('provider'), max_length=32, choices=Provider.choices)
    application_states = models.ManyToManyField(
        'ApplicationState',
        verbose_name=_('application states'),
        related_name='services',
        blank=True,
    )

    objects = ServiceManager()

    def __str__(self):
        return str(self.sdc_id)


class ApplicationStateManager(models.Manager):
    def default(self):
        return self.filter(default=True)


class ApplicationState(TimestampedModel):
    class Direction(models.TextChoices):
        INBOUND = ('inbound', _('inbound'))
        OUTBOUND = ('outbound', _('outbound'))

    id = models.BigAutoField(primary_key=True)  # noqa
    name = models.CharField(_('name'), max_length=64, unique=True)
    code = models.CharField(_('code'), max_length=16, unique=True)
    direction = models.CharField(_('direction'), max_length=16, choices=Direction.choices)
    integrations = models.BooleanField(_('integrations'))
    default = models.BooleanField(_('default'))
    update_sdc = models.BooleanField(_('update SDC'))

    objects = ApplicationStateManager()

    def __str__(self):
        return f'{self.name}/{self.code}/{self.direction}'


class DatagraphConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.DATAGRAPH,
            service__application_states__name=application_state,
        )


class DatagraphConfig(TimestampedModel):
    datagraph_client_class = DatagraphClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='datagraph_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    url = models.URLField(_('URL'))
    codente = models.CharField(_('codente'), max_length=64)
    username = models.CharField(_('username'), max_length=64)
    password = models.CharField(_('password'), max_length=64)
    institution_name = models.CharField(_('institution name'), max_length=1024)
    institution_code = models.CharField(_('institution code'), max_length=32)
    institution_email = models.EmailField(_('institution email'))
    institution_ou = models.CharField(_('institution OU'), max_length=32)
    institution_aoo_code = models.CharField(_('AOO code'), max_length=32)
    classification_institution_code = models.CharField(
        _('institution code for classification'),
        max_length=32,
        null=True,
    )
    classification_aoo_code = models.CharField(_('AOO code for classification'), max_length=32, null=True)
    classification_hierarchy = models.CharField(_('classification hierarchy'), max_length=16, null=True)
    folder_number = models.CharField(_('folder number'), max_length=16, null=True)
    folder_year = models.CharField(_('folder year'), max_length=4, null=True)
    document_format = models.CharField(_('document format'), max_length=32)

    objects = DatagraphConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.DATAGRAPH

    @cached_property
    def _client(self):
        return self.datagraph_client_class(
            credentials=DatagraphCredentials(
                codente=self.codente,
                username=self.username,
                password=self.password,
            ),
            url=self.url,
        )

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def sdc_institution_code(self):
        return self.tenant.sdc_institution_code

    @property
    def sdc_aoo_code(self):
        return self.tenant.sdc_aoo_code

    @property
    def registration_number(self):
        return f'{self.tenant.next_registration_number:07d}'

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, mimetype, data):
        return self._client.inserimento(mimetype, data)

    def register(self, document_id, application):
        rendered = self._render(application)
        result = self._client.protocollazione(rendered)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result

    def _render(self, application):
        template_class = self._get_template_class(application.direction)
        return template_class(self).render(application)

    @staticmethod
    def _get_template_class(direction):
        path = {
            ApplicationState.Direction.INBOUND: settings.DATAGRAPH_INBOUND_TEMPLATE_CLASS,
            ApplicationState.Direction.OUTBOUND: settings.DATAGRAPH_OUTBOUND_TEMPLATE_CLASS,
        }[direction]
        return import_string(path)

    def get_types(self):
        return self._client.get_types()


class MaggioliConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.MAGGIOLI,
            service__application_states__name=application_state,
        )


class MaggioliConfig(TimestampedModel):
    class SendingMode(models.TextChoices):
        CARTACEO = ('Cartaceo', 'Cartaceo')
        PEC = ('PEC', 'PEC')
        INTEROPERABILE = ('Interoperabile', 'Interoperabile')
        INTERPRO = ('InterPRO', 'InterPRO')

    maggioli_client_class = MaggioliClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='maggioli_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    url = models.URLField(_('URL'))
    connection_string = models.CharField(_('connection string'), max_length=64)
    institution_name = models.CharField(_('institution name'), max_length=1024)
    institution_code = models.CharField(_('institution code'), max_length=32)
    institution_email = models.EmailField(_('institution email'))
    institution_ou = models.CharField(_('institution OU'), max_length=32)
    institution_aoo_code = models.CharField(_('AOO code'), max_length=32)
    sending_mode = models.CharField(_('sending mode'), max_length=32, choices=SendingMode.choices, null=True)
    sending_method = models.CharField(_('sending method'), max_length=32, null=True)
    classification_hierarchy = models.CharField(_('classification hierarchy'), max_length=16, null=True)
    folder_number = models.CharField(_('folder number'), max_length=16, null=True)
    folder_year = models.CharField(_('folder year'), max_length=4, null=True)
    document_type = models.CharField(_('document type'), max_length=32)

    objects = MaggioliConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.MAGGIOLI

    @cached_property
    def _client(self):
        return self.maggioli_client_class(connection_string=self.connection_string, url=self.url)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def sdc_institution_code(self):
        return self.tenant.sdc_institution_code

    @property
    def sdc_aoo_code(self):
        return self.tenant.sdc_aoo_code

    @property
    def registration_number(self):
        return f'{self.tenant.next_registration_number:07d}'

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, filename, mimetype, data):
        return self._client.insert_documento(filename, mimetype, data)

    def register(self, document_id, application):
        rendered = self._render(application)
        result = self._client.registra_protocollo(rendered)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result

    def _render(self, application):
        template_class = self._get_template_class(application.direction)
        return template_class(self).render(application)

    @staticmethod
    def _get_template_class(direction):
        path = {
            ApplicationState.Direction.INBOUND: settings.MAGGIOLI_INBOUND_TEMPLATE_CLASS,
            ApplicationState.Direction.OUTBOUND: settings.MAGGIOLI_OUTBOUND_TEMPLATE_CLASS,
        }[direction]
        return import_string(path)


class HalleyConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.HALLEY,
            service__application_states__name=application_state,
        )


class HalleyConfig(TimestampedModel):
    halley_client_class = HalleyClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='halley_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    login_url = models.URLField(_('login URL'))
    registration_url = models.URLField(_('registration URL'))
    username = models.CharField(_('username'), max_length=64)
    password = models.CharField(_('password'), max_length=64)
    institution_name = models.CharField(_('institution name'), max_length=1024)
    institution_aoo_id = models.PositiveIntegerField(_('AOO ID'))
    institution_office_ids = ArrayField(
        base_field=models.PositiveIntegerField(),
        blank=True,
        default=list,
        size=1,  # Until we can make Halley work with >1 IDs.
        verbose_name=_('institution office IDs'),
    )
    classification_category = models.CharField(_('classification category'), max_length=16)
    classification_class = models.CharField(_('classification class'), max_length=16)
    classification_subclass = models.CharField(_('classification subclass'), max_length=16, null=True)
    folder_id = models.PositiveIntegerField(_('folder ID'), null=True)

    objects = HalleyConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.HALLEY

    @cached_property
    def _client(self):
        return self.halley_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def register(self, document_id, application):
        result = self._client.insert(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result


class HalleyCloudConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.HALLEY_CLOUD,
            service__application_states__name=application_state,
        )


class HalleyCloudConfig(TimestampedModel):
    halley_cloud_client_class = HalleyCloudClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='halley_cloud_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    registration_url = models.URLField(_('registration URL'))
    username = models.CharField(_('username'), max_length=64)
    password = models.CharField(_('password'), max_length=64)
    mailbox = models.EmailField(_('mailbox'), max_length=254, null=True)
    institution_office_ids = ArrayField(
        base_field=models.PositiveIntegerField(),
        blank=True,
        default=list,
        size=1,  # Until we can make Halley work with >1 IDs.
        verbose_name=_('institution office IDs'),
        help_text='The list of office ids is provided to the institution by the protocol manager, '
        'if omitted the default office will be used',
    )
    classification_category = models.CharField(
        _('classification category'),
        max_length=16,
        null=True,
        help_text='The list of classifications is provided to the institution by the protocol manager, '
        'if omitted the default classification will be used',
    )
    classification_class = models.CharField(_('classification class'), max_length=16, null=True)
    classification_subclass = models.CharField(_('classification subclass'), max_length=16, null=True)
    folder_name = models.CharField(
        _('Folder name'),
        max_length=64,
        help_text='The list of folder names is provided to the institution by the protocol manager, '
        'if omitted the default folder will be used',
        null=True,
    )

    objects = HalleyCloudConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.HALLEY_CLOUD

    @cached_property
    def _client(self):
        return self.halley_cloud_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def register(self, document_id, application):
        result = self._client.insert(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result


class DedagroupConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.DEDAGROUP,
            service__application_states__name=application_state,
        )


class DedagroupConfig(TimestampedModel):
    dedagroup_client_class = DedagroupClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='dedagroup_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    token_url = models.URLField(_('token URL'))
    base_url = models.URLField(_('base URL'))
    client_id = models.CharField(_('client ID'), max_length=64)
    client_secret = models.CharField(_('client secret'), max_length=64)
    grant_type = models.CharField(_('grant type'), max_length=64)
    resource = models.CharField(_('resource'), max_length=64)
    registry_id = models.PositiveIntegerField(_('registry ID'))
    author_id = models.PositiveIntegerField(_('author ID'))
    organization_chart_level_code = models.CharField(_('organization chart level code'), max_length=64)
    institution_aoo_id = models.PositiveIntegerField(_('AOO ID'))
    classification_title = models.CharField(_('classification title'), max_length=16, null=True)
    classification_class = models.CharField(_('classification class'), max_length=16, null=True)
    classification_subclass = models.CharField(_('classification subclass'), max_length=16, null=True)
    folder_code = models.CharField(_('folder code'), max_length=64, null=True)

    objects = DedagroupConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.DEDAGROUP

    @cached_property
    def _client(self):
        return self.dedagroup_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    @property
    def full_classification_class(self):
        if not (self.classification_title and self.classification_class):
            return None
        return f'{self.classification_title}.{self.classification_class}'

    @property
    def full_classification_subclass(self):
        if not (self.classification_title and self.classification_class and self.classification_subclass):
            return None
        return f'{self.classification_title}.{self.classification_class}.{self.classification_subclass}'

    def register(self, document_id, application):
        result = self._client.protocollazione(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result

    def add_to_folder(self, remote_application_id, remote_document_ids):
        if self.folder_code:
            self._client.fascicola_documento(remote_application_id, remote_document_ids)


class PitreConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.PITRE,
            service__application_states__name=application_state,
        )


class PitreConfig(TimestampedModel):
    pitre_client_class = PitreClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='pitre_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    base_url = models.URLField(_('base URL'))
    certificate = models.TextField(_('certificate'))
    username = models.CharField(_('username'), max_length=32)
    code_role = models.CharField(_('code role'), max_length=32)
    code_application = models.CharField(_('code application'), max_length=32)
    code_adm = models.CharField(_('code ADM'), max_length=32)
    code_node_classification = models.CharField(_('code node classification'), max_length=8)  # ramo del titolario
    transmission_reason = models.CharField(_('transmission reason'), max_length=128, default='TRASM ONLINE')
    transmission_type = models.CharField(_('transmission_type'), max_length=128, default='T', null=True)
    receiver_codes = ArrayField(
        base_field=models.CharField(_('transmission receiver code'), max_length=32),
        verbose_name=_('transmission receiver codes'),
        help_text='List of role codes to transmit the creation of the project or document to',
    )
    recipient_code = models.CharField(
        _('recipient code'),
        max_length=32,
        null=True,
        help_text='Recipient role code for inbound documents, sender for outbound documents. '
        'If not set the role used for integration will be used',
    )
    code_register = models.CharField(_('code register'), max_length=32)
    means_of_sending = models.CharField(_('means of sending'), max_length=128, default='SERVIZI ONLINE')
    code_rf = models.CharField(_('code RF'), max_length=8, null=True)
    note = models.CharField(
        _('note'),
        max_length=256,
        default='Comunicazione spedita tramite piattaforma',
        blank=True,
        help_text='Outbound document note',
    )

    objects = PitreConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.PITRE

    @cached_property
    def _client(self):
        return self.pitre_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def register(self, document_id, application):
        pre_registration = PreRegistration.objects.get(
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
        )

        result = self._client.protocollazione(pre_registration.document)
        self._client.execute_transmission_document(pre_registration.document)

        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )

        pre_registration.delete()
        return result

    def create_project(self, application):
        result = self._client.create_project(application)
        self._client.execute_transmission_project(result.folder_id)

        PreRegistration.objects.create(
            folder=result.folder,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            pre_registration_data={'attachments': []},
        )
        return result

    def create_document(self, application, folder):
        result = self._client.create_document(application)

        pre_registration_data = PreRegistration.objects.application_pre_registration_data(application)
        PreRegistration.objects.update_or_create(
            folder=folder,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            pre_registration_data=pre_registration_data or {'attachments': []},
            defaults={'document': result.document},
        )
        return result

    def upload_file_to_document(self, application, document):
        pre_registration_data = PreRegistration.objects.application_pre_registration_data(application)
        if document.original_name not in [el['filename'] for el in pre_registration_data['attachments']]:
            self._client.upload_file_to_document(document)
            pre_registration_data['attachments'].append({'filename': document.original_name})
            PreRegistration.objects.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            ).update(pre_registration_data=pre_registration_data)

    def add_doc_in_project(self, application, folder, document):
        self._client.add_doc_in_project(folder, document)
        Registration.objects.filter(
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
        ).update(folder=folder)


class InsielConfigManager(models.Manager):
    def active(self):
        return self.filter(is_active=True)

    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.INSIEL,
            service__application_states__name=application_state,
        )


class InsielConfig(TimestampedModel):
    insiel_client_class = InsielClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='insiel_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    login_url = models.URLField(_('Login URL'))
    registry_url = models.URLField(_('Protocol Web service URL'))
    file_transfer_url = models.URLField(_('File transfer Web service URL'))
    client_id = models.CharField(_('Client id'), max_length=32)
    client_secret = models.CharField(_('Client secret'), max_length=32)
    grant_type = models.CharField(_('Grant type'), max_length=32, default='client_credentials')
    user_code = models.CharField(_("User's code"), max_length=32)
    user_password = models.CharField(_("User's password"), max_length=32)
    operating_office_code = models.CharField(_("operating office's code"), max_length=32, null=True)
    office_code = models.CharField(_("Office's code"), max_length=32)
    office_registry = models.CharField(_("Office's registry"), max_length=32)
    internal_offices_codes = ArrayField(
        base_field=models.CharField(_("Internal office's code"), max_length=32),
        blank=True,
        default=list,
        verbose_name=_("internal office's codes"),
    )
    internal_offices_types = ArrayField(
        base_field=models.CharField(_("Internal office's type"), max_length=32),
        blank=True,
        default=list,
        verbose_name=_("internal office's types"),
    )
    register_code = models.CharField(
        _('Register code'),
        max_length=32,
        null=True,
        help_text="Right aligned levels: ex: '1  2  3'. "
        'This field is required if prog_doc and prog_movi are not defined.',
    )
    prog_doc = models.CharField(
        _('Register prog doc'),
        max_length=32,
        null=True,
        help_text='This field is required if prog_movi is defined',
    )
    prog_movi = models.CharField(
        _('Register prog movi'),
        max_length=32,
        null=True,
        help_text='This field is required if prog_doc is defined',
    )

    objects = InsielConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
            models.CheckConstraint(
                check=models.Q(
                    models.Q(register_code__isnull=False)
                    & models.Q(prog_doc__isnull=True)
                    & models.Q(prog_movi__isnull=True),
                )
                | models.Q(
                    models.Q(register_code__isnull=True)
                    & models.Q(prog_doc__isnull=False)
                    & models.Q(prog_movi__isnull=False),
                ),
                name='%(app_label)s_%(class)s_valid_folder_configuration',
            ),
        ]

    def clean(self):
        if self.register_code and (self.prog_doc or self.prog_movi):
            raise ValidationError(
                {'prog_movi': _('Choose ONLY ONE option between Register code or Prog Doc and Prog movi')},
            )
        if self.prog_doc and not self.prog_movi:
            raise ValidationError({'prog_movi': _('If prog_movi is provided prog_doc is required')})
        if self.prog_movi and not self.prog_doc:
            raise ValidationError({'prog_doc': _('If prog_doc is provided prog_movi is required')})
        if not (self.register_code or self.prog_movi or self.prog_doc):
            raise ValidationError(
                {'prog_doc': _('Specify AT LEAST ONE option between Register code or Prog Doc and Prog movi')},
            )

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.INSIEL

    @cached_property
    def _client(self):
        return self.insiel_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, data):
        return self._client.upload_file(data)

    def predispose_applicant(self, data):
        return self._client.predispose_applicant(data)

    def create_folder(self, application):
        result = self._client.inserisci_pratica(application)

        Registration.objects.create(
            folder=result.folder_number,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            registration_data={'folder': result.folder_data},
        )
        return result

    def register(self, document_id, application):
        registration_data = {}
        if self.register_code:
            registration_data = (
                Registration.objects.filter(application_sdc_id=application.id, registration_data__folder__isnull=False)
                .values_list('registration_data', flat=True)
                .first()
            )

        folder = registration_data['folder'] if registration_data and 'folder' in registration_data else None
        result = self._client.inserisci_protocollo(
            application,
            folder,
        )
        registration_data['document'] = result.document_data

        Registration.objects.update_or_create(
            document_id=document_id,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            defaults={
                'folder': folder['numero'] if folder and 'numero' in folder else None,
                'number': result.number,
                'date': result.created_at,
                'registration_data': registration_data,
                'is_completed': True,
            },
        )
        return result


class InforConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.INFOR,
            service__application_states__name=application_state,
        )


class InforConfig(TimestampedModel):
    infor_client_class = InforClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='infor_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    auth_username = models.CharField(_('auth username'), max_length=64, null=True)
    auth_password = models.CharField(_('auth password'), max_length=64, null=True)
    username = models.CharField(_('username'), max_length=64)
    denomination = models.CharField(_('denomination'), max_length=128)
    email = models.EmailField(_('email'), max_length=254)
    wsdl_url = models.URLField(_('Protocol WSDL URL'))
    registry_url = models.URLField(_('Protocol Web service URL'))
    incoming_document_type = models.CharField(_('incoming document type'), max_length=64)
    incoming_intermediary = models.CharField(_('incoming intermediary'), max_length=64)
    incoming_sorting = models.CharField(_('incoming sorting'), max_length=64)
    incoming_classification = models.CharField(_('incoming classification'), max_length=16)
    incoming_folder = models.CharField(_('incoming folder'), max_length=64)
    response_internal_sender = models.CharField(_('response internal sender'), max_length=254)
    response_document_type = models.CharField(_('response document type'), max_length=64)
    response_intermediary = models.CharField(_('response intermediary'), max_length=64)
    response_sorting = models.CharField(_('response sorting'), max_length=64)
    response_classification = models.CharField(_('response classification'), max_length=16)
    response_folder = models.CharField(_('response folder'), max_length=64)

    objects = InforConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.INFOR

    @cached_property
    def _client(self):
        return self.infor_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, application, document):
        registration_data = (
            Registration.objects.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            )
            .values_list('registration_data', flat=True)
            .first()
        )

        # se il documento non è presente nella lista degli
        # allegati in registration_data allora bisogna caricarlo
        if document.original_name not in [el['filename'] for el in registration_data['attachments']]:
            result = self._client.upload_file(document)

            registration_data['attachments'].append({'filename': document.original_name})

            # la protocollazione della pratica può essere completata
            # solo quando tutti gli allegati sono stati caricati
            is_upload_completed = len(registration_data['attachments']) == len(application.current_documents)
            Registration.objects.filter(
                tenant_sdc_id=self.tenant.sdc_id,
                service_sdc_id=self.service.sdc_id,
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            ).update(
                registration_data=registration_data,
                is_completed=is_upload_completed,
            )
            return result

    def register(self, document_id, application):
        if Registration.objects.exists_incomplete_for_application(application):
            return None

        result = self._client.insert(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            registration_data={
                'folder': None,
                'document': {
                    'number': result.number,
                    'year': result.year,
                },
                'attachments': [],
            },
            is_completed=False,
        )
        return result


class D3ConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.D3,
            service__application_states__name=application_state,
        )


class D3Config(TimestampedModel):
    d3_client_class = D3Client

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='d3_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    username = models.CharField(_('Username'), max_length=64)
    password = models.CharField(_('Password'), max_length=32)
    registry_url = models.URLField(_('Registry URL'))
    repository_id = models.CharField(_('Repository ID'), max_length=64)
    main_source_category = models.CharField(_('Main Source Category'), max_length=64)
    register_attachments = models.BooleanField(_('Include attachments in registration'))
    attachment_source_category = models.CharField(_('Attachment Source Category'), max_length=64, null=True)
    document_subtype = models.CharField(_('Document Subtype'), max_length=128, null=True)
    classification = models.CharField(_('Classification'), max_length=128, null=True)
    folder_name = models.CharField(
        _('Folder name'),
        max_length=64,
        help_text='The list of folder names is provided to the institution by the protocol manager, '
        'if omitted the default folder will be used',
        null=True,
    )
    competent_service = models.CharField(_('Competent service'), max_length=254)
    visibility = models.CharField(_('Visibility'), max_length=254, null=True)

    objects = D3ConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.D3

    @cached_property
    def _client(self):
        return self.d3_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, application, document):
        registration_data = (
            Registration.objects.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            )
            .values_list('registration_data', flat=True)
            .first()
        )

        # se il documento non è presente nella lista degli allegati
        # in registration_data o registration data non è popolato
        # allora bisogna caricare il documento
        # altrimenti ritorno solo i dati del protocollo da usare
        # successivamente per eventuali allegati secondari
        if registration_data:
            found = False
            for el in registration_data['attachments']:
                if document.original_name == el['filename']:
                    found = True
            if not found:
                return self._client.upload(document), registration_data
            return None, registration_data
        return self._client.upload(document), None

    def register(self, document_id, application, document, registration_data):
        result = self._client.insert(application, document)
        if Registration.objects.exists_incomplete_for_application(application):
            registration_data['attachments'].append({'filename': document.original_name})

            # la protocollazione della pratica può essere completata
            # solo quando tutti gli allegati sono stati caricati
            is_upload_completed = len(registration_data['attachments']) == len(application.current_documents)
            Registration.objects.filter(
                tenant_sdc_id=self.tenant.sdc_id,
                service_sdc_id=self.service.sdc_id,
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            ).update(
                registration_data=registration_data,
                is_completed=is_upload_completed,
            )
            return result

        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            registration_data={
                'folder': None,
                'document': {
                    'number': result.number,
                    'year': result.year,
                    'registered_at': result.created_at.isoformat(),
                },
                'attachments': [{'filename': document.original_name}],
            },
            is_completed=False if self.register_attachments and len(application.current_documents) > 1 else True,
        )
        return result


class HypersicConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.HYPERSIC,
            service__application_states__name=application_state,
        )


class HypersicConfig(TimestampedModel):
    hypersic_client_class = HypersicClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='hypersic_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    username = models.CharField(_('Username'), max_length=64)
    password = models.CharField(_('Password'), max_length=64)
    registry_url = models.URLField(_('Protocol Web service URL'))
    office_ids = ArrayField(
        base_field=models.PositiveIntegerField(),
        default=list,
        verbose_name=_('Office ids'),
        help_text='The list of office ids is provided to the institution by the protocol manager',
    )
    classification = models.CharField(_('Classification'), max_length=16, null=True)
    folder_number = models.CharField(_('Folder number'), max_length=16, null=True)
    folder_year = models.CharField(_('Folder year'), max_length=16, null=True)
    annotation = models.CharField(_('Registration annotation'), max_length=128, null=True)

    objects = HypersicConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.HYPERSIC

    @cached_property
    def _client(self):
        return self.hypersic_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, application, document):
        registration_data = (
            Registration.objects.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            )
            .values_list('registration_data', flat=True)
            .first()
        )

        # se il documento non è presente nella lista degli
        # allegati in registration_data allora bisogna caricarlo
        if document.original_name not in [el['filename'] for el in registration_data['attachments']]:
            self._client.upload_file(document)

            registration_data['attachments'].append({'filename': document.original_name})

            # la protocollazione della pratica può essere completata
            # solo quando tutti gli allegati sono stati caricati
            is_upload_completed = len(registration_data['attachments']) == len(application.current_documents)
            Registration.objects.filter(
                tenant_sdc_id=self.tenant.sdc_id,
                service_sdc_id=self.service.sdc_id,
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            ).update(
                registration_data=registration_data,
                is_completed=is_upload_completed,
            )

    def register(self, document_id, application):
        if Registration.objects.exists_incomplete_for_application(application):
            return None

        previous_registration = Registration.objects.most_recent_registration(application)

        result = self._client.insert(
            application=application,
            previous_registration_number=(
                previous_registration['document']['number'] if previous_registration else None
            ),
            previous_registration_year=previous_registration['document']['year'] if previous_registration else None,
        )

        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            registration_data={
                'folder': None,
                'document': {
                    'registry_code': result.registry_code,
                    'number': result.number,
                    'year': result.year,
                    'registered_at': result.created_at.isoformat(),
                },
                'attachments': [],
            },
            is_completed=False,
        )
        return result


class TinnConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.TINN,
            service__application_states__name=application_state,
        )


class TinnConfig(TimestampedModel):
    tinn_client_class = TinnClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='tinn_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    url = models.URLField(_('URL'))
    codente = models.CharField(_('codente'), max_length=64)
    username = models.CharField(_('username'), max_length=64)
    password = models.CharField(_('password'), max_length=64)
    institution_name = models.CharField(_('institution name'), max_length=1024, null=True)
    institution_code = models.CharField(_('institution code'), max_length=32, null=True)
    institution_email = models.EmailField(_('institution email'), null=True)
    institution_ou = models.CharField(_('institution OU'), max_length=32, null=True)
    institution_aoo_code = models.CharField(_('AOO code'), max_length=32, null=True)
    classification_institution_code = models.CharField(
        _('institution code for classification'),
        max_length=32,
        null=True,
    )
    classification_aoo_code = models.CharField(_('AOO code for classification'), max_length=32, null=True)
    classification_hierarchy = models.CharField(_('classification hierarchy'), max_length=16)
    folder_number = models.CharField(_('folder number'), max_length=16, null=True)
    folder_year = models.CharField(_('folder year'), max_length=4, null=True)

    objects = TinnConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.TINN

    @cached_property
    def _client(self):
        return self.tinn_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def sdc_institution_code(self):
        return self.tenant.sdc_institution_code

    @property
    def sdc_aoo_code(self):
        return self.tenant.sdc_aoo_code

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def upload(self, data):
        return self._client.inserimento(data)

    def register(self, document_id, application):
        result = self._client.protocollazione(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result


class SicrawebWSProtocolloDMConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.SICRAWEB_WSPROTOCOLLODM,
            service__application_states__name=application_state,
        )


class SicrawebWSProtocolloDMConfig(TimestampedModel):
    sicraweb_wsprotocollodm_client_class = SicrawebWSProtocolloDMClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='sicraweb_wsprotocollodm_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    url = models.URLField(_('URL'))
    connection_string = models.CharField(_('connection string'), max_length=64)
    institution_code = models.CharField(_('institution code'), max_length=32, null=True)
    institution_ou = models.CharField(_('institution OU'), max_length=32, null=True)
    institution_aoo_code = models.CharField(_('AOO code'), max_length=32)
    sending_mode = models.CharField(_('sending mode'), max_length=32, null=True)
    internal_sender = models.CharField(_('internal sender'), max_length=32, null=True)
    update_records = models.CharField(_('update records'), max_length=32, help_text='One of: N, S or F', null=True)
    classification_hierarchy = models.CharField(_('classification hierarchy'), max_length=16, null=True)
    folder_number = models.CharField(_('folder number'), max_length=16, null=True)
    folder_year = models.CharField(_('folder year'), max_length=4, null=True)
    connection_user = models.CharField('connection user', max_length=32, null=True)
    connection_user_role = models.CharField('connection user role', max_length=32, null=True)
    standard_subject = models.CharField('standard subject', max_length=32, null=True)
    document_type = models.CharField(_('document type'), max_length=32, null=True)

    objects = SicrawebWSProtocolloDMConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.SICRAWEB_WSPROTOCOLLODM

    @cached_property
    def _client(self):
        return self.sicraweb_wsprotocollodm_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def register(self, document_id, application):
        result = self._client.insert(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            is_completed=True,
        )
        return result


class SiscomConfigManager(ProviderConfigManager):
    def active_for_service(self, sdc_id, application_state):
        return self.select_related('service', 'service__tenant').filter(
            is_active=True,
            service__sdc_id=sdc_id,
            service__provider=Service.Provider.SISCOM,
            service__application_states__name=application_state,
        )


class SiscomConfig(TimestampedModel):
    siscom_client_class = SiscomClient

    id = models.BigAutoField(primary_key=True)  # noqa
    description = models.CharField(_('description'), max_length=128, blank=True)
    service = models.ForeignKey(
        'Service',
        verbose_name=_('service'),
        related_name='siscom_configs',
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(_('active'))
    url = models.URLField(_('URL'))
    license_code = models.CharField(_('codice licenza'), max_length=16)
    operator_denomination = models.CharField(_('operator denomination'), max_length=128, null=True)
    classification_category = models.CharField(_('classification category'), max_length=16, null=True)
    classification_class = models.CharField(_('classification class'), max_length=16, null=True)
    folder_id = models.CharField(_('folder ID'), max_length=16, null=True)
    mail_type = models.CharField(_('mail type'), max_length=16, null=True)
    office_id = models.CharField(
        verbose_name=_('office id'),
        help_text='The office id is provided to the institution by the protocol manager',
        max_length=16,
    )

    objects = SiscomConfigManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['service', 'is_active'],
                condition=models.Q(is_active=True),
                name='%(app_label)s_%(class)s_unique_service_is_active_true',
            ),
        ]

    def __str__(self):
        return str(self.id)

    @property
    def provider(self):
        return Service.Provider.SISCOM

    @cached_property
    def _client(self):
        return self.siscom_client_class(self)

    @property
    def tenant(self):
        return self.service.tenant

    @property
    def sdc_institution_code(self):
        return self.tenant.sdc_institution_code

    @property
    def sdc_aoo_code(self):
        return self.tenant.sdc_aoo_code

    @property
    def tenant_slug(self):
        return self.tenant.slug

    def register(self, document_id, application):
        if Registration.objects.exists_incomplete_for_application(application):
            return None

        result = self._client.insert(application)
        Registration.objects.create(
            document_id=document_id,
            number=result.number,
            date=result.created_at,
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
            registration_data={
                'folder': None,
                'document': {
                    'number': result.number,
                    'year': result.year,
                    'registered_at': result.created_at.isoformat(),
                },
                'attachments': [],
            },
            is_completed=False,
        )
        return result

    def upload(self, application, document):
        registration_data = (
            Registration.objects.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            )
            .values_list('registration_data', flat=True)
            .first()
        )

        # se il documento non è presente nella lista degli
        # allegati in registration_data allora bisogna caricarlo
        if document.original_name not in [el['filename'] for el in registration_data['attachments']]:
            result = self._client.upload(document)

            registration_data['attachments'].append({'filename': document.original_name})

            # la protocollazione della pratica può essere completata
            # solo quando tutti gli allegati sono stati caricati
            is_upload_completed = len(registration_data['attachments']) == len(application.current_documents)
            Registration.objects.filter(
                tenant_sdc_id=self.tenant.sdc_id,
                service_sdc_id=self.service.sdc_id,
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                event_sdc_id=application.event_id,
            ).update(
                registration_data=registration_data,
                is_completed=is_upload_completed if self.folder_id else False,
            )
            return result

    def collate(self, application, number, year):
        result = self._client.collate(application, number, year)
        Registration.objects.filter(
            tenant_sdc_id=self.tenant.sdc_id,
            service_sdc_id=self.service.sdc_id,
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            event_sdc_id=application.event_id,
        ).update(
            is_completed=True,
        )
        return result


class RegistrationEventManager(models.Manager):
    class State(models.TextChoices):
        PROCESSING = ('processing', _('processing'))
        COMPLETED = ('completed', _('completed'))
        FAILED = ('failed', _('failed'))

    def exists_for_application(self, application):
        registration_event = (
            self.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                state__in=[self.State.FAILED, self.State.COMPLETED],
            )
            .values('document_id', 'state')
            .order_by('-created_at')
            .first()
        )
        return registration_event and registration_event['state'] == self.State.FAILED

    def last_failed_registration_event(self, application):
        registration_event = (
            self.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
            )
            .values('document_id', 'state')
            .order_by('-created_at')
            .first()
        )
        return (
            registration_event['document_id']
            if registration_event and registration_event['state'] == self.State.FAILED
            else None
        )


class RegistrationEvent(TimestampedModel):
    class State(models.TextChoices):
        PROCESSING = ('processing', _('processing'))
        COMPLETED = ('completed', _('completed'))
        FAILED = ('failed', _('failed'))

    id = models.BigAutoField(primary_key=True)  # noqa
    provider = models.CharField(_('provider'), max_length=32, choices=Service.Provider.choices)
    config_id = models.PositiveBigIntegerField(_('config ID'))
    document_id = models.UUIDField(_('document ID'), null=True)
    application_sdc_id = models.UUIDField(_('application SDC ID'))
    application_state_name = models.CharField(_('application state name'), max_length=64, null=True)
    event_sdc_id = models.UUIDField(_('event SDC ID'))
    state = models.CharField(
        _('state'),
        max_length=32,
        choices=State.choices,
        default=State.PROCESSING,
    )
    data = models.JSONField(_('data'))
    traceback = models.TextField(_('traceback'), null=True)
    retry_traceback = models.TextField(_('retry traceback'), null=True)
    objects = RegistrationEventManager()

    def __str__(self):
        return str(self.id)

    def complete(self):
        self.state = self.State.COMPLETED
        self.save()

    def fail(self, exc_type, exc_value, traceback):
        self.state = self.State.FAILED
        self.traceback = ''.join(tb.format_exception(exc_type, exc_value, traceback))
        self.save()

    def fail_to_retry(self, exc_type, exc_value, traceback):
        self.retry_traceback = ''.join(tb.format_exception(exc_type, exc_value, traceback, chain=False))
        self.save()


class RegistrationManager(models.Manager):
    def exists_for_application(self, application):
        # Fixme: Gestione di eventi doppi
        #   L'identificativo di una pratica e il suo stato non sono
        #   valori sufficienti per determinare se la protocollazione
        #   è necessaria in quanto esistono stati di una pratica che
        #   possono ripetersi (es: richiesta/risposta integrazioni)
        if application.status_name in ['status_request_integration', 'status_submitted_after_integration']:
            return False

        return self.filter(
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            is_completed=True,
        ).exists()

    def exists_incomplete_for_application(self, application):
        return self.filter(
            application_sdc_id=application.id,
            application_state_name=application.status_name,
            is_completed=False,
        ).exists()

    def application_registration_data(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
                application_state_name=application.status_name,
                is_completed=True,
                is_updated=False,
            )
            .values('number', 'date', 'folder')
            .first()
        )

    def application_folder(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
            )
            .values_list('folder', flat=True)
            .first()
        )

    def exist_folder_for_application(self, application):
        return self.filter(application_sdc_id=application.id, registration_data__folder__isnull=False).exists()

    def most_recent_registration(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
                is_completed=True,
            )
            .values_list('registration_data', flat=True)
            .order_by('-created_at')
            .first()
        )


class Registration(TimestampedModel):
    id = models.BigAutoField(primary_key=True)  # noqa
    document_id = models.UUIDField(_('document ID'), null=True)
    number = models.CharField(_('number'), max_length=64, null=True)
    folder = models.CharField(_('folder'), max_length=64, null=True)
    date = models.DateTimeField(_('date'), null=True)
    tenant_sdc_id = models.UUIDField(_('tenant SDC ID'))
    service_sdc_id = models.UUIDField(_('service SDC ID'))
    application_sdc_id = models.UUIDField(_('application SDC ID'))
    application_state_name = models.CharField(_('application state name'), max_length=64)
    event_sdc_id = models.UUIDField(_('event SDC ID'))
    is_completed = models.BooleanField(_('Completed'), default=False)
    is_updated = models.BooleanField(_('Updated'), default=False)
    registration_data = models.JSONField(_('Registration data'), null=True)

    objects = RegistrationManager()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(models.Q(is_completed=False))
                | models.Q(models.Q(number__isnull=False) & models.Q(date__isnull=False)),
                name='%(app_label)s_%(class)s_number_and_folder_exists_if_completed',
            ),
        ]

    def __str__(self):
        return str(self.id)


class PreRegistrationManager(models.Manager):
    def application_folder(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
                service_sdc_id=application.service_id,
                tenant_sdc_id=application.tenant_id,
            )
            .values_list('folder', flat=True)
            .first()
        )

    def application_document(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
                service_sdc_id=application.service_id,
                tenant_sdc_id=application.tenant_id,
            )
            .values_list('document', flat=True)
            .first()
        )

    def application_pre_registration_data(self, application):
        return (
            self.filter(
                application_sdc_id=application.id,
                service_sdc_id=application.service_id,
                tenant_sdc_id=application.tenant_id,
            )
            .values_list('pre_registration_data', flat=True)
            .first()
        )


class PreRegistration(TimestampedModel):
    id = models.BigAutoField(primary_key=True)  # noqa
    folder = models.CharField(_('folder'), max_length=64, null=True)
    document = models.CharField('document', max_length=64, null=True)
    number = models.CharField(_('number'), max_length=64, null=True)
    date = models.DateTimeField(_('date'), null=True)
    tenant_sdc_id = models.UUIDField(_('tenant SDC ID'))
    service_sdc_id = models.UUIDField(_('service SDC ID'))
    application_sdc_id = models.UUIDField(_('application SDC ID'))
    application_state_name = models.CharField(_('application state name'), max_length=64)
    event_sdc_id = models.UUIDField(_('event SDC ID'))
    pre_registration_data = models.JSONField(_('PreRegistration data'), null=True)

    objects = PreRegistrationManager()

    def __str__(self):
        return str(self.id)


class DeadLetterQuerySet(models.QuerySet):
    def delete(self):
        self.update(is_deleted=True)

    delete.queryset_only = True


class DeadLetterManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().exclude(is_deleted=True)


class DeadLetter(TimestampedModel):
    class State(models.TextChoices):
        NEW = ('new', _('new'))
        QUEUED = ('queued', _('queued'))
        FAILED = ('failed', _('failed'))

    retry_producer_class = RetryProducer

    id = models.BigAutoField(primary_key=True)  # noqa
    tenant_sdc_id = models.UUIDField(_('tenant SDC ID'))
    service_sdc_id = models.UUIDField(_('service SDC ID'))
    application_sdc_id = models.UUIDField(_('application SDC ID'))
    state = models.CharField(
        _('state'),
        max_length=32,
        choices=State.choices,
        default=State.NEW,
    )
    data = models.JSONField(_('data'))
    is_deleted = models.BooleanField(default=False)

    objects = DeadLetterManager.from_queryset(DeadLetterQuerySet)()
    all_objects = models.Manager()

    def __str__(self):
        return str(self.id)

    def delete(self, using=None):
        self.is_deleted = True
        self.save(using=using)

    def retry_async(self):
        self._ensure_retryable()
        self._retry_async()

    def _ensure_retryable(self):
        if self.state != self.State.NEW:
            raise errors.NotRetryable

    def _retry_async(self):
        try:
            with self.retry_producer_class() as p:
                p.send(settings.RETRY_SCHEDULER_DESTINATION_TOPIC, self._encoded)
        except Exception:
            self.state = self.State.FAILED
            logger.exception('An error occurred.')
            raise
        else:
            self.state = self.State.QUEUED
        finally:
            self.save()

    @property
    def _encoded(self):
        return json.dumps(self.data).encode()
