import json

from django.conf import settings
from django.urls import reverse
from django_filters import rest_framework as filters
from drf_spectacular.utils import extend_schema_view
from rest_framework import mixins, response, views, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from apps.accounts.permissions import HasValidAPIKey
from apps.applications import errors
from apps.applications import mixins as app_mixins
from apps.applications import schema
from apps.applications.filters import (
    ApplicationStateFilterSet,
    D3ConfigFilterSet,
    DatagraphConfigFilterSet,
    DeadLetterFilterSet,
    DedagroupConfigFilterSet,
    HalleyCloudConfigFilterSet,
    HalleyConfigFilterSet,
    HypersicConfigFilterSet,
    InforConfigFilterSet,
    InsielConfigFilterSet,
    MaggioliConfigFilterSet,
    PitreConfigFilterSet,
    PreRegistrationFilterSet,
    RegistrationFilterSet,
    ServiceFilterSet,
    SicrawebWSProtocolloDMConfigFilterSet,
    SiscomConfigFilterSet,
    TenantFilterSet,
    TinnConfigFilterSet,
)
from apps.applications.metadata import TranslatableSimpleMetadata
from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    InsielConfig,
    MaggioliConfig,
    PitreConfig,
    PreRegistration,
    Registration,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)
from apps.applications.pagination import DefaultPagination
from apps.applications.serializers import (
    ApplicationStateSerializer,
    D3ConfigSerializer,
    D3ServiceSerializer,
    DatagraphConfigSerializer,
    DatagraphServiceSerializer,
    DeadLetterSerializer,
    DedagroupConfigSerializer,
    DedagroupServiceSerializer,
    FormSchemaSerializer,
    HalleyCloudConfigSerializer,
    HalleyCloudServiceSerializer,
    HalleyConfigSerializer,
    HalleyServiceSerializer,
    HypersicConfigSerializer,
    HypersicServiceSerializer,
    InforConfigSerializer,
    InforServiceSerializer,
    InsielConfigSerializer,
    InsielServiceSerializer,
    MaggioliConfigSerializer,
    MaggioliServiceSerializer,
    PitreConfigSerializer,
    PitreServiceSerializer,
    PreRegistrationSerializer,
    ProviderSerializer,
    RegistrationSerializer,
    ServiceSerializer,
    SicrawebWSProtocolloDMConfigSerializer,
    SicrawebWSProtocolloDMServiceSerializer,
    SiscomConfigSerializer,
    SiscomServiceSerializer,
    TenantSerializer,
    TinnConfigSerializer,
    TinnServiceSerializer,
)


@extend_schema_view(**schema.SERVICE_VIEWSET_OVERRIDES)
class ServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    viewsets.ModelViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = ServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = Service.objects.select_related('tenant').prefetch_related('application_states').order_by('-modified_at')
    lookup_field = 'sdc_id'


@extend_schema_view(**schema.DATAGRAPH_SERVICE_VIEWSET_OVERRIDES)
class DatagraphServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = DatagraphServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'datagraph_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.MAGGIOLI_SERVICE_VIEWSET_OVERRIDES)
class MaggioliServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = MaggioliServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'maggioli_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.HALLEY_SERVICE_VIEWSET_OVERRIDES)
class HalleyServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HalleyServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'halley_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.HALLEY_CLOUD_SERVICE_VIEWSET_OVERRIDES)
class HalleyCloudServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HalleyCloudServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'halley_cloud_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.DEDAGROUP_SERVICE_VIEWSET_OVERRIDES)
class DedagroupServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = DedagroupServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'dedagroup_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.PITRE_SERVICE_VIEWSET_OVERRIDES)
class PitreServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = PitreServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'pitre_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.INSIEL_SERVICE_VIEWSET_OVERRIDES)
class InsielServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = InsielServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'insiel_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.INFOR_SERVICE_VIEWSET_OVERRIDES)
class InforServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = InforServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'infor_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.D3_SERVICE_VIEWSET_OVERRIDES)
class D3ServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = D3ServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'd3_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.HYPERSIC_SERVICE_VIEWSET_OVERRIDES)
class HypersicServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HypersicServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'hypersic_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.TINN_SERVICE_VIEWSET_OVERRIDES)
class TinnServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = TinnServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'tinn_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.SICRAWEB_WSPROTOCOLLODM_SERVICE_VIEWSET_OVERRIDES)
class SicrawebWSProtocolloDMServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = SicrawebWSProtocolloDMServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'sicraweb_wsprotocollodm_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.SISCOM_SERVICE_VIEWSET_OVERRIDES)
class SiscomServiceViewSet(
    app_mixins.ApplicationStatesMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    app_mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = SiscomServiceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ServiceFilterSet
    queryset = (
        Service.objects.select_related('tenant')
        .prefetch_related('application_states', 'siscom_configs')
        .order_by('-modified_at')
    )
    lookup_field = 'sdc_id'
    metadata_class = TranslatableSimpleMetadata


@extend_schema_view(**schema.PROVIDER_LIST_APIVIEW_OVERRIDES)
class ProviderListAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        name, endpoint = 'name', 'endpoint'
        providers = [
            {name: Service.Provider.DATAGRAPH, endpoint: reverse('datagraph-service-list')},
            {name: Service.Provider.HALLEY, endpoint: reverse('halley-service-list')},
            {name: Service.Provider.HALLEY_CLOUD, endpoint: reverse('halley-cloud-service-list')},
            {name: Service.Provider.MAGGIOLI, endpoint: reverse('maggioli-service-list')},
            {name: Service.Provider.PITRE, endpoint: reverse('pitre-service-list')},
            {name: Service.Provider.INSIEL, endpoint: reverse('insiel-service-list')},
            {name: Service.Provider.INFOR, endpoint: reverse('infor-service-list')},
            {name: Service.Provider.D3, endpoint: reverse('d3-service-list')},
            {name: Service.Provider.HYPERSIC, endpoint: reverse('hypersic-service-list')},
            {name: Service.Provider.TINN, endpoint: reverse('tinn-service-list')},
            {
                name: Service.Provider.SICRAWEB_WSPROTOCOLLODM,
                endpoint: reverse('sicraweb-wsprotocollodm-service-list'),
            },
            {name: Service.Provider.SISCOM, endpoint: reverse('siscom-service-list')},
        ]
        serializer = ProviderSerializer(providers, many=True)
        return response.Response(serializer.data)


class ApplicationStateViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    serializer_class = ApplicationStateSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ApplicationStateFilterSet
    queryset = ApplicationState.objects.all().order_by('name')


class DatagraphConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = DatagraphConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = DatagraphConfigFilterSet
    queryset = DatagraphConfig.objects.all().order_by('-modified_at')


class DedagroupConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = DedagroupConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = DedagroupConfigFilterSet
    queryset = DedagroupConfig.objects.all().order_by('-modified_at')


class HalleyConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HalleyConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HalleyConfigFilterSet
    queryset = HalleyConfig.objects.all().order_by('-modified_at')


class HalleyCloudConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HalleyCloudConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HalleyCloudConfigFilterSet
    queryset = HalleyCloudConfig.objects.all().order_by('-modified_at')


class MaggioliConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = MaggioliConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MaggioliConfigFilterSet
    queryset = MaggioliConfig.objects.all().order_by('-modified_at')


class PitreConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = PitreConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    queryset = PitreConfig.objects.all().order_by('-modified_at')
    filterset_class = PitreConfigFilterSet


class InsielConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = InsielConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = InsielConfigFilterSet
    queryset = InsielConfig.objects.all().order_by('-modified_at')


class InforConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = InforConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = InforConfigFilterSet
    queryset = InforConfig.objects.all().order_by('-modified_at')


class D3ConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = D3ConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = D3ConfigFilterSet
    queryset = D3Config.objects.all().order_by('-modified_at')


class HypersicConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = HypersicConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = HypersicConfigFilterSet
    queryset = HypersicConfig.objects.all().order_by('-modified_at')


class TinnConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = TinnConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TinnConfigFilterSet
    queryset = TinnConfig.objects.all().order_by('-modified_at')


class SicrawebWSProtocolloDMConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = SicrawebWSProtocolloDMConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = SicrawebWSProtocolloDMConfigFilterSet
    queryset = SicrawebWSProtocolloDMConfig.objects.all().order_by('-modified_at')


class SiscomConfigViewSet(viewsets.ModelViewSet):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = SiscomConfigSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = SiscomConfigFilterSet
    queryset = SiscomConfig.objects.all().order_by('-modified_at')


class RegistrationViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = RegistrationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = RegistrationFilterSet
    queryset = Registration.objects.all().order_by('-created_at')


class PreRegistrationViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = PreRegistrationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PreRegistrationFilterSet
    queryset = PreRegistration.objects.all().order_by('-created_at')


@extend_schema_view(**schema.DEAD_LETTER_VIEWSET_OVERRIDES)
class DeadLetterViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = DeadLetterSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = DeadLetterFilterSet
    queryset = DeadLetter.objects.all().order_by('-created_at')

    @action(detail=True, methods=['post'])
    def retry(self, request, pk=None):
        dead_letter = self.get_object()
        try:
            dead_letter.retry_async()
        except errors.NotRetryable:
            raise PermissionDenied
        except Exception:
            raise errors.ServiceUnavailable
        return response.Response()


class TenantViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = (HasValidAPIKey,)
    pagination_class = DefaultPagination
    serializer_class = TenantSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TenantFilterSet
    queryset = Tenant.objects.all().order_by('-created_at')


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class PitreSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'pitre-form-schema.json'
        with open(path) as f:
            pitre_schema = json.load(f)
        serializer = FormSchemaSerializer(pitre_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class MaggioliSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'maggioli-form-schema.json'
        with open(path) as f:
            maggioli_schema = json.load(f)
        serializer = FormSchemaSerializer(maggioli_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class DatagraphSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'datagraph-form-schema.json'
        with open(path) as f:
            datagraph_schema = json.load(f)
        serializer = FormSchemaSerializer(datagraph_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class DedagroupSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'dedagroup-form-schema.json'
        with open(path) as f:
            dedagroup_schema = json.load(f)
        serializer = FormSchemaSerializer(dedagroup_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class InsielSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'insiel-form-schema.json'
        with open(path) as f:
            insiel_schema = json.load(f)
        serializer = FormSchemaSerializer(insiel_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class HalleySchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'halley-form-schema.json'
        with open(path) as f:
            halley_schema = json.load(f)
        serializer = FormSchemaSerializer(halley_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class HalleyCloudSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'halley-cloud-form-schema.json'
        with open(path) as f:
            halley_cloud_schema = json.load(f)
        serializer = FormSchemaSerializer(halley_cloud_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class InforSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'infor-form-schema.json'
        with open(path) as f:
            infor_schema = json.load(f)
        serializer = FormSchemaSerializer(infor_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class D3SchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'd3-form-schema.json'
        with open(path) as f:
            d3_schema = json.load(f)
        serializer = FormSchemaSerializer(d3_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class HypersicSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'hypersic-form-schema.json'
        with open(path) as f:
            hypersic_schema = json.load(f)
        serializer = FormSchemaSerializer(hypersic_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class TinnSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'tinn-form-schema.json'
        with open(path) as f:
            tinn_schema = json.load(f)
        serializer = FormSchemaSerializer(tinn_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class SicrawebWSProtocolloDMSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'sicraweb-wsprotocollodm-form-schema.json'
        with open(path) as f:
            sicraweb_wsprotocollodm_schema = json.load(f)
        serializer = FormSchemaSerializer(sicraweb_wsprotocollodm_schema)
        return response.Response(serializer.data)


@extend_schema_view(**schema.FORM_SCHEMA_API_VIEW_OVERRIDES)
class SiscomSchemaAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def get(self, request):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'siscom-form-schema.json'
        with open(path) as f:
            siscom_schema = json.load(f)
        serializer = FormSchemaSerializer(siscom_schema)
        return response.Response(serializer.data)
