import sentry_sdk
from django.conf import settings
from prometheus_client import CollectorRegistry, Counter, Gauge, pushadd_to_gateway
from prometheus_client.exposition import default_handler

TIMEOUT = 5


class GravelGatewayClient:
    def __init__(self, *, handler=None):
        self._handler = handler or default_handler

    def incr_retries(self, tenant_slug):
        try:
            self._incr_retries(tenant_slug)
        except Exception:
            sentry_sdk.capture_exception()

    def _incr_retries(self, tenant_slug):
        cr = CollectorRegistry()
        Counter(
            'retries',
            'Registration retries',
            labelnames=['tenant', 'clearmode'],
            namespace='registry',
            registry=cr,
        ).labels(tenant_slug, 'aggregate').inc()
        self._push(cr)

    def set_latency(self, delta):
        try:
            self._set_latency(delta)
        except Exception:
            sentry_sdk.capture_exception()

    def _set_latency(self, delta):
        cr = CollectorRegistry()
        Gauge(
            'latency',
            'Registration latency',
            labelnames=['clearmode'],
            namespace='registry',
            unit='seconds',
            registry=cr,
        ).labels('replace').set(delta.total_seconds())
        self._push(cr)

    def incr_dead_lettered(self, tenant_slug):
        try:
            self._incr_dead_lettered(tenant_slug)
        except Exception:
            sentry_sdk.capture_exception()

    def _incr_dead_lettered(self, tenant_slug):
        cr = CollectorRegistry()
        Counter(
            'dead_lettered',
            'Dead-lettered applications',
            labelnames=['tenant', 'clearmode'],
            namespace='registry',
            registry=cr,
        ).labels(tenant_slug, 'aggregate').inc()
        self._push(cr)

    def _push(self, registry):
        if not settings.GRAVEL_GATEWAY_HOST:
            return

        def auth_handler(url, method, timeout, headers, data):
            timeout = TIMEOUT
            headers.append(('Authorization', f'Basic {settings.GRAVEL_GATEWAY_PASSWORD}'))
            return self._handler(url, method, timeout, headers, data)

        pushadd_to_gateway(
            settings.GRAVEL_GATEWAY_HOST,
            job='registration',
            registry=registry,
            handler=auth_handler,
        )
