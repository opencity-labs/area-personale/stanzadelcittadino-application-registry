from rest_framework import mixins


class UpdateModelMixin:
    def update(self, request, *args, **kwargs):
        return mixins.UpdateModelMixin.update(self, request, *args, **kwargs)

    def perform_update(self, serializer):
        return mixins.UpdateModelMixin.perform_update(self, serializer)


class ApplicationStatesMixin:
    def get_serializer(self, *args, **kwargs):
        if self.action in {'create', 'metadata'}:
            kwargs['default_application_states'] = True
        return super().get_serializer(*args, **kwargs)
