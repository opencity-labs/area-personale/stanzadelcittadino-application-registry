from django.middleware.locale import LocaleMiddleware
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext_noop
from rest_framework.metadata import SimpleMetadata

# Mark undetectable strings for translation, so that `makemessages` can
# pull them out.
gettext_noop('Application states')
gettext_noop('Configs')
gettext_noop('Created at')
gettext_noop('Modified at')


class MetadataLocaleMiddleware(LocaleMiddleware):
    def __call__(self, request):
        if request.method.upper() == 'OPTIONS':
            return super().__call__(request)
        return self.get_response(request)


class TranslatableSimpleMetadata(SimpleMetadata):
    _label_replacements = {
        'Id': 'ID',
        'Tenant sdc id': gettext_noop('Tenant SDC ID'),
        'Institution office ids': gettext_noop('Institution office IDs'),
    }

    def get_field_info(self, field):
        field_info = super().get_field_info(field)
        if label := field_info.get('label'):
            label = self._label_replacements.get(label) or label
            field_info['label'] = _(label)
        return field_info
