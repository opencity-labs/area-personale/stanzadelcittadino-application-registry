from django import forms
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from apps.applications import errors
from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    InsielConfig,
    MaggioliConfig,
    PitreConfig,
    PreRegistration,
    Registration,
    RegistrationEvent,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)


class ReadOnlyAdminMixin:
    def has_add_permission(self, *args, **kwargs):
        return False

    def has_change_permission(self, *args, **kwargs):
        return False

    def has_delete_permission(self, *args, **kwargs):
        return False


class ReadOnlyTimestampsAdminMixin:
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            return (*readonly_fields, 'created_at', 'modified_at')
        return readonly_fields


class ServiceInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = Service
    fields = ('id', 'description', 'sdc_id')
    show_change_link = True


class DatagraphConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = DatagraphConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class MaggioliConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = MaggioliConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class HalleyConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = HalleyConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class HalleyCloudConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = HalleyCloudConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class DedagroupConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = DedagroupConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class PitreConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = PitreConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class InsielConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = InsielConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class InforConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = InforConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class D3ConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = D3Config
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class HypersicConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = HypersicConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class TinnConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = TinnConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class SicrawebWSProtocolloDMConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = SicrawebWSProtocolloDMConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class SiscomConfigInline(
    ReadOnlyAdminMixin,
    admin.TabularInline,
):
    model = SiscomConfig
    fields = ('id', 'description', 'is_active')
    show_change_link = True


class TenantForm(forms.ModelForm):
    def clean_sdc_base_url(self):
        return self.cleaned_data['sdc_base_url'].strip('/')


@admin.register(Tenant)
class TenantAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = TenantForm
    list_display = (
        'id',
        'description',
        'sdc_id',
        'created_at',
        'modified_at',
    )
    search_fields = ('description', 'sdc_id')
    inlines = [ServiceInline]
    ordering = ('-created_at',)

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(Service)
class ServiceAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    autocomplete_fields = ('tenant',)
    list_display = (
        'id',
        'description',
        'sdc_id',
        'provider',
        'created_at',
        'modified_at',
    )
    list_filter = (('tenant', admin.RelatedOnlyFieldListFilter),)
    search_fields = ('description', 'sdc_id')
    filter_horizontal = ('application_states',)
    inlines = [
        DatagraphConfigInline,
        MaggioliConfigInline,
        HalleyConfigInline,
        HalleyCloudConfigInline,
        DedagroupConfigInline,
        PitreConfigInline,
        InsielConfigInline,
        InforConfigInline,
        D3ConfigInline,
        HypersicConfigInline,
        TinnConfigInline,
        SicrawebWSProtocolloDMConfigInline,
        SiscomConfigInline,
    ]
    ordering = ('-created_at',)

    def get_changeform_initial_data(self, request):
        initial = super().get_changeform_initial_data(request)
        initial['application_states'] = [str(state.pk) for state in ApplicationState.objects.default()]
        return initial

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


class ApplicationStateForm(forms.ModelForm):
    def clean_name(self):
        return self.cleaned_data['name'].lower()


@admin.register(ApplicationState)
class ApplicationStateAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ApplicationStateForm
    list_display = (
        'id',
        'name',
        'code',
        'direction',
        'integrations',
        'default',
        'update_sdc',
        'created_at',
        'modified_at',
    )
    list_filter = ('direction', 'integrations', 'default', 'update_sdc')
    ordering = ('-created_at',)


class ConfigForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        service = cleaned_data.get('service')
        is_active = cleaned_data.get('is_active')

        if service and is_active:
            if self._meta.model.objects.exclude(pk=self.instance.pk).filter(service=service, is_active=True).exists():
                raise ValidationError(
                    _('An active configuration already exists for the selected service.'),
                    code='active_config_exists',
                )

        return cleaned_data


class InsielConfigForm(ConfigForm):
    def __init__(self, *args, **kwargs):
        super(ConfigForm, self).__init__(*args, **kwargs)
        self.fields['register_code'].strip = False


@admin.register(DatagraphConfig)
class DatagraphConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'classification_institution_code',
        'classification_aoo_code',
        'classification_hierarchy',
        'folder_number',
        'folder_year',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form


@admin.register(MaggioliConfig)
class MaggioliConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'sending_mode',
        'sending_method',
        'classification_hierarchy',
        'folder_number',
        'folder_year',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(HalleyConfig)
class HalleyConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'institution_office_ids',
        'classification_subclass',
        'folder_id',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(HalleyCloudConfig)
class HalleyCloudConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'institution_office_ids',
        'classification_category',
        'classification_class',
        'classification_subclass',
        'mailbox',
        'folder_name',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(DedagroupConfig)
class DedagroupConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'classification_title',
        'classification_class',
        'classification_subclass',
        'folder_code',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(PitreConfig)
class PitreConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'code_rf',
        'transmission_reason',
        'transmission_type',
        'recipient_code',
        'means_of_sending',
        'note',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(InsielConfig)
class InsielConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = InsielConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'operating_office_code',
        'internal_offices_codes',
        'internal_offices_types',
        'register_code',
        'prog_doc',
        'prog_movi',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(InforConfig)
class InforConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = ()

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(D3Config)
class D3ConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'attachment_source_category',
        'document_subtype',
        'classification',
        'folder_name',
        'visibility',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(HypersicConfig)
class HypersicConfigAdmin(ReadOnlyTimestampsAdminMixin, admin.ModelAdmin):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'classification',
        'folder_number',
        'folder_year',
        'annotation',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form

    def get_queryset(self, request):
        # TODO: Workaround for Vicopisano
        qs = super().get_queryset(request)
        if request.user.username == 'ivano.miscali':
            return qs.filter(service__tenant__sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs


@admin.register(TinnConfig)
class TinnConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'institution_name',
        'institution_code',
        'institution_email',
        'institution_ou',
        'institution_aoo_code',
        'classification_institution_code',
        'classification_aoo_code',
        'folder_number',
        'folder_year',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form


@admin.register(SicrawebWSProtocolloDMConfig)
class SicrawebWSProtocolloDMConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'institution_code',
        'institution_ou',
        'sending_mode',
        'internal_sender',
        'update_records',
        'classification_hierarchy',
        'folder_number',
        'folder_year',
        'connection_user',
        'connection_user_role',
        'standard_subject',
        'document_type',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form


@admin.register(SiscomConfig)
class SiscomConfigAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    form = ConfigForm
    autocomplete_fields = ('service',)
    list_display = (
        'id',
        'description',
        'is_active',
        'created_at',
        'modified_at',
    )
    list_filter = (
        'is_active',
        ('service', admin.RelatedOnlyFieldListFilter),
    )
    search_fields = ('description',)
    ordering = ('-created_at',)
    _optional_fields = (
        'operator_denomination',
        'classification_category',
        'classification_class',
        'folder_id',
        'mail_type',
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        for field in self._optional_fields:
            form.base_fields[field].required = False
        return form


@admin.register(RegistrationEvent)
class RegistrationEventAdmin(
    ReadOnlyAdminMixin,
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    list_display = (
        'id',
        'provider',
        'config_id',
        'application_sdc_id',
        'event_sdc_id',
        'state',
        'created_at',
        'modified_at',
    )
    list_filter = ('provider', 'config_id', 'state')
    search_fields = ('application_sdc_id',)
    ordering = ('-modified_at',)


@admin.register(Registration)
class RegistrationAdmin(
    ReadOnlyAdminMixin,
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    list_display = (
        'id',
        'is_completed',
        'is_updated',
        'number',
        'date',
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'application_state_name',
        'event_sdc_id',
        'created_at',
        'modified_at',
    )
    list_filter = ('tenant_sdc_id', 'service_sdc_id')
    search_fields = (
        'number',
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'event_sdc_id',
    )
    ordering = ('-created_at',)


@admin.register(PreRegistration)
class PreRegistrationAdmin(
    ReadOnlyAdminMixin,
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    list_display = (
        'id',
        'folder',
        'number',
        'date',
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'application_state_name',
        'event_sdc_id',
        'pre_registration_data',
        'created_at',
        'modified_at',
    )
    list_filter = ('tenant_sdc_id', 'service_sdc_id')
    search_fields = (
        'folder' 'number',
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'event_sdc_id',
    )
    ordering = ('-created_at',)


@admin.register(DeadLetter)
class DeadLetterAdmin(
    ReadOnlyTimestampsAdminMixin,
    admin.ModelAdmin,
):
    readonly_fields = (
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'data',
    )
    list_display = (
        'id',
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
        'state',
        'is_deleted',
        'created_at',
        'modified_at',
    )
    list_filter = ('state', 'is_deleted')
    search_fields = (
        'tenant_sdc_id',
        'service_sdc_id',
        'application_sdc_id',
    )
    ordering = ('-created_at',)
    actions = ['retry']

    def get_queryset(self, request):
        qs = self.model.all_objects.get_queryset().order_by(*self.ordering)
        # TODO: Workaround for Vicopisano
        if request.user.username == 'ivano.miscali':
            return qs.filter(tenant_sdc_id='285dff7a-090d-473c-a076-9e54547aa1a6')
        return qs

    def has_add_permission(self, *args, **kwargs):
        return False

    def has_change_permission(self, *args, **kwargs):
        return True

    def has_delete_permission(self, *args, **kwargs):
        return False

    @admin.action(description=_('Retry registration of selected application'))
    def retry(self, request, queryset):
        try:
            queryset.get().retry_async()
        except DeadLetter.MultipleObjectsReturned:
            self.message_user(request, _('Please select a single application.'), messages.ERROR)
        except errors.NotRetryable:
            self.message_user(request, _('The selected application is currently not retryable.'), messages.ERROR)
        except Exception:
            self.message_user(request, _('An unexpected error occurred.'), messages.ERROR)
        else:
            self.message_user(request, _('The application was scheduled for retry.'), messages.SUCCESS)
