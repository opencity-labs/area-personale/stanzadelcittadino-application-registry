from django.db import models
from django_filters import rest_framework as filters

from apps.applications.models import ApplicationState


class ServiceFilterSet(filters.FilterSet):
    tenant_sdc_id = filters.UUIDFilter(field_name='tenant__sdc_id', lookup_expr='exact')


class Boolean(models.TextChoices):
    TRUE = 'true'
    FALSE = 'false'


class ApplicationStateFilterSet(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='exact')
    direction = filters.ChoiceFilter(
        field_name='direction',
        lookup_expr='exact',
        choices=ApplicationState.Direction.choices,
    )
    default = filters.TypedChoiceFilter(
        field_name='default',
        lookup_expr='exact',
        choices=Boolean.choices,
        coerce=lambda value: {Boolean.TRUE: True, Boolean.FALSE: False}[value],
    )


class ProviderConfigFilterSetMixin(filters.FilterSet):
    service_sdc_id = filters.UUIDFilter(field_name='service__sdc_id', lookup_expr='exact')
    is_active = filters.TypedChoiceFilter(
        field_name='is_active',
        lookup_expr='exact',
        choices=Boolean.choices,
        coerce=lambda value: {Boolean.TRUE: True, Boolean.FALSE: False}[value],
    )


class DatagraphConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class DedagroupConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class HalleyConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class HalleyCloudConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class MaggioliConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class PitreConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class InsielConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class InforConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class D3ConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class HypersicConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class TinnConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class SicrawebWSProtocolloDMConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class SiscomConfigFilterSet(ProviderConfigFilterSetMixin):
    pass


class RegistrationFilterSet(filters.FilterSet):
    application_sdc_id = filters.UUIDFilter(field_name='application_sdc_id', lookup_expr='exact')
    is_completed = filters.TypedChoiceFilter(
        field_name='is_completed',
        lookup_expr='exact',
        choices=Boolean.choices,
        coerce=lambda value: {Boolean.TRUE: True, Boolean.FALSE: False}[value],
    )


class PreRegistrationFilterSet(filters.FilterSet):
    application_sdc_id = filters.UUIDFilter(field_name='application_sdc_id', lookup_expr='exact')
    service_sdc_id = filters.UUIDFilter(field_name='service__sdc_id', lookup_expr='exact')
    tenant_sdc_id = filters.UUIDFilter(field_name='tenant_sdc_id', lookup_expr='exact')
    event_sdc_id = filters.UUIDFilter(field_name='event_sdc_id', lookup_expr='exact')


class DeadLetterFilterSet(filters.FilterSet):
    tenant_sdc_id = filters.UUIDFilter(field_name='tenant_sdc_id', lookup_expr='exact')
    service_sdc_id = filters.UUIDFilter(field_name='service_sdc_id', lookup_expr='exact')


class TenantFilterSet(filters.FilterSet):
    sdc_id = filters.UUIDFilter(field_name='sdc_id', lookup_expr='exact')
    slug = filters.CharFilter(field_name='slug', lookup_expr='exact')
