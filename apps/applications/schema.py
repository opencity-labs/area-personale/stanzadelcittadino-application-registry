from drf_spectacular.utils import extend_schema
from rest_framework import serializers

from apps.applications.models import ApplicationState
from apps.applications.serializers import (
    D3ServiceSerializer,
    DatagraphServiceSerializer,
    DedagroupServiceSerializer,
    FormSchemaSerializer,
    HalleyCloudServiceSerializer,
    HalleyServiceSerializer,
    HypersicServiceSerializer,
    InforServiceSerializer,
    InsielServiceSerializer,
    MaggioliServiceSerializer,
    PitreServiceSerializer,
    ProviderSerializer,
    ServiceSerializer,
    SicrawebWSProtocolloDMServiceSerializer,
    SiscomServiceSerializer,
    TinnServiceSerializer,
)


class ServiceCreateSchemaSerializer(serializers.Serializer):
    application_states = serializers.SlugRelatedField(
        slug_field='name',
        queryset=ApplicationState.objects.all(),
        many=True,
        default=lambda: list(ApplicationState.objects.default()),
    )

    def __init__(self, *args, **kwargs):
        serializers.ModelSerializer.__init__(self, *args, **kwargs)


class ServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    ServiceSerializer,
):
    pass


class DatagraphServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    DatagraphServiceSerializer,
):
    pass


class MaggioliServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    MaggioliServiceSerializer,
):
    pass


class HalleyServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    HalleyServiceSerializer,
):
    pass


class HalleyCloudServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    HalleyCloudServiceSerializer,
):
    pass


class DedagroupServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    DedagroupServiceSerializer,
):
    pass


class PitreServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    PitreServiceSerializer,
):
    pass


class InsielServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    InsielServiceSerializer,
):
    pass


class InforServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    InforServiceSerializer,
):
    pass


class D3ServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    D3ServiceSerializer,
):
    pass


class HypersicServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    HypersicServiceSerializer,
):
    pass


class TinnServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    TinnServiceSerializer,
):
    pass


class SicrawebWSProtocolloDMServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    SicrawebWSProtocolloDMServiceSerializer,
):
    pass


class SiscomServiceCreateSerializer(
    ServiceCreateSchemaSerializer,
    SiscomServiceSerializer,
):
    pass


SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=ServiceCreateSerializer,
        responses=ServiceCreateSerializer,
    ),
}
DATAGRAPH_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=DatagraphServiceCreateSerializer,
        responses=DatagraphServiceCreateSerializer,
    ),
}
MAGGIOLI_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=MaggioliServiceCreateSerializer,
        responses=MaggioliServiceCreateSerializer,
    ),
}
HALLEY_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=HalleyServiceCreateSerializer,
        responses=HalleyServiceCreateSerializer,
    ),
}
HALLEY_CLOUD_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=HalleyCloudServiceCreateSerializer,
        responses=HalleyCloudServiceCreateSerializer,
    ),
}
DEDAGROUP_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=DedagroupServiceCreateSerializer,
        responses=DedagroupServiceCreateSerializer,
    ),
}
PITRE_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=PitreServiceCreateSerializer,
        responses=PitreServiceCreateSerializer,
    ),
}
INSIEL_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=InsielServiceCreateSerializer,
        responses=InsielServiceCreateSerializer,
    ),
}
INFOR_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=InforServiceCreateSerializer,
        responses=InforServiceCreateSerializer,
    ),
}
D3_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=D3ServiceCreateSerializer,
        responses=D3ServiceCreateSerializer,
    ),
}
HYPERSIC_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=HypersicServiceCreateSerializer,
        responses=HypersicServiceCreateSerializer,
    ),
}
TINN_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=TinnServiceCreateSerializer,
        responses=TinnServiceCreateSerializer,
    ),
}
SICRAWEB_WSPROTOCOLLODM_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=SicrawebWSProtocolloDMServiceCreateSerializer,
        responses=SicrawebWSProtocolloDMServiceCreateSerializer,
    ),
}
SISCOM_SERVICE_VIEWSET_OVERRIDES = {
    'create': extend_schema(
        request=SiscomServiceCreateSerializer,
        responses=SiscomServiceCreateSerializer,
    ),
}
PROVIDER_LIST_APIVIEW_OVERRIDES = {
    'get': extend_schema(
        responses=ProviderSerializer(many=True),
    ),
}
DEAD_LETTER_VIEWSET_OVERRIDES = {
    'retry': extend_schema(
        request=None,
        responses=None,
    ),
}
FORM_SCHEMA_API_VIEW_OVERRIDES = {
    'get': extend_schema(
        responses=FormSchemaSerializer,
    ),
}
