from datetime import datetime, timezone, tzinfo
from typing import Optional

from dateutil import parser


def localtime(value: Optional[datetime] = None, tz: Optional[tzinfo] = None) -> datetime:
    """
    Convert an aware datetime.datetime to local time.

    Only aware datetimes are allowed.
    When value is omitted, it defaults to now().

    Local time is defined by the current time zone,
    unless another time zone is specified.
    """
    if value is None:
        value = datetime.now(timezone.utc)  # Usa datetime.now con timezone.utc per un datetime consapevole
    if value.tzinfo is None or value.tzinfo.utcoffset(value) is None:
        raise ValueError('localtime() cannot be applied to a naive datetime')

    if tz is None:
        tz = datetime.now().astimezone().tzinfo  # Ottieni il fuso orario locale corrente

    return value.astimezone(tz)


def make_aware(value: datetime, timezone: Optional[tzinfo] = None) -> datetime:
    """
    Make a naive datetime.datetime in a given time zone aware.

    :param value: Naive datetime object.
    :param tz: Optional timezone.
    If not provided, defaults to the local timezone.
    :return: Aware datetime object.
    """
    if value.tzinfo is not None:
        raise ValueError('make_aware expects a naive datetime, got %s' % value)

    if timezone is None:
        # Imposta il fuso orario di default al fuso orario locale
        timezone = datetime.now().astimezone().tzinfo

    # Restituisce il datetime consapevole
    # impostando il tzinfo direttamente
    return value.replace(tzinfo=timezone)


def safe_parse_and_localtime(date_str: str) -> datetime:
    try:
        dt = parser.isoparse(date_str)
        if dt.year < 1900:
            return datetime.min.replace(tzinfo=timezone.utc)
        return localtime(dt)
    except OverflowError:
        return datetime.min.replace(tzinfo=timezone.utc)
