import os

from rest_framework import permissions


class HasValidAPIKey(permissions.BasePermission):
    def has_permission(self, request, view):
        if api_key := request.headers.get('X-Registry-API-Key'):
            return api_key == os.getenv('REGISTRY_API_KEY')
        return False
