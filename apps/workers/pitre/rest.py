import logging
import os
import tempfile
from datetime import datetime, timezone

import requests
import urllib3
from django.utils.functional import cached_property
from requests.exceptions import RequestException

from apps.workers.errors import RESTError, RESTValidationError

logger = logging.getLogger('application_registry')

TIMEOUT = 30
SUCCESS_CODE = 0
TMP_DIR = '/tmp/'

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Client:
    http_client = requests

    def __init__(self, config):
        self._config = config
        self._temp_certificate = None

        with tempfile.NamedTemporaryFile(mode='w+', delete=False, dir=TMP_DIR) as temp:
            temp.write(self._config.certificate)
            temp.seek(os.SEEK_SET)
            self._temp_certificate = temp.name

    @cached_property
    def _token(self):
        try:
            response = self.http_client.post(
                self._config.base_url,
                json={
                    'Username': self._config.username,
                    'CodeRole': self._config.code_role,
                    'CodeApplication': self._config.code_application,
                    'CodeAdm': self._config.code_adm,
                },
                headers={
                    'CODE_ADM': self._config.code_adm,
                    'ROUTED_ACTION': 'GetToken',
                    'Content-Type': 'application/json',
                },
                cert=self._temp_certificate,
                verify=False,
            )
            response.raise_for_status()

            return response.json()['Token']
        except (RequestException, KeyError) as e:
            raise RESTValidationError(e) from e

    def create_project(self, application):
        try:
            return self._create_project(application)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _create_project(self, application):
        body = {
            'Project': {
                'Description': application.project_description,
                'ClassificationScheme': {
                    'Id': self.classification_scheme.classification_scheme_id,
                },
                'Private': False,
                'CodeNodeClassification': self._config.code_node_classification,
            },
        }

        response = self.http_client.put(
            self._config.base_url,
            json=body,
            headers=self.headers('CreateProject'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def create_document(self, application):
        try:
            return self._create_document(application)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _create_document(self, application):
        body = {
            'CodeRegister': self._config.code_register,
            'Document': {
                'Object': application.document_description,
                'DocumentType': 'A' if application.is_inbound else 'P',
                'Predisposed': True,
                'MeansOfSending': self._config.means_of_sending,
                'Sender': self.inbound_sender(application) if application.is_inbound else self.default_sender.sender,
                'Recipients': [self.inbound_sender(application)] if application.is_outbound else [],
                'ArrivalDate': application.registration_date.strftime('%d/%m/%Y %H:%M:%S'),
                'DataProtocolSender': application.registration_date.strftime('%d/%m/%Y %H:%M:%S'),
                'PrivateDocument': False,
                'Note': [{'Description': self._config.note}] if application.is_outbound else None,
            },
            'CodeRF': self._config.code_rf,
        }

        response = self.http_client.put(
            self._config.base_url,
            json=body,
            headers=self.headers('CreateDocument'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def upload_file_to_document(self, document):
        try:
            return self._upload_file_to_document(document)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _upload_file_to_document(self, document):
        document_name = document.original_name
        if document.original_name.endswith('.p7m') and not document.original_name.endswith('.pdf.p7m'):
            document_name = document.original_name.replace('.p7m', '.pdf.p7m')

        body = {
            'IdDocument': document.attributes.remote_id,
            'File': {'Content': document.attributes.content, 'Name': document_name},
            'CreateAttachment': document.attributes.is_attachment,
            'Description': document.description,
        }

        response = self.http_client.put(
            self._config.base_url,
            json=body,
            headers=self.headers('UploadFileToDocument'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def protocollazione(self, document):
        try:
            return self._protocollazione(document)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _protocollazione(self, document):
        body = {'CodeRegister': self._config.code_register, 'CodeRF': self._config.code_rf, 'IdDocument': document}
        response = self.http_client.post(
            self._config.base_url,
            json=body,
            headers=self.headers('ProtocolPredisposed'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def add_doc_in_project(self, folder, document):
        try:
            return self._add_doc_in_project(folder, document)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _add_doc_in_project(self, folder, document):
        body = {'IdDocument': document, 'CodeProject': folder}
        response = self.http_client.post(
            self._config.base_url,
            json=body,
            headers=self.headers('AddDocInProject'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def execute_transmission_project(self, folder):
        try:
            for transmission_receiver_code in self._config.receiver_codes:
                self._execute_transmission_project(folder, transmission_receiver_code)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _execute_transmission_project(self, project_id, transmission_receiver_code):
        body = {
            'IdProject': project_id,
            'TransmissionReason': self._config.transmission_reason,
            'Receiver': {'Code': transmission_receiver_code},
            'Notify': True,
            'TransmissionType': self._config.transmission_type,
        }
        response = self.http_client.post(
            self._config.base_url,
            json=body,
            headers=self.headers('ExecuteTransmissionProject'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def execute_transmission_document(self, document_id):
        try:
            for transmission_receiver_code in self._config.receiver_codes:
                self._execute_transmission_document(document_id, transmission_receiver_code)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _execute_transmission_document(self, document_id, transmission_receiver_code):
        body = {
            'IdDocument': document_id,
            'TransmissionReason': self._config.transmission_reason,
            'Receiver': {'Code': transmission_receiver_code},
            'Notify': True,
            'TransmissionType': self._config.transmission_type,
        }
        response = self.http_client.post(
            self._config.base_url,
            json=body,
            headers=self.headers('ExecuteTransmissionDocument'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )

        response.raise_for_status()
        return PitreResult(response.json())

    @cached_property
    def classification_scheme(self):
        response = self.http_client.get(
            self._config.base_url,
            headers=self.headers('GetActiveClassificationScheme'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    @cached_property
    def default_sender(self):
        body = {
            'Filters': [
                {'Name': 'EXACT_CODE', 'Value': self._config.recipient_code or self._config.code_role},
                {'Name': 'TYPE', 'Value': 'INTERNAL'},
                {'Name': 'ROLES', 'Value': 'TRUE'},
            ],
        }

        response = self.http_client.post(
            self._config.base_url,
            json=body,
            headers=self.headers('SearchCorrespondents'),
            timeout=TIMEOUT,
            cert=self._temp_certificate,
            verify=False,
        )
        response.raise_for_status()
        return PitreResult(response.json())

    def headers(self, action):
        return {
            'CODE_ADM': self._config.code_adm,
            'ROUTED_ACTION': action,
            'AuthToken': self._token,
            'Content-Type': 'application/json',
        }

    def inbound_sender(self, application):
        return {
            'Description': application.applicant.description,
            'Type': 'O',
            'CorrespondentType': 'P',
            'Name': application.applicant.name,
            'Surname': application.applicant.surname,
            'NationalIdentificationNumber': application.applicant.fiscal_code,
            'Email': application.applicant.email,
        }


class PitreResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result['Code']
        if errcode != SUCCESS_CODE:
            errmsg = self._result['ErrorMessage']
            raise RESTValidationError(errcode, errmsg)

    @property
    def folder(self):
        return str(self._result['Project']['Code'])

    @property
    def folder_id(self):
        return str(self._result['Project']['Id'])

    @property
    def document(self):
        return str(self._result['Document']['Id'])

    @property
    def sender(self):
        return next(iter(self._result['Correspondents'] or []), None)

    @property
    def number(self):
        if 'Document' in self._result:
            return str(self._result['Document']['Signature'])
        else:
            return str(self._result['Stamp']['SignatureValue'])

    @property
    def classification_scheme_id(self):
        return str(self._result['ClassificationScheme']['Id'])
