import logging
import uuid
from base64 import b64encode
from dataclasses import dataclass
from datetime import datetime
from os import path

from django.conf import settings

from apps.applications.models import D3Config, Registration, RegistrationEvent
from apps.applications.sdc.entities import Application, DocumentStatus
from apps.applications.tracker import RegistrationEventTracker
from apps.workers.worker import Producer

DEFAULT_EXTENSION = 'dat'

logger = logging.getLogger('application_registry')


def run(event):
    application = Application(data=event)
    configs = D3Config.objects.active_for_service(application.service_id, application.status_name)
    logger.debug(application.service_id)
    logger.debug(application.status_name)
    for config in configs:
        document_id = RegistrationEvent.objects.last_failed_registration_event(application) or uuid.uuid4()
        with RegistrationEventTracker(document_id, application, config, produce_document=settings.PRODUCE_D3_DOCUMENT):
            Job(document_id, application, config).run()


class Job:
    def __init__(self, document_id, application, config):
        self._document_id = document_id
        self._application = application
        self._config = config
        self._tenant = self._config.tenant

    def run(self):
        is_registration_date_allowed = self._tenant.is_registration_date_allowed(self._application.registration_date)
        registration_exists = Registration.objects.exists_for_application(self._application)
        failed_registration_event_exists = RegistrationEvent.objects.exists_for_application(self._application)
        if not self._application.is_registration_required:
            raise Exception('Main document not found.')
        if (
            is_registration_date_allowed
            and not registration_exists
            and not failed_registration_event_exists
            and settings.PRODUCE_D3_DOCUMENT
        ):
            self._create_document(number=None, date=None, folder=None, status=DocumentStatus.REGISTRATION_PENDING)
        if is_registration_date_allowed and not registration_exists:
            self._set_document_attributes()
            result = None
            # carico l'allegato principale e protocollo la pratica
            upload_result, registration_data = self._upload(self._application.current_document)
            if upload_result:
                self._application.current_document.attributes.content_location_uri = upload_result.content_location_uri
                self._application.current_document.attributes.to_register = True
                result = self._register(self._application.current_document)
            # carico gli allegati secondari senza protocollare
            if self._config.register_attachments:
                for attachment in self._application.current_attachments:
                    attachment_result, registration_data = self._upload(attachment)
                    if attachment_result:
                        attachment.attributes.content_location_uri = attachment_result.content_location_uri
                        self._register(attachment, registration_data)
            # aggiorno utilizzando i dati del protocollo
            # ottenuti dopo aver caricato l'allegato principale
            # oppure recuperando i dati del protocollo
            # salvati nella registration se il caricamento
            # degli allegati secondari è fallito precedentemente
            if not settings.PRODUCE_D3_DOCUMENT:
                self._update_sdc(
                    result.number if result else registration_data['document']['number'],
                    result.created_at
                    if result
                    else datetime.fromisoformat(registration_data['document']['registered_at']),
                )
            else:
                self._create_document(
                    result.number if result else registration_data['document']['number'],
                    result.created_at
                    if result
                    else datetime.fromisoformat(registration_data['document']['registered_at']),
                    status=DocumentStatus.REGISTRATION_COMPLETE,
                )
        elif data := Registration.objects.application_registration_data(self._application):
            # Find registrations completed but not updated yet
            if not settings.PRODUCE_D3_DOCUMENT:
                self._update_sdc(**data)
            else:
                self._create_document(
                    **data,
                    status=DocumentStatus.REGISTRATION_COMPLETE,
                    registration_exists=registration_exists,
                )
        else:
            # Registration date not allowed or no registration data
            self._skip()

    def _set_document_attributes(self):
        logger.debug('Setting attributes')
        for document in self._application.current_documents:
            data = self._tenant.get_document(document.url)
            encoded = b64encode(data).decode()
            ext = path.splitext(document.original_name)[1].removeprefix('.') or DEFAULT_EXTENSION
            document.attributes = Attributes(content=data, extension=ext)
            logger.debug('%s%s, %s', encoded[:30], '...' if len(encoded) > 30 else '', ext)

    def _upload(self, document):
        logger.debug('Uploading document')
        result, registration_data = self._config.upload(self._application, document)
        if result:
            logger.debug('%s', result.content_location_uri)
        else:
            logger.debug('Document %s has already been uploaded', document.original_name)
        return result, registration_data

    def _register(self, document, registration_data=None):
        logger.debug('protocollazione')
        result = self._config.register(self._document_id, self._application, document, registration_data)
        if document.attributes.to_register:
            logger.info(
                'Registered application %s with number %s and year %s',
                self._application.id,
                result.number,
                result.year,
            )
        return result

    def _update_sdc(self, number, date, folder=None):
        if data := self._application.build_registration_data(number, date, folder):
            logger.debug('Updating SDC')
            self._tenant.update_application(self._application.id, data)
        else:
            logger.debug('SDC update skipped')

    def _create_document(self, number, date, folder=None, status=None, registration_exists=False):
        if not registration_exists:
            logger.debug('Producing Document')
            document = self._tenant.create_document(self._document_id, self._application, number, date, folder, status)
            with Producer() as producer:
                producer.send(
                    value=document.json().encode('utf-8'),
                    key=str(document.remote_collection.id).encode('utf-8'),
                )
        else:
            logger.debug('Document production skipped')

    def _skip(self):
        logger.debug('Skipped: application does not require registration.')


@dataclass
class Attributes:
    content: str
    extension: str
    content_location_uri: str = ''
    remote_id: str = ''
    to_register: bool = False
