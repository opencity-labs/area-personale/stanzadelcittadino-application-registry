import logging
from datetime import datetime, timezone
from http.cookies import SimpleCookie
from urllib.parse import urlparse

import requests
from django.utils.functional import cached_property
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException

from apps.workers.errors import RESTError, RESTValidationError

logger = logging.getLogger('application_registry')

TIMEOUT = 30
SUCCESS_CODE = [200, 201, 204]


class Client:
    http_client = requests

    def __init__(self, config):
        self._config = config

    @cached_property
    def _cookie(self):
        try:
            auth = HTTPBasicAuth(self._config.username, self._config.password)
            headers = {
                'Accept': 'application/json',
                'Accept-Encoding': 'gzip, deflate, br',
                'Connection': 'keep-alive',
            }

            response = self.http_client.get(
                url=f'{self._config.registry_url}/identityprovider/login',
                headers=headers,
                auth=auth,
                timeout=TIMEOUT,
            )
            response.raise_for_status()

            cookie = SimpleCookie()
            cookie.load(response.cookies)
            cookie.pop('usrProfileUri')

            return '; '.join([f'{k}={v.value}' for k, v in cookie.items()])
        except (RequestException, KeyError) as e:
            raise RESTValidationError(e) from e

    def _get_headers(self, content_type, accept):
        return {
            'Cookie': self._cookie,
            'Content-Type': content_type,
            'Accept': accept,
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Origin': self._config.registry_url,
        }

    def upload(self, document):
        try:
            return self._upload(document)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _upload(self, document):
        response = self.http_client.post(
            url=f'{self._config.registry_url}/dms/r/{self._config.repository_id}/blob/chunk/',
            headers=self._get_headers(content_type='application/octet-stream', accept='*/*'),
            data=document.attributes.content,
            timeout=TIMEOUT,
        )
        return InsertResult(response)

    def insert(self, application, document):
        try:
            return self._insert(application, document)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _insert(self, application, document):
        body = {
            'sourceId': f'/dms/r/{self._config.repository_id}/source',
            'sourceCategory': self._get_source_category(document),
            'filename': self._get_filename(application, document),
            'contentLocationUri': document.attributes.content_location_uri,
            'sourceProperties': {
                'properties': self._get_properties(application, document),
            },
        }

        response = self.http_client.post(
            url=f'{self._config.registry_url}/dms/r/{self._config.repository_id}/o2m',
            headers=self._get_headers(content_type='application/hal+json', accept='application/hal+json'),
            json=body,
            timeout=TIMEOUT,
        )

        if document.attributes.to_register:
            result = InsertResult(response)
            response = self._get_registration_details(result.document_id)

        return InsertResult(response)

    def _get_registration_details(self, document_id):
        response = self.http_client.get(
            url=(
                f'{self._config.registry_url}/dms/r/{self._config.repository_id}/srm'
                f'?sourceid=/dms/r/{self._config.repository_id}/source'
                f'&sourceproperties={{"property_document_id":["{document_id}"]}}&page=1&pagesize=50&ascending=false'
            ),
            headers=self._get_headers(content_type='application/hal+json', accept='application/hal+json'),
            timeout=TIMEOUT,
        )
        return response

    def _get_properties(self, application, document):
        properties = [
            self._get_oggetto(application, document),
            self._get_to_protocol(document.attributes.to_register),
            self._get_competent_service(),
        ]

        if self._config.classification:
            properties.append(self._get_classification())
        if self._config.folder_name:
            properties.append(self._get_folder())
        if self._config.visibility:
            properties.append(self._get_visibility())
        if self._config.document_subtype:
            properties.append(self._get_document_subtype())
        if document.attributes.to_register:
            properties.append(self._get_inbound_or_outbound(application))
            properties.append(self._get_the_cookie())
            properties.append(self._get_sender(application))
            properties.append(self._get_receiver(application))

        return properties

    def _get_source_category(self, document):
        if document.attributes.to_register:
            return self._config.main_source_category
        return self._config.attachment_source_category or self._config.main_source_category

    def _get_filename_prefix(self, application, document):
        return f'{datetime.now().year}_{application.id} {self._get_source_category(document)}'

    def _get_filename(self, application, document):
        prefix = self._get_filename_prefix(application, document)
        if application.is_inbound:
            return (
                f'{prefix}_{application.applicant.surname}_{application.applicant.name}'
                f'_{application.applicant.fiscal_code}.{document.attributes.extension}'
            )
        return f'{prefix}_RISPOSTA.{document.attributes.extension}'

    def _get_oggetto(self, application, document):
        prefix = self._get_filename_prefix(application, document)
        if application.is_inbound:
            subject = (
                f'{prefix}_{application.applicant.surname}'
                f'_{application.applicant.name}_{application.applicant.fiscal_code}'
            )
        else:
            subject = f'{prefix}_RISPOSTA'

        return {
            'key': '1',
            'values': [
                subject,
            ],
        }

    def _get_to_protocol(self, to_protocol):
        return {
            'key': '83',
            'values': [
                'ja - si' if to_protocol else 'nein - no',
            ],
        }

    def _get_inbound_or_outbound(self, application):
        return {
            'key': '84',
            'values': [
                'eingang - entrata' if application.is_inbound else 'ausgang - uscita',
            ],
        }

    def _get_the_cookie(self):
        return {
            'key': '23',
            'values': [
                'TheCookie',
            ],
        }

    def _get_classification(self):
        return {
            'key': '4',
            'values': [
                self._config.classification,
            ],
        }

    def _get_folder(self):
        return {
            'key': '9',
            'values': [
                self._config.folder_name,
            ],
        }

    def _get_competent_service(self):
        return {
            'key': '31',
            'values': [
                self._config.competent_service,
            ],
        }

    def _get_visibility(self):
        return {
            'key': '78',
            'values': [
                self._config.visibility,
            ],
        }

    def _get_document_subtype(self):
        return {
            'key': '39',
            'values': [
                self._config.document_subtype,
            ],
        }

    def _get_sender(self, application):
        return {
            'key': '27',
            'values': [
                application.applicant.description if application.is_inbound else self._config.tenant.description,
            ],
        }

    def _get_receiver(self, application):
        return {
            'key': '28',
            'values': [
                self._config.tenant.description if application.is_inbound else application.applicant.description,
            ],
        }


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.status_code
        if errcode not in SUCCESS_CODE:
            try:
                errmsg = self._result.json()['reason']
            except (KeyError, AttributeError):
                errmsg = bytes.decode(self._result.content)
            raise RESTValidationError(errcode, errmsg)

    @property
    def document_id(self):
        return str(urlparse(self._result.headers['Location']).path.split('/')[-1])

    @property
    def content_location_uri(self):
        return str(self._result.headers['Location'])

    @property
    def number(self):
        data = self._result.json()
        if 'items' in data and 'sourceProperties' in data['items'][0]:
            source_properties = data['items'][0]['sourceProperties']
            for item in source_properties:
                if 'key' in item and 'value' in item and item['key'] == '23':
                    return item['value']

    @property
    def year(self):
        data = self._result.json()
        if 'items' in data and 'sourceProperties' in data['items'][0]:
            source_properties = data['items'][0]['sourceProperties']
            for item in source_properties:
                if 'key' in item and 'value' in item and item['key'] == '24':
                    return datetime.strptime(item['value'], '%Y-%m-%d').strftime('%Y')
