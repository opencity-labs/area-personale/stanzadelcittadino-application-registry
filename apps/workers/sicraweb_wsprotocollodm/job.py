import logging
import uuid
from base64 import b64encode
from dataclasses import dataclass
from os import path

from django.conf import settings

from apps.applications.models import Registration, RegistrationEvent, SicrawebWSProtocolloDMConfig
from apps.applications.sdc.entities import Application, DocumentStatus
from apps.applications.tracker import RegistrationEventTracker
from apps.workers.worker import Producer

DEFAULT_EXTENSION = 'dat'

logger = logging.getLogger('application_registry')


def run(event):
    application = Application(data=event)
    configs = SicrawebWSProtocolloDMConfig.objects.active_for_service(application.service_id, application.status_name)
    for config in configs:
        document_id = RegistrationEvent.objects.last_failed_registration_event(application) or uuid.uuid4()
        with RegistrationEventTracker(
            document_id=document_id,
            application=application,
            config=config,
            produce_document=settings.PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT,
        ):
            Job(document_id, application, config).run()


class Job:
    def __init__(self, document_id, application, config):
        self._document_id = document_id
        self._application = application
        self._config = config
        self._tenant = self._config.tenant

    def run(self):
        is_registration_date_allowed = self._tenant.is_registration_date_allowed(self._application.registration_date)
        registration_exists = Registration.objects.exists_for_application(self._application)
        failed_registration_event_exists = RegistrationEvent.objects.exists_for_application(self._application)
        if not self._application.is_registration_required:
            raise Exception('Main document not found.')
        if (
            is_registration_date_allowed
            and not registration_exists
            and not failed_registration_event_exists
            and settings.PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT
        ):
            self._create_document(number=None, date=None, folder=None, status=DocumentStatus.REGISTRATION_PENDING)
        if is_registration_date_allowed and not registration_exists:
            self._set_document_attributes()
            result = self._register()
            if not settings.PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT:
                self._update_sdc(result.number, result.created_at, self._config.folder_number)
            else:
                self._create_document(
                    result.number,
                    result.created_at,
                    self._config.folder_number,
                    DocumentStatus.REGISTRATION_COMPLETE,
                )
        elif data := Registration.objects.application_registration_data(self._application):
            # Find registrations completed but not updated yet
            if not settings.PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT:
                self._update_sdc(**data)
            else:
                self._create_document(
                    **data,
                    status=DocumentStatus.REGISTRATION_COMPLETE,
                    registration_exists=registration_exists,
                )
        else:
            # Registration date not allowed or no registration data
            self._skip()

    def _set_document_attributes(self):
        logger.debug('Setting attributes')
        for document in self._application.current_documents:
            data = self._tenant.get_document(document.url)
            encoded = b64encode(data).decode()
            ext = path.splitext(document.original_name)[1].removeprefix('.') or DEFAULT_EXTENSION
            document.attributes = Attributes(content=encoded, extension=ext)
            logger.debug('%s%s, %s', encoded[:30], '...' if len(encoded) > 30 else '', ext)

    def _register(self):
        logger.debug('protocollazione')
        result = self._config.register(self._document_id, self._application)
        logger.info(
            'Registered application %s with number %s and year %s',
            self._application.id,
            result.number,
            result.year,
        )
        logger.debug('documento protocollato')
        logger.debug('%s', result.document_id)
        return result

    def _update_sdc(self, number, date, folder=None):
        if data := self._application.build_registration_data(number, date, folder):
            logger.debug('Updating SDC')
            self._tenant.update_application(self._application.id, data)
        else:
            logger.debug('SDC update skipped')

    def _create_document(self, number, date, folder=None, status=None, registration_exists=False):
        if not registration_exists:
            logger.debug('Producing Document')
            document = self._tenant.create_document(self._document_id, self._application, number, date, folder, status)
            with Producer() as producer:
                producer.send(
                    value=document.json().encode('utf-8'),
                    key=str(document.remote_collection.id).encode('utf-8'),
                )
        else:
            logger.debug('Document production skipped')

    def _skip(self):
        logger.debug('Skipped: application does not require registration.')


@dataclass
class Attributes:
    content: str
    extension: str
    remote_id: str = ''
