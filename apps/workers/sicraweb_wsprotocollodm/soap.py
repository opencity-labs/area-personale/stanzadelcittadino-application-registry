import logging
from datetime import datetime, timezone

from dicttoxml import dicttoxml
from django.utils.functional import cached_property
from lxml import etree
from requests import Session
from zeep import Client as ZeepClient
from zeep import exceptions as zeep_exceptions
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TIMEOUT = 120

logger = logging.getLogger('application_registry')
INBOUND = 'A'
OUTBOUND = 'P'


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()
    session = Session()
    session.verify = False

    def __init__(self, config):
        self._config = config

    @cached_property
    def _soap_client(self):
        wsdl = f'{self._config.url}?wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT, session=self.session),
        )

    def insert(self, application):
        try:
            return self._insert(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application):
        protocollo_in_str = {
            'ProtoIn': {
                'Data': datetime.now().strftime('%d/%m/%Y'),
                'DataProt': None,
                'NumProt': None,
                'Classifica': self._config.classification_hierarchy,
                'TipoDocumento': self._config.document_type,
                'Oggetto': application.document_description,
                'OggettoBilingue': None,
                'Origine': INBOUND if application.is_inbound else OUTBOUND,
                'MezzoInvio': self._config.sending_mode,
                'MittenteInterno': self._config.internal_sender,
                'MittentiDestinatari': {
                    'MittenteDestinatario': {
                        'CodiceFiscale': application.applicant.fiscal_code,
                        'CognomeNome': application.applicant.surname,
                        'Nome': application.applicant.name,
                        'Indirizzo': application.applicant.address,
                        'Localita': application.applicant.municipality,
                        'CodiceComuneResidenza': None,
                        'DataNascita': None,
                        'CodiceComuneNascita': None,
                        'Nazionalita': None,
                        'DataInvio_DataProt': datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                        'Spese_NProt': None,
                        'Mezzo': self._config.sending_mode,
                        'DataRicevimento': datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                        'TipoSogg': 'S',
                        'TipoPersona': 'FI',
                        'Recapiti': {
                            'Recapito': {
                                'TipoRecapito': 'EMAIL',
                                'ValoreRecapito': application.applicant.email,
                            },
                        },
                    },
                },
                'AggiornaAnagrafiche': self._config.update_records,
                'InCaricoA': self._config.institution_ou,
                'AnnoPratica': self._config.folder_year,
                'NumeroPratica': self._config.folder_number,
                'DataDocumento': None,
                'NumeroDocumento': None,
                'NumeroAllegati': None,
                'DataEvid': None,
                'OggettoStandard': self._config.standard_subject,
                'Utente': self._config.connection_user,
                'Ruolo': self._config.connection_user_role,
                'CodiceAmministrazione': self._config.institution_code,
                'Allegati': [
                    {
                        'TipoFile': application.current_document.attributes.extension,
                        'ContentType': 'application/pdf',
                        'Image': application.current_document.attributes.content,
                        'Commento': None,
                        'IdAllegatoPrincipale': None,
                        'Schema': None,
                        'NomeAllegato': application.current_document.original_name,
                        'TipoAllegato': None,
                        'URI': None,
                        'Hash': None,
                    },
                    *[
                        {
                            'TipoFile': attachment.attributes.extension,
                            'ContentType': None,
                            'Image': attachment.attributes.content,
                            'Commento': None,
                            'IdAllegatoPrincipale': None,
                            'Schema': None,
                            'NomeAllegato': attachment.original_name,
                            'TipoAllegato': None,
                            'URI': None,
                            'Hash': None,
                        }
                        for attachment in application.current_attachments or []
                    ],
                ],
            },
        }

        protocollo_in_str = dicttoxml(
            protocollo_in_str,
            root=False,
            custom_root='ProtoIn',
            attr_type=False,
            item_func=lambda x: 'Allegato',
        ).decode()

        logger.debug(protocollo_in_str)

        try:
            result = self._soap_client.service.InserisciProtocolloEAnagraficheString(
                ProtocolloInStr=protocollo_in_str,
                CodiceAmministrazione=self._config.connection_string,
                CodiceAOO=self._config.institution_aoo_code,
            )

            result = InsertResult(self._prepare_result(result))
            application.current_document.attributes.remote_id = result.document_id

            return result
        except zeep_exceptions.Fault as zef:
            raise ValidationError(zef)

    def _prepare_result(self, result):
        ProtocolloOut = self._soap_client.get_type('ns0:ProtocolloOut')  # noqa
        ArrayOfAllegatoInseritoOut = self._soap_client.get_type('ns0:ArrayOfAllegatoInseritoOut')  # noqa
        AllegatoInseritoOut = self._soap_client.get_type('ns0:AllegatoInseritoOut')  # noqa

        result = etree.fromstring(result)
        return ProtocolloOut(
            IdDocumento=result.findtext('.//IdDocumento'),
            AnnoProtocollo=result.findtext('.//AnnoProtocollo'),
            NumeroProtocollo=result.findtext('.//NumeroProtocollo'),
            DataProtocollo=result.findtext('.//DataProtocollo'),
            Messaggio=result.findtext('.//Messaggio'),
            Registri=None,
            Allegati=ArrayOfAllegatoInseritoOut(
                [
                    AllegatoInseritoOut(
                        Serial=allegato.findtext('./Serial'),
                        IDBase=allegato.findtext('./IDBase'),
                        Versione=allegato.findtext('./Versione'),
                    )
                    for allegato in result.find('.//Allegati') or []
                ],
            ),
            Errore=result.findtext('.//Errore'),
        )


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.Errore
        if errcode:
            errmsg = self._result.Messaggio
            raise ValidationError(errcode, errmsg)

    @property
    def number(self):
        return self._result.NumeroProtocollo

    @property
    def year(self):
        return self._result.AnnoProtocollo

    @property
    def document_id(self):
        return self._result.IdDocumento
