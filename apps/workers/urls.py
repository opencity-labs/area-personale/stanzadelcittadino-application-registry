from django.urls import path

from apps.workers.views import (
    DatagraphSubmissionAPIView,
    DedagroupSubmissionAPIView,
    HalleySubmissionAPIView,
    MaggioliSubmissionAPIView,
)

urlpatterns = [
    path('datagraph/submission/', DatagraphSubmissionAPIView.as_view(), name='datagraph-submission'),
    path('dedagroup/submission/', DedagroupSubmissionAPIView.as_view(), name='dedagroup-submission'),
    path('halley/submission/', HalleySubmissionAPIView.as_view(), name='halley-submission'),
    path('maggioli/submission/', MaggioliSubmissionAPIView.as_view(), name='maggioli-submission'),
]
