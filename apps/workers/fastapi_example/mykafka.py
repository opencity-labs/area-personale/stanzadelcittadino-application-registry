# flake8: noqa
import asyncio
import json
import logging
from typing import Set

from aiokafka import AIOKafkaConsumer, TopicPartition
from asgiref.sync import sync_to_async

logger = logging.getLogger('application_registry')


class Kafka:
    def __init__(self, topic, bootstrap_servers, group_id):
        self.topic = topic
        self.bootstrap_servers = bootstrap_servers
        self.group_id = group_id

        self.consumer = None
        self.consumer_task = None

    async def initialize(self):

        logger.debug(
            f'Initializing KafkaConsumer for topic {self.topic}, group_id {self.group_id} and using bootstrap servers {self.bootstrap_servers}',
        )
        self.consumer = AIOKafkaConsumer(
            self.topic,
            bootstrap_servers=self.bootstrap_servers,
            group_id=self.group_id,
            enable_auto_commit=True,
        )
        # get cluster layout and join group
        await self.consumer.start()

        partitions: Set[TopicPartition] = self.consumer.assignment()
        nr_partitions = len(partitions)
        if nr_partitions != 1:
            logger.warning(
                f'Found {nr_partitions} partitions for topic {self.topic}. Expecting only one, remaining partitions will be ignored!',
            )
        for tp in partitions:
            # get the log_end_offset
            end_offset_dict = await self.consumer.end_offsets([tp])
            end_offset = end_offset_dict[tp]

            if end_offset == 0:
                logger.warning(
                    f'Topic ({self.topic}) has no messages (log_end_offset: {end_offset}), skipping initialization ...',
                )
                return

            logger.debug(f'Found log_end_offset: {end_offset} seeking to {end_offset - 1}')
            # consumer.seek(tp, end_offset-1)
            await self.consumer.seek_to_beginning(tp)

    async def consume(self, check_func, process_func):
        self.consumer_task = asyncio.create_task(self.send_consumer_message(check_func, process_func))

    async def send_consumer_message(self, check_func, process_func):
        try:
            # consume messages
            async for msg in self.consumer:
                event = json.loads(msg.value)
                try:
                    if check_func(event):
                        logger.info(f"Event {event['id']} from application: Processing event")
                        # sync_to_async is required to execute synchronous Django code (i.e. ORM)
                        # within an asynchronous context.
                        # See https://docs.djangoproject.com/en/3.2/topics/async/#async-adapter-functions
                        await sync_to_async(process_func)(event)
                    else:
                        logger.info(f"Event {event['id']} from application: Processing not required")
                except Exception as ex:
                    logger.error(f"Event {event['id']} from application: Processing failed {ex}")
        except Exception as ex:
            logger.error(f'An error occurred while consuming messages: {str(ex)}')
        finally:
            # will leave consumer group; perform autocommit if enabled
            logger.warning('Stopping consumer')
            await self.consumer.stop()
            exit(1)
