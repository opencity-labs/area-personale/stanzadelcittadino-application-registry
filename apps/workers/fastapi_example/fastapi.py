# flake8: noqa

# Run with: DJANGO_LOG_LEVEL=DEBUG DJANGO_SETTINGS_MODULE=application_registry.settings_local uvicorn apps.workers.fastapi_example.fastapi:app --reload
import logging

import django

django.setup()

import uvicorn
from fastapi import FastAPI

from apps.workers.fastapi_example.mykafka import Kafka
from apps.workers.halley.job import run

logger = logging.getLogger('application_registry')

app = FastAPI()


kafka = Kafka(
    topic='applications',
    bootstrap_servers='localhost:29092',
    group_id='consumer1',
)


@app.on_event('startup')
async def startup_event():
    logger.info('Initializing API ...')

    await kafka.initialize()
    await kafka.consume(lambda _: True, run)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host='0.0.0.0',
        port=8000,
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )
