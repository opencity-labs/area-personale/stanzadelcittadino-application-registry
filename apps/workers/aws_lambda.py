import itertools
import json
import logging
from base64 import b64decode

logger = logging.getLogger('application_registry')


class Handler:
    def __init__(self, event, callback):
        self._event = event
        self._callback = callback

    def handle(self):
        events = self._decode()
        self._process(events)

    def _decode(self):
        if self._is_msk_event:
            return self._decode_msk_records()
        return [self._event]

    @property
    def _is_msk_event(self):
        try:
            return self._event['eventSource'] == 'aws:kafka'
        except Exception:
            return False

    def _decode_msk_records(self):
        events = []
        for record in self._records:
            try:
                events.append(json.loads(record.decode('utf8')))
            except Exception:
                self._fail(record=record)
        return events

    @property
    def _records(self):
        # Any exception raised here means that AWS has changed the
        # MSK event format.
        records = itertools.chain(*self._event['records'].values())
        # We noticed an event with no `value` key, thus the `if` part
        # of the listcomp.
        return [b64decode(record['value']) for record in records if 'value' in record]

    @staticmethod
    def _fail(**kwargs):
        logger.exception('An error occurred.', extra=kwargs)

    def _process(self, events):
        for event in events:
            try:
                self._callback(event)
            except Exception:
                self._fail(event=event)
