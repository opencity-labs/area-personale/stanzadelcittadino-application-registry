import logging
from dataclasses import dataclass
from datetime import datetime, timezone
from types import SimpleNamespace

import requests
from django.utils.functional import cached_property
from lxml import etree
from requests import Session
from requests.exceptions import RequestException
from rest_framework.status import is_client_error, is_server_error
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers import dime
from apps.workers.errors import SOAPError, TransportError, ValidationError

TIMEOUT = 840
SUCCESS_CODE = '0'
EMPTY_ELEMENT = etree.Element('_')
UTF8 = 'UTF-8'
XMLNS_PREFIX_MAP = {
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'ns': 'http://tempuri.org/',
}
INSERIMENTO_RESULT_XPATH = './soap:Body/ns:InserimentoResponse/ns:InserimentoResult'
PROTOCOLLAZIONE_RESULT_XPATH = './soap:Body/ns:ProtocollazioneResponse/ns:ProtocollazioneResult'
TYPE_NAMES = [
    'AOO',
    'Allegati',
    'Amministrazione',
    'ApplicativoProtocollo',
    'Classifica',
    'Descrizione',
    'Destinatario',
    'Documento',
    'Fascicolo',
    'Identificatore',
    'IndirizzoPostaleType',
    'IndirizzoTelematico',
    'Intestazione',
    'Mittente',
    'Parametro',
    'Persona',
    'Segnatura',
    'UnitaOrganizzativa',
]
ZERO = '0'
INBOUND_REGISTRATION = 'E'
OUTBOUND_REGISTRATION = 'U'
SMTP = 'smtp'
REGISTRY_APP = 'DGPROTONET'
PARAM_FORMATODOC = 'FORMATODOC'
PARAM_DATADOC = 'DATADOC'
PARAM_DATAARRIVO = 'DATAARRIVO'

logger = logging.getLogger('application_registry')


@dataclass
class Credentials:
    codente: str
    username: str
    password: str


class Client:
    soap_client_class = ZeepClient
    http_client = requests
    cache = InMemoryCache()
    session = Session()
    session.verify = False

    def __init__(self, *, credentials, url):
        self._auth = credentials
        self._url = url
        self._wsdl = f'{self._url}?wsdl'

    @cached_property
    def _soap_client(self):
        return self.soap_client_class(
            wsdl=self._wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT, session=self.session),
        )

    @cached_property
    def _token(self):
        token = self._soap_client.service.Login(
            self._auth.codente,
            self._auth.username,
            self._auth.password,
        ).strDST
        if not token:
            raise ValidationError
        return token

    def inserimento(self, mediatype, document):
        try:
            return self._inserimento(mediatype, document)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _inserimento(self, mediatype, document):
        envelope = self._get_envelope('Inserimento')
        message = dime.Message(
            dime.Envelope(data=envelope),
            dime.Record(type=mediatype, data=document),
        ).as_bytes()
        headers = {
            'Content-Type': 'application/dime',
            'SOAPAction': 'http://tempuri.org/Inserimento',
        }
        xml = self._post(message, headers)
        return InserimentoResult(xml)

    def protocollazione(self, document):
        try:
            return self._protocollazione(document)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _protocollazione(self, document):
        envelope = self._get_envelope('Protocollazione')
        message = dime.Message(
            dime.Envelope(data=envelope),
            dime.Record(type='text/xml', data=document),
        ).as_bytes()
        headers = {
            'Content-Type': 'application/dime',
            'SOAPAction': 'http://tempuri.org/Protocollazione',
        }
        xml = self._post(message, headers)
        return ProtocollazioneResult(xml)

    def _get_envelope(self, method):
        element = self._soap_client.create_message(
            self._soap_client.service,
            method,
            strUserName=self._auth.username,
            strDST=self._token,
        )
        return etree.tostring(element, encoding=UTF8, xml_declaration=True)

    def _post(self, data, headers):
        response = self.http_client.post(
            self._url,
            data=data,
            headers=headers,
            timeout=TIMEOUT,
            verify=False,
        )
        self._log_error(response)
        response.raise_for_status()
        return response.content

    @staticmethod
    def _log_error(response):
        if is_client_error(response.status_code) or is_server_error(response.status_code):
            logger.debug('response: %s', response.content.decode(errors='replace'))

    def get_types(self):
        try:
            types = {name: self._soap_client.get_type(name) for name in TYPE_NAMES}
        except Exception as e:
            raise SOAPError(e) from e
        return SimpleNamespace(**types)


class BaseResult:
    errcode_xpath = '.'
    errmsg_xpath = '.'
    xpath_map = {}

    def __init__(self, xml):
        try:
            self._root = etree.fromstring(xml)
        except Exception:
            self._root = EMPTY_ELEMENT
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()
        self._validate_elements()

    def _validate_error_code(self):
        errcode = self._findtext(self.errcode_xpath)
        if errcode != SUCCESS_CODE:
            errmsg = self._findtext(self.errmsg_xpath)
            raise ValidationError(errcode, errmsg)

    def _validate_elements(self):
        for xpath in self.xpath_map.values():
            text = self._findtext(xpath)
            if not text:
                raise ValidationError(xpath, text)

    def _findtext(self, xpath):
        return self._root.findtext(xpath, namespaces=XMLNS_PREFIX_MAP)


class InserimentoResult(BaseResult):
    xpath_prefix = INSERIMENTO_RESULT_XPATH
    errcode_xpath = f'{xpath_prefix}/ns:lngErrNumber'
    errmsg_xpath = f'{xpath_prefix}/ns:strErrString'
    xpath_map = {
        'doc_id': f'{xpath_prefix}/ns:lngDocID',
    }

    @property
    def doc_id(self):
        return self._findtext(self.xpath_map['doc_id'])


class ProtocollazioneResult(BaseResult):
    xpath_prefix = PROTOCOLLAZIONE_RESULT_XPATH
    errcode_xpath = f'{xpath_prefix}/ns:lngErrNumber'
    errmsg_xpath = f'{xpath_prefix}/ns:strErrString'
    xpath_map = {
        'number': f'{xpath_prefix}/ns:lngNumPG',
        'year': f'{xpath_prefix}/ns:lngAnnoPG',
        'date': f'{xpath_prefix}/ns:strDataPG',
    }

    @property
    def number(self):
        return self._findtext(self.xpath_map['number'])

    @property
    def year(self):
        return self._findtext(self.xpath_map['year'])

    @property
    def date(self):
        return self._findtext(self.xpath_map['date'])


class BaseTemplate:
    def __init__(self, config):
        self._config = config

    def render(self, application):
        root = etree.Element('Segnatura')
        return etree.tostring(root)


class SeriateInboundTemplate(BaseTemplate):
    def render(self, application):
        t = self._config.get_types()

        segnatura = t.Segnatura(
            Intestazione=t.Intestazione(
                Oggetto=application.document_description,
                Identificatore=t.Identificatore(
                    CodiceAmministrazione=self._config.sdc_institution_code,
                    CodiceAOO=self._config.sdc_aoo_code,
                    NumeroRegistrazione=self._config.registration_number,
                    DataRegistrazione=datetime.now(timezone.utc).astimezone().date().isoformat(),
                    Flusso=INBOUND_REGISTRATION,
                ),
                Mittente=t.Mittente(
                    Persona=t.Persona(
                        id=application.applicant.fiscal_code,
                        Nome=application.applicant.name,
                        Cognome=application.applicant.surname,
                        IndirizzoPostaleItem=t.IndirizzoPostaleType(
                            Denominazione=application.applicant.address,
                            Civico=application.applicant.house_number_digits,
                            Lettera=application.applicant.house_number_letters,
                            CAP=application.applicant.postal_code,
                            Comune=application.applicant.municipality,
                            Provincia=application.applicant.county,
                        ),
                        IndirizzoTelematico=t.IndirizzoTelematico(
                            application.applicant.email,
                            tipo=SMTP,
                        ),
                    ),
                ),
                Destinatario=t.Destinatario(
                    Amministrazione=t.Amministrazione(
                        Denominazione=self._config.institution_name,
                        CodiceAmministrazione=self._config.institution_code,
                        IndirizzoTelematico=t.IndirizzoTelematico(
                            self._config.institution_email,
                            tipo=SMTP,
                        ),
                        UnitaOrganizzativa=t.UnitaOrganizzativa(id=self._config.institution_ou),
                    ),
                    AOO=t.AOO(CodiceAOO=self._config.institution_aoo_code),
                ),
                Classifica=t.Classifica(
                    CodiceAmministrazione=self._config.classification_institution_code,
                    CodiceAOO=self._config.classification_aoo_code,
                    CodiceTitolario=self._config.classification_hierarchy,
                ),
                Fascicolo=t.Fascicolo(
                    anno=self._config.folder_year,
                    numero=self._config.folder_number,
                ),
            ),
            Descrizione=t.Descrizione(
                t.Documento(
                    nome=application.current_document.original_name,
                    id=application.current_document.attributes.remote_id,
                ),
                t.Allegati(
                    Documento=[
                        t.Documento(nome=attachment.original_name, id=attachment.attributes.remote_id)
                        for attachment in application.current_attachments
                    ],
                ),
            ),
            ApplicativoProtocollo=t.ApplicativoProtocollo(
                nome=REGISTRY_APP,
                Parametro=[
                    t.Parametro(
                        nome=PARAM_FORMATODOC,
                        valore=self._config.document_format,
                    ),
                    t.Parametro(
                        nome=PARAM_DATADOC,
                        valore=application.registration_date.date().isoformat(),
                    ),
                    t.Parametro(
                        nome=PARAM_DATAARRIVO,
                        valore=application.registration_date.strftime('%Y-%m-%d %H:%M:%S'),
                    ),
                ],
            ),
        )
        root = etree.Element('Segnatura')
        t.Segnatura.render(root, segnatura)
        rendered = etree.tostring(root, encoding=UTF8, xml_declaration=True)
        logger.debug('direction: inbound')
        logger.debug(rendered)
        return rendered


class SeriateOutboundTemplate(BaseTemplate):
    def render(self, application):
        t = self._config.get_types()

        segnatura = t.Segnatura(
            Intestazione=t.Intestazione(
                Oggetto=application.document_description,
                Identificatore=t.Identificatore(
                    CodiceAmministrazione=self._config.sdc_institution_code,
                    CodiceAOO=self._config.sdc_aoo_code,
                    NumeroRegistrazione=ZERO,
                    DataRegistrazione=ZERO,
                    Flusso=OUTBOUND_REGISTRATION,
                ),
                Mittente=t.Mittente(
                    Amministrazione=t.Amministrazione(
                        Denominazione=self._config.institution_name,
                        CodiceAmministrazione=self._config.institution_code,
                        IndirizzoTelematico=t.IndirizzoTelematico(
                            self._config.institution_email,
                            tipo=SMTP,
                        ),
                        UnitaOrganizzativa=t.UnitaOrganizzativa(id=self._config.institution_ou),
                    ),
                    AOO=t.AOO(CodiceAOO=self._config.institution_aoo_code),
                ),
                Destinatario=t.Destinatario(
                    Persona=t.Persona(
                        id=application.applicant.fiscal_code,
                        Nome=application.applicant.name,
                        Cognome=application.applicant.surname,
                        IndirizzoPostaleItem=t.IndirizzoPostaleType(
                            Denominazione=application.applicant.address,
                            Civico=application.applicant.house_number_digits,
                            Lettera=application.applicant.house_number_letters,
                            CAP=application.applicant.postal_code,
                            Comune=application.applicant.municipality,
                            Provincia=application.applicant.county,
                        ),
                        IndirizzoTelematico=t.IndirizzoTelematico(
                            application.applicant.email,
                            tipo=SMTP,
                        ),
                    ),
                ),
                Classifica=t.Classifica(
                    CodiceAmministrazione=self._config.classification_institution_code,
                    CodiceAOO=self._config.classification_aoo_code,
                    CodiceTitolario=self._config.classification_hierarchy,
                ),
                Fascicolo=t.Fascicolo(
                    anno=self._config.folder_year,
                    numero=self._config.folder_number,
                ),
            ),
            Descrizione=t.Descrizione(
                t.Documento(
                    nome=application.current_document.original_name,
                    id=application.current_document.attributes.remote_id,
                ),
                t.Allegati(
                    Documento=[
                        t.Documento(nome=attachment.original_name, id=attachment.attributes.remote_id)
                        for attachment in application.current_attachments
                    ],
                ),
            ),
            ApplicativoProtocollo=t.ApplicativoProtocollo(
                nome=REGISTRY_APP,
                Parametro=[
                    t.Parametro(
                        nome=PARAM_FORMATODOC,
                        valore=self._config.document_format,
                    ),
                    t.Parametro(
                        nome=PARAM_DATADOC,
                        valore=application.registration_date.date().isoformat(),
                    ),
                    t.Parametro(
                        nome=PARAM_DATAARRIVO,
                        valore=application.registration_date.strftime('%Y-%m-%d %H:%M:%S'),
                    ),
                ],
            ),
        )

        root = etree.Element('Segnatura')
        t.Segnatura.render(root, segnatura)
        rendered = etree.tostring(root, encoding=UTF8, xml_declaration=True)
        logger.debug('direction: outbound')
        logger.debug(rendered)
        return rendered
