import logging
from datetime import datetime, timezone

import requests
import sentry_sdk
from django.utils.functional import cached_property
from requests.exceptions import RequestException

from apps.workers.errors import RESTError, RESTValidationError

logger = logging.getLogger('application_registry')

TIMEOUT = 30
SUCCESS_CODE = 1


class Client:
    http_client = requests

    def __init__(self, config):
        self._config = config

    @cached_property
    def _token(self):
        try:
            response = self.http_client.post(
                self._config.token_url,
                data={
                    'client_id': self._config.client_id,
                    'client_secret': self._config.client_secret,
                    'grant_type': self._config.grant_type,
                    'resource': self._config.resource,
                },
            )
            response.raise_for_status()
            return response.json()['access_token']
        except (RequestException, KeyError) as e:
            raise RESTValidationError(e) from e

    def protocollazione(self, application):
        try:
            return self._protocollazione(application)
        except RESTValidationError:
            raise
        except Exception as e:
            raise RESTError(e) from e

    def _protocollazione(self, application):
        headers = {'Authorization': f'Bearer {self._token}'}
        body = {
            'idRegistro': self._config.registry_id,
            'idAutore': self._config.author_id,
            'codiceLivelloOrganigramma': self._config.organization_chart_level_code,
            'pratica': {
                'oggetto': application.document_description,
                'tipoProtocollo': 'INGRESSO' if application.is_inbound else 'USCITA',
                'dataConsegnaDocumento': application.registration_date.astimezone().replace(tzinfo=None).isoformat(),
                'idCorrispondentiList': [
                    {
                        'nome': application.applicant.name,
                        'cognome': application.applicant.surname,
                        'email': application.applicant.email,
                        'tipoIndividuoProtocollo': 'NONCERTIFICATO',
                    },
                ],
                'allegatiList': [
                    {
                        'nomeFile': application.current_document.original_name,
                        'mimeType': application.current_document.mimetype,
                        'isPrincipale': True,
                        'file': application.current_document.attributes.content,
                    },
                    *[
                        {
                            'nomeFile': attachment.original_name,
                            'mimeType': attachment.mimetype,
                            'isPrincipale': False,
                            'file': attachment.attributes.content,
                        }
                        for attachment in application.current_attachments
                    ],
                ],
                'codiceLivelloOrganigramma': self._config.organization_chart_level_code,
                'idCodiceAOO': self._config.institution_aoo_id,
                'idOperatore': self._config.author_id,
                'classificazione': (
                    {
                        'codiceTitolo': self._config.classification_title,
                        'codiceClasse': self._config.full_classification_class,
                        'codiceSottoClasse': self._config.full_classification_subclass,
                    }
                    if self._config.classification_title
                    else None
                ),
            },
        }
        response = self.http_client.post(
            f'{self._config.base_url}/Protocollo/Protocollo/Protocollazione',
            json=body,
            headers=headers,
            timeout=TIMEOUT,
        )
        response.raise_for_status()
        return ProtocollazioneResult(response.json())

    def fascicola_documento(self, remote_application_id, remote_document_ids):
        for id_ in remote_document_ids:
            logger.debug('%d', id_)
            try:
                self._fascicola_documento(remote_application_id, id_)
            except Exception:
                logger.debug('Error in FascicolaDocumento API call')
                sentry_sdk.capture_exception()
                continue

    def _fascicola_documento(self, remote_application_id, remote_document_id):
        headers = {'Authorization': f'Bearer {self._token}'}
        body = {
            'codiceFascicolo': self._config.folder_code,
            'idOperatore': self._config.author_id,
            'codiceLivelloOrganigrammaOperatore': self._config.organization_chart_level_code,
            'allegato': {
                'idAllegato': remote_document_id,
                'idPratica': remote_application_id,
                'tipoPermesso': 'Pubblico',
            },
        }
        response = self.http_client.post(
            f'{self._config.base_url}/Protocollo/Protocollo/FascicolaDocumento',
            json=body,
            headers=headers,
            timeout=TIMEOUT,
        )
        response.raise_for_status()

        resp = response.json()
        errcode = resp['resultType']
        if errcode != SUCCESS_CODE:
            errmsg = resp['resultDescription']
            raise RESTValidationError(errcode, errmsg)


class ProtocollazioneResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result['resultType']
        if errcode != SUCCESS_CODE:
            errmsg = self._result['resultDescription']
            raise RESTValidationError(errcode, errmsg)

    @property
    def number(self):
        return str(self._result['result']['numeroProtocollo'])

    @property
    def date(self):
        return self._result['result']['dataRegistrazione']

    @property
    def remote_document_ids(self):
        return [doc['id'] for doc in self._result['result']['allegati']]

    @property
    def remote_application_id(self):
        return self._result['result']['idPratica']
