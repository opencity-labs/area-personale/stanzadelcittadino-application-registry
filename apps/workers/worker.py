import json
import logging
import time

from django.conf import settings
from kafka import KafkaConsumer, KafkaProducer
from kafka.consumer.fetcher import NoOffsetForPartitionError
from kafka.structs import OffsetAndMetadata, TopicPartition

logger = logging.getLogger('application_registry')


class Message:
    def __init__(self, client, record):
        self._client = client
        self._record = record

    def __str__(self):
        pad = ' ' * 4
        return (
            f'{pad}topic: {self._record.topic}\n'
            f'{pad}partition: {self._record.partition}\n'
            f'{pad}offset: {self._record.offset}\n'
            f"{pad}value: {self._record.value.decode('utf8')}"
        )

    @property
    def value(self):
        return json.loads(self._record.value.decode('utf8'))

    def commit(self):
        tp = TopicPartition(topic=self._record.topic, partition=self._record.partition)
        om = OffsetAndMetadata(offset=self._record.offset + 1, metadata='')
        try:
            self._client.commit(offsets={tp: om})
        except Exception:
            logger.exception('An error occurred.')


class Consumer:
    client_class = KafkaConsumer
    message_class = Message
    consumer_timeout_ms = 5000

    def __init__(self, *, client_id):
        self._client_id = client_id

    def __enter__(self):
        self._client = self.client_class(
            settings.KAFKA_TOPIC_NAME,
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            api_version=tuple(int(el) for el in settings.KAFKA_API_VERSION.split('.')),
            client_id=self._client_id,
            group_id=self._client_id,
            auto_offset_reset='latest',
            enable_auto_commit=False,
            consumer_timeout_ms=self.consumer_timeout_ms,
            security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._client.close()
        except Exception:
            logger.exception('An error occurred.')

    @property
    def messages(self):
        try:
            for record in self._client:
                yield self.message_class(self._client, record)
        except NoOffsetForPartitionError as e:
            self._set_starting_offset(e.args[0])

    def _set_starting_offset(self, tp):
        offset = self._client.end_offsets([tp])[tp]
        self._client.commit(offsets={tp: OffsetAndMetadata(offset=offset, metadata='')})
        logger.info('Starting offset set to %d for topic %s, partition %d.', offset, tp.topic, tp.partition)


class Producer:
    def __init__(self):
        self.producer_class = KafkaProducer

    def __enter__(self):
        self._producer = self.producer_class(
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._producer.close()

    def send(self, value, key):
        self._producer.send(settings.KAFKA_DESTINATION_TOPIC_NAME, value, key)
        self._producer.flush(timeout=10)


class Worker:
    consumer_class = Consumer
    clock = time

    def __init__(self, *, callback, max_messages=None):
        self._client_id = settings.KAFKA_GROUP_ID
        self._callback = callback
        self._max_messages = max_messages or float('inf')
        self._running = True

    def __str__(self):
        return self._client_id

    @property
    def _is_running(self):
        return self._running and self._max_messages > 0

    def start(self):
        while self._is_running:
            try:
                logger.info(f'Starting {self._client_id} worker')
                self._run()
            except Exception as ex:
                logger.exception(f'An error occurred: {ex}')
                self._pause()

    def _run(self):
        with self.consumer_class(client_id=self._client_id) as consumer:
            while self._is_running:
                try:
                    message = next(consumer.messages)
                except StopIteration:
                    continue
                self._process(message)
                message.commit()
                self._max_messages -= 1

    def _pause(self):
        self.clock.sleep(1)

    def stop(self):
        self._running = False

    @staticmethod
    def _fail(**kwargs):
        logger.exception('An error occurred.', extra=kwargs)

    def _process(self, message):
        try:
            logger.debug(f'Got new message:\n{message}\n')

            message_data = message.value  # Questo solleva un'eccezione se il JSON non è valido

            logger.debug(f'Event {message_data["event_id"]} from application {message_data["id"]}: Processing...')
            self._callback(message_data)
            logger.debug(
                f'Event {message_data["event_id"]} from application {message_data["id"]}: Processing completed.',
            )
        except json.JSONDecodeError:
            logger.error(f'Invalid JSON message skipped: {message}')
            return
        except Exception:
            self._fail(event=message.value)
