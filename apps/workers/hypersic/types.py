from typing import List

import dicttoxml
import requests
from lxml import etree


class Protocollo:
    def __init__(
        self,
        tipologia,
        oggetto,
        classificazione,
        fascicolo,
        anno_fascicolo,
        numero_protocollo_precedente,
        anno_protocollo_precedente,
        annotazione,
    ):
        self.tipologia = tipologia
        self.oggetto = oggetto
        self.classificazione = classificazione
        self.fascicolo = fascicolo
        self.anno_fascicolo = anno_fascicolo
        self.numero_protocollo_precedente = numero_protocollo_precedente
        self.anno_protocollo_precedente = anno_protocollo_precedente
        self.annotazione = annotazione

    def to_xml(self):
        return dicttoxml.dicttoxml(self.to_dict(), custom_root='protocollo', attr_type=False, root=False).decode()

    def to_dict(self):
        return dict(protocollo=self.__dict__)  # noqa


class Mittente:
    def __init__(self, codice, descrizione, indirizzo, cap, descrizione_comune):
        self.codice = codice
        self.descrizione = descrizione
        self.indirizzo = indirizzo
        self.cap = cap
        self.descrizione_comune = descrizione_comune

    def to_xml(self):
        return dicttoxml.dicttoxml(self.to_dict(), custom_root='mittente', attr_type=False, root=False).decode()

    def to_dict(self):
        return dict(mittente=self.__dict__)  # noqa


class Destinatario(Mittente):
    def __init__(self, codice, descrizione, indirizzo, cap, descrizione_comune):
        super().__init__(codice, descrizione, indirizzo, cap, descrizione_comune)

    def to_xml(self):
        return dicttoxml.dicttoxml(self.to_dict(), custom_root='destinatario', attr_type=False, root=False).decode()

    def to_dict(self):
        return dict(destinatario=self.__dict__)  # noqa


class InsertProtocolloGenerale:
    INSERT_PROTOCOLLO_GENERALE_PAYLOAD = """
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                      xmlns:ser="http://services.apsystems.it/"
                      xmlns:xs="http://www.w3.org/2001/XMLSchema">
       <soapenv:Header>
          <ser:AuthenticationDetails>
             <ser:UserName>{username}</ser:UserName>
             <ser:Password>{password}</ser:Password>
          </ser:AuthenticationDetails>
       </soapenv:Header>
       <soapenv:Body>
          <ser:InsertProtocolloGenerale>
             <!--Optional:-->
             <ser:dsDati>
                <xs:schema id="protocolli" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
                   <xs:element name="protocolli" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
                      <xs:complexType>
                         <xs:choice minOccurs="0" maxOccurs="unbounded">
                            <xs:element name="protocollo">
                               <xs:complexType>
                                  <xs:sequence>
                                     <xs:element name="tipologia" type="xs:string" minOccurs="0"/>
                                     <xs:element name="oggetto" type="xs:string" minOccurs="0"/>
                                     <xs:element name="classificazione" type="xs:string" minOccurs="0"/>
                                     <xs:element name="fascicolo" type="xs:string" minOccurs="0"/>
                                     <xs:element name="anno_fascicolo" type="xs:string" minOccurs="0"/>
                                     <xs:element name="riservato" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_procedimento" type="xs:string" minOccurs="0"/>
                                     <xs:element name="numero_protocollo_precedente" type="xs:string" minOccurs="0"/>
                                     <xs:element name="anno_protocollo_precedente" type="xs:string" minOccurs="0"/>
                                     <xs:element name="numero_protocollo_riferimento" type="xs:string" minOccurs="0"/>
                                     <xs:element name="data_protocollo_riferimento" type="xs:string" minOccurs="0"/>
                                     <xs:element name="fogli_protocollo_riferimento" type="xs:string" minOccurs="0"/>
                                     <xs:element name="annotazione" type="xs:string" minOccurs="0"/>
                                  </xs:sequence>
                               </xs:complexType>
                            </xs:element>
                            <xs:element name="mittente">
                               <xs:complexType>
                                  <xs:sequence>
                                     <xs:element name="codice" type="xs:string" minOccurs="0"/>
                                     <xs:element name="descrizione" type="xs:string" minOccurs="0"/>
                                     <xs:element name="indirizzo" type="xs:string" minOccurs="0"/>
                                     <xs:element name="cap" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_comune" type="xs:string" minOccurs="0"/>
                                     <xs:element name="descrizione_comune" type="xs:string" minOccurs="0"/>
                                  </xs:sequence>
                               </xs:complexType>
                            </xs:element>
                            <xs:element name="destinatario">
                               <xs:complexType>
                                  <xs:sequence>
                                     <xs:element name="codice" type="xs:string" minOccurs="0"/>
                                     <xs:element name="descrizione" type="xs:string" minOccurs="0"/>
                                     <xs:element name="indirizzo" type="xs:string" minOccurs="0"/>
                                     <xs:element name="cap" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_comune" type="xs:string" minOccurs="0"/>
                                     <xs:element name="descrizione_comune" type="xs:string" minOccurs="0"/>
                                     <xs:element name="per_conoscenza" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_spedizione" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_ufficio" type="xs:string" minOccurs="0"/>
                                     <xs:element name="descrizione_ufficio" type="xs:string" minOccurs="0"/>
                                     <xs:element name="codice_processo" type="xs:string" minOccurs="0"/>
                                     <xs:element name="email" type="xs:string" minOccurs="0"/>
                                  </xs:sequence>
                               </xs:complexType>
                            </xs:element>
                         </xs:choice>
                      </xs:complexType>
                   </xs:element>
                </xs:schema>
                <!--You may enter ANY elements at this point-->
                <diffgr:diffgram xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"
                                 xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
                   <protocolli>
                      {protocollo}
                      {mittente}
                      {destinatari}
                   </protocolli>
                </diffgr:diffgram>
             </ser:dsDati>
          </ser:InsertProtocolloGenerale>
       </soapenv:Body>
    </soapenv:Envelope>
    """

    def __init__(
        self,
        username: str,
        password: str,
        protocollo: Protocollo,
        mittente: Mittente,
        destinatari: List[Destinatario],
    ):
        self.username = username
        self.password = password
        self.protocollo = protocollo
        self.mittente = mittente
        self.destinatari = destinatari

    def insert_protocollo_generale(self, url):
        response = requests.post(
            url=url,
            data=self.INSERT_PROTOCOLLO_GENERALE_PAYLOAD.format(
                username=self.username,
                password=self.password,
                protocollo=self.protocollo.to_xml(),
                mittente=self.mittente.to_xml(),
                destinatari='\n'.join([destinatario.to_xml() for destinatario in self.destinatari]),
            ),
            headers={'content-type': 'text/xml'},
        )
        response.raise_for_status()
        return InsertProtocolloGeneraleResponse(response=response.content).result


class InsertProtocolloGeneraleResponse:
    def __init__(self, response):
        self.result = self._to_dict(self._load_xml(response))

    @staticmethod
    def _load_xml(xml):
        parser = etree.XMLParser(remove_blank_text=True, remove_comments=True, resolve_entities=False)
        return etree.fromstring(xml.strip(), parser=parser)

    @staticmethod
    def _to_dict(data):
        if data.find('.//ERRORI') is not None:
            return dict(  # noqa
                _value_1=dict(  # noqa
                    _value_1=[
                        dict(  # noqa
                            ERRORE=dict(  # noqa
                                DESCRIZIONE=errore.findtext('./DESCRIZIONE'),
                            ),
                        )
                        for errore in data.find('.//ERRORI')
                    ],
                ),
            )
        return dict(  # noqa
            _value_1=dict(  # noqa
                _value_1=[
                    dict(  # noqa
                        protocollo=dict(  # noqa
                            codice=data.findtext('.//codice'),
                            numero_protocollo=data.findtext('.//numero_protocollo'),
                            data_protocollo=data.findtext('.//data_protocollo'),
                        ),
                    ),
                ],
            ),
        )
