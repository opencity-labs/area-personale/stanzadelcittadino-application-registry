import logging
from datetime import datetime, timezone

from django.utils.functional import cached_property
from zeep import Client as ZeepClient
from zeep import xsd
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError
from apps.workers.hypersic.types import Destinatario, InsertProtocolloGenerale, Mittente, Protocollo

TIMEOUT = 30
EMPTY = ''
INBOUND = 'A'
OUTBOUND = 'P'

logger = logging.getLogger('application_registry')


class CorrispondenteNotFoundError(Exception):
    pass


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _soap_client(self):
        return self.soap_client_class(
            wsdl=f'{self._config.registry_url}?wsdl',
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    def insert(self, application, previous_registration_number, previous_registration_year):
        try:
            return self._insert(application, previous_registration_number, previous_registration_year)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application, previous_registration_number, previous_registration_year):
        mittente = self._get_mittente(application)
        destinatari = self._get_destinatari(application)
        payload = InsertProtocolloGenerale(
            username=self._config.username,
            password=self._config.password,
            protocollo=Protocollo(
                tipologia=INBOUND if application.is_inbound else OUTBOUND,
                oggetto=application.document_description,
                classificazione=self._config.classification or EMPTY,
                fascicolo=self._config.folder_number or EMPTY,
                anno_fascicolo=self._config.folder_year or EMPTY,
                numero_protocollo_precedente=previous_registration_number or EMPTY,
                anno_protocollo_precedente=previous_registration_year or EMPTY,
                annotazione=self._config.annotation or EMPTY,
            ),
            mittente=Mittente(
                codice=mittente.correspondent_code if application.is_inbound else next(iter(self._config.office_ids)),
                descrizione=mittente.correspondent_description,
                indirizzo=application.applicant.address if application.is_inbound else EMPTY,
                cap=application.applicant.postal_code if application.is_inbound else EMPTY,
                descrizione_comune=application.applicant.municipality if application.is_inbound else EMPTY,
            ),
            destinatari=(
                destinatari
                if application.is_inbound
                else [
                    Destinatario(
                        codice=destinatari.correspondent_code,
                        descrizione=application.applicant.full_name,
                        indirizzo=application.applicant.address or EMPTY,
                        cap=application.applicant.postal_code or EMPTY,
                        descrizione_comune=application.applicant.municipality or EMPTY,
                    ),
                ]
            ),
        )
        result = payload.insert_protocollo_generale(self._config.registry_url)
        return InsertResult(result)

    def upload_file(self, document):
        try:
            return self._upload_file(document)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _upload_file(self, document):
        result = self._soap_client.service.InsertAllegatoProtocolloGenerale(
            _soapheaders=[self._get_authentication_details()],
            codice=document.attributes.registry_code,
            fileBytes=document.attributes.content,
            fileName=document.original_name,
        )
        return InsertResult(result)

    def _get_authentication_details(self):
        auth = xsd.Element(
            '{http://services.apsystems.it/}AuthenticationDetails',
            xsd.ComplexType(
                [
                    xsd.Element('{http://services.apsystems.it/}UserName', xsd.String()),
                    xsd.Element('{http://services.apsystems.it/}Password', xsd.String()),
                ],
            ),
        )
        return auth(UserName=self._config.username, Password=self._config.password)

    def _get_mittente(self, application, new_correspondent=None):
        is_office = False
        try:
            if application.is_inbound:
                if new_correspondent:
                    result = self._soap_client.service.GetCorrispondente(
                        _soapheaders=[self._get_authentication_details()],
                        codice=new_correspondent,
                    )
                else:
                    result = self._soap_client.service.GetCorrispondente(
                        _soapheaders=[self._get_authentication_details()],
                        codiceFiscale=application.applicant.fiscal_code,
                    )
            else:
                result = self._soap_client.service.GetCorrispondente(
                    _soapheaders=[self._get_authentication_details()],
                    codice=next(iter(self._config.office_ids)),
                )
                is_office = True
            return InsertResult(result)
        except CorrispondenteNotFoundError:
            if is_office:
                raise ValidationError(f'Il mittente ufficio {next(iter(self._config.office_ids))} non è stato trovato')
            return self._insert_corrispondente(application)

    def _get_destinatari(self, application):
        try:
            if application.is_inbound:
                return [
                    Destinatario(
                        codice=office_id,
                        descrizione=EMPTY,
                        indirizzo=EMPTY,
                        cap=EMPTY,
                        descrizione_comune=EMPTY,
                    )
                    for office_id in self._config.office_ids
                ]
            else:
                result = self._soap_client.service.GetCorrispondente(
                    _soapheaders=[self._get_authentication_details()],
                    codiceFiscale=application.applicant.fiscal_code,
                )
            return InsertResult(result)
        except CorrispondenteNotFoundError:
            raise ValidationError(f'Il destinatario utente {application.applicant.fiscal_code} non è stato trovato')

    def _insert_corrispondente(self, application):
        result = self._soap_client.service.InsertCorrispondente(
            _soapheaders=[self._get_authentication_details()],
            codiceFiscale=application.applicant.fiscal_code,
            descrizione=application.applicant.full_name,
            indirizzo=application.applicant.address_with_house_number,
            cap=application.applicant.postal_code,
            eMail=application.applicant.email,
        )
        result = InsertResult(result)
        return self._get_mittente(application, new_correspondent=result.correspondent_code)


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        if self._result is not None:
            if self._result['_value_1'] is not None and '_value_1' not in self._result['_value_1']:
                raise CorrispondenteNotFoundError()
            elif self._result['_value_1'] is not None and 'ERRORE' in self._result['_value_1']['_value_1'][0]:
                errmsg = ' | '.join(el['ERRORE']['DESCRIZIONE'] for el in self._result['_value_1']['_value_1'])
                raise ValidationError(errmsg)

    @property
    def correspondent_code(self):
        return self._result['_value_1']['_value_1'][0]['corrispondente']['codice']

    @property
    def correspondent_description(self):
        return self._result['_value_1']['_value_1'][0]['corrispondente']['descrizione']

    @property
    def correspondent(self):
        return self._result['_value_1']['_value_1'][0]['corrispondente']

    @property
    def registry_code(self):
        return self._result['_value_1']['_value_1'][0]['protocollo']['codice']

    @property
    def number(self):
        return self._result['_value_1']['_value_1'][0]['protocollo']['numero_protocollo']

    @property
    def year(self):
        return datetime.strptime(
            self._result['_value_1']['_value_1'][0]['protocollo']['data_protocollo'],
            '%d/%m/%Y',
        ).strftime('%Y')
