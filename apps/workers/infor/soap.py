import logging
from datetime import datetime, timezone

import requests
from django.utils.functional import cached_property
from requests.auth import HTTPBasicAuth
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TIMEOUT = 30
SUCCESS_CODE = 'OK'
MAX_DOC_DESCRIPTION_LENGTH = 100

logger = logging.getLogger('application_registry')


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _registration_soap_client(self):
        wsdl = self._config.wsdl_url
        if self._config.auth_username and self._config.auth_password:
            session = requests.Session()
            session.auth = HTTPBasicAuth(self._config.auth_username, self._config.auth_password)
            return self.soap_client_class(
                wsdl=wsdl,
                transport=Transport(session=session, cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
            )
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    def insert(self, application):
        try:
            return self._insert(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application):
        if application.is_inbound:
            payload = self._get_inbound_payload(application)
            result = self._registration_soap_client.service.inserisciArrivo(**payload)
        else:
            payload = self._get_outbound_payload(application)
            result = self._registration_soap_client.service.inserisciPartenza(**payload)
        return InsertResult(result)

    def upload_file(self, document):
        try:
            return self._upload_file(document)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _upload_file(self, document):
        attachment_payload = self._get_attachment_payload(document)
        result = self._registration_soap_client.service.allegaDocumento(**attachment_payload)
        return InsertResult(result)

    def _get_inbound_payload(self, application):
        return {
            'richiestaProtocollaArrivo': {
                'username': self._config.username,
                'protocollaArrivo': {
                    'classificazione': self._get_classificazione(application.is_inbound),
                    'altriDati': self._get_altri_dati(application.is_inbound),
                    'soggetti': self._get_soggetti(application.applicant),
                    'smistamenti': self._get_smistamenti(application.is_inbound),
                    'oggetto': application.document_description,
                },
            },
        }

    def _get_outbound_payload(self, application):
        return {
            'richiestaProtocollaPartenza': {
                'username': self._config.username,
                'protocollaPartenza': {
                    'soggetti': self._get_soggetti(application.applicant),
                    'classificazione': self._get_classificazione(application.is_inbound),
                    'altriDati': self._get_altri_dati(application.is_inbound),
                    'smistamenti': self._get_smistamenti(application.is_inbound),
                    'oggetto': application.document_description,
                },
            },
        }

    def _get_attachment_payload(self, document):
        return {
            'richiestaAllegaDocumento': {
                'username': self._config.username,
                'riferimento': {
                    'anno': document.attributes.year,
                    'numero': document.attributes.number,
                },
                'documento': {
                    'titolo': self._get_document_description(document),
                    'nomeFile': document.original_name,
                    'file': document.attributes.content,
                },
            },
        }

    @staticmethod
    def _get_document_description(document):
        # se la lunghezza della descirizione è maggiore della
        # soglia massima elimino i caratteri in eccesso a
        # partire dal penultimo carattere, poi sostituisco
        # i tre caratteri prima dell'ultimo con xxx
        # per evidenziare il troncamento
        if len(document.description) > MAX_DOC_DESCRIPTION_LENGTH:
            title = document.description.split('.')[0]
            ext = document.description.split('.')[1]
            offset = len(document.description) - MAX_DOC_DESCRIPTION_LENGTH
            tmp = title[:-1]
            tmp = tmp[:-offset]
            positions_to_replace = len(tmp) - 3
            tmp = tmp.replace(tmp[positions_to_replace:], 'xxx')
            title = f'{tmp}{title[-1]}'
            return f'{title}.{ext}'
        return document.description

    def _get_soggetti(self, applicant):
        return {
            '_value_1': [
                {
                    'soggetto': {
                        'denominazione': applicant.description,
                        'indirizzo': applicant.email,
                    },
                },
            ],
        }

    def _get_classificazione(self, is_inbound):
        classification = self._config.incoming_classification if is_inbound else self._config.response_classification
        folder = self._config.incoming_folder if is_inbound else self._config.response_folder
        return {
            'titolario': classification,
            'fascicolo': {
                'anno': folder.split('/')[0],
                'numero': folder.split('/')[1],
            },
        }

    def _get_altri_dati(self, is_inbound):
        document_type = self._config.incoming_document_type if is_inbound else self._config.response_document_type
        intermediary = self._config.incoming_intermediary if is_inbound else self._config.response_intermediary
        return {
            'tipoDocumento': {
                'codice': document_type,
            },
            'tramite': {
                'codice': intermediary,
            },
        }

    def _get_smistamenti(self, is_inbound):
        sorting = self._config.incoming_sorting if is_inbound else self._config.response_sorting
        return {
            'smistamento': {
                'corrispondente': {
                    'codice': sorting,
                },
            },
        }


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.esito
        if errcode != SUCCESS_CODE:
            errmsg = self._result.messaggio
            raise ValidationError(errcode, errmsg)

    @property
    def outcome(self):
        return str(self._result.esito)

    @property
    def message(self):
        return str(self._result.messaggio)

    @property
    def number(self):
        return str(self._result.segnatura.numero)

    @property
    def year(self):
        return str(self._result.segnatura.anno)
