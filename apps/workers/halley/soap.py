import logging
from datetime import datetime, timezone

from django.utils.functional import cached_property
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TIMEOUT = 30
SUCCESS_CODE = 0

logger = logging.getLogger('application_registry')


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _login_soap_client(self):
        wsdl = f'{self._config.login_url}?wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    @cached_property
    def _registration_soap_client(self):
        wsdl = f'{self._config.registration_url}?wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    @cached_property
    def _token(self):
        token = self._login_soap_client.service.Login(
            Ente=self._config.institution_name,  # Same as configured in Halley.
            Username=self._config.username,
            Password=self._config.password,
        ).Token
        if not token:
            raise ValidationError
        return token

    def insert(self, application):
        try:
            return self._insert(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application):
        wReProtocollo = self._registration_soap_client.get_type('ns0:wReProtocollo')  # noqa
        wReAnagrafica = self._registration_soap_client.get_type('ns0:wReAnagrafica')  # noqa
        wReAllegato = self._registration_soap_client.get_type('ns0:wReAllegato')  # noqa
        wReUfficio = self._registration_soap_client.get_type('ns0:wReUfficio')  # noqa

        result = self._registration_soap_client.service.Insert(
            Token=self._token,
            Ente=self._config.institution_name,  # Same as configured in Halley.
            ReProtocollo=wReProtocollo(
                Numero=0,  # placeholder
                Anno=0,  # placeholder
                Esito=0,  # placeholder
                #
                AreaOmogenea=self._config.institution_aoo_id,
                Oggetto=application.document_description,
                Tipo='Arrivo' if application.is_inbound else 'Partenza',
                #
                Categoria=self._config.classification_category,
                Classe=self._config.classification_class,
                Sottoclasse=self._config.classification_subclass,
                #
                Fascicolo=self._config.folder_id,
                #
                Anagrafiche=[
                    wReAnagrafica(
                        Nome=application.applicant.name,
                        Cognome=application.applicant.surname,
                        Email=application.applicant.email,
                        CodiceFiscale=application.applicant.fiscal_code,
                        Esito=0,  # placeholder
                    ),
                ],
                #
                Allegati=[
                    wReAllegato(
                        Nome=application.current_document.original_name,
                        Estensione=application.current_document.attributes.extension,
                        Contenuto=application.current_document.attributes.content,
                        Primario=1,
                        Esito=0,  # placeholder
                    ),
                    *[
                        wReAllegato(
                            Nome=attachment.original_name,
                            Estensione=attachment.attributes.extension,
                            Contenuto=attachment.attributes.content,
                            Primario=0,
                            Esito=0,  # placeholder
                        )
                        for attachment in application.current_attachments
                    ],
                ],
                #
                Uffici=[
                    wReUfficio(
                        Id=id_,
                        Esito=0,  # placeholder
                    )
                    for id_ in self._config.institution_office_ids
                ]
                or None,
            ),
        )
        return InsertResult(result)


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.Esito
        if errcode != SUCCESS_CODE:
            errmsg = self._result.MessaggioEsito
            raise ValidationError(errcode, errmsg)

    @property
    def number(self):
        return str(self._result.Numero)

    @property
    def year(self):
        return str(self._result.Anno)

    @property
    def date(self):
        return self._result.DataRegistrazione
