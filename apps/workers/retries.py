import json
import logging
import time
from datetime import datetime, timedelta, timezone

from dateutil import parser
from django.conf import settings
from kafka import KafkaConsumer, KafkaProducer
from kafka.consumer.fetcher import NoOffsetForPartitionError
from kafka.structs import OffsetAndMetadata, TopicPartition

from apps.timezone_utils import make_aware

NIL_UUID = '00000000-0000-0000-0000-000000000000'

logger = logging.getLogger('application_registry')


def _utc(dt):
    return make_aware(dt, timezone.utc)


class TooSoonError(Exception):
    def __init__(self, *args, ready_at, **kwargs):
        self.ready_at = ready_at
        super().__init__(*args, **kwargs)


class RetryScheduler:
    def __init__(self, *, schedule=None, relay=None, clock=None):
        self._schedule = schedule or Schedule()
        self._relay = relay or ApplicationRelay()
        self._clock = clock or time
        self._running = True

    def __str__(self):
        return self.__class__.__name__

    def start(self, *, iterations=float('inf')):
        while self._running and iterations > 0:
            try:
                self._run()
            except Exception:
                logger.exception('An error occurred.')
            iterations -= 1
            self._sleep(1)

    def _run(self):
        if topics := self._schedule.ready_topics:
            with self._relay as r:
                for topic in topics:
                    logger.debug('Starting to work on topic %s.', topic)
                    next_ready_at = r.move_ready_applications(topic)
                    self._schedule.set(topic, next_ready_at)
                    logger.debug(
                        'Stopped working on topic %s, next at approx. %s.',
                        topic,
                        next_ready_at.replace(microsecond=0).isoformat(),
                    )

    def _sleep(self, seconds):
        self._clock.sleep(seconds)

    def stop(self):
        self._running = False


class Schedule:
    def __init__(self):
        self._timetable = {topic: _utc(datetime.min) for topic in settings.RETRY_SCHEDULER_SOURCE_TOPICS}

    @property
    def ready_topics(self):
        return [topic for topic, ready_at in self._timetable.items() if datetime.now(timezone.utc) >= ready_at]

    def set(self, topic, ready_at):  # noqa
        self._timetable[topic] = ready_at


class ApplicationRelay:
    def __init__(self, *, consumer_class=None, producer_class=None, application_class=None):
        self._consumer_class = consumer_class or KafkaConsumer
        self._producer_class = producer_class or KafkaProducer
        self._application_class = application_class or DeferredApplication

    def __enter__(self):
        self._consumer = self._consumer_class(
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            client_id=settings.RETRY_SCHEDULER_CONSUMER_CLIENT_ID,
            group_id=settings.RETRY_SCHEDULER_CONSUMER_GROUP_ID,
            enable_auto_commit=False,
            consumer_timeout_ms=5000,
            auto_offset_reset='none',
            security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        )
        self._producer = self._producer_class(
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            client_id=settings.RETRY_SCHEDULER_PRODUCER_CLIENT_ID,
            security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._consumer.close()
        self._producer.close()

    def move_ready_applications(self, topic):
        self._subscribe(topic)
        for application in self._applications:
            try:
                application.move()
            except TooSoonError as e:
                return e.ready_at
        return datetime.now(timezone.utc) + timedelta(seconds=10)

    def _subscribe(self, topic):
        self._consumer.subscribe([topic])

    @property
    def _applications(self):
        try:
            for record in self._consumer:
                yield self._application_class(record, self)
        except NoOffsetForPartitionError as e:
            self._set_starting_offset(e.args[0])

    def _set_starting_offset(self, tp):
        offset = self._consumer.end_offsets([tp])[tp]
        self._consumer.commit(offsets={tp: OffsetAndMetadata(offset=offset, metadata='')})
        logger.debug('Starting offset set to %d for topic %s, partition %d.', offset, tp.topic, tp.partition)

    def send(self, value):
        self._producer.send(settings.RETRY_SCHEDULER_DESTINATION_TOPIC, value)
        self._producer.flush(timeout=10)

    def commit(self, *, topic, partition, offset):
        tp = TopicPartition(topic=topic, partition=partition)
        om = OffsetAndMetadata(offset=offset + 1, metadata='')
        self._consumer.commit(offsets={tp: om})


class DeferredApplication:
    def __init__(self, record, relay):
        self._record = record
        self._relay = relay
        decoded = json.loads(record.value)
        self._ready_at = parser.isoparse(decoded[settings.REGISTRY_METADATA]['retry_at'])
        self._id = decoded['id']
        self._event_id = decoded['event_id'] or NIL_UUID

    def move(self):
        if self._is_ready:
            logger.debug('Moving application (id: %s, event id: %s).', self._id, self._event_id)
            self._send()
            self._commit()
            logger.info('Application moved (id: %s, event id: %s).', self._id, self._event_id)
        else:
            raise TooSoonError(ready_at=self._ready_at)

    @property
    def _is_ready(self):
        return datetime.now(timezone.utc) >= self._ready_at

    def _send(self):
        self._relay.send(self._record.value)

    def _commit(self):
        self._relay.commit(
            topic=self._record.topic,
            partition=self._record.partition,
            offset=self._record.offset,
        )
