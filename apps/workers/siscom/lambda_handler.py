import django

django.setup()

from apps.workers.aws_lambda import Handler  # noqa: E402
from apps.workers.siscom.job import run  # noqa: E402


def handle(event, context):
    Handler(event, run).handle()
