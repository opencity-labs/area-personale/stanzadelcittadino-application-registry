import logging
from datetime import datetime, timezone
from types import SimpleNamespace

from django.utils.functional import cached_property
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TIMEOUT = 30
SUCCESS_CODE = 'OK'
ERROR_CODES = [f'E{i}' for i in range(5)]

logger = logging.getLogger('application_registry')

TYPE_NAMES = [
    'Protocollo',
    'ArrayOfProtoMittDest',
    'ProtoMittDest',
    'MittDest',
    'MittDestSede',
    'ArrayOfDocFile',
    'DocFile',
]
INBOUND = 'A'
OUTBOUND = 'P'
EMPTY = ''
ZERO = '0'
SPORTELLO_AL_CITTADINO = 4


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _soap_client(self):
        return self.soap_client_class(
            wsdl=f'{self._config.url}?wsdl',
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    def _get_types(self):
        try:
            types = {name: self._soap_client.get_type(f'ns1:{name}') for name in TYPE_NAMES}
        except Exception as e:
            raise SOAPError(e) from e
        return SimpleNamespace(**types)

    def insert(self, application):
        try:
            return self._insert(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application):
        t = self._get_types()
        result = self._soap_client.service.RegisterNewProto(
            CodiceLicenza=self._config.license_code,
            pDatiProtocollo=t.Protocollo(
                Tipo=INBOUND if application.is_inbound else OUTBOUND,
                Risposta=False,
                Oggetto=application.subject,
                IDCategoria=self._config.classification_category or ZERO,
                IDClasse=self._config.classification_class or ZERO,
                IDFascicolo=self._config.folder_id or ZERO,
                Operatore=(
                    application.applicant.full_name if application.is_inbound else self._config.operator_denomination
                ),
                IDTipo=self._config.mail_type or ZERO,
                Annullato=False,
                AnnoPadre=ZERO,
                NumPadre=ZERO,
                Riservato=False,
                ImportoPosta=ZERO,
                DocInformatico=False,
                IDPratica=ZERO,
                IDPratica2=ZERO,
                IDUfficio=self._config.office_id,
                ComunicazImporto=ZERO,
                ComunicazTipoProcedura='20',
                ComunicazTipoFunzione='Ricezione Pratica Web',
                ComunicazTipoDocumento='Documento',
                ListaMittDest=t.ArrayOfProtoMittDest(
                    ProtoMittDest=[
                        t.ProtoMittDest(
                            MittDest=t.MittDest(
                                IDMittDest=ZERO,
                                RagSociale=application.applicant.full_name,
                                Indirizzo=application.applicant.address,
                                CAP=application.applicant.postal_code,
                                Città=application.applicant.municipality,
                                Provincia=application.applicant.county,
                                CodFiscale=application.applicant.fiscal_code,
                                PartitaIVA=None,
                                Note=None,
                                Titolo=None,
                                NumeroTelefonico=None,
                                NumeroFax=None,
                                Nazione=None,
                                SitoInternet=None,
                                EMail=application.applicant.email,
                                TipoEMail=ZERO,
                                MittDestSede=t.MittDestSede(
                                    IDSedeMD=ZERO,
                                    Descrizione=None,
                                    Indirizzo=None,
                                    CAP=None,
                                    Città=None,
                                    Provincia=None,
                                    EMail=None,
                                    Note=None,
                                    NumeroTelefonico=None,
                                    NumeroFax=None,
                                    NumeroCellulare=None,
                                ),
                            ),
                            IDTipoPosta=ZERO,
                            TipoCC=ZERO,
                        ),
                    ],
                ),
            ),
        )
        return InsertResult(result, service='RegisterNewProto')

    def upload(self, document):
        try:
            return self._upload(document)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _upload(self, document):
        t = self._get_types()
        result = self._soap_client.service.AppendFileToProto(
            CodiceLicenza=self._config.license_code,
            ProtNumero=document.attributes.number,
            ProtAnno=document.attributes.year,
            pDocFile=t.DocFile(
                Descrizione=document.description,
                NomeFile=document.original_name,
                TipoTesto=None,
                FileAncoraSuWeb=False,
                FileBinario=document.attributes.content,
            ),
        )
        return InsertResult(result, service='AppendFileToProto')

    def collate(self, application, number, year):
        try:
            return self._collate(application, number, year)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _collate(self, application, number, year):
        result = self._soap_client.service.SetFascDigitaleToProto(
            CodiceLicenza=self._config.license_code,
            ProtNumero=number,
            ProtAnno=year,
            FascDigitale=application.project_description,
            TipoFascDigitale=SPORTELLO_AL_CITTADINO,
        )
        return InsertResult(result, service='SetFascDigitaleToProto')


class InsertResult:
    def __init__(self, result, service):
        self._result = result['_value_1']['_value_1'][0][service]
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.CodRis
        if errcode != SUCCESS_CODE:
            errmsg = f'{self._result.DescErrore} {self._result.DescRis or EMPTY}'
            raise ValidationError(errcode, errmsg)

    @property
    def document_id(self):
        return self._result.IDDocFile

    @property
    def number(self):
        return self._result.ProtoNum

    @property
    def year(self):
        return self._result.ProtoAnno

    @property
    def document_id_in_folder(self):
        return self._result.IDDocFascDigit
