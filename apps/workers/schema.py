from drf_spectacular.utils import extend_schema
from rest_framework import serializers


class SubmissionSerializer(serializers.Serializer):
    pass


SUBMISSION_APIVIEW_OVERRIDES = {
    'post': extend_schema(
        request=SubmissionSerializer,
        responses=SubmissionSerializer,
    ),
}
