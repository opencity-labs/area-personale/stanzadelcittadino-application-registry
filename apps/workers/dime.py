import uuid
from dataclasses import dataclass, field

from construct import (
    Aligned,
    BitsInteger,
    BitStruct,
    Bytes,
    Bytewise,
    Const,
    Flag,
    Int16ub,
    Int32ub,
    Nibble,
    Struct,
    this,
)

MEDIATYPE_TYPE_T = 1
ABSOLUTEURI_TYPE_T = 2
SOAP11_ENVELOPE_TYPE = 'http://schemas.xmlsoap.org/soap/envelope/'


_RecordHeaderStruct = BitStruct(
    'version' / Const(1, BitsInteger(5)),
    'mb' / Flag,
    'me' / Flag,
    'cf' / Const(0, Flag),
    'type_t' / Nibble,
    'resrvd' / Const(0, Nibble),
    'options_length' / Const(0, Bytewise(Int16ub)),
    'id_length' / Bytewise(Int16ub),
    'type_length' / Bytewise(Int16ub),
    'data_length' / Bytewise(Int32ub),
)
_RecordBodyStruct = Struct(
    # 'options'
    'id' / Aligned(4, Bytes(this._.header.id_length)),
    'type' / Aligned(4, Bytes(this._.header.type_length)),
    'data' / Aligned(4, Bytes(this._.header.data_length)),
)
_RecordStruct = Struct(
    'header' / _RecordHeaderStruct,
    'body' / _RecordBodyStruct,
)
_MessageStruct = _RecordStruct[2]


@dataclass
class Record:
    type: str  # noqa
    data: bytes
    id: str = field(default_factory=lambda: str(uuid.uuid4()))  # noqa

    btype: bytes = field(init=False)
    btype_length: int = field(init=False)
    data_length: int = field(init=False)
    bid: bytes = field(init=False)
    bid_length: int = field(init=False)

    def __post_init__(self):
        self.btype = self.type.encode('utf8')
        self.btype_length = len(self.btype)
        self.data_length = len(self.data)
        self.bid = self.id.encode('utf8')
        self.bid_length = len(self.bid)


@dataclass
class Envelope(Record):
    type: str = field(init=False)  # noqa
    id: str = field(init=False)  # noqa

    def __post_init__(self):
        self.type = SOAP11_ENVELOPE_TYPE
        self.id = ''
        super().__post_init__()


class Message:
    struct = _MessageStruct

    def __init__(self, envelope, document):
        self._envelope = envelope
        self._document = document

    def as_bytes(self):
        return self.struct.build(
            [
                dict(  # noqa
                    header=dict(  # noqa
                        mb=True,
                        me=False,
                        type_t=ABSOLUTEURI_TYPE_T,
                        id_length=self._envelope.bid_length,
                        type_length=self._envelope.btype_length,
                        data_length=self._envelope.data_length,
                    ),
                    body=dict(  # noqa
                        id=self._envelope.bid,
                        type=self._envelope.btype,
                        data=self._envelope.data,
                    ),
                ),
                dict(  # noqa
                    header=dict(  # noqa
                        mb=False,
                        me=True,
                        type_t=MEDIATYPE_TYPE_T,
                        id_length=self._document.bid_length,
                        type_length=self._document.btype_length,
                        data_length=self._document.data_length,
                    ),
                    body=dict(  # noqa
                        id=self._document.bid,
                        type=self._document.btype,
                        data=self._document.data,
                    ),
                ),
            ],
        )
