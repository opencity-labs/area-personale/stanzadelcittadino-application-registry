import hashlib
import logging
from datetime import datetime, timezone

import requests
from django.utils.functional import cached_property
from lxml import etree
from requests import RequestException
from zeep import Client as ZeepClient
from zeep import Settings as ZeepSettings
from zeep.cache import InMemoryCache
from zeep.plugins import HistoryPlugin
from zeep.transports import Transport

from apps.workers.errors import RESTValidationError, SOAPError, TransportError, ValidationError

TIMEOUT = 30

logger = logging.getLogger('application_registry')


def log_history(history):
    try:
        for hist in [history.last_sent, history.last_received]:
            print(etree.tostring(hist['envelope'], encoding='unicode', pretty_print=True))
    except (IndexError, TypeError):
        # catch cases where it fails before being put on the wire
        pass


class Client:
    soap_client_class = ZeepClient
    http_client = requests
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config
        self.history = HistoryPlugin()

    @cached_property
    def _token(self):
        try:
            response = self.http_client.post(
                f'{self._config.login_url}?grant_type={self._config.grant_type}',
                auth=(self._config.client_id, self._config.client_secret),
            )
            response.raise_for_status()
            return response.json()['access_token']
        except (RequestException, KeyError) as e:
            raise RESTValidationError(e) from e

    @cached_property
    def _registration_soap_client(self):
        wsdl = 'apps/workers/insiel/wsdl/protocollo.v1/protocollo.wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
            settings=ZeepSettings(extra_http_headers={'Authorization': 'Bearer ' + self._token}),
            plugins=[self.history],
        )

    @cached_property
    def _registration_soap_service(self):
        return self._registration_soap_client.create_service(
            '{http://insiel.it/protocollo/services/protocollo}ProtocolloSoapBinding',
            self._config.registry_url,
        )

    @cached_property
    def _file_transfer_soap_client(self):
        wsdl = 'apps/workers/insiel/wsdl/files.v1/filetransfer.wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
            settings=ZeepSettings(extra_http_headers={'Authorization': 'Bearer ' + self._token}),
        )

    @cached_property
    def _file_transfer_soap_service(self):
        return self._file_transfer_soap_client.create_service(
            '{http://insiel.it/filetransfer/services}FileTransferSoapBinding',
            self._config.file_transfer_url,
        )

    def predispose_applicant(self, application):
        try:
            return self._predisoni_anagrafica(application)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _predisoni_anagrafica(self, applicant):
        DatiAnagrafica = self._registration_soap_client.get_type('ns27:DatiAnagrafica')  # noqa
        Utente = self._registration_soap_client.get_type('ns1:Utente')  # noqa
        EmailList = self._registration_soap_client.get_type('ns2:EmailList')  # noqa
        EmailAnagrafica = self._registration_soap_client.get_type('ns2:EmailAnagrafica')  # noqa

        result = self._registration_soap_service.predisponiAnagrafica(
            utente=Utente(codice=self._config.user_code, password=self._config.user_password),
            anagrafica=DatiAnagrafica(
                cognome=applicant.surname,
                nome=applicant.name,
                codiceFiscale=applicant.fiscal_code,
                indirizzo=applicant.address_with_house_number,
                comune=applicant.municipality,
                provincia=applicant.county,
                cap=applicant.postal_code,
                emailList=EmailList(EmailAnagrafica(email=applicant.email, tipo='peo')) if applicant.email else [],
            ),
        )
        return InsertResult(result)

    def inserisci_pratica(self, application):
        try:
            return self._inserisci_pratica(application)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _inserisci_pratica(self, application):
        Utente = self._registration_soap_client.get_type('ns1:Utente')  # noqa
        Classifica = self._registration_soap_client.get_type('ns1:Classifica')  # noqa
        Uffici = self._registration_soap_client.get_type('ns18:Uffici')  # noqa
        Ufficio = self._registration_soap_client.get_type('ns1:UfficioPrat')  # noqa
        RiferimentiAnagrafici = self._registration_soap_client.get_type('ns18:RiferimentiAnagrafici')  # noqa
        RiferimentoAnafìgrafico = self._registration_soap_client.get_type('ns1:RiferimentoAnagrafico')  # noqa

        anagrafica = self.predispose_applicant(application.applicant)

        result = self._registration_soap_service.aperturaPratica(
            utente=Utente(codice=self._config.user_code, password=self._config.user_password),
            codiceUfficio=self._config.office_code,
            codiceRegistro=Classifica(codice=self._config.register_code),
            oggetto=application.project_description,
            anno=application.registration_date.strftime('%Y'),
            data=application.registration_date.date().isoformat(),
            codiceUfficioOperante=self._config.operating_office_code,
            uffici=Uffici(
                [
                    Ufficio(codice=internal_offices_code)
                    for internal_offices_code in self._config.internal_offices_codes
                ],
            ),
            riferimentiAnagrafici=RiferimentiAnagrafici([RiferimentoAnafìgrafico(descrizione=anagrafica.description)]),
        )
        return InsertResult(result)

    def upload_file(self, document):
        try:
            return self._upload_file(document)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _upload_file(self, document):
        result = self._file_transfer_soap_service.uploadFile(
            md5Checksum=hashlib.md5(document).hexdigest(),
            binaryData=document,
        )
        return InsertResult(result)

    def inserisci_protocollo(self, application, folder_data=None):
        try:
            return self._inserisci_protocollo(application, folder_data)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _inserisci_protocollo(self, application, folder_data=None):
        Utente = self._registration_soap_client.get_type('ns1:Utente')  # noqa
        OggettoDoc = self._registration_soap_client.get_type('ns22:OggettoDoc')  # noqa
        Mittenti = self._registration_soap_client.get_type('ns22:Mittenti')  # noqa
        Destinatari = self._registration_soap_client.get_type('ns22:Destinatari')  # noqa
        Mittente = self._registration_soap_client.get_type('ns1:MittenteInsProto')  # noqa
        Destinatario = self._registration_soap_client.get_type('ns1:DestinatarioIOPInsProto')  # noqa
        Documenti = self._registration_soap_client.get_type('ns22:DocumentiInsProto')  # noqa
        Documento = self._registration_soap_client.get_type('ns1:DocumentoInsProto')  # noqa
        Uffici = self._registration_soap_client.get_type('ns22:Uffici')  # noqa
        Ufficio = self._registration_soap_client.get_type('ns1:UfficioInsProto')  # noqa
        Classifiche = self._registration_soap_client.get_type('ns22:Classifiche')  # noqa
        Classifica = self._registration_soap_client.get_type('ns1:Classifica')  # noqa
        Pratiche = self._registration_soap_client.get_type('ns1:UfficioInsProto')  # noqa
        Pratiche = self._registration_soap_client.get_type('ns22:Pratiche')  # noqa
        Pratica = self._registration_soap_client.get_type('ns1:PraticaRequest')  # noqa
        IdPrat = self._registration_soap_client.get_type('ns1:IdProtocollo')  # noqa

        anagrafica = self.predispose_applicant(application.applicant)

        result = self._registration_soap_service.inserisciProtocollo(
            utente=Utente(codice=self._config.user_code, password=self._config.user_password),
            codiceUfficio=self._config.office_code,
            codiceRegistro=self._config.office_registry,
            codiceUfficioOperante=self._config.operating_office_code,
            verso='A' if application.is_inbound else 'P',
            oggettoDocumento=OggettoDoc(oggetto=application.document_description),
            mittenti=Mittenti(Mittente(descrizione=anagrafica.description)) if application.is_inbound else None,
            destinatari=(
                Destinatari(Destinatario(descrizione=anagrafica.description)) if not application.is_inbound else None
            ),
            uffici=(
                Uffici(
                    [
                        Ufficio(
                            codice=internal_offices_code,
                            tipo=internal_offices_type if internal_offices_type != '-' else None,
                        )
                        for internal_offices_code, internal_offices_type in zip(
                            self._config.internal_offices_codes,
                            self._config.internal_offices_types,
                        )
                    ],
                )
                if self._config.internal_offices_codes
                else None
            ),
            documenti=Documenti(
                [
                    Documento(
                        id=application.current_document.attributes.uploaded_file_id,
                        nome=application.current_document.original_name,
                        primario=True,
                        inviaIOP=True,
                    ),
                    *[
                        Documento(
                            id=attachment.attributes.uploaded_file_id,
                            nome=attachment.original_name,
                            primario=False,
                            inviaIOP=True,
                        )
                        for attachment in application.current_attachments or []
                    ],
                ],
            ),
            classifiche=Classifiche([Classifica(codice=self._config.register_code)]),
            inserisciInPratica=Pratiche(
                [
                    Pratica(
                        idPrat=IdPrat(
                            progDoc=folder_data['progDoc'] if folder_data else self._config.prog_doc,
                            progMovi=folder_data['progMovi'] if folder_data else self._config.prog_movi,
                        ),
                    ),
                ],
            ),
        )
        return InsertResult(result)


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.esito
        if not errcode:
            errmsg = self._result.errore
            raise ValidationError(errcode, errmsg)

    @property
    def description(self):
        return str(self._result.anagrafica.descAna)

    @property
    def id_file(self):
        return str(self._result.UploadedFile.idFile)

    @property
    def number(self):
        return str(self._result.registrazione[0].numero)

    @property
    def date(self):
        return str(self._result.registrazione[0].data.strftime('%Y-%m-%d'))

    @property
    def folder_number(self):
        return str(self._result.pratica.numero)

    @property
    def document_data(self):
        return {
            'codiceUfficio': self._result.registrazione[0].codiceUfficio,
            'codiceRegistro': self._result.registrazione[0].codiceRegistro,
            'anno': self._result.registrazione[0].anno,
            'numero': self._result.registrazione[0].numero,
            'verso': self._result.registrazione[0].verso,
            'data': self._result.registrazione[0].data.strftime('%Y-%m-%d'),
            'progDoc': self._result.registrazione[0].progDoc,
            'progMovi': self._result.registrazione[0].progMovi,
        }

    @property
    def folder_data(self):
        return {
            'codiceUfficio': self._result.pratica.codiceUfficio,
            'codiceRegistro': self._result.pratica.codiceRegistro,
            'anno': self._result.pratica.anno,
            'numero': self._result.pratica.numero,
            'subNumero': self._result.pratica.subNumero,
            'dataApertura': self._result.pratica.dataApertura.strftime('%Y-%m-%d'),
            'progDoc': self._result.pratica.progDoc,
            'progMovi': self._result.pratica.progMovi,
        }
