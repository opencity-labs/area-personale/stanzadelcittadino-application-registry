class SOAPError(Exception):
    pass


class TransportError(SOAPError):
    pass


class ValidationError(SOAPError):
    pass


class RESTError(Exception):
    pass


class RESTValidationError(RESTError):
    pass


class RegistrationError(Exception):
    pass
