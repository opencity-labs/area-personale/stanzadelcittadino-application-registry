from lxml.builder import E


def _filter_contents(contents):
    return tuple(content for content in contents if content is not None)


def _filter_attributes(attributes):
    return {name: value for name, value in attributes.items() if value is not None}


class RequiredType:
    def __init__(self, name):
        self._name = name

    def __call__(self, *contents, **attributes):
        contents = _filter_contents(contents)
        attributes = _filter_attributes(attributes)
        return getattr(E, self._name)(*contents, **attributes)


class OptionalType:
    def __init__(self, name, *, weak_attrs=None):
        self._name = name
        self._weak_attrs = weak_attrs or []

    def __call__(self, *contents, **attributes):
        contents = _filter_contents(contents)
        attributes = _filter_attributes(attributes)
        strong_attrs = set(attributes) - set(self._weak_attrs)
        if contents or strong_attrs:
            return getattr(E, self._name)(*contents, **attributes)
        return None
