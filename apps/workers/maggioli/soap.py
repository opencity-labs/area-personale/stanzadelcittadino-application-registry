import logging
from datetime import datetime, timezone
from types import SimpleNamespace

import requests
from django.utils.functional import cached_property
from lxml import etree
from requests.exceptions import RequestException
from rest_framework.status import is_client_error, is_server_error
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport
from zeep.wsdl.messages.multiref import process_multiref

from application_registry import __version__ as app_version
from apps.workers import dime
from apps.workers.errors import SOAPError, TransportError, ValidationError
from apps.workers.maggioli.types import OptionalType, RequiredType

DOCUMENT_DESCRIPTION = 'Documento ricevuto da Stanza del cittadino'
TIMEOUT = 60
SUCCESS_CODE = '0'
EMPTY_ELEMENT = etree.Element('_')
UTF8 = 'UTF-8'
XMLNS_PREFIX_MAP = {
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'ns': 'urn:ProWSApi',
}
INSERT_DOCUMENTO_RETURN_XPATH = './soap:Body/ns:insertDocumentoResponse/insertDocumentoReturn'
REGISTRA_PROTOCOLLO_RETURN_XPATH = './soap:Body/ns:registraProtocolloResponse/registraProtocolloReturn'
INBOUND_REGISTRATION = 'E'
OUTBOUND_REGISTRATION = 'U'
SMTP = 'smtp'
REGISTRY_APP = 'SICRAWEB'
PARAM_TIPO_DOCUMENTO = 'TipoDocumento'

logger = logging.getLogger('application_registry')


class Client:
    soap_client_class = ZeepClient
    http_client = requests
    cache = InMemoryCache()
    headers = {
        'Content-Type': 'application/dime',
        'SOAPAction': '""',
        'User-Agent': f'sdc-registry/{app_version}',
    }

    def __init__(self, *, connection_string, url):
        self._conn_string = connection_string
        self._url = url
        self._wsdl = f'{self._url}?wsdl'

    @cached_property
    def _soap_client(self):
        return self.soap_client_class(
            wsdl=self._wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    def insert_documento(self, filename, mediatype, document):
        try:
            return self._insert_documento(filename, mediatype, document)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert_documento(self, filename, mediatype, document):
        envelope = self._get_envelope(
            'insertDocumento',
            {'documentName': filename, 'documentDescription': DOCUMENT_DESCRIPTION},
        )
        message = dime.Message(
            dime.Envelope(data=envelope),
            dime.Record(type=mediatype, data=document),
        ).as_bytes()
        xml = self._post(message)
        return InsertDocumentoResult(xml)

    def registra_protocollo(self, document):
        try:
            return self._registra_protocollo(document)
        except RequestException as e:
            raise TransportError(e) from e
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _registra_protocollo(self, document):
        envelope = self._get_envelope('registraProtocollo')
        message = dime.Message(
            dime.Envelope(data=envelope),
            dime.Record(type='text/xml', data=document),
        ).as_bytes()
        xml = self._post(message)
        return RegistraProtocolloResult(xml)

    def _get_envelope(self, method, params=None):
        params = params or {}
        element = self._soap_client.create_message(
            self._soap_client.service,
            method,
            connectionString=self._conn_string,
            **params,
        )
        return etree.tostring(element, encoding=UTF8, xml_declaration=True)

    def _post(self, data):
        response = self.http_client.post(
            self._url,
            data=data,
            headers=self.headers,
            timeout=TIMEOUT,
        )
        self._log_error(response)
        response.raise_for_status()
        return response.content

    @staticmethod
    def _log_error(response):
        if is_client_error(response.status_code) or is_server_error(response.status_code):
            logger.debug('response: %s', response.content.decode(errors='replace'))


class BaseResult:
    errcode_xpath = '.'
    errmsg_xpath = '.'
    xpath_map = {}

    def __init__(self, xml):
        try:
            self._root = etree.fromstring(xml)
        except Exception:
            self._root = EMPTY_ELEMENT
        self.created_at = datetime.now(timezone.utc)
        self._process_multiref()
        self._validate_error_code()
        self._validate_elements()

    def _process_multiref(self):
        elements = self._root.xpath('soap:Body', namespaces=XMLNS_PREFIX_MAP)
        try:
            body = elements[0]
        except IndexError as e:
            raise ValidationError(e) from e
        process_multiref(body)

    def _validate_error_code(self):
        errcode = self._findtext(self.errcode_xpath)
        if errcode != SUCCESS_CODE:
            errmsg = self._findtext(self.errmsg_xpath)
            raise ValidationError(errcode, errmsg)

    def _validate_elements(self):
        for xpath in self.xpath_map.values():
            text = self._findtext(xpath)
            if not text:
                raise ValidationError(xpath, text)

    def _findtext(self, xpath):
        return self._root.findtext(xpath, namespaces=XMLNS_PREFIX_MAP)


class InsertDocumentoResult(BaseResult):
    xpath_prefix = INSERT_DOCUMENTO_RETURN_XPATH
    errcode_xpath = f'{xpath_prefix}/lngErrNumber'
    errmsg_xpath = f'{xpath_prefix}/strErrString'
    xpath_map = {
        'doc_id': f'{xpath_prefix}/lngDocID',
    }

    @property
    def doc_id(self):
        return self._findtext(self.xpath_map['doc_id'])


class RegistraProtocolloResult(BaseResult):
    xpath_prefix = REGISTRA_PROTOCOLLO_RETURN_XPATH
    errcode_xpath = f'{xpath_prefix}/lngErrNumber'
    errmsg_xpath = f'{xpath_prefix}/strErrString'
    xpath_map = {
        'number': f'{xpath_prefix}/lngNumPG',
        'year': f'{xpath_prefix}/lngAnnoPG',
    }

    @property
    def number(self):
        return self._findtext(self.xpath_map['number'])

    @property
    def year(self):
        return self._findtext(self.xpath_map['year'])


class BaseTemplate:
    def __init__(self, config):
        self._config = config

    def render(self, application):
        root = etree.Element('Segnatura')
        return etree.tostring(root)

    @staticmethod
    def get_types():
        required_types = {
            name: RequiredType(name)
            for name in [
                'AOO',
                'Amministrazione',
                'ApplicativoProtocollo',
                'CodiceAOO',
                'CodiceAmministrazione',
                'DataRegistrazione',
                'Denominazione',
                'Descrizione',
                'Destinatario',
                'Documento',
                'Flusso',
                'Identificatore',
                'IndirizzoPostale',
                'Intestazione',
                'Mittente',
                'NumeroRegistrazione',
                'Oggetto',
                'Parametro',
                'Persona',
                'Segnatura',
                'UnitaOrganizzativa',
            ]
        }
        optional_types = {
            name: OptionalType(name)
            for name in [
                'Allegati',
                'CAP',
                'Classifica',
                'CodiceTitolario',
                'Cognome',
                'Comune',
                'Denominazione',
                'Fascicolo',
                'MezzoInvio',
                'ModalitaInvio',
                'Nome',
                'Provincia',
            ]
        }
        optional_types['IndirizzoTelematico'] = OptionalType(
            'IndirizzoTelematico',
            weak_attrs=['tipo'],
        )

        return SimpleNamespace(
            required=SimpleNamespace(**required_types),
            optional=SimpleNamespace(**optional_types),
        )


class VicopisanoInboundTemplate(BaseTemplate):
    def render(self, application):
        t = self.get_types()
        req, opt = t.required, t.optional

        root = req.Segnatura(
            req.Intestazione(
                req.Oggetto(application.document_description),
                req.Identificatore(
                    req.CodiceAmministrazione(self._config.sdc_institution_code),
                    req.CodiceAOO(self._config.sdc_aoo_code),
                    req.NumeroRegistrazione(self._config.registration_number),
                    req.DataRegistrazione(application.registration_date.date().isoformat()),
                    req.Flusso(INBOUND_REGISTRATION),
                ),
                req.Mittente(
                    req.Persona(
                        opt.Nome(application.applicant.name),
                        opt.Cognome(application.applicant.surname),
                        opt.IndirizzoTelematico(application.applicant.email, tipo=SMTP),
                        id=application.applicant.fiscal_code,
                    ),
                    req.IndirizzoPostale(
                        opt.Denominazione(application.applicant.address_with_house_number),
                        opt.CAP(application.applicant.postal_code),
                        opt.Comune(application.applicant.municipality),
                        opt.Provincia(application.applicant.county),
                    ),
                ),
                req.Destinatario(
                    req.Amministrazione(
                        req.Denominazione(self._config.institution_name),
                        req.CodiceAmministrazione(self._config.institution_code),
                        req.UnitaOrganizzativa(id=self._config.institution_ou),
                        opt.ModalitaInvio(self._config.sending_mode),
                        opt.MezzoInvio(self._config.sending_method),
                    ),
                    req.AOO(
                        req.CodiceAOO(self._config.institution_aoo_code),
                    ),
                ),
                opt.Classifica(
                    opt.CodiceTitolario(self._config.classification_hierarchy),
                ),
                opt.Fascicolo(
                    anno=self._config.folder_year,
                    numero=self._config.folder_number,
                ),
            ),
            req.Descrizione(
                req.Documento(
                    nome=application.current_document.original_name,
                    id=application.current_document.attributes.remote_id,
                ),
                opt.Allegati(
                    *[
                        req.Documento(nome=attachment.original_name, id=attachment.attributes.remote_id)
                        for attachment in application.current_attachments
                    ],
                ),
            ),
            req.ApplicativoProtocollo(
                req.Parametro(nome=PARAM_TIPO_DOCUMENTO, valore=self._config.document_type),
                nome=REGISTRY_APP,
            ),
        )
        rendered = etree.tostring(root, encoding=UTF8, xml_declaration=True)
        logger.debug('direction: inbound')
        logger.debug(rendered)
        return rendered


class VicopisanoOutboundTemplate(BaseTemplate):
    def render(self, application):
        t = self.get_types()
        req, opt = t.required, t.optional

        root = req.Segnatura(
            req.Intestazione(
                req.Oggetto(application.document_description),
                req.Identificatore(
                    req.CodiceAmministrazione(self._config.sdc_institution_code),
                    req.CodiceAOO(self._config.sdc_aoo_code),
                    req.NumeroRegistrazione(self._config.registration_number),
                    req.DataRegistrazione(application.registration_date.date().isoformat()),
                    req.Flusso(OUTBOUND_REGISTRATION),
                ),
                req.Mittente(
                    req.Amministrazione(
                        req.Denominazione(self._config.institution_name),
                        req.CodiceAmministrazione(self._config.institution_code),
                        req.UnitaOrganizzativa(id=self._config.institution_ou),
                    ),
                    req.AOO(
                        req.CodiceAOO(self._config.institution_aoo_code),
                    ),
                ),
                req.Destinatario(
                    req.Persona(
                        opt.Nome(application.applicant.name),
                        opt.Cognome(application.applicant.surname),
                        id=application.applicant.fiscal_code,
                    ),
                    opt.IndirizzoTelematico(application.applicant.email, tipo=SMTP),
                    opt.ModalitaInvio(self._config.sending_mode),
                    opt.MezzoInvio(self._config.sending_method),
                    req.IndirizzoPostale(
                        opt.Denominazione(application.applicant.address_with_house_number),
                        opt.CAP(application.applicant.postal_code),
                        opt.Comune(application.applicant.municipality),
                        opt.Provincia(application.applicant.county),
                    ),
                ),
                opt.Classifica(
                    opt.CodiceTitolario(self._config.classification_hierarchy),
                ),
                opt.Fascicolo(
                    anno=self._config.folder_year,
                    numero=self._config.folder_number,
                ),
            ),
            req.Descrizione(
                req.Documento(
                    nome=application.current_document.original_name,
                    id=application.current_document.attributes.remote_id,
                ),
                opt.Allegati(
                    *[
                        req.Documento(nome=attachment.original_name, id=attachment.attributes.remote_id)
                        for attachment in application.current_attachments
                    ],
                ),
            ),
            req.ApplicativoProtocollo(
                req.Parametro(nome=PARAM_TIPO_DOCUMENTO, valore=self._config.document_type),
                nome=REGISTRY_APP,
            ),
        )
        rendered = etree.tostring(root, encoding=UTF8, xml_declaration=True)
        logger.debug('direction: outbound')
        logger.debug(rendered)
        return rendered
