import logging
import os
from datetime import datetime, timezone
from types import SimpleNamespace

from django.utils.functional import cached_property
from lxml import etree
from zeep import Client as ZeepClient
from zeep import xsd
from zeep.cache import InMemoryCache
from zeep.loader import load_external
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TYPE_NAMES = [
    'Segnatura',
    'Intestazione',
    'Identificatore',
    'Mittente',
    'Destinatario',
    'Persona',
    'Amministrazione',
    'AOO',
    'Classifica',
    'Fascicolo',
    'Descrizione',
    'Documento',
    'DescrizioneDocumento',
    'Allegati',
    'ApplicativoProtocollo',
    'Parametro',
    'UnitaOrganizzativa',
    'IndirizzoTelematico',
]

TIMEOUT = 30
SUCCESS_CODE = 0
ZERO = '0'
INBOUND = 'E'
OUTBOUND = 'U'
SMTP = 'smtp'
EMPTY = ''
REGISTRY_APP = 'SDCREGISTRY'

logger = logging.getLogger('application_registry')


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _soap_client(self):
        client = self.soap_client_class(
            wsdl=f'{self._config.url}/wsdl/DOCAREAProto',
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )
        client = self._load_external_xsd(soap_client=client, file_path='apps/workers/tinn/xsd/segnatura.xsd')
        return client

    @staticmethod
    def _load_external_xsd(soap_client, file_path):
        xsd_schema_file = file_path
        container_dir = os.path.dirname(xsd_schema_file)
        schema_doc = load_external(open(xsd_schema_file, 'rb'), None)
        doc = soap_client.wsdl.types.create_new_document(schema_doc, f'file://{container_dir}')
        doc.resolve()
        return soap_client

    @property
    def _docareaproto_service(self):
        return self._soap_client.create_service(
            binding_name='{http://tempuri.org/}DOCAREAProtobinding',
            address=f'{self._config.url}/soap/DOCAREAProto',
        )

    def get_types(self):
        try:
            types = {name: self._soap_client.get_element(name).type for name in TYPE_NAMES}
            types['DescrizioneDocumento'] = xsd.Element(
                'DescrizioneDocumento',
                xsd.ComplexType([xsd.Element('_value_1', xsd.String())]),
            )
        except Exception as e:
            raise SOAPError(e) from e
        return SimpleNamespace(**types)

    @cached_property
    def _token(self):
        result = self._docareaproto_service.Login(
            CodiceEnte=self._config.codente,
            Username=self._config.username,
            UserPassword=self._config.password,
        )
        if result.IngErrNumber != SUCCESS_CODE:
            raise ValidationError(result.IngErrNumber, result.strErrString)
        return result.strDST

    def inserimento(self, document):
        try:
            return self._inserimento(document)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _inserimento(self, document):
        result = self._docareaproto_service.Inserimento(
            Username=self._config.username,
            DSTLogin=self._token,
            FileBinario=document,
        )
        return InsertResult(result)

    def protocollazione(self, application):
        try:
            return self._protocollazione(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _protocollazione(self, application):
        t = self.get_types()
        segnatura = t.Segnatura(
            Intestazione=t.Intestazione(
                Oggetto=application.document_description,
                Identificatore=t.Identificatore(
                    CodiceAmministrazione=self._config.sdc_institution_code,
                    CodiceAOO=self._config.sdc_aoo_code,
                    NumeroRegistrazione=ZERO,
                    DataRegistrazione=ZERO,
                    Flusso=INBOUND if application.is_inbound else OUTBOUND,
                ),
                Mittente=self._mittente(t, application),
                Destinatario=self._destinatario(t, application),
                Classifica=t.Classifica(
                    CodiceAmministrazione=self._config.classification_institution_code or EMPTY,
                    CodiceAOO=self._config.classification_aoo_code or EMPTY,
                    CodiceTitolario=self._config.classification_hierarchy,
                ),
                Fascicolo=t.Fascicolo(
                    numero=self._config.folder_number or EMPTY,
                    anno=self._config.folder_year or EMPTY,
                ),
            ),
            Descrizione=self._descrizione(t, application),
            ApplicativoProtocollo=t.ApplicativoProtocollo(
                nome=REGISTRY_APP,
                Parametro=[t.Parametro(nome=EMPTY, valore=EMPTY)],
            ),
        )

        result = self._docareaproto_service.Protocollazione(
            Username=self._config.username,
            DSTLogin=self._token,
            FileXML=self._prepare_segnatura(t, segnatura),
        )
        return InsertResult(result)

    @staticmethod
    def _prepare_segnatura(t: SimpleNamespace, segnatura):
        root = etree.Element('Segnatura')
        t.Segnatura.render(root, segnatura)
        rendered = etree.tostring(root, encoding='utf-8', pretty_print=True, xml_declaration=True).decode()
        rendered = rendered.replace('<_value_1>', EMPTY)
        rendered = rendered.replace('</_value_1>', EMPTY)
        return rendered.encode()

    def _mittente(self, t: SimpleNamespace, application):
        if application.is_inbound:
            return t.Mittente(Persona=self._persona(t, application))
        return t.Mittente(Amministrazione=self._amministrazione(t), AOO=self._aoo(t))

    def _destinatario(self, t: SimpleNamespace, application):
        if application.is_inbound:
            return t.Destinatario(Amministrazione=self._amministrazione(t), AOO=self._aoo(t))
        return t.Destinatario(Persona=self._persona(t, application))

    def _descrizione(self, t: SimpleNamespace, application):
        if application.current_attachments:
            return t.Descrizione(
                Documento=self._documento(t, application.current_document),
                Allegati=t.Allegati(
                    Documento=[self._documento(t, attachment) for attachment in application.current_attachments],
                ),
            )
        return t.Descrizione(Documento=self._documento(t, application.current_document))

    @staticmethod
    def _persona(t: SimpleNamespace, application):
        return t.Persona(
            id=application.applicant.fiscal_code,
            Nome=application.applicant.name,
            Cognome=application.applicant.surname,
            CodiceFiscale=application.applicant.fiscal_code,
            Denominazione=application.applicant.full_name,
            IndirizzoTelematico=t.IndirizzoTelematico(application.applicant.email, tipo=SMTP),
        )

    def _amministrazione(self, t: SimpleNamespace):
        return t.Amministrazione(
            Denominazione=self._config.institution_name or EMPTY,
            CodiceAmministrazione=self._config.institution_code or EMPTY,
            IndirizzoTelematico=t.IndirizzoTelematico(self._config.institution_email or EMPTY, tipo=SMTP),
            UnitaOrganizzativa=t.UnitaOrganizzativa(id=self._config.institution_ou or EMPTY),
        )

    def _aoo(self, t: SimpleNamespace):
        return t.AOO(CodiceAOO=self._config.institution_aoo_code or EMPTY)

    @staticmethod
    def _documento(t: SimpleNamespace, document):
        return t.Documento(
            id=document.attributes.remote_id,
            nome=document.original_name,
            DescrizioneDocumento=t.DescrizioneDocumento(document.original_name),
        )


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.IngErrNumber
        if errcode != SUCCESS_CODE:
            errmsg = self._result.strErrString
            raise ValidationError(errcode, errmsg)

    @property
    def document_id(self):
        return str(self._result.IngDocID)

    @property
    def number(self):
        return str(self._result.IngNumPG)

    @property
    def year(self):
        return str(self._result.IngAnnoPG)
