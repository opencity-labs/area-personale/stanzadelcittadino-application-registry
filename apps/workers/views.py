import sentry_sdk
from django.urls import reverse
from drf_spectacular.utils import extend_schema_view
from rest_framework import exceptions, response, serializers, status, views

from apps.accounts.permissions import HasValidAPIKey
from apps.workers import schema
from apps.workers.datagraph.job import run as datagraph_run
from apps.workers.dedagroup.job import run as dedagroup_run
from apps.workers.halley.job import run as halley_run
from apps.workers.halley_cloud.job import run as halley_cloud_run
from apps.workers.maggioli.job import run as maggioli_run


class BaseSubmissionAPIView(views.APIView):
    permission_classes = (HasValidAPIKey,)

    def post(self, request):
        serializer = serializers.Serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            self.process(request.data)
        except Exception:
            sentry_sdk.capture_exception()
            raise exceptions.ValidationError()
        result_url = request.build_absolute_uri(reverse('admin:applications_registrationevent_changelist'))
        return response.Response(
            {'done': f'Request processed, please check {result_url} for the result.'},
            status=status.HTTP_200_OK,
        )

    def process(self, data):
        pass


@extend_schema_view(**schema.SUBMISSION_APIVIEW_OVERRIDES)
class DatagraphSubmissionAPIView(BaseSubmissionAPIView):
    process = staticmethod(datagraph_run)


@extend_schema_view(**schema.SUBMISSION_APIVIEW_OVERRIDES)
class MaggioliSubmissionAPIView(BaseSubmissionAPIView):
    process = staticmethod(maggioli_run)


@extend_schema_view(**schema.SUBMISSION_APIVIEW_OVERRIDES)
class HalleySubmissionAPIView(BaseSubmissionAPIView):
    process = staticmethod(halley_run)


@extend_schema_view(**schema.SUBMISSION_APIVIEW_OVERRIDES)
class HalleyCloudSubmissionAPIView(BaseSubmissionAPIView):
    process = staticmethod(halley_cloud_run)


@extend_schema_view(**schema.SUBMISSION_APIVIEW_OVERRIDES)
class DedagroupSubmissionAPIView(BaseSubmissionAPIView):
    process = staticmethod(dedagroup_run)
