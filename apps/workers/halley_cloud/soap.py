import logging
from datetime import datetime, timezone

import dicttoxml
from django.utils.functional import cached_property
from zeep import Client as ZeepClient
from zeep.cache import InMemoryCache
from zeep.transports import Transport

from apps.workers.errors import SOAPError, ValidationError

TIMEOUT = 30
SUCCESS_CODE = 0

logger = logging.getLogger('application_registry')


class Client:
    soap_client_class = ZeepClient
    cache = InMemoryCache()

    def __init__(self, config):
        self._config = config

    @cached_property
    def _registration_soap_client(self):
        wsdl = f'{self._config.registration_url}?wsdl'
        return self.soap_client_class(
            wsdl=wsdl,
            transport=Transport(cache=self.cache, timeout=TIMEOUT, operation_timeout=TIMEOUT),
        )

    def insert(self, application):
        try:
            return self._insert(application)
        except ValidationError:
            raise
        except Exception as e:
            raise SOAPError(e) from e

    def _insert(self, application):
        if not self._config.classification_category:
            classification = ''
        elif not self._config.classification_class:
            classification = f'{self._config.classification_category}'
        elif not self._config.classification_subclass:
            classification = f'{self._config.classification_category}.{self._config.classification_class}'
        else:
            classification = (
                f'{self._config.classification_category}.{self._config.classification_class}.'
                f'{self._config.classification_subclass}'
            )

        # utilizziamo una lista di uffici in previsione
        # dell'implementazione da parte di halley della
        # gestione di più uffici per il momento l'inserimento
        # è limitato ad un solo elemento quindi se non vuota
        # prendiamo sempre e solo il primo elemento della lista
        office_id = next(iter(self._config.institution_office_ids or []), None)

        dati_protocollazione = {
            'Protocollo': {
                'Tipo': 'Arrivo' if application.is_inbound else 'Partenza',
                'Spedizione': 'E-mail' if self._config.mailbox else '',
                'Oggetto': application.document_description,
                'CasellaEmail': self._config.mailbox or '',
                'OggettoEmail': application.document_description,
                'Classificazione': classification,
                'Anagrafiche': {
                    'Anagrafica': {
                        'Nome': application.applicant.name,
                        'Cognome': application.applicant.surname,
                        'RagioneSociale': '',
                        'CodiceFiscale': application.applicant.fiscal_code,
                        'PartitaIva': '',
                        'Comune': application.applicant.municipality,
                        'Indirizzo': application.applicant.address,
                        'Civico': application.applicant.house_number,
                        'Provincia': application.applicant.county,
                        'Telefono': '',
                        'Email': application.applicant.email,
                    },
                },
                'Ufficio': {
                    'UfficioMittente': office_id if application.is_outbound else '',
                    'UfficioDestinat': office_id if application.is_inbound else '',
                },
                'Fascicolo': self._config.folder_name or '',
            },
        }

        segnatura = dicttoxml.dicttoxml(dati_protocollazione, custom_root='Protocollo', attr_type=False, root=False)

        file_type = self._registration_soap_client.get_type('ns0:FileType')
        result = self._registration_soap_client.service.NuovoProtocollo(
            operatore=self._config.username,
            password=self._config.password,
            segnatura=segnatura,
            filePrincipale=file_type(
                NomeFile=application.current_document.original_name,
                CidFile=application.current_document.attributes.content,
            ),
            allegati=[
                *[
                    file_type(
                        NomeFile=attachment.original_name,
                        CidFile=attachment.attributes.content,
                    )
                    for attachment in application.current_attachments
                ],
            ],
        )

        return InsertResult(result)


class InsertResult:
    def __init__(self, result):
        self._result = result
        self.created_at = datetime.now(timezone.utc)
        self._validate_error_code()

    def _validate_error_code(self):
        errcode = self._result.coderrore
        if errcode != SUCCESS_CODE:
            errmsg = self._result.deserrore
            raise ValidationError(errcode, errmsg)

    @property
    def number(self):
        return str(self._result.numero)

    @property
    def year(self):
        return str(self._result.anno)
