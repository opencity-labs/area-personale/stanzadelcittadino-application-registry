from django.core.management.base import BaseCommand

from apps.workers.management.commands._test_application import TEST_APPLICATION
from apps.workers.sicraweb_wsprotocollodm.job import run


class Command(BaseCommand):
    help = 'Sends a test application to Sicraweb WSProtocolloDM.'  # noqa

    def handle(self, *args, **options):
        run(TEST_APPLICATION)
