from apps.workers.management.commands._worker_base import WorkerCommand
from apps.workers.pitre.job import run
from apps.workers.worker import Worker


class Command(WorkerCommand):
    help = 'Starts a Pitre worker.'  # noqa

    def handle(self, *args, **options):
        Worker(callback=run).start()
