from apps.workers.management.commands._worker_base import WorkerCommand
from apps.workers.tinn.job import run
from apps.workers.worker import Worker


class Command(WorkerCommand):
    help = 'Starts a Tinn worker.'  # noqa

    def handle(self, *args, **options):
        Worker(callback=run).start()
