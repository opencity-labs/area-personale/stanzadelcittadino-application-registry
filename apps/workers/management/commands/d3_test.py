from django.core.management.base import BaseCommand

from apps.workers.d3.job import run
from apps.workers.management.commands._test_application import TEST_APPLICATION


class Command(BaseCommand):
    help = 'Sends a test application to D3.'  # noqa

    def handle(self, *args, **options):
        run(TEST_APPLICATION)
