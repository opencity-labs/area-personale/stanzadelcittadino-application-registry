import signal
import sys

from daemon import DaemonContext
from django.core.management.base import BaseCommand


class WorkerCommand(BaseCommand):
    help = 'Starts a worker process.'  # noqa

    worker_class = None
    daemon_context_class = DaemonContext

    def handle(self, *args, **options):
        worker = self.worker_class()
        self._run(worker)

    def _run(self, worker):
        self.stdout.write(f'Starting worker {worker}...')
        with self.daemon_context_class(
            detach_process=False,
            signal_map=self._get_signal_map(worker),
            stdout=sys.stdout,
            stderr=sys.stderr,
        ):
            worker.start()

    def _get_signal_map(self, worker):
        def stop(signal_number, stack_frame):
            self.stdout.write(f'Stopping worker {worker}...')
            worker.stop()

        return {
            signal.SIGTERM: stop,
            signal.SIGINT: stop,
        }
