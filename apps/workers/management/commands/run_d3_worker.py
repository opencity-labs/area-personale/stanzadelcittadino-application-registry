from apps.workers.d3.job import run
from apps.workers.management.commands._worker_base import WorkerCommand
from apps.workers.worker import Worker


class Command(WorkerCommand):
    help = 'Starts a D3 worker.'  # noqa

    def handle(self, *args, **options):
        Worker(callback=run).start()
