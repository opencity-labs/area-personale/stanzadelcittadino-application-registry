from apps.workers.management.commands._worker_base import WorkerCommand
from apps.workers.retries import RetryScheduler


class Command(WorkerCommand):
    help = 'Starts a RetryScheduler worker.'  # noqa

    worker_class = RetryScheduler
