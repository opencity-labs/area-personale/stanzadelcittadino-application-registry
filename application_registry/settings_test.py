import os
import warnings

from .settings_base import *  # noqa

warnings.filterwarnings('ignore', r'No directory at', UserWarning, r'whitenoise\.base$')

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': 'application_registry',
        'USER': 'dev',
        'PASSWORD': 'dev',
        'HOST': os.getenv('DJANGO_DATABASE_HOST', '127.0.0.1'),
        'PORT': '5432',
        'CONN_MAX_AGE': float(os.getenv('DJANGO_DATABASE_CONN_MAX_AGE', 0)),
    },
}

# Application Registry

DATAGRAPH_INBOUND_TEMPLATE_CLASS = 'tests._helpers.TestInboundTemplate'
DATAGRAPH_OUTBOUND_TEMPLATE_CLASS = 'tests._helpers.TestOutboundTemplate'
MAGGIOLI_INBOUND_TEMPLATE_CLASS = 'tests._helpers.TestInboundTemplate'
MAGGIOLI_OUTBOUND_TEMPLATE_CLASS = 'tests._helpers.TestOutboundTemplate'

KAFKA_BOOTSTRAP_SERVERS = ['kafka:9092']
KAFKA_API_VERSION = os.getenv('KAFKA_API_VERSION', '0.11.0')
KAFKA_TOPIC_NAME = 'applications'
KAFKA_DESTINATION_TOPIC_NAME = 'documents'
KAFKA_GROUP_ID = os.getenv('KAFKA_GROUP_ID', 'registry_test')
KAFKA_SECURITY_PROTOCOL = 'PLAINTEXT'

APP_NAME = 'stanzadelcittadino-application-registry'
PRODUCE_D3_DOCUMENT = os.getenv('PRODUCE_D3_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_DATAGRAPH_DOCUMENT = os.getenv('PRODUCE_DATAGRAPH_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_DEDAGROUP_DOCUMENT = os.getenv('PRODUCE_DEDAGROUP_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HALLEY_DOCUMENT = os.getenv('PRODUCE_HALLEY_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HALLEY_CLOUD_DOCUMENT = os.getenv('PRODUCE_HALLEY_CLOUD_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HYPERSIC_DOCUMENT = os.getenv('PRODUCE_HYPERSIC_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_INFOR_DOCUMENT = os.getenv('PRODUCE_INFOR_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_INSIEL_DOCUMENT = os.getenv('PRODUCE_INSIEL_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_MAGGIOLI_DOCUMENT = os.getenv('PRODUCE_MAGGIOLI_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_PITRE_DOCUMENT = os.getenv('PRODUCE_PITRE_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT = (
    os.getenv('PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT', 'false').lower().strip() == 'true'
)
PRODUCE_SISCOM_DOCUMENT = os.getenv('PRODUCE_SISCOM_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_TINN_DOCUMENT = os.getenv('PRODUCE_TINN_DOCUMENT', 'false').lower().strip() == 'true'

RETRY_PRODUCER_CLIENT_ID = 'retry_producer_test'
RETRY_TOPIC_10 = 'retry_10_test'
RETRY_TOPIC_30 = 'retry_30_test'
RETRY_TOPIC_180 = 'retry_180_test'
RETRY_TOPIC_720 = 'retry_720_test'
RETRY_SCHEDULER_SOURCE_TOPICS = [RETRY_TOPIC_10, RETRY_TOPIC_30, RETRY_TOPIC_180, RETRY_TOPIC_720]
RETRY_SCHEDULER_DESTINATION_TOPIC = 'applications_test'
RETRY_SCHEDULER_CONSUMER_CLIENT_ID = 'retry_scheduler_consumer_test'
RETRY_SCHEDULER_CONSUMER_GROUP_ID = 'retry_scheduler_consumer_group_test'
RETRY_SCHEDULER_PRODUCER_CLIENT_ID = 'retry_scheduler_producer_test'

GRAVEL_GATEWAY_HOST = 'gravel-test:4278'
GRAVEL_GATEWAY_PASSWORD = 's3cr3t'
