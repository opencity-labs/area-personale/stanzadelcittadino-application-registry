from .settings_base import *  # noqa

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': 'application_registry',
        'USER': 'dev',
        'PASSWORD': 'dev',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}

# Application Registry
APP_NAME = 'stanzadelcittadino-application-registry'
KAFKA_BOOTSTRAP_SERVERS = ['localhost:19092']
KAFKA_API_VERSION = '0.11.0'
KAFKA_SECURITY_PROTOCOL = 'PLAINTEXT'
KAFKA_GROUP_ID = 'registry_dev'
KAFKA_TOPIC_NAME = 'applications'
KAFKA_DESTINATION_TOPIC_NAME = 'documents'

PRODUCE_D3_DOCUMENT = True
PRODUCE_DATAGRAPH_DOCUMENT = True
PRODUCE_DEDAGROUP_DOCUMENT = True
PRODUCE_HALLEY_DOCUMENT = True
PRODUCE_HALLEY_CLOUD_DOCUMENT = True
PRODUCE_HYPERSIC_DOCUMENT = True
PRODUCE_INFOR_DOCUMENT = True
PRODUCE_INSIEL_DOCUMENT = True
PRODUCE_MAGGIOLI_DOCUMENT = True
PRODUCE_PITRE_DOCUMENT = True
PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT = True
PRODUCE_SISCOM_DOCUMENT = True
PRODUCE_TINN_DOCUMENT = True

RETRY_PRODUCER_CLIENT_ID = 'retry_producer_dev'
RETRY_TOPIC_10 = 'retry_10_dev'
RETRY_TOPIC_30 = 'retry_30_dev'
RETRY_TOPIC_180 = 'retry_180_dev'
RETRY_TOPIC_720 = 'retry_720_dev'
RETRY_SCHEDULER_SOURCE_TOPICS = [RETRY_TOPIC_10, RETRY_TOPIC_30, RETRY_TOPIC_180, RETRY_TOPIC_720]
RETRY_SCHEDULER_DESTINATION_TOPIC = 'applications_dev'
RETRY_SCHEDULER_CONSUMER_CLIENT_ID = 'retry_scheduler_consumer_dev'
RETRY_SCHEDULER_CONSUMER_GROUP_ID = 'retry_scheduler_consumer_group_dev'
RETRY_SCHEDULER_PRODUCER_CLIENT_ID = 'retry_scheduler_producer_dev'

GRAVEL_GATEWAY_HOST = None
