from .settings_test import *  # noqa

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': 'application_registry',
        'USER': 'ci',
        'PASSWORD': 'ci',
        'HOST': 'postgres',
        'PORT': '5432',
    },
}

# unittest-xml-reporting

TEST_RUNNER = 'xmlrunner.extra.djangotestrunner.XMLTestRunner'
TEST_OUTPUT_FILE_NAME = 'tests.xml'
