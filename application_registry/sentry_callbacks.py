def filter_out_kafka_fetcher_noise(event, hint):
    # Workaround for https://github.com/dpkp/kafka-python/issues/1583
    try:
        has_error_level = event['level'] == 'error'
        has_fetcher_logger = event['logger'] == 'kafka.consumer.fetcher'
        has_exact_message = event['logentry']['message'] == 'Fetch to node %s failed: %s'
    except Exception:
        return event
    if has_error_level and has_fetcher_logger and has_exact_message:
        return None
    return event
