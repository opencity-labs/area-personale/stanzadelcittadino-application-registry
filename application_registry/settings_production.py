import os

import sentry_sdk

from .sentry_callbacks import filter_out_kafka_fetcher_noise
from .settings_base import *  # noqa

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')

DEBUG = False

ALLOWED_HOSTS = ['*']
CSRF_TRUSTED_ORIGINS = [s.strip() for s in os.getenv('CSRF_TRUSTED_ORIGINS').split(',')]

CSRF_COOKIE_NAME = 'csrftoken'
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = False
CSRF_USE_SESSIONS = False

dbhost, dbport, dbname, dbuser, dbpassword = (s.strip() for s in os.getenv('DJANGO_DATABASE').split(':', 4))
DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': dbname,
        'USER': dbuser,
        'PASSWORD': dbpassword,
        'HOST': dbhost,
        'PORT': dbport,
        'CONN_MAX_AGE': float(os.getenv('DJANGO_DATABASE_CONN_MAX_AGE', 0)),
    },
}

# Sentry

enable_before_send = os.getenv('ENABLE_SENTRY_EVENT_FILTER', 'false').lower().strip() == 'true'
sentry_sdk.init(
    dsn=SENTRY_DSN,  # noqa
    integrations=SENTRY_INTEGRATIONS,  # noqa
    environment='production',
    before_send=filter_out_kafka_fetcher_noise if enable_before_send else None,
)

# Application Registry
APP_NAME = 'stanzadelcittadino-application-registry'
KAFKA_BOOTSTRAP_SERVERS = [s.strip() for s in os.getenv('KAFKA_BOOTSTRAP_SERVERS').split(',')]
KAFKA_API_VERSION = os.getenv('KAFKA_API_VERSION', '0.11.0')
KAFKA_TOPIC_NAME = os.getenv('KAFKA_TOPIC_NAME')
KAFKA_DESTINATION_TOPIC_NAME = os.getenv('KAFKA_DESTINATION_TOPIC_NAME')
KAFKA_GROUP_ID = os.getenv('KAFKA_GROUP_ID')
KAFKA_SECURITY_PROTOCOL = os.getenv('KAFKA_SECURITY_PROTOCOL', 'SSL')

PRODUCE_D3_DOCUMENT = os.getenv('PRODUCE_D3_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_DATAGRAPH_DOCUMENT = os.getenv('PRODUCE_DATAGRAPH_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_DEDAGROUP_DOCUMENT = os.getenv('PRODUCE_DEDAGROUP_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HALLEY_DOCUMENT = os.getenv('PRODUCE_HALLEY_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HALLEY_CLOUD_DOCUMENT = os.getenv('PRODUCE_HALLEY_CLOUD_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_HYPERSIC_DOCUMENT = os.getenv('PRODUCE_HYPERSIC_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_INFOR_DOCUMENT = os.getenv('PRODUCE_INFOR_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_INSIEL_DOCUMENT = os.getenv('PRODUCE_INSIEL_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_MAGGIOLI_DOCUMENT = os.getenv('PRODUCE_MAGGIOLI_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_PITRE_DOCUMENT = os.getenv('PRODUCE_PITRE_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT = (
    os.getenv('PRODUCE_SICRAWEB_WSPROTOCOLLODM_DOCUMENT', 'false').lower().strip() == 'true'
)
PRODUCE_SISCOM_DOCUMENT = os.getenv('PRODUCE_SISCOM_DOCUMENT', 'false').lower().strip() == 'true'
PRODUCE_TINN_DOCUMENT = os.getenv('PRODUCE_TINN_DOCUMENT', 'false').lower().strip() == 'true'

RETRY_PRODUCER_CLIENT_ID = 'retry_producer_production'
RETRY_TOPIC_10 = 'retry_10_production'
RETRY_TOPIC_30 = 'retry_30_production'
RETRY_TOPIC_180 = 'retry_180_production'
RETRY_TOPIC_720 = 'retry_720_production'
RETRY_SCHEDULER_SOURCE_TOPICS = [RETRY_TOPIC_10, RETRY_TOPIC_30, RETRY_TOPIC_180, RETRY_TOPIC_720]
RETRY_SCHEDULER_DESTINATION_TOPIC = 'applications'
RETRY_SCHEDULER_CONSUMER_CLIENT_ID = 'retry_scheduler_consumer_production'
RETRY_SCHEDULER_CONSUMER_GROUP_ID = 'retry_scheduler_consumer_group_production'
RETRY_SCHEDULER_PRODUCER_CLIENT_ID = 'retry_scheduler_producer_production'

GRAVEL_GATEWAY_HOST = os.getenv('GRAVEL_GATEWAY_HOST')
GRAVEL_GATEWAY_PASSWORD = os.getenv('GRAVEL_GATEWAY_PASSWORD', '')
