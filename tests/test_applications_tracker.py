import uuid
from datetime import datetime, timedelta, timezone
from types import SimpleNamespace
from uuid import UUID

from django.test import SimpleTestCase, TestCase
from freezegun import freeze_time

from apps.applications.kafka import RetryProducer
from apps.applications.models import DeadLetter, RegistrationEvent
from apps.applications.sdc.entities import Application
from apps.applications.tracker import RegistrationEventTracker

from ._helpers import (
    RETRY_DATA_VALUE,
    GravelGatewayTestClient,
    TestKafkaProducer,
    TestRetryProducer,
    get_application_data,
)

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class RegistrationEventTrackerTestCase(TestCase):
    def test_success(self):
        application = Application(get_application_data())
        config = SimpleNamespace(id=1, provider='acme', tenant_slug='comune-di-bugliano')
        retry_producer = TestRetryProducer()
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with RegistrationEventTracker(
            document_id=document_id,
            application=application,
            config=config,
            retry_producer=retry_producer,
            gravel_client=gravel_client,
        ):
            pass
        event = RegistrationEvent.objects.get()
        self.assertEqual(event.provider, config.provider)
        self.assertEqual(event.config_id, config.id)
        self.assertEqual(event.application_sdc_id, UUID(application.id))
        self.assertEqual(event.event_sdc_id, UUID(application.event_id))
        self.assertEqual(event.data, application.raw)

        self.assertEqual(event.state, RegistrationEvent.State.COMPLETED)
        self.assertIsNone(event.traceback)
        self.assertIsNone(event.retry_traceback)

        retry_producer.assert_send_not_called()
        self.assertFalse(DeadLetter._base_manager.exists())

        gravel_client.assert_incr_retries_not_called()
        gravel_client.assert_incr_dead_lettered_not_called()

    def test_failure_then_retry(self):
        application = Application(get_application_data())
        config = SimpleNamespace(id=1, provider=SimpleNamespace(value='acme'), tenant_slug='comune-di-bugliano')
        retry_producer = TestRetryProducer()
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with freeze_time(NOW):
            with RegistrationEventTracker(
                document_id,
                application,
                config,
                retry_producer=retry_producer,
                gravel_client=gravel_client,
            ):
                1 / 0
        event = RegistrationEvent.objects.get()
        self.assertEqual(event.provider, str(config.provider))
        self.assertEqual(event.config_id, config.id)
        self.assertEqual(event.application_sdc_id, UUID(application.id))
        self.assertEqual(event.event_sdc_id, UUID(application.event_id))
        self.assertEqual(event.data, application.raw)

        self.assertEqual(event.state, RegistrationEvent.State.FAILED)
        self.assertTrue(
            event.traceback.startswith('Traceback (most recent call last):')
            and event.traceback.endswith('ZeroDivisionError: division by zero\n'),
        )
        self.assertIsNone(event.retry_traceback)

        retry_producer.assert_send('retry_180_test', RETRY_DATA_VALUE)
        self.assertFalse(DeadLetter._base_manager.exists())

        gravel_client.assert_incr_retries('comune-di-bugliano')
        gravel_client.assert_incr_dead_lettered_not_called()

    def test_failure_then_dead_lettering(self):
        application = Application(get_application_data())
        for _ in range(9):
            application.metadata.incr_attempts()
        config = SimpleNamespace(id=1, provider=SimpleNamespace(value='acme'), tenant_slug='comune-di-bugliano')
        retry_producer = TestRetryProducer()
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with RegistrationEventTracker(
            document_id=document_id,
            application=application,
            config=config,
            retry_producer=retry_producer,
            gravel_client=gravel_client,
        ):
            1 / 0
        event = RegistrationEvent.objects.get()
        self.assertEqual(event.provider, str(config.provider))
        self.assertEqual(event.config_id, config.id)
        self.assertEqual(event.application_sdc_id, UUID(application.id))
        self.assertEqual(event.event_sdc_id, UUID(application.event_id))
        self.assertEqual(event.data, application.raw)

        self.assertEqual(event.state, RegistrationEvent.State.FAILED)
        self.assertTrue(
            event.traceback.startswith('Traceback (most recent call last):')
            and event.traceback.endswith('ZeroDivisionError: division by zero\n'),
        )
        self.assertIsNone(event.retry_traceback)

        retry_producer.assert_send_not_called()
        dead_letter = DeadLetter._base_manager.get()
        self.assertEqual(dead_letter.tenant_sdc_id, UUID(application.tenant_id))
        self.assertEqual(dead_letter.service_sdc_id, UUID(application.service_id))
        self.assertEqual(dead_letter.application_sdc_id, UUID(application.id))
        self.assertEqual(dead_letter.state, DeadLetter.State.NEW)
        self.assertEqual(dead_letter.data, application.dead_letter_data)
        self.assertFalse(dead_letter.is_deleted)

        gravel_client.assert_incr_retries_not_called()
        gravel_client.assert_incr_dead_lettered('comune-di-bugliano')

    def test_retry_failure(self):
        application = Application(get_application_data())
        config = SimpleNamespace(id=1, provider=SimpleNamespace(value='acme'), tenant_slug='comune-di-bugliano')
        retry_producer = TestRetryProducer(send_error=Exception('xyz failed'))
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with RegistrationEventTracker(
            document_id=document_id,
            application=application,
            config=config,
            retry_producer=retry_producer,
            gravel_client=gravel_client,
        ):
            1 / 0
        event = RegistrationEvent.objects.get()
        self.assertIsNotNone(event.retry_traceback)
        self.assertTrue(
            event.retry_traceback.startswith('Traceback (most recent call last):')
            and event.retry_traceback.endswith('Exception: xyz failed\n'),
        )

        gravel_client.assert_incr_retries('comune-di-bugliano')

    def test_latency_metric_updated_on_first_attempt(self):
        data = get_application_data()
        data.pop('__registry_metadata')
        application = Application(data)
        config = SimpleNamespace(id=1, provider='acme')
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with freeze_time(NOW):
            with RegistrationEventTracker(document_id, application, config, gravel_client=gravel_client):
                pass
        gravel_client.assert_set_latency(timedelta(days=6, seconds=76394, microseconds=123456))

    def test_latency_metric_ignored_on_retry(self):
        application = Application(get_application_data())
        config = SimpleNamespace(id=1, provider='acme')
        gravel_client = GravelGatewayTestClient()
        document_id = uuid.uuid4()
        with RegistrationEventTracker(document_id, application, config, gravel_client=gravel_client):
            pass
        gravel_client.assert_set_latency_not_called()


class RetryProducerTestCase(SimpleTestCase):
    def test_context_management_on_success(self):
        producer = TestKafkaProducer()

        with RetryProducer(producer_class=producer):
            producer.assert_call(
                bootstrap_servers=['kafka:9092'],
                client_id='retry_producer_test',
                security_protocol='PLAINTEXT',
            )

        producer.assert_close()

    def test_context_management_on_error(self):
        producer = TestKafkaProducer()

        with self.assertRaises(ZeroDivisionError):
            with RetryProducer(producer_class=producer):
                producer.assert_call(
                    bootstrap_servers=['kafka:9092'],
                    client_id='retry_producer_test',
                    security_protocol='PLAINTEXT',
                )

                1 / 0

        producer.assert_close()

    def test_send(self):
        producer = TestKafkaProducer()

        with RetryProducer(producer_class=producer) as retry_producer:
            retry_producer.send('the_topic', b'value')

        producer.assert_send('the_topic', b'value')
        producer.assert_flush(timeout=10)
