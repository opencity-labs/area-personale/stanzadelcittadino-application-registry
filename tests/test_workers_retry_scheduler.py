from datetime import datetime, timedelta, timezone

from django.test import SimpleTestCase
from freezegun import freeze_time
from kafka.consumer.fetcher import NoOffsetForPartitionError
from kafka.structs import OffsetAndMetadata, TopicPartition

from apps.workers.retries import ApplicationRelay, DeferredApplication, RetryScheduler, Schedule, TooSoonError
from tests._helpers import (
    COMMIT,
    RETRY_DATA_VALUE,
    SEND,
    TestApplicationRelay,
    TestClock,
    TestDeferredApplication,
    TestKafkaConsumer,
    TestKafkaProducer,
    TestKafkaRecord,
)

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
READY_AT = NOW + timedelta(hours=3)


class RetrySchedulerTestCase(SimpleTestCase):
    def setUp(self):
        self.all_topics = ['retry_10_test', 'retry_30_test', 'retry_180_test', 'retry_720_test']

    def test_str(self):
        self.assertEqual(str(RetryScheduler()), 'RetryScheduler')

    def test_start_with_initial_state(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(next_ready_at=READY_AT), TestClock()
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW) as frozen_time:
            scheduler.start(iterations=1)

            frozen_time.move_to(READY_AT - timedelta(seconds=1))
            self.assertEqual(schedule.ready_topics, [])

            frozen_time.move_to(READY_AT)
            self.assertEqual(schedule.ready_topics, self.all_topics)

    def test_start_with_all_topics_paused(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(next_ready_at=READY_AT), TestClock()
        for topic in self.all_topics:
            schedule.set(topic, NOW + timedelta(days=1))
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW) as frozen_time:
            scheduler.start(iterations=1)

            frozen_time.move_to(READY_AT)
            self.assertEqual(schedule.ready_topics, [])

    def test_start_with_some_topics_ready(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(next_ready_at=READY_AT), TestClock()
        schedule.set('retry_10_test', NOW + timedelta(days=1))  # paused
        schedule.set('retry_30_test', NOW + timedelta(days=1))  # paused
        schedule.set('retry_180_test', NOW)  # ready
        schedule.set('retry_720_test', NOW)  # ready
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW) as frozen_time:
            scheduler.start(iterations=1)

            frozen_time.move_to(READY_AT - timedelta(seconds=1))
            self.assertEqual(schedule.ready_topics, [])

            frozen_time.move_to(READY_AT)
            self.assertEqual(schedule.ready_topics, ['retry_180_test', 'retry_720_test'])

    def test_start_with_all_topics_ready(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(next_ready_at=READY_AT), TestClock()
        for topic in self.all_topics:
            schedule.set(topic, NOW)
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW) as frozen_time:
            scheduler.start(iterations=1)

            frozen_time.move_to(READY_AT - timedelta(seconds=1))
            self.assertEqual(schedule.ready_topics, [])

            frozen_time.move_to(READY_AT)
            self.assertEqual(schedule.ready_topics, self.all_topics)

    def test_start_with_move_error(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(move_error=Exception), TestClock()
        schedule.set('retry_10_test', NOW + timedelta(days=1))  # paused
        schedule.set('retry_30_test', NOW + timedelta(days=1))  # paused
        schedule.set('retry_180_test', NOW)  # ready
        schedule.set('retry_720_test', NOW)  # ready
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW):
            scheduler.start(iterations=1)

            self.assertEqual(schedule.ready_topics, ['retry_180_test', 'retry_720_test'])

    def test_stop(self):
        schedule, relay, clock = Schedule(), TestApplicationRelay(), TestClock()
        scheduler = RetryScheduler(schedule=schedule, relay=relay, clock=clock)

        with freeze_time(NOW):
            scheduler.stop()

            scheduler.start(iterations=1)
            self.assertEqual(schedule.ready_topics, self.all_topics)


class ScheduleTestCase(SimpleTestCase):
    def setUp(self):
        self.all_topics = ['retry_10_test', 'retry_30_test', 'retry_180_test', 'retry_720_test']

    def test_readiness(self):
        schedule = Schedule()
        schedule.set('retry_10_test', NOW)

        for delta, is_ready in [(-1, False), (0, True), (1, True)]:
            with self.subTest(delta=delta), freeze_time(NOW + timedelta(seconds=delta)):
                self.assertIs('retry_10_test' in schedule.ready_topics, is_ready)

    def test_initial_state(self):
        schedule = Schedule()

        with freeze_time(NOW):
            self.assertEqual(schedule.ready_topics, self.all_topics)

    def test_all_topics_paused(self):
        schedule = Schedule()
        for topic in self.all_topics:
            schedule.set(topic, NOW + timedelta(seconds=1))

        with freeze_time(NOW):
            self.assertEqual(schedule.ready_topics, [])

    def test_some_topics_ready(self):
        schedule = Schedule()
        schedule.set('retry_10_test', NOW + timedelta(seconds=1))  # paused
        schedule.set('retry_30_test', NOW + timedelta(seconds=1))  # paused
        schedule.set('retry_180_test', NOW)  # ready
        schedule.set('retry_720_test', NOW)  # ready

        with freeze_time(NOW):
            self.assertEqual(schedule.ready_topics, ['retry_180_test', 'retry_720_test'])

    def test_all_topics_ready(self):
        schedule = Schedule()
        for topic in self.all_topics:
            schedule.set(topic, NOW)

        with freeze_time(NOW):
            self.assertEqual(schedule.ready_topics, self.all_topics)


class ApplicationRelayTestCase(SimpleTestCase):
    def test_context_management_on_success(self):
        consumer, producer = TestKafkaConsumer(), TestKafkaProducer()

        with ApplicationRelay(consumer_class=consumer, producer_class=producer):
            consumer.assert_call(
                bootstrap_servers=['kafka:9092'],
                client_id='retry_scheduler_consumer_test',
                group_id='retry_scheduler_consumer_group_test',
                enable_auto_commit=False,
                consumer_timeout_ms=5000,
                auto_offset_reset='none',
                security_protocol='PLAINTEXT',
            )
            producer.assert_call(
                bootstrap_servers=['kafka:9092'],
                client_id='retry_scheduler_producer_test',
                security_protocol='PLAINTEXT',
            )

        consumer.assert_close()
        producer.assert_close()

    def test_context_management_on_error(self):
        consumer, producer = TestKafkaConsumer(), TestKafkaProducer()

        with self.assertRaises(ZeroDivisionError):
            with ApplicationRelay(consumer_class=consumer, producer_class=producer):
                consumer.assert_call(
                    bootstrap_servers=['kafka:9092'],
                    client_id='retry_scheduler_consumer_test',
                    group_id='retry_scheduler_consumer_group_test',
                    enable_auto_commit=False,
                    consumer_timeout_ms=5000,
                    auto_offset_reset='none',
                    security_protocol='PLAINTEXT',
                )
                producer.assert_call(
                    bootstrap_servers=['kafka:9092'],
                    client_id='retry_scheduler_producer_test',
                    security_protocol='PLAINTEXT',
                )

                1 / 0

        consumer.assert_close()
        producer.assert_close()

    def test_move_ready_applications_of_empty_topic(self):
        consumer, producer = TestKafkaConsumer(), TestKafkaProducer()
        applications = (a for a in [])

        with freeze_time(NOW):
            with ApplicationRelay(
                consumer_class=consumer,
                producer_class=producer,
                application_class=lambda *a, **kw: next(applications)(*a, **kw),
            ) as relay:
                next_ready_at = relay.move_ready_applications('the_topic')

        self.assertEqual(next_ready_at, NOW + timedelta(seconds=10))

        consumer.assert_subscribe(['the_topic'])

    def test_move_ready_applications_until_empty(self):
        r1, r2 = object(), object()
        consumer, producer = TestKafkaConsumer(records=[r1, r2]), TestKafkaProducer()
        a1, a2 = TestDeferredApplication(), TestDeferredApplication()
        applications = (a for a in [a1, a2])

        with freeze_time(NOW):
            with ApplicationRelay(
                consumer_class=consumer,
                producer_class=producer,
                application_class=lambda *a, **kw: next(applications)(*a, **kw),
            ) as relay:
                next_ready_at = relay.move_ready_applications('the_topic')

        self.assertEqual(next_ready_at, NOW + timedelta(seconds=10))

        consumer.assert_subscribe(['the_topic'])

        a1.assert_call(r1, relay)
        a1.assert_move()
        a2.assert_call(r2, relay)
        a2.assert_move()
        self.assertLess(a1.move_call_order, a2.move_call_order)

    def test_move_ready_applications_until_too_soon(self):
        r1, r2, r3 = object(), object(), object()
        consumer, producer = TestKafkaConsumer(records=[r1, r2, r3]), TestKafkaProducer()
        ready_at = NOW + timedelta(seconds=600)
        a1, a2, a3 = (
            TestDeferredApplication(),
            TestDeferredApplication(move_error=TooSoonError(ready_at=ready_at)),
            TestDeferredApplication(),
        )
        applications = (a for a in [a1, a2, a3])

        with ApplicationRelay(
            consumer_class=consumer,
            producer_class=producer,
            application_class=lambda *a, **kw: next(applications)(*a, **kw),
        ) as relay:
            next_ready_at = relay.move_ready_applications('the_topic')

        self.assertEqual(next_ready_at, ready_at)

        consumer.assert_subscribe(['the_topic'])

        a1.assert_call(r1, relay)
        a1.assert_move()
        a2.assert_call(r2, relay)
        a2.assert_move()
        self.assertLess(a1.move_call_order, a2.move_call_order)
        a3.assert_not_called()

    def test_no_offset_for_partition_error(self):
        tp = TopicPartition(topic='the_topic', partition=0)
        consumer, producer = (
            TestKafkaConsumer(records=[object()], iter_error=NoOffsetForPartitionError(tp)),
            TestKafkaProducer(),
        )
        application = TestDeferredApplication()

        with freeze_time(NOW):
            with ApplicationRelay(
                consumer_class=consumer,
                producer_class=producer,
                application_class=application,
            ) as relay:
                next_ready_at = relay.move_ready_applications('the_topic')

        self.assertEqual(next_ready_at, NOW + timedelta(seconds=10))

        consumer.assert_subscribe(['the_topic'])
        consumer.assert_commit(offsets={tp: OffsetAndMetadata(offset=42, metadata='')})

        application.assert_not_called()

    def test_send(self):
        consumer, producer = TestKafkaConsumer(), TestKafkaProducer()

        with ApplicationRelay(consumer_class=consumer, producer_class=producer) as relay:
            relay.send(b'value')

        producer.assert_send('applications_test', b'value')
        producer.assert_flush(timeout=10)

    def test_commit(self):
        consumer, producer = TestKafkaConsumer(), TestKafkaProducer()

        with ApplicationRelay(consumer_class=consumer, producer_class=producer) as relay:
            relay.commit(topic='the_topic', partition=0, offset=42)

        consumer.assert_commit(
            offsets={TopicPartition(topic='the_topic', partition=0): OffsetAndMetadata(offset=43, metadata='')},
        )


class DeferredApplicationTestCase(SimpleTestCase):
    def test_move_ready(self):
        record = TestKafkaRecord(topic='retry_180_test', partition=0, offset=42, value=RETRY_DATA_VALUE)

        for delta in (0, 1):
            with self.subTest(delta=delta), freeze_time(READY_AT + timedelta(seconds=delta)):
                relay = TestApplicationRelay()

                DeferredApplication(record, relay).move()

                relay.assert_send(RETRY_DATA_VALUE)
                relay.assert_commit(topic='retry_180_test', partition=0, offset=42)

    def test_move_too_soon(self):
        record = TestKafkaRecord(topic='retry_180_test', partition=0, offset=42, value=RETRY_DATA_VALUE)
        relay = TestApplicationRelay()

        with freeze_time(READY_AT - timedelta(seconds=1)):
            with self.assertRaises(TooSoonError) as e:
                DeferredApplication(record, relay).move()

                self.assertEqual(e.ready_at, READY_AT)

        relay.assert_send_not_called()
        relay.assert_commit_not_called()

    def test_at_least_once_semantics(self):
        record = TestKafkaRecord(topic='retry_180_test', partition=0, offset=42, value=RETRY_DATA_VALUE)
        relay = TestApplicationRelay()

        with freeze_time(READY_AT):
            DeferredApplication(record, relay).move()

        self.assertEqual(relay.send_commit_sequence, [SEND, COMMIT])
