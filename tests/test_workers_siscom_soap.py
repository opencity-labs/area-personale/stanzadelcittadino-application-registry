from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.siscom.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(
                {
                    'schema': None,
                    '_value_1': {
                        '_value_1': [
                            {
                                'RegisterNewProto': SimpleNamespace(
                                    CodErrore=None,
                                    DescErrore='0',
                                    CodRis='E3',
                                    DescRis='OPERAZIONE NON RIUSCITA',
                                    ProtoID=None,
                                    ProtoData=None,
                                    ProtoAnno=None,
                                    ProtoNum=None,
                                ),
                            },
                        ],
                    },
                },
                service='RegisterNewProto',
            )

    @freeze_time(NOW)
    def test_register_success(self):
        result = InsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'RegisterNewProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                ProtoID='1856',
                                ProtoData='14/11/2023',
                                ProtoAnno='2023',
                                ProtoNum='91',
                            ),
                        },
                    ],
                },
            },
            service='RegisterNewProto',
        )
        self.assertEqual(result.number, '91')
        self.assertEqual(result.year, '2023')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_upload_success(self):
        result = InsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'AppendFileToProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                IDDocFile='1738',
                            ),
                        },
                    ],
                },
            },
            service='AppendFileToProto',
        )
        self.assertEqual(result.document_id, '1738')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_collate_success(self):
        result = InsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'SetFascDigitaleToProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                IDDocFascDigit='168',
                            ),
                        },
                    ],
                },
            },
            service='SetFascDigitaleToProto',
        )
        self.assertEqual(result.document_id_in_folder, '168')
        self.assertEqual(result.created_at, NOW)
