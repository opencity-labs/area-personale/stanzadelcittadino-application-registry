import sys
import uuid
from contextlib import contextmanager
from datetime import datetime, timedelta, timezone
from uuid import uuid4

from dateutil import tz
from django.conf import settings
from django.db import IntegrityError, transaction
from django.db.models import Manager, ProtectedError
from django.test import TestCase
from freezegun import freeze_time

from apps.applications import errors
from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    MaggioliConfig,
    Registration,
    RegistrationEvent,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)
from apps.applications.sdc.client import Credentials as SDCCredentials
from apps.applications.sdc.entities import Application
from apps.workers.datagraph import soap

from ._helpers import (
    BASE_URL,
    D3TestClient,
    DatagraphTestClient,
    DedagroupTestClient,
    HalleyCloudTestClient,
    HalleyTestClient,
    HypersicTestClient,
    InforTestClient,
    MaggioliTestClient,
    SDCTestClient,
    SicrawebWSProtocolloDMTestClient,
    SiscomTestClient,
    TestRetryProducer,
    TinnTestClient,
    get_application_data,
    get_application_state_data,
    get_d3_config_data,
    get_datagraph_config_data,
    get_dead_letter_data,
    get_dedagroup_config_data,
    get_halley_cloud_config_data,
    get_halley_config_data,
    get_hypersic_config_data,
    get_infor_config_data,
    get_maggioli_config_data,
    get_registration_data,
    get_registration_event_data,
    get_service_data,
    get_sicraweb_wsprotocollodm_config_data,
    get_siscom_config_data,
    get_tenant_data,
    get_tinn_config_data,
    stub_retry_producer_class,
)

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


@contextmanager
def _stub_sdc_client_class(stub):
    original, Tenant.sdc_client_class = Tenant.sdc_client_class, stub
    try:
        yield
    finally:
        Tenant.sdc_client_class = original


@contextmanager
def _stub_datagraph_client_class(stub):
    original, DatagraphConfig.datagraph_client_class = DatagraphConfig.datagraph_client_class, stub
    try:
        yield
    finally:
        DatagraphConfig.datagraph_client_class = original


@contextmanager
def _stub_maggioli_client_class(stub):
    original, MaggioliConfig.maggioli_client_class = MaggioliConfig.maggioli_client_class, stub
    try:
        yield
    finally:
        MaggioliConfig.maggioli_client_class = original


@contextmanager
def _stub_halley_client_class(stub):
    original, HalleyConfig.halley_client_class = HalleyConfig.halley_client_class, stub
    try:
        yield
    finally:
        HalleyConfig.halley_client_class = original


@contextmanager
def _stub_halley_cloud_client_class(stub):
    original, HalleyCloudConfig.halley_cloud_client_class = HalleyCloudConfig.halley_cloud_client_class, stub
    try:
        yield
    finally:
        HalleyCloudConfig.halley_cloud_client_class = original


@contextmanager
def _stub_infor_client_class(stub):
    original, InforConfig.infor_client_class = InforConfig.infor_client_class, stub
    try:
        yield
    finally:
        InforConfig.infor_client_class = original


@contextmanager
def _stub_dedagroup_client_class(stub):
    original, DedagroupConfig.dedagroup_client_class = DedagroupConfig.dedagroup_client_class, stub
    try:
        yield
    finally:
        DedagroupConfig.dedagroup_client_class = original


@contextmanager
def _stub_d3_client_class(stub):
    original, D3Config.d3_client_class = D3Config.d3_client_class, stub
    try:
        yield
    finally:
        D3Config.d3_client_class = original


@contextmanager
def _stub_hypersic_client_class(stub):
    original, HypersicConfig.hypersic_client_class = HypersicConfig.hypersic_client_class, stub
    try:
        yield
    finally:
        HypersicConfig.hypersic_client_class = original


@contextmanager
def _stub_tinn_client_class(stub):
    original, TinnConfig.tinn_client_class = TinnConfig.tinn_client_class, stub
    try:
        yield
    finally:
        TinnConfig.tinn_client_class = original


@contextmanager
def _stub_sicraweb_wsprotocollodm_client_class(stub):
    original, SicrawebWSProtocolloDMConfig.sicraweb_wsprotocollodm_client_class = (
        SicrawebWSProtocolloDMConfig.sicraweb_wsprotocollodm_client_class,
        stub,
    )
    try:
        yield
    finally:
        SicrawebWSProtocolloDMConfig.sicraweb_wsprotocollodm_client_class = original


@contextmanager
def _stub_siscom_client_class(stub):
    original, SiscomConfig.siscom_client_class = (
        SiscomConfig.siscom_client_class,
        stub,
    )
    try:
        yield
    finally:
        SiscomConfig.siscom_client_class = original


class TenantTestCase(TestCase):
    def test_creation(self):
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**get_tenant_data())
        self.assertEqual(tenant.description, 'Comune Di Bugliano')
        self.assertEqual(tenant.slug, 'comune-di-bugliano')
        self.assertEqual(tenant.sdc_id, '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc')
        self.assertEqual(tenant.sdc_base_url, 'https://example.local/comune-di-bugliano/')
        self.assertEqual(tenant.sdc_username, 'johndoe')
        self.assertEqual(tenant.sdc_password, 's3cr3t')
        self.assertEqual(tenant.sdc_institution_code, 'SDC')
        self.assertEqual(tenant.sdc_aoo_code, 'P_SDC')
        self.assertEqual(tenant.latest_registration_number, 42)
        self.assertEqual(
            tenant.latest_registration_number_issued_at,
            datetime(2021, 1, 2, 10, 20, 30, 123456, tzinfo=timezone.utc),
        )
        self.assertEqual(tenant.created_at, NOW)
        self.assertEqual(tenant.modified_at, NOW)
        self.assertEqual(str(tenant), '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc')

    def test_defaults(self):
        data = get_tenant_data()
        data.pop('latest_registration_number')
        data.pop('latest_registration_number_issued_at')
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**data)
        self.assertEqual(tenant.latest_registration_number, 0)
        self.assertEqual(tenant.latest_registration_number_issued_at, NOW)

    def test_slug_uniqueness(self):
        Tenant.objects.create(**get_tenant_data(sdc_id=uuid4()))
        with self.assertRaisesMessage(
            IntegrityError,
            'Key (slug)=(comune-di-bugliano) already exists.',
        ):
            Tenant.objects.create(**get_tenant_data())

    def test_sdc_id_uniqueness(self):
        Tenant.objects.create(**get_tenant_data(slug='other-slug'))
        with self.assertRaisesMessage(
            IntegrityError,
            'Key (sdc_id)=(3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc) already exists.',
        ):
            Tenant.objects.create(**get_tenant_data())

    def test_modified_at(self):
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            tenant = Tenant.objects.create(**get_tenant_data())
            clock.move_to(future)
            tenant.save()
        self.assertEqual(tenant.created_at, NOW)
        self.assertEqual(tenant.modified_at, future)

    def test_next_registration_number(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        tzinfo = tz.gettz(settings.TIME_ZONE)
        before_midnight = datetime(2021, 12, 31, 23, 59, 59, tzinfo=tzinfo)
        midnight = datetime(2022, 1, 1, 0, 0, 0, tzinfo=tzinfo)
        after_midnight = datetime(2022, 1, 1, 0, 0, 1, tzinfo=tzinfo)

        with freeze_time(before_midnight) as clock:
            self.assertEqual(tenant.next_registration_number, 43)
            tenant.refresh_from_db()
            self.assertEqual(tenant.latest_registration_number, 43)
            self.assertEqual(tenant.latest_registration_number_issued_at, before_midnight)

            clock.move_to(midnight)
            self.assertEqual(tenant.next_registration_number, 1)
            tenant.refresh_from_db()
            self.assertEqual(tenant.latest_registration_number, 1)
            self.assertEqual(tenant.latest_registration_number_issued_at, midnight)

            clock.move_to(after_midnight)
            self.assertEqual(tenant.next_registration_number, 2)
            tenant.refresh_from_db()
            self.assertEqual(tenant.latest_registration_number, 2)
            self.assertEqual(tenant.latest_registration_number_issued_at, after_midnight)

    def test_is_registration_date_allowed(self):
        with self.subTest(), transaction.atomic():
            data = get_tenant_data()
            data.pop('register_after_date')  # Use default value.
            tenant = Tenant.objects.create(**data)
            self.assertTrue(tenant.is_registration_date_allowed(NOW))
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            tenant = Tenant.objects.create(**get_tenant_data())
            self.assertTrue(tenant.is_registration_date_allowed(NOW))
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            tenant = Tenant.objects.create(**get_tenant_data(register_after_date=NOW + timedelta(days=1)))
            self.assertFalse(tenant.is_registration_date_allowed(NOW))
            transaction.set_rollback(True)

    def test_get_document(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        document_url = f'{BASE_URL}/documents/1'
        stub = SDCTestClient()

        with _stub_sdc_client_class(stub):
            ret = tenant.get_document(document_url)

        self.assertEqual(ret, b'abc')
        stub.assert_call(
            credentials=SDCCredentials(username=tenant.sdc_username, password=tenant.sdc_password),
            base_url=tenant.sdc_base_url,
        )
        stub.assert_get_document(document_url)

    def test_update_application(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        application_id, data = 1, {'key': 'value'}
        stub = SDCTestClient()

        with _stub_sdc_client_class(stub):
            ret = tenant.update_application(application_id, data)

        self.assertIsNone(ret)
        stub.assert_call(
            credentials=SDCCredentials(username=tenant.sdc_username, password=tenant.sdc_password),
            base_url=tenant.sdc_base_url,
        )
        stub.assert_update_application(application_id, data)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        document_url = f'{BASE_URL}/documents/1'
        stub = SDCTestClient()

        with _stub_sdc_client_class(stub):
            tenant.get_document(document_url)
            tenant.get_document(document_url)

        stub.assert_call(
            credentials=SDCCredentials(username=tenant.sdc_username, password=tenant.sdc_password),
            base_url=tenant.sdc_base_url,
        )


class ServiceTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])
        self.assertEqual(service.description, 'Posizione TARI')
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.sdc_id, '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca')
        self.assertEqual(service.provider, Service.Provider.DATAGRAPH)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertEqual(service.created_at, NOW)
        self.assertEqual(service.modified_at, NOW)
        self.assertEqual(str(service), '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))
        with self.assertRaisesMessage(
            IntegrityError,
            'Key (sdc_id)=(5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca) already exists.',
        ):
            Service.objects.create(**get_service_data(tenant=tenant))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            service = Service.objects.create(**get_service_data(tenant=tenant))
            clock.move_to(future)
            service.save()
        self.assertEqual(service.created_at, NOW)
        self.assertEqual(service.modified_at, future)

    def test_tenant_on_delete_protect(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))
        with self.assertRaisesMessage(
            ProtectedError,
            "Cannot delete some instances of model 'Tenant' because they are "
            "referenced through protected foreign keys: 'Service.tenant'.",
        ):
            tenant.delete()

    def test_datagraph(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4(), provider=Service.Provider.MAGGIOLI))
        self.assertQuerySetEqual(Service.objects.datagraph(), [service])

    def test_maggioli(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.maggioli(), [service])

    def test_halley(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.halley(), [service])

    def test_halley_cloud(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.halley_cloud(), [service])

    def test_dedagroup(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.dedagroup(), [service])

    def test_insiel(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.insiel(), [service])

    def test_pitre(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.pitre(), [service])

    def test_infor(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INFOR))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.infor(), [service])

    def test_d3(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.D3))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.d3(), [service])

    def test_hypersic(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HYPERSIC))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.hypersic(), [service])

    def test_tinn(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.TINN))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.tinn(), [service])

    def test_sicraweb_wsprotocollodm(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(
            **get_service_data(tenant=tenant, provider=Service.Provider.SICRAWEB_WSPROTOCOLLODM),
        )
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.sicraweb_wsprotocollodm(), [service])

    def test_siscom(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.SISCOM))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id=uuid4()))
        self.assertQuerySetEqual(Service.objects.siscom(), [service])


class ApplicationStateTestCase(TestCase):
    def test_creation(self):
        with freeze_time(NOW):
            state = ApplicationState.objects.create(**get_application_state_data())
        self.assertEqual(state.name, 'example_state')
        self.assertEqual(state.code, '12345')
        self.assertEqual(state.direction, ApplicationState.Direction.INBOUND)
        self.assertFalse(state.integrations)
        self.assertTrue(state.default)
        self.assertTrue(state.update_sdc)
        self.assertEqual(state.created_at, NOW)
        self.assertEqual(state.modified_at, NOW)
        self.assertEqual(str(state), 'example_state/12345/inbound')

    def test_name_uniqueness(self):
        ApplicationState.objects.create(**get_application_state_data(code='9999'))
        with self.assertRaisesMessage(
            IntegrityError,
            'Key (name)=(example_state) already exists.',
        ):
            ApplicationState.objects.create(**get_application_state_data())

    def test_code_uniqueness(self):
        ApplicationState.objects.create(**get_application_state_data(name='other_state'))
        with self.assertRaisesMessage(
            IntegrityError,
            'Key (code)=(12345) already exists.',
        ):
            ApplicationState.objects.create(**get_application_state_data())

    def test_modified_at(self):
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            state = ApplicationState.objects.create(**get_application_state_data())
            clock.move_to(future)
            state.save()
        self.assertEqual(state.created_at, NOW)
        self.assertEqual(state.modified_at, future)

    def test_default(self):
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())
        self.assertQuerySetEqual(ApplicationState.objects.default(), [state])


class DatagraphConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.url, 'https://example.local/datagraph/')
        self.assertEqual(config.codente, 'COD-111')
        self.assertEqual(config.username, 'WSDOCAREA')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.institution_name, 'Comune di Bugliano - Ufficio Tributi')
        self.assertEqual(config.institution_code, 'COD-222')
        self.assertEqual(config.institution_email, 'bugliano-tributi@example.local')
        self.assertEqual(config.institution_ou, 'TRI')
        self.assertEqual(config.institution_aoo_code, 'COD-333')
        self.assertEqual(config.classification_institution_code, 'COD-444')
        self.assertEqual(config.classification_aoo_code, 'COD-555')
        self.assertEqual(config.classification_hierarchy, '7.2.0')
        self.assertEqual(config.folder_number, '42')
        self.assertEqual(config.folder_year, '2021')
        self.assertEqual(config.document_format, 'servizio online')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.DATAGRAPH)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.sdc_institution_code, 'SDC')
        self.assertEqual(config.sdc_aoo_code, 'P_SDC')
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service, is_active=False))

        try:
            DatagraphConfig.objects.create(**get_datagraph_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'classification_institution_code',
            'classification_aoo_code',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
        }
        data = get_datagraph_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = DatagraphConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        service.delete()
        self.assertEqual(DatagraphConfig.objects.count(), 0)

    def test_registration_number(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        with freeze_time(NOW, tick=True):
            for rn in ['0000043', '0000044', '0000045']:
                self.assertEqual(config.registration_number, rn)

    def test_upload(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        mimetype, data = 'application/pdf', b'abc'
        stub = DatagraphTestClient()

        with _stub_datagraph_client_class(stub):
            ret = config.upload(mimetype, data)

        self.assertEqual(ret.doc_id, '1170840885')
        stub.assert_call(
            credentials=soap.Credentials(codente=config.codente, username=config.username, password=config.password),
            url=config.url,
        )
        stub.assert_inserimento(mimetype, data)

    def test_register_inbound(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = DatagraphTestClient()

        with _stub_datagraph_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '376')
        stub.assert_call(
            credentials=soap.Credentials(codente=config.codente, username=config.username, password=config.password),
            url=config.url,
        )
        stub.assert_protocollazione(b'<Segnatura><inbound/></Segnatura>')
        self.assertEqual(Registration.objects.count(), 1)

    def test_register_outbound(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = DatagraphTestClient()

        with _stub_datagraph_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '376')
        stub.assert_call(
            credentials=soap.Credentials(codente=config.codente, username=config.username, password=config.password),
            url=config.url,
        )
        stub.assert_protocollazione(b'<Segnatura><outbound/></Segnatura>')
        self.assertEqual(Registration.objects.count(), 1)

    def test_get_types(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        stub = DatagraphTestClient()

        with _stub_datagraph_client_class(stub):
            ret = config.get_types()

        self.assertEqual(ret.Documento.name, 'doc')
        stub.assert_call(
            credentials=soap.Credentials(codente=config.codente, username=config.username, password=config.password),
            url=config.url,
        )
        stub.assert_get_types()

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        mimetype, data = 'application/pdf', b'abc'
        stub = DatagraphTestClient()

        with _stub_datagraph_client_class(stub):
            config.upload(mimetype, data)
            config.upload(mimetype, data)

        stub.assert_call(
            credentials=soap.Credentials(codente=config.codente, username=config.username, password=config.password),
            url=config.url,
        )

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        qs = DatagraphConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            DatagraphConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class MaggioliConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.url, 'https://example.local/maggioli/')
        self.assertEqual(config.connection_string, 'test_conn_string')
        self.assertEqual(config.institution_name, 'Comune di Bugliano - Ufficio Tributi')
        self.assertEqual(config.institution_code, 'COD-111')
        self.assertEqual(config.institution_email, 'bugliano-tributi@example.local')
        self.assertEqual(config.institution_ou, 'TRI')
        self.assertEqual(config.institution_aoo_code, 'COD-222')
        self.assertEqual(config.sending_mode, MaggioliConfig.SendingMode.PEC)
        self.assertEqual(config.sending_method, 'COD-333')
        self.assertEqual(config.classification_hierarchy, '7.2.0')
        self.assertEqual(config.folder_number, '42')
        self.assertEqual(config.folder_year, '2021')
        self.assertEqual(config.document_type, 'servizio online')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.MAGGIOLI)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.sdc_institution_code, 'SDC')
        self.assertEqual(config.sdc_aoo_code, 'P_SDC')
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service, is_active=False))

        try:
            MaggioliConfig.objects.create(**get_maggioli_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'sending_mode',
            'sending_method',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
        }
        data = get_maggioli_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = MaggioliConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        service.delete()
        self.assertEqual(MaggioliConfig.objects.count(), 0)

    def test_registration_number(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        with freeze_time(NOW, tick=True):
            for rn in ['0000043', '0000044', '0000045']:
                self.assertEqual(config.registration_number, rn)

    def test_upload(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        filename, mimetype, data = 'foobar.pdf', 'application/pdf', b'abc'
        stub = MaggioliTestClient()

        with _stub_maggioli_client_class(stub):
            ret = config.upload(filename, mimetype, data)

        self.assertEqual(ret.doc_id, '516187')
        stub.assert_call(connection_string=config.connection_string, url=config.url)
        stub.assert_insert_documento(filename, mimetype, data)

    def test_register_inbound(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = MaggioliTestClient()

        with _stub_maggioli_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '84')
        stub.assert_call(connection_string=config.connection_string, url=config.url)
        stub.assert_registra_protocollo(b'<Segnatura><inbound/></Segnatura>')
        self.assertEqual(Registration.objects.count(), 1)

    def test_register_outbound(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = MaggioliTestClient()

        with _stub_maggioli_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '84')
        stub.assert_call(connection_string=config.connection_string, url=config.url)
        stub.assert_registra_protocollo(b'<Segnatura><outbound/></Segnatura>')
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        filename, mimetype, data = 'foobar.pdf', 'application/pdf', b'abc'
        stub = MaggioliTestClient()

        with _stub_maggioli_client_class(stub):
            config.upload(filename, mimetype, data)
            config.upload(filename, mimetype, data)

        stub.assert_call(connection_string=config.connection_string, url=config.url)

    def test_active(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service, is_active=False))

        qs = MaggioliConfig.objects.active()

        self.assertQuerySetEqual(qs, [config])

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
        service.application_states.set([state])
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        qs = MaggioliConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            MaggioliConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class HalleyConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.login_url, 'https://example.local/halley/login/')
        self.assertEqual(config.registration_url, 'https://example.local/halley/registration/')
        self.assertEqual(config.username, 'console')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.institution_name, 'Comune di Bugliano')
        self.assertEqual(config.institution_aoo_id, 1)
        self.assertEqual(config.institution_office_ids, [])
        self.assertEqual(config.classification_category, '7')
        self.assertEqual(config.classification_class, '2')
        self.assertEqual(config.classification_subclass, '0')
        self.assertEqual(config.folder_id, 1)
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.HALLEY)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))
        HalleyConfig.objects.create(**get_halley_config_data(service=service, is_active=False))

        try:
            HalleyConfig.objects.create(**get_halley_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            HalleyConfig.objects.create(**get_halley_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'classification_subclass',
            'folder_id',
        }
        data = get_halley_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = HalleyConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))
        service.delete()
        self.assertEqual(HalleyConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HalleyTestClient()

        with _stub_halley_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '84')
        stub.assert_call(config)
        stub.assert_insert(application)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HalleyTestClient()

        with _stub_halley_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
        service.application_states.set([state])
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        qs = HalleyConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            HalleyConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class HalleyCloudConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.registration_url, 'https://example.local/halley_cloud/registration/')
        self.assertEqual(config.username, 'console')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.mailbox, 'example@example.com')
        self.assertEqual(config.institution_office_ids, [])
        self.assertEqual(config.classification_category, '7')
        self.assertEqual(config.classification_class, '2')
        self.assertEqual(config.classification_subclass, '0')
        self.assertEqual(config.folder_name, 'Fascicolo test')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.HALLEY_CLOUD)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service, is_active=False))

        try:
            HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'classification_category',
            'classification_class',
            'classification_subclass',
            'mailbox',
            'folder_name',
        }
        data = get_halley_cloud_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = HalleyCloudConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        service.delete()
        self.assertEqual(HalleyConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HalleyTestClient()

        with _stub_halley_cloud_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '84')
        stub.assert_call(config)
        stub.assert_insert(application)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HalleyCloudTestClient()

        with _stub_halley_cloud_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
        service.application_states.set([state])
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        qs = HalleyCloudConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            HalleyCloudConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class DedagroupConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.token_url, 'https://token.example.local')
        self.assertEqual(config.base_url, 'https://base.example.local')
        self.assertEqual(config.client_id, 'abc')
        self.assertEqual(config.client_secret, 's3cr3t')
        self.assertEqual(config.grant_type, 'client_credentials')
        self.assertEqual(config.resource, 'http://base.example.local')
        self.assertEqual(config.registry_id, 3)
        self.assertEqual(config.author_id, 123456)
        self.assertEqual(config.organization_chart_level_code, 'abcde12345')
        self.assertEqual(config.institution_aoo_id, 123)
        self.assertEqual(config.classification_title, '7')
        self.assertEqual(config.classification_class, '2')
        self.assertEqual(config.classification_subclass, '0')
        self.assertEqual(config.folder_code, '2022.1.1')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.DEDAGROUP)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')
        self.assertEqual(config.full_classification_class, '7.2')
        self.assertEqual(config.full_classification_subclass, '7.2.0')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service, is_active=False))

        try:
            DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'classification_title',
            'classification_class',
            'classification_subclass',
            'folder_code',
        }
        data = get_dedagroup_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = DedagroupConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        service.delete()
        self.assertEqual(DedagroupConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = DedagroupTestClient()

        with _stub_dedagroup_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '84')
        stub.assert_call(config)
        stub.assert_protocollazione(application)
        self.assertEqual(Registration.objects.count(), 1)

    def test_add_to_folder(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        stub = DedagroupTestClient()

        with _stub_dedagroup_client_class(stub):
            config.add_to_folder(54321, [123, 456])

        stub.assert_call(config)
        stub.assert_fascicola_documento(54321, [123, 456])

    def test_add_to_folder_skipped(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service, folder_code=None))
        stub = DedagroupTestClient()

        with _stub_dedagroup_client_class(stub):
            config.add_to_folder(54321, [123, 456])

        stub.assert_fascicola_documento_not_called()

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = DedagroupTestClient()

        with _stub_dedagroup_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
        service.application_states.set([state])
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        qs = DedagroupConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            DedagroupConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class InforConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.username, 'sdc')
        self.assertEqual(config.denomination, 'sdc')
        self.assertEqual(config.email, 'user@example.com')
        self.assertEqual(config.wsdl_url, 'https://base.example.local/protocollo?wsdl')
        self.assertEqual(config.registry_url, 'https://base.example.local/protocollo')
        self.assertEqual(config.incoming_document_type, 'xx')
        self.assertEqual(config.incoming_intermediary, 'WEB')
        self.assertEqual(config.incoming_sorting, 'ABC')
        self.assertEqual(config.incoming_classification, '16.02')
        self.assertEqual(config.incoming_folder, '2012/6')
        self.assertEqual(config.response_internal_sender, 'sdc')
        self.assertEqual(config.response_document_type, 'xx')
        self.assertEqual(config.response_intermediary, 'WEB')
        self.assertEqual(config.response_sorting, 'ABC')
        self.assertEqual(config.response_classification, '16.02')
        self.assertEqual(config.response_folder, '2012/6')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.INFOR)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        InforConfig.objects.create(**get_infor_config_data(service=service))
        InforConfig.objects.create(**get_infor_config_data(service=service, is_active=False))

        try:
            InforConfig.objects.create(**get_infor_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            InforConfig.objects.create(**get_infor_config_data(service=service))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = InforConfig.objects.create(**get_infor_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        InforConfig.objects.create(**get_infor_config_data(service=service))
        service.delete()
        self.assertEqual(InforConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InforConfig.objects.create(**get_infor_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = InforTestClient()

        with _stub_infor_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '8790')
        stub.assert_call(config)
        stub.assert_insert(application)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InforConfig.objects.create(**get_infor_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = InforTestClient()

        with _stub_infor_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INFOR))
        service.application_states.set([state])
        config = InforConfig.objects.create(**get_infor_config_data(service=service))

        qs = InforConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            InforConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class D3ConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.username, 'sdc')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.registry_url, 'https://base.example.local/')
        self.assertEqual(config.repository_id, '0b07a6e1-dee9-4f54-889a-fbf459143c26')
        self.assertEqual(config.main_source_category, 'EXAMPLE')
        self.assertEqual(config.register_attachments, True)
        self.assertEqual(config.attachment_source_category, 'EXAMPLE2')
        self.assertEqual(config.document_subtype, 'Allgmein - generico')
        self.assertEqual(config.classification, '15.02.02')
        self.assertEqual(config.folder_name, '1234')
        self.assertEqual(config.competent_service, 'Sekretariat - Segreteria')
        self.assertEqual(config.visibility, 'Sekretariat - Segreteria: S')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.D3)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        D3Config.objects.create(**get_d3_config_data(service=service))
        D3Config.objects.create(**get_d3_config_data(service=service, is_active=False))

        try:
            D3Config.objects.create(**get_d3_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            D3Config.objects.create(**get_d3_config_data(service=service))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = D3Config.objects.create(**get_d3_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        D3Config.objects.create(**get_d3_config_data(service=service))
        service.delete()
        self.assertEqual(D3Config.objects.count(), 0)

    def test_upload(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))
        application = Application(get_application_data())
        stub = D3TestClient()

        with _stub_d3_client_class(stub):
            ret, _ = config.upload(application, application.current_document)

        self.assertEqual(
            ret.content_location_uri,
            '/dms/r/7ff67ade-4bbe-5aea-b370-19904efbd7cb/blob/chunk/'
            '2023-03-21_temp_master_file_test99_sgv_ea86e5db-5e27-',
        )
        stub.assert_call(config)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = D3TestClient()

        with _stub_d3_client_class(stub):
            ret = config.register(document_id, application, application.current_document, None)

        self.assertEqual(ret.number, '0000825')
        stub.assert_call(config)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = D3TestClient()

        with _stub_d3_client_class(stub):
            config.register(document_id, application, application.current_document, None)
            config.register(
                document_id,
                application,
                application.current_document,
                get_registration_data(
                    registration_data={
                        'folder': None,
                        'document': {
                            'number': '0000825',
                            'year': '2023',
                        },
                        'attachments': [],
                    },
                )['registration_data'],
            )

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.D3))
        service.application_states.set([state])
        config = D3Config.objects.create(**get_d3_config_data(service=service))

        qs = D3Config.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            D3Config.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class HypersicConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.username, 'sdc')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.registry_url, 'https://base.example.local/protocollo')
        self.assertEqual(config.office_ids, [123])
        self.assertEqual(config.classification, '6-3-0')
        self.assertEqual(config.folder_number, '2')
        self.assertEqual(config.folder_year, '2021')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.HYPERSIC)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        HypersicConfig.objects.create(**get_hypersic_config_data(service=service, is_active=False))

        try:
            HypersicConfig.objects.create(**get_hypersic_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        service.delete()
        self.assertEqual(HypersicConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HypersicTestClient()

        with _stub_hypersic_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, 456)
        stub.assert_call(config)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = HypersicTestClient()

        with _stub_hypersic_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HYPERSIC))
        service.application_states.set([state])
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        qs = HypersicConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            HypersicConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class TinnConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.url, 'https://example.local/tinn/')
        self.assertEqual(config.codente, 'COD-111')
        self.assertEqual(config.username, 'WSDOCAREA')
        self.assertEqual(config.password, 's3cr3t')
        self.assertEqual(config.institution_name, 'Comune di Bugliano - Ufficio Tributi')
        self.assertEqual(config.institution_code, 'COD-222')
        self.assertEqual(config.institution_email, 'bugliano-tributi@example.local')
        self.assertEqual(config.institution_ou, 'TRI')
        self.assertEqual(config.institution_aoo_code, 'COD-333')
        self.assertEqual(config.classification_institution_code, 'COD-444')
        self.assertEqual(config.classification_aoo_code, 'COD-555')
        self.assertEqual(config.classification_hierarchy, '7.2.0')
        self.assertEqual(config.folder_number, '42')
        self.assertEqual(config.folder_year, '2021')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.TINN)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.sdc_institution_code, 'SDC')
        self.assertEqual(config.sdc_aoo_code, 'P_SDC')
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        TinnConfig.objects.create(**get_tinn_config_data(service=service))
        TinnConfig.objects.create(**get_tinn_config_data(service=service, is_active=False))

        try:
            TinnConfig.objects.create(**get_tinn_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            TinnConfig.objects.create(**get_tinn_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'institution_name',
            'institution_code',
            'institution_email',
            'institution_ou',
            'institution_aoo_code',
            'classification_institution_code',
            'classification_aoo_code',
            'folder_number',
            'folder_year',
        }
        data = get_tinn_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = TinnConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        TinnConfig.objects.create(**get_tinn_config_data(service=service))
        service.delete()
        self.assertEqual(TinnConfig.objects.count(), 0)

    def test_upload(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        data = b'abc'
        stub = TinnTestClient()

        with _stub_tinn_client_class(stub):
            ret = config.upload(data)

        self.assertEqual(ret.document_id, '17330')
        stub.assert_inserimento(data)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = TinnTestClient()

        with _stub_tinn_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '38')
        stub.assert_protocollazione(application)
        stub.assert_call(config)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        data = b'abc'
        stub = TinnTestClient()

        with _stub_tinn_client_class(stub):
            config.upload(data)
            config.upload(data)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.TINN))
        service.application_states.set([state])
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))

        qs = TinnConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            TinnConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class SicrawebWSProtocolloDMConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.url, 'https://example.local/sicraweb-wsprotocollodm/')
        self.assertEqual(config.connection_string, 'test_conn_string')
        self.assertEqual(config.institution_code, 'COD-111')
        self.assertEqual(config.institution_ou, 'TRI')
        self.assertEqual(config.institution_aoo_code, 'COD-222')
        self.assertEqual(config.sending_mode, 'PEC')
        self.assertEqual(config.internal_sender, 'ANA')
        self.assertEqual(config.update_records, 'S')
        self.assertEqual(config.classification_hierarchy, '7.2.0')
        self.assertEqual(config.folder_number, '42')
        self.assertEqual(config.folder_year, '2021')
        self.assertEqual(config.connection_user, 'opencontent')
        self.assertEqual(config.connection_user_role, 'user')
        self.assertEqual(config.standard_subject, 'subject')
        self.assertEqual(config.document_type, 'servizio online')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.SICRAWEB_WSPROTOCOLLODM)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))
        SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service, is_active=False),
        )

        try:
            SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service, is_active=False),
            )
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'institution_code',
            'institution_ou',
            'sending_mode',
            'internal_sender',
            'update_records',
            'classification_hierarchy',
            'folder_number',
            'folder_year',
            'connection_user',
            'connection_user_role',
            'standard_subject',
            'document_type',
        }
        data = get_sicraweb_wsprotocollodm_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = SicrawebWSProtocolloDMConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))
        service.delete()
        self.assertEqual(SicrawebWSProtocolloDMConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = SicrawebWSProtocolloDMTestClient()

        with _stub_sicraweb_wsprotocollodm_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '12345')
        self.assertEqual(ret.year, '2021')
        self.assertEqual(ret.document_id, '123456789')
        stub.assert_insert(application)
        stub.assert_call(config)
        self.assertEqual(Registration.objects.count(), 1)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = SicrawebWSProtocolloDMTestClient()

        with _stub_sicraweb_wsprotocollodm_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(
            **get_service_data(tenant=tenant, provider=Service.Provider.SICRAWEB_WSPROTOCOLLODM),
        )
        service.application_states.set([state])
        config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )

        qs = SicrawebWSProtocolloDMConfig.objects.active_for_service(
            '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
            'example_state',
        )

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            SicrawebWSProtocolloDMConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class SiscomConfigTestCase(TestCase):
    def test_creation(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = SiscomConfig.objects.create(
                **get_siscom_config_data(service=service),
            )
        self.assertEqual(config.description, 'Test configuration')
        self.assertEqual(config.service, service)
        self.assertTrue(config.is_active)
        self.assertEqual(config.url, 'https://example.local/siscom/')
        self.assertEqual(config.license_code, '1008')
        self.assertEqual(config.operator_denomination, 'John Doe')
        self.assertEqual(config.classification_category, '0')
        self.assertEqual(config.classification_class, '0')
        self.assertEqual(config.folder_id, '0')
        self.assertEqual(config.mail_type, '0')
        self.assertEqual(config.office_id, '7')
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, NOW)
        self.assertEqual(str(config), str(config.id))

        self.assertEqual(config.provider, Service.Provider.SISCOM)
        self.assertEqual(config.tenant, tenant)
        self.assertEqual(config.tenant_slug, 'comune-di-bugliano')

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service, is_active=False))

        try:
            SiscomConfig.objects.create(**get_siscom_config_data(service=service, is_active=False))
        except Exception:
            self.fail('should not raise')

        with self.assertRaisesMessage(
            IntegrityError,
            f'Key (service_id, is_active)=({service.id}, t) already exists.',
        ):
            SiscomConfig.objects.create(**get_siscom_config_data(service=service))

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        nullables = {
            'operator_denomination',
            'classification_category',
            'classification_class',
            'folder_id',
            'mail_type',
        }
        data = get_siscom_config_data(service=service)
        for field in nullables:
            data.pop(field)
        config = SiscomConfig.objects.create(**data)
        for field in nullables:
            self.assertIsNone(getattr(config, field))

    def test_modified_at(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            config = SiscomConfig.objects.create(
                **get_siscom_config_data(service=service),
            )
            clock.move_to(future)
            config.save()
        self.assertEqual(config.created_at, NOW)
        self.assertEqual(config.modified_at, future)

    def test_service_on_delete_cascade(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        service.delete()
        self.assertEqual(SiscomConfig.objects.count(), 0)

    def test_register(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = SiscomTestClient()

        with _stub_siscom_client_class(stub):
            ret = config.register(document_id, application)

        self.assertEqual(ret.number, '91')
        self.assertEqual(ret.year, '2023')
        stub.assert_insert(application)
        stub.assert_call(config)
        self.assertEqual(Registration.objects.count(), 1)

    def test_upload(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        Registration.objects.create(
            **get_registration_data(
                is_completed=False,
                is_updated=False,
                registration_data={
                    'folder': None,
                    'document': {
                        'number': '91',
                        'year': '2023',
                        'registered_at': '2023-11-14T12:30:00',
                    },
                    'attachments': [],
                },
            ),
        )
        application = Application(get_application_data())
        stub = SiscomTestClient()

        with _stub_siscom_client_class(stub):
            ret = config.upload(application, application.current_document)

        self.assertEqual(ret.document_id, '1738')
        stub.assert_call(config)

    def test_collate(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        application = Application(get_application_data())
        stub = SiscomTestClient()

        with _stub_siscom_client_class(stub):
            ret = config.collate(application, '123', '2023')

        self.assertEqual(ret.document_id_in_folder, '168')
        stub.assert_collate(application, '123', '2023')
        stub.assert_call(config)

    def test_client_cache(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        document_id = uuid.uuid4()
        application = Application(get_application_data())
        stub = SiscomTestClient()

        with _stub_siscom_client_class(stub):
            config.register(document_id, application)
            config.register(document_id, application)

        stub.assert_call(config)

    def test_active_for_service(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        service = Service.objects.create(
            **get_service_data(tenant=tenant, provider=Service.Provider.SISCOM),
        )
        service.application_states.set([state])
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))

        qs = SiscomConfig.objects.active_for_service('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'example_state')

        with self.subTest():
            self.assertQuerySetEqual(qs.all(), [config])

        with self.subTest(), transaction.atomic():
            SiscomConfig.objects.filter(pk=config.pk).update(is_active=False)
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(sdc_id=uuid4())
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            Service.objects.filter(pk=service.pk).update(provider='acme')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)

        with self.subTest(), transaction.atomic():
            ApplicationState.objects.filter(pk=state.pk).update(name='foo')
            self.assertQuerySetEqual(qs.all(), [])
            transaction.set_rollback(True)


class RegistrationEventTestCase(TestCase):
    def test_creation(self):
        with freeze_time(NOW):
            event = RegistrationEvent.objects.create(**get_registration_event_data())
        self.assertEqual(event.provider, Service.Provider.DATAGRAPH)
        self.assertEqual(event.config_id, 1)
        self.assertEqual(event.application_sdc_id, 'b8a44017-6db1-4abb-af6c-c95f34f08af6')
        self.assertEqual(event.event_sdc_id, 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5')
        self.assertEqual(event.state, RegistrationEvent.State.PROCESSING)
        self.assertEqual(event.data, {'key': 'value'})
        self.assertEqual(event.traceback, 'a lengthy error')
        self.assertEqual(event.retry_traceback, 'another lengthy error')
        self.assertEqual(event.created_at, NOW)
        self.assertEqual(event.modified_at, NOW)
        self.assertEqual(str(event), str(event.id))

    def test_defaults(self):
        data = get_registration_event_data()
        data.pop('state')
        event = RegistrationEvent.objects.create(**data)
        self.assertEqual(event.state, RegistrationEvent.State.PROCESSING)

    def test_nullable_fields(self):
        data = get_registration_event_data()
        data.pop('traceback')
        data.pop('retry_traceback')
        event = RegistrationEvent.objects.create(**data)
        self.assertIsNone(event.traceback)
        self.assertIsNone(event.retry_traceback)

    def test_modified_at(self):
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            event = RegistrationEvent.objects.create(**get_registration_event_data())
            clock.move_to(future)
            event.save()
        self.assertEqual(event.created_at, NOW)
        self.assertEqual(event.modified_at, future)

    def test_complete(self):
        event = RegistrationEvent.objects.create(**get_registration_event_data())
        event.complete()
        event.refresh_from_db()
        self.assertEqual(event.state, RegistrationEvent.State.COMPLETED)

    def test_fail(self):
        event = RegistrationEvent.objects.create(**get_registration_event_data())
        try:
            1 / 0
        except Exception:
            exc_type, exc_value, traceback = sys.exc_info()
        event.fail(exc_type, exc_value, traceback)
        event.refresh_from_db()
        self.assertEqual(event.state, RegistrationEvent.State.FAILED)
        self.assertTrue(
            event.traceback.startswith('Traceback (most recent call last):')
            and event.traceback.endswith('ZeroDivisionError: division by zero\n'),
        )

    def test_fail_to_retry(self):
        event = RegistrationEvent.objects.create(**get_registration_event_data())
        try:
            1 / 0
        except Exception:
            exc_type, exc_value, traceback = sys.exc_info()
        event.fail_to_retry(exc_type, exc_value, traceback)
        event.refresh_from_db()
        self.assertTrue(
            event.retry_traceback.startswith('Traceback (most recent call last):')
            and event.retry_traceback.endswith('ZeroDivisionError: division by zero\n'),
        )


class RegistrationTestCase(TestCase):
    def test_creation(self):
        with freeze_time(NOW):
            registration = Registration.objects.create(**get_registration_data())
        self.assertEqual(registration.number, '1234')
        self.assertEqual(registration.date, datetime(2021, 1, 2, 10, 20, 30, 123456, tzinfo=timezone.utc))
        self.assertEqual(registration.tenant_sdc_id, '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc')
        self.assertEqual(registration.service_sdc_id, '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca')
        self.assertEqual(registration.application_sdc_id, 'b8a44017-6db1-4abb-af6c-c95f34f08af6')
        self.assertEqual(registration.application_state_name, 'example_state')
        self.assertEqual(registration.event_sdc_id, 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5')
        self.assertEqual(registration.created_at, NOW)
        self.assertEqual(registration.modified_at, NOW)
        self.assertEqual(str(registration), str(registration.id))

    def test_modified_at(self):
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            registration = Registration.objects.create(**get_registration_data())
            clock.move_to(future)
            registration.save()
        self.assertEqual(registration.created_at, NOW)
        self.assertEqual(registration.modified_at, future)

    def test_exists_for_application(self):
        application = Application(get_application_data())

        with self.subTest():
            self.assertFalse(Registration.objects.exists_for_application(application))

        with self.subTest():
            Registration.objects.create(**get_registration_data())
            self.assertTrue(Registration.objects.exists_for_application(application))

    def test_exists_for_application_request_integration(self):
        application = Application(get_application_data(status_name='status_request_integration'))

        with self.subTest():
            Registration.objects.create(**get_registration_data(application_state_name='status_request_integration'))
            self.assertFalse(Registration.objects.exists_for_application(application))

    def test_exists_for_application_submitted_integration(self):
        application = Application(get_application_data(status_name='status_submitted_after_integration'))

        with self.subTest():
            Registration.objects.create(
                **get_registration_data(application_state_name='status_submitted_after_integration'),
            )
            self.assertFalse(Registration.objects.exists_for_application(application))


class DeadLetterTestCase(TestCase):
    def test_creation(self):
        with freeze_time(NOW):
            dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        self.assertEqual(dead_letter.tenant_sdc_id, '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc')
        self.assertEqual(dead_letter.service_sdc_id, '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca')
        self.assertEqual(dead_letter.application_sdc_id, 'b8a44017-6db1-4abb-af6c-c95f34f08af6')
        self.assertEqual(dead_letter.state, DeadLetter.State.NEW)
        self.assertEqual(dead_letter.data, {'key': 'value'})
        self.assertFalse(dead_letter.is_deleted)
        self.assertEqual(dead_letter.created_at, NOW)
        self.assertEqual(dead_letter.modified_at, NOW)
        self.assertEqual(str(dead_letter), str(dead_letter.id))

    def test_defaults(self):
        data = get_dead_letter_data()
        data.pop('state')
        data.pop('is_deleted')
        dead_letter = DeadLetter.objects.create(**data)
        self.assertEqual(dead_letter.state, DeadLetter.State.NEW)
        self.assertFalse(dead_letter.is_deleted)

    def test_modified_at(self):
        future = NOW + timedelta(hours=1)
        with freeze_time(NOW) as clock:
            dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
            clock.move_to(future)
            dead_letter.save()
        self.assertEqual(dead_letter.created_at, NOW)
        self.assertEqual(dead_letter.modified_at, future)

    def test_all_objects_manager_class(self):
        self.assertIs(type(DeadLetter.all_objects), Manager)

    def test_objects_manager_lacks_delete_method(self):
        self.assertFalse(hasattr(DeadLetter.objects, 'delete'))

    def test_soft_delete_instance(self):
        dead_letter = DeadLetter._base_manager.create(**get_dead_letter_data())
        dead_letter.delete()
        self.assertFalse(DeadLetter.objects.exists())
        self.assertTrue(DeadLetter._base_manager.get().is_deleted)

    def test_soft_delete_queryset(self):
        DeadLetter._base_manager.create(**get_dead_letter_data())
        DeadLetter.objects.all().delete()
        self.assertFalse(DeadLetter.objects.exists())
        self.assertTrue(DeadLetter._base_manager.get().is_deleted)

    def test_retry_async_non_retryable_object(self):
        for state in [DeadLetter.State.QUEUED, DeadLetter.State.FAILED]:
            with self.subTest(state=state):
                dead_letter = DeadLetter.objects.create(**get_dead_letter_data(state=state))
                stub = TestRetryProducer()

                with stub_retry_producer_class(stub):
                    with self.assertRaises(errors.NotRetryable):
                        dead_letter.retry_async()

                dead_letter.refresh_from_db()
                self.assertEqual(dead_letter.state, state)
                stub.assert_send_not_called()

    def test_retry_async_success(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        stub = TestRetryProducer()

        with stub_retry_producer_class(stub):
            dead_letter.retry_async()

        dead_letter.refresh_from_db()
        self.assertEqual(dead_letter.state, DeadLetter.State.QUEUED)
        stub.assert_send('applications_test', b'{"key": "value"}')

    def test_retry_async_failure(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        stub = TestRetryProducer(send_error=Exception('xyz failed'))

        with stub_retry_producer_class(stub):
            with self.assertRaisesRegex(Exception, r'^xyz failed$'):
                dead_letter.retry_async()

        dead_letter.refresh_from_db()
        self.assertEqual(dead_letter.state, DeadLetter.State.FAILED)
        stub.assert_send('applications_test', b'{"key": "value"}')
