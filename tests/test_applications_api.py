import json
import os
from datetime import datetime, timedelta, timezone

from django.conf import settings
from django.urls import reverse
from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase

from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    InsielConfig,
    MaggioliConfig,
    PitreConfig,
    PreRegistration,
    Registration,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)

from ._helpers import (
    TestRetryProducer,
    get_application_state_data,
    get_d3_config_data,
    get_datagraph_config_data,
    get_dead_letter_data,
    get_dedagroup_config_data,
    get_halley_cloud_config_data,
    get_halley_config_data,
    get_hypersic_config_data,
    get_infor_config_data,
    get_insiel_config_data,
    get_maggioli_config_data,
    get_pitre_config_data,
    get_pre_registration_data,
    get_registration_data,
    get_service_data,
    get_sicraweb_wsprotocollodm_config_data,
    get_siscom_config_data,
    get_tenant_data,
    get_tinn_config_data,
    stub_retry_producer_class,
)

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class ServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'provider': 'datagraph',
                        'application_states': ['example_state'],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
                'application_states': ['example_state'],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'provider': 'datagraph',
                    'application_states': ['example_state'],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
                'application_states': ['example_state'],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'provider': 'maggioli',
                    'application_states': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'provider': 'maggioli',
                'application_states': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'provider': 'maggioli',
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {'provider': 'maggioli'},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'maggioli',
                'application_states': ['example_state'],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant sdc id',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'provider': {
                            'type': 'choice',
                            'required': True,
                            'read_only': False,
                            'label': 'Provider',
                            'choices': [
                                {'value': 'datagraph', 'display_name': 'datagraph'},
                                {'value': 'dedagroup', 'display_name': 'dedagroup'},
                                {'value': 'halley', 'display_name': 'halley'},
                                {'value': 'halley-cloud', 'display_name': 'halley-cloud'},
                                {'value': 'maggioli', 'display_name': 'maggioli'},
                                {'value': 'pitre', 'display_name': 'pitre'},
                                {'value': 'insiel', 'display_name': 'insiel'},
                                {'value': 'infor', 'display_name': 'infor'},
                                {'value': 'd3', 'display_name': 'd3'},
                                {'value': 'hypersic', 'display_name': 'hypersic'},
                                {'value': 'tinn', 'display_name': 'tinn'},
                                {'value': 'sicraweb-wsprotocollodm', 'display_name': 'sicraweb-wsprotocollodm'},
                                {'value': 'siscom', 'display_name': 'siscom'},
                            ],
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('service-detail', args=(sdc_id,))
        return reverse('service-list')


class DatagraphServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'url': 'https://example.local/datagraph/',
                                'codente': 'COD-111',
                                'username': 'WSDOCAREA',
                                'password': 's3cr3t',
                                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                                'institution_code': 'COD-222',
                                'institution_email': 'bugliano-tributi@example.local',
                                'institution_ou': 'TRI',
                                'institution_aoo_code': 'COD-333',
                                'classification_institution_code': 'COD-444',
                                'classification_aoo_code': 'COD-555',
                                'classification_hierarchy': '7.2.0',
                                'folder_number': '42',
                                'folder_year': '2021',
                                'document_format': 'servizio online',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_format': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'url': 'https://example.local/datagraph/',
                            'codente': 'COD-111',
                            'username': 'WSDOCAREA',
                            'password': 's3cr3t',
                            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                            'institution_code': 'COD-222',
                            'institution_email': 'bugliano-tributi@example.local',
                            'institution_ou': 'TRI',
                            'institution_aoo_code': 'COD-333',
                            'classification_institution_code': 'COD-444',
                            'classification_aoo_code': 'COD-555',
                            'classification_hierarchy': '7.2.0',
                            'folder_number': '42',
                            'folder_year': '2021',
                            'document_format': 'servizio online',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': DatagraphConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_format': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Datagraph Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'codente': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codente',
                                        'max_length': 64,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution name',
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution code',
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution email',
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution OU',
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'AOO code',
                                        'max_length': 32,
                                    },
                                    'classification_institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution code for classification',
                                        'max_length': 32,
                                    },
                                    'classification_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'AOO code for classification',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification hierarchy',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder number',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder year',
                                        'max_length': 4,
                                    },
                                    'document_format': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Document format',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('datagraph-service-detail', args=(sdc_id,))
        return reverse('datagraph-service-list')


class MaggioliServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'url': 'https://example.local/maggioli/',
                                'connection_string': 'test_conn_string',
                                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                                'institution_code': 'COD-111',
                                'institution_email': 'bugliano-tributi@example.local',
                                'institution_ou': 'TRI',
                                'institution_aoo_code': 'COD-222',
                                'sending_mode': 'PEC',
                                'sending_method': 'COD-333',
                                'classification_hierarchy': '7.2.0',
                                'folder_number': '42',
                                'folder_year': '2021',
                                'document_type': 'servizio online',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'sending_method': 'COD-333',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'url': 'https://example.local/maggioli/',
                            'connection_string': 'test_conn_string',
                            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                            'institution_code': 'COD-111',
                            'institution_email': 'bugliano-tributi@example.local',
                            'institution_ou': 'TRI',
                            'institution_aoo_code': 'COD-222',
                            'sending_mode': 'PEC',
                            'sending_method': 'COD-333',
                            'classification_hierarchy': '7.2.0',
                            'folder_number': '42',
                            'folder_year': '2021',
                            'document_type': 'servizio online',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': MaggioliConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'sending_method': 'COD-333',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.MAGGIOLI))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Maggioli Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'connection_string': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Connection string',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution name',
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution code',
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution email',
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution OU',
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'AOO code',
                                        'max_length': 32,
                                    },
                                    'sending_mode': {
                                        'type': 'choice',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sending mode',
                                        'choices': [
                                            {'value': 'Cartaceo', 'display_name': 'Cartaceo'},
                                            {'value': 'PEC', 'display_name': 'PEC'},
                                            {'value': 'Interoperabile', 'display_name': 'Interoperabile'},
                                            {'value': 'InterPRO', 'display_name': 'InterPRO'},
                                        ],
                                    },
                                    'sending_method': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sending method',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification hierarchy',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder number',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder year',
                                        'max_length': 4,
                                    },
                                    'document_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Document type',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('maggioli-service-detail', args=(sdc_id,))
        return reverse('maggioli-service-list')


class HalleyServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'login_url': 'https://example.local/halley/login/',
                                'registration_url': 'https://example.local/halley/registration/',
                                'username': 'console',
                                'password': 's3cr3t',
                                'institution_name': 'Comune di Bugliano',
                                'institution_aoo_id': 1,
                                'institution_office_ids': [],
                                'classification_category': '7',
                                'classification_class': '2',
                                'classification_subclass': '0',
                                'folder_id': 1,
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_id': 1,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'login_url': 'https://example.local/halley/login/',
                            'registration_url': 'https://example.local/halley/registration/',
                            'username': 'console',
                            'password': 's3cr3t',
                            'institution_name': 'Comune di Bugliano',
                            'institution_aoo_id': 1,
                            'institution_office_ids': [],
                            'classification_category': '7',
                            'classification_class': '2',
                            'classification_subclass': '0',
                            'folder_id': 1,
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': HalleyConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_id': 1,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'login_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Login URL',
                                        'max_length': 200,
                                    },
                                    'registration_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Registration URL',
                                        'max_length': 200,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Institution name',
                                        'max_length': 1024,
                                    },
                                    'institution_aoo_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'AOO ID',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'institution_office_ids': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution office IDs',
                                        'child': {
                                            'type': 'integer',
                                            'required': True,
                                            'read_only': False,
                                            'label': 'Institution office IDs',
                                            'min_value': 0,
                                            'max_value': 2147483647,
                                        },
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Classification category',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Classification class',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification subclass',
                                        'max_length': 16,
                                    },
                                    'folder_id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder ID',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('halley-service-detail', args=(sdc_id,))
        return reverse('halley-service-list')


class HalleyCloudServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'registration_url': 'https://example.local/halley_cloud/registration/',
                                'username': 'console',
                                'password': 's3cr3t',
                                'mailbox': 'example@example.com',
                                'institution_office_ids': [],
                                'classification_category': '7',
                                'classification_class': '2',
                                'classification_subclass': '0',
                                'folder_name': 'Fascicolo test',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'mailbox': 'example@example.com',
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_name': 'Fascicolo test',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'registration_url': 'https://example.local/halley_cloud/registration/',
                            'username': 'console',
                            'password': 's3cr3t',
                            'mailbox': 'example@example.com',
                            'institution_office_ids': [],
                            'classification_category': '7',
                            'classification_class': '2',
                            'classification_subclass': '0',
                            'folder_name': 'Fascicolo test',
                            'created_at': '2021-05-25T10:20:30.123456Z',
                            'modified_at': '2021-05-25T10:20:30.123456Z',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': HalleyCloudConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'mailbox': 'example@example.com',
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_name': 'Fascicolo test',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.HALLEY_CLOUD))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Cloud Service List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'registration_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Registration URL',
                                        'max_length': 200,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'mailbox': {
                                        'type': 'email',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Mailbox',
                                        'max_length': 254,
                                    },
                                    'institution_office_ids': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution office IDs',
                                        'help_text': 'The list of office ids is provided to the institution by the '
                                        'protocol manager, if omitted the default office will be used',
                                        'child': {
                                            'type': 'integer',
                                            'required': True,
                                            'read_only': False,
                                            'label': 'Institution office IDs',
                                            'min_value': 0,
                                            'max_value': 2147483647,
                                        },
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification category',
                                        'help_text': 'The list of classifications is provided to the institution by '
                                        'the protocol manager, if omitted the default classification '
                                        'will be used',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification class',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification subclass',
                                        'max_length': 16,
                                    },
                                    'folder_name': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder name',
                                        'help_text': 'The list of folder names is provided to the institution by the '
                                        'protocol manager, if omitted the default folder will be used',
                                        'max_length': 64,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('halley-cloud-service-detail', args=(sdc_id,))
        return reverse('halley-cloud-service-list')


class DedagroupServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'token_url': 'https://token.example.local',
                                'base_url': 'https://base.example.local',
                                'client_id': 'abc',
                                'client_secret': 's3cr3t',
                                'grant_type': 'client_credentials',
                                'resource': 'http://base.example.local',
                                'registry_id': 3,
                                'author_id': 123456,
                                'organization_chart_level_code': 'abcde12345',
                                'institution_aoo_id': 123,
                                'classification_title': '7',
                                'classification_class': '2',
                                'classification_subclass': '0',
                                'folder_code': '2022.1.1',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                        'classification_title': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_code': '2022.1.1',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'token_url': 'https://token.example.local',
                            'base_url': 'https://base.example.local',
                            'client_id': 'abc',
                            'client_secret': 's3cr3t',
                            'grant_type': 'client_credentials',
                            'resource': 'http://base.example.local',
                            'registry_id': 3,
                            'author_id': 123456,
                            'organization_chart_level_code': 'abcde12345',
                            'institution_aoo_id': 123,
                            'classification_title': '7',
                            'classification_class': '2',
                            'classification_subclass': '0',
                            'folder_code': '2022.1.1',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': DedagroupConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                        'classification_title': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_code': '2022.1.1',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.DEDAGROUP))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Dedagroup Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'token_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Token URL',
                                        'max_length': 200,
                                    },
                                    'base_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Base URL',
                                        'max_length': 200,
                                    },
                                    'client_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client ID',
                                        'max_length': 64,
                                    },
                                    'client_secret': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client secret',
                                        'max_length': 64,
                                    },
                                    'grant_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Grant type',
                                        'max_length': 64,
                                    },
                                    'resource': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Resource',
                                        'max_length': 64,
                                    },
                                    'registry_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Registry ID',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'author_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Author ID',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'organization_chart_level_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Organization chart level code',
                                        'max_length': 64,
                                    },
                                    'institution_aoo_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'AOO ID',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'classification_title': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification title',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification class',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification subclass',
                                        'max_length': 16,
                                    },
                                    'folder_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder code',
                                        'max_length': 64,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('dedagroup-service-detail', args=(sdc_id,))
        return reverse('dedagroup-service-list')


class PitreServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'base_url': 'https://base.example.local',
                                'certificate': 's3cr3t',
                                'username': 'sdc',
                                'code_role': 'rda_sdc',
                                'code_application': 'sdc',
                                'code_adm': 'ABC',
                                'code_node_classification': '1.23',
                                'transmission_reason': 'TRASM ONLINE',
                                'transmission_type': 'T',
                                'receiver_codes': ['123'],
                                'recipient_code': 'rda_recipient',
                                'code_register': 'ABC',
                                'means_of_sending': 'SERVIZI ONLINE',
                                'code_rf': 'ABC',
                                'note': 'Outbound document note',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'base_url': 'https://base.example.local',
                        'certificate': 's3cr3t',
                        'username': 'sdc',
                        'code_role': 'rda_sdc',
                        'code_application': 'sdc',
                        'code_adm': 'ABC',
                        'code_node_classification': '1.23',
                        'transmission_reason': 'TRASM ONLINE',
                        'transmission_type': 'T',
                        'receiver_codes': ['123'],
                        'recipient_code': 'rda_recipient',
                        'code_register': 'ABC',
                        'means_of_sending': 'SERVIZI ONLINE',
                        'code_rf': 'ABC',
                        'note': 'Outbound document note',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'base_url': 'https://base.example.local',
                            'certificate': 's3cr3t',
                            'username': 'sdc',
                            'code_role': 'rda_sdc',
                            'code_application': 'sdc',
                            'code_adm': 'ABC',
                            'code_node_classification': '1.23',
                            'transmission_reason': 'TRASM ONLINE',
                            'transmission_type': 'T',
                            'receiver_codes': ['123'],
                            'recipient_code': 'rda_recipient',
                            'code_register': 'ABC',
                            'means_of_sending': 'SERVIZI ONLINE',
                            'code_rf': 'ABC',
                            'note': 'Outbound document note',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': PitreConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'base_url': 'https://base.example.local',
                        'certificate': 's3cr3t',
                        'username': 'sdc',
                        'code_role': 'rda_sdc',
                        'code_application': 'sdc',
                        'code_adm': 'ABC',
                        'code_node_classification': '1.23',
                        'transmission_reason': 'TRASM ONLINE',
                        'transmission_type': 'T',
                        'receiver_codes': ['123'],
                        'recipient_code': 'rda_recipient',
                        'code_register': 'ABC',
                        'means_of_sending': 'SERVIZI ONLINE',
                        'code_rf': 'ABC',
                        'note': 'Outbound document note',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))
        PitreConfig.objects.create(**get_pitre_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))
        PitreConfig.objects.create(**get_pitre_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.PITRE))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Pitre Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {'type': 'string', 'required': True, 'read_only': False, 'label': 'SDC ID'},
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'base_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Base URL',
                                        'max_length': 200,
                                    },
                                    'certificate': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Certificate',
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 32,
                                    },
                                    'code_role': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code role',
                                        'max_length': 32,
                                    },
                                    'code_application': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code application',
                                        'max_length': 32,
                                    },
                                    'code_adm': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code ADM',
                                        'max_length': 32,
                                    },
                                    'code_node_classification': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code node classification',
                                        'max_length': 8,
                                    },
                                    'transmission_reason': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Transmission reason',
                                        'max_length': 128,
                                    },
                                    'transmission_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Transmission_type',
                                        'max_length': 128,
                                    },
                                    'receiver_codes': {
                                        'type': 'list',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Transmission receiver codes',
                                        'help_text': 'List of role codes to transmit the creation of the project '
                                        'or document to',
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': 'Transmission receiver code',
                                            'max_length': 32,
                                        },
                                    },
                                    'recipient_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Recipient code',
                                        'help_text': 'Recipient role code for inbound documents, sender for outbound '
                                        'documents. If not set the role used for integration will be used',
                                        'max_length': 32,
                                    },
                                    'code_register': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code register',
                                        'max_length': 32,
                                    },
                                    'means_of_sending': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Means of sending',
                                        'max_length': 128,
                                    },
                                    'code_rf': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Code RF',
                                        'max_length': 8,
                                    },
                                    'note': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Note',
                                        'help_text': 'Outbound document note',
                                        'max_length': 256,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('pitre-service-detail', args=(sdc_id,))
        return reverse('pitre-service-list')


class InsielServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'login_url': 'https://base.example.local/token',
                                'registry_url': 'https://base.example.local/protocollo',
                                'file_transfer_url': 'https://base.example.local/filetransfer',
                                'client_id': 'sdc',
                                'client_secret': 's3cr3t',
                                'grant_type': 'client_credentials',
                                'user_code': 'sdc',
                                'user_password': 's3cr3t',
                                'operating_office_code': '1',
                                'office_code': 'ABC',
                                'office_registry': 'ABC',
                                'internal_offices_codes': [],
                                'internal_offices_types': [],
                                'register_code': '  1',
                                'prog_doc': None,
                                'prog_movi': None,
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'login_url': 'https://base.example.local/token',
                        'registry_url': 'https://base.example.local/protocollo',
                        'file_transfer_url': 'https://base.example.local/filetransfer',
                        'client_id': 'sdc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'user_code': 'sdc',
                        'user_password': 's3cr3t',
                        'operating_office_code': '1',
                        'office_code': 'ABC',
                        'office_registry': 'ABC',
                        'internal_offices_codes': [],
                        'internal_offices_types': [],
                        'register_code': '  1',
                        'prog_doc': None,
                        'prog_movi': None,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'login_url': 'https://base.example.local/token',
                            'registry_url': 'https://base.example.local/protocollo',
                            'file_transfer_url': 'https://base.example.local/filetransfer',
                            'client_id': 'sdc',
                            'client_secret': 's3cr3t',
                            'grant_type': 'client_credentials',
                            'user_code': 'sdc',
                            'user_password': 's3cr3t',
                            'operating_office_code': '1',
                            'office_code': 'ABC',
                            'office_registry': 'ABC',
                            'internal_offices_codes': [],
                            'internal_offices_types': [],
                            'register_code': '  1',
                            'prog_doc': None,
                            'prog_movi': None,
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': InsielConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'login_url': 'https://base.example.local/token',
                        'registry_url': 'https://base.example.local/protocollo',
                        'file_transfer_url': 'https://base.example.local/filetransfer',
                        'client_id': 'sdc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'user_code': 'sdc',
                        'user_password': 's3cr3t',
                        'operating_office_code': '1',
                        'office_code': 'ABC',
                        'office_registry': 'ABC',
                        'internal_offices_codes': [],
                        'internal_offices_types': [],
                        'register_code': '  1',
                        'prog_doc': None,
                        'prog_movi': None,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))
        InsielConfig.objects.create(**get_insiel_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))
        InsielConfig.objects.create(**get_insiel_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant, provider=Service.Provider.INSIEL))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Insiel Service List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'login_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Login URL',
                                        'max_length': 200,
                                    },
                                    'registry_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Protocol Web service URL',
                                        'max_length': 200,
                                    },
                                    'file_transfer_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'File transfer Web service URL',
                                        'max_length': 200,
                                    },
                                    'client_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client id',
                                        'max_length': 32,
                                    },
                                    'client_secret': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client secret',
                                        'max_length': 32,
                                    },
                                    'grant_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Grant type',
                                        'max_length': 32,
                                    },
                                    'user_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "User's code",
                                        'max_length': 32,
                                    },
                                    'user_password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "User's password",
                                        'max_length': 32,
                                    },
                                    'operating_office_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Operating office's code",
                                        'max_length': 32,
                                    },
                                    'office_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Office's code",
                                        'max_length': 32,
                                    },
                                    'office_registry': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Office's registry",
                                        'max_length': 32,
                                    },
                                    'internal_offices_codes': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Internal office's codes",
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': "Internal office's code",
                                            'max_length': 32,
                                        },
                                    },
                                    'internal_offices_types': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Internal office's types",
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': "Internal office's type",
                                            'max_length': 32,
                                        },
                                    },
                                    'register_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register code',
                                        'help_text': "Right aligned levels: ex: '1  2  3'. This field is required "
                                        'if prog_doc and prog_movi are not defined.',
                                        'max_length': 32,
                                    },
                                    'prog_doc': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register prog doc',
                                        'help_text': 'This field is required if prog_movi is defined',
                                        'max_length': 32,
                                    },
                                    'prog_movi': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register prog movi',
                                        'help_text': 'This field is required if prog_doc is defined',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('insiel-service-detail', args=(sdc_id,))
        return reverse('insiel-service-list')


class ProviderListAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            [
                {
                    'name': 'datagraph',
                    'endpoint': '/api/v1/services/datagraph/',
                },
                {
                    'name': 'halley',
                    'endpoint': '/api/v1/services/halley/',
                },
                {
                    'name': 'halley-cloud',
                    'endpoint': '/api/v1/services/halley-cloud/',
                },
                {
                    'name': 'maggioli',
                    'endpoint': '/api/v1/services/maggioli/',
                },
                {
                    'name': 'pitre',
                    'endpoint': '/api/v1/services/pitre/',
                },
                {
                    'name': 'insiel',
                    'endpoint': '/api/v1/services/insiel/',
                },
                {
                    'name': 'infor',
                    'endpoint': '/api/v1/services/infor/',
                },
                {
                    'name': 'd3',
                    'endpoint': '/api/v1/services/d3/',
                },
                {
                    'name': 'hypersic',
                    'endpoint': '/api/v1/services/hypersic/',
                },
                {
                    'name': 'tinn',
                    'endpoint': '/api/v1/services/tinn/',
                },
                {
                    'name': 'sicraweb-wsprotocollodm',
                    'endpoint': '/api/v1/services/sicraweb-wsprotocollodm/',
                },
                {
                    'name': 'siscom',
                    'endpoint': '/api/v1/services/siscom/',
                },
            ],
        )

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Provider List Api',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
            },
        )

    @staticmethod
    def _url():
        return reverse('provider')


class ApplicationStateAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'direction': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        with freeze_time(NOW):
            state = ApplicationState.objects.create(**get_application_state_data())

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            [
                {
                    'id': state.id,
                    'name': 'example_state',
                    'direction': 'inbound',
                    'integrations': False,
                    'default': True,
                    'created_at': '2021-05-25T10:20:30.123456Z',
                    'modified_at': '2021-05-25T10:20:30.123456Z',
                },
            ],
        )

    def test_list_filter(self):
        ApplicationState.objects.create(**get_application_state_data())

        response = self.client.get(self._url(), {'name': 'unknown'})

        self.assertEqual(response.json(), [])

    def test_retrieve(self):
        with freeze_time(NOW):
            state = ApplicationState.objects.create(**get_application_state_data())

        response = self.client.get(self._url(pk=state.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': state.id,
                'name': 'example_state',
                'direction': 'inbound',
                'integrations': False,
                'default': True,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Application State List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('application-state-detail', args=(pk,))
        return reverse('application-state-list')


class MaggioliConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'sending_method': 'COD-333',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'url': 'https://example.local/maggioli/',
                    'connection_string': 'test_conn_string',
                    'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                    'institution_code': 'COD-111',
                    'institution_email': 'bugliano-tributi@example.local',
                    'institution_ou': 'TRI',
                    'institution_aoo_code': 'COD-222',
                    'sending_mode': 'PEC',
                    'sending_method': 'COD-333',
                    'classification_hierarchy': '7.2.0',
                    'folder_number': '42',
                    'folder_year': '2021',
                    'document_type': 'servizio online',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': MaggioliConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'url': 'https://update.example.local/maggioli/',
                    'connection_string': 'update_test_conn_string',
                    'institution_name': 'Comune di Bugliano - Ufficio Anagrafe',
                    'institution_code': 'COD-444',
                    'institution_email': 'bugliano-anagrafe@example.local',
                    'institution_ou': 'ANA',
                    'institution_aoo_code': 'COD-555',
                    'sending_mode': 'InterPRO',
                    'sending_method': 'COD-666',
                    'classification_hierarchy': '8.3.1',
                    'folder_number': '99',
                    'folder_year': '2023',
                    'document_type': 'servizio anagrafe online',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'url': 'https://update.example.local/maggioli/',
                'connection_string': 'update_test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Anagrafe',
                'institution_code': 'COD-444',
                'institution_email': 'bugliano-anagrafe@example.local',
                'institution_ou': 'ANA',
                'institution_aoo_code': 'COD-555',
                'sending_mode': 'InterPRO',
                'sending_method': 'COD-666',
                'classification_hierarchy': '8.3.1',
                'folder_number': '99',
                'folder_year': '2023',
                'document_type': 'servizio anagrafe online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Maggioli Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Active',
                        },
                        'url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'URL',
                            'max_length': 200,
                        },
                        'connection_string': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Connection string',
                            'max_length': 64,
                        },
                        'institution_name': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution name',
                            'max_length': 1024,
                        },
                        'institution_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution code',
                            'max_length': 32,
                        },
                        'institution_email': {
                            'type': 'email',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution email',
                            'max_length': 254,
                        },
                        'institution_ou': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution OU',
                            'max_length': 32,
                        },
                        'institution_aoo_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'AOO code',
                            'max_length': 32,
                        },
                        'sending_mode': {
                            'type': 'choice',
                            'required': False,
                            'read_only': False,
                            'label': 'Sending mode',
                            'choices': [
                                {'value': 'Cartaceo', 'display_name': 'Cartaceo'},
                                {'value': 'PEC', 'display_name': 'PEC'},
                                {'value': 'Interoperabile', 'display_name': 'Interoperabile'},
                                {'value': 'InterPRO', 'display_name': 'InterPRO'},
                            ],
                        },
                        'sending_method': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Sending method',
                            'max_length': 32,
                        },
                        'classification_hierarchy': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification hierarchy',
                            'max_length': 16,
                        },
                        'folder_number': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder number',
                            'max_length': 16,
                        },
                        'folder_year': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder year',
                            'max_length': 4,
                        },
                        'document_type': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Document type',
                            'max_length': 32,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('maggioli-config-detail', args=(pk,))
        return reverse('maggioli-config-list')


class DatagraphConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_format': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/datagraph/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_format': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'url': 'https://example.local/datagraph/',
                    'codente': 'COD-111',
                    'username': 'WSDOCAREA',
                    'password': 's3cr3t',
                    'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                    'institution_code': 'COD-222',
                    'institution_email': 'bugliano-tributi@example.local',
                    'institution_ou': 'TRI',
                    'institution_aoo_code': 'COD-333',
                    'classification_institution_code': 'COD-444',
                    'classification_aoo_code': 'COD-555',
                    'classification_hierarchy': '7.2.0',
                    'folder_number': '42',
                    'folder_year': '2021',
                    'document_format': 'servizio online',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': DatagraphConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/datagraph/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_format': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'url': 'https://example.local/datagraph/',
                    'codente': 'COD-111',
                    'username': 'WSDOCAREA',
                    'password': 's3cr3t',
                    'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                    'institution_code': 'COD-222',
                    'institution_email': 'bugliano-tributi@example.local',
                    'institution_ou': 'TRI',
                    'institution_aoo_code': 'COD-333',
                    'classification_institution_code': 'COD-444',
                    'classification_aoo_code': 'COD-555',
                    'classification_hierarchy': '7.2.0',
                    'folder_number': '42',
                    'folder_year': '2021',
                    'document_format': 'servizio online',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'url': 'https://example.local/datagraph/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_format': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'url': 'https://example.local/datagraph/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_format': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Datagraph Config List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Active',
                        },
                        'url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'URL',
                            'max_length': 200,
                        },
                        'codente': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Codente',
                            'max_length': 64,
                        },
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Password',
                            'max_length': 64,
                        },
                        'institution_name': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution name',
                            'max_length': 1024,
                        },
                        'institution_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution code',
                            'max_length': 32,
                        },
                        'institution_email': {
                            'type': 'email',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution email',
                            'max_length': 254,
                        },
                        'institution_ou': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution OU',
                            'max_length': 32,
                        },
                        'institution_aoo_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'AOO code',
                            'max_length': 32,
                        },
                        'classification_institution_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Institution code for classification',
                            'max_length': 32,
                        },
                        'classification_aoo_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'AOO code for classification',
                            'max_length': 32,
                        },
                        'classification_hierarchy': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification hierarchy',
                            'max_length': 16,
                        },
                        'folder_number': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder number',
                            'max_length': 16,
                        },
                        'folder_year': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder year',
                            'max_length': 4,
                        },
                        'document_format': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Document format',
                            'max_length': 32,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('datagraph-config-detail', args=(pk,))
        return reverse('datagraph-config-list')


class DedagroupConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                        'classification_title': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_code': '2022.1.1',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'token_url': 'https://token.example.local',
                'base_url': 'https://base.example.local',
                'client_id': 'abc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'resource': 'http://base.example.local',
                'registry_id': 3,
                'author_id': 123456,
                'organization_chart_level_code': 'abcde12345',
                'institution_aoo_id': 123,
                'classification_title': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_code': '2022.1.1',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'token_url': 'https://token.example.local',
                    'base_url': 'https://base.example.local',
                    'client_id': 'abc',
                    'client_secret': 's3cr3t',
                    'grant_type': 'client_credentials',
                    'resource': 'http://base.example.local',
                    'registry_id': 3,
                    'author_id': 123456,
                    'organization_chart_level_code': 'abcde12345',
                    'institution_aoo_id': 123,
                    'classification_title': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_code': '2022.1.1',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': DedagroupConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'token_url': 'https://token.example.local',
                'base_url': 'https://base.example.local',
                'client_id': 'abc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'resource': 'http://base.example.local',
                'registry_id': 3,
                'author_id': 123456,
                'organization_chart_level_code': 'abcde12345',
                'institution_aoo_id': 123,
                'classification_title': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_code': '2022.1.1',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'token_url': 'https://token.example.local',
                    'base_url': 'https://base.example.local',
                    'client_id': 'abc',
                    'client_secret': 's3cr3t',
                    'grant_type': 'client_credentials',
                    'resource': 'http://base.example.local',
                    'registry_id': 3,
                    'author_id': 123456,
                    'organization_chart_level_code': 'abcde12345',
                    'institution_aoo_id': 123,
                    'classification_title': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_code': '2022.1.1',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'token_url': 'https://token.example.local',
                'base_url': 'https://base.example.local',
                'client_id': 'abc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'resource': 'http://base.example.local',
                'registry_id': 3,
                'author_id': 123456,
                'organization_chart_level_code': 'abcde12345',
                'institution_aoo_id': 123,
                'classification_title': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_code': '2022.1.1',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'token_url': 'https://token.example.local',
                'base_url': 'https://base.example.local',
                'client_id': 'abc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'resource': 'http://base.example.local',
                'registry_id': 3,
                'author_id': 123456,
                'organization_chart_level_code': 'abcde12345',
                'institution_aoo_id': 123,
                'classification_title': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_code': '2022.1.1',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Dedagroup Config List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Active',
                        },
                        'token_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Token URL',
                            'max_length': 200,
                        },
                        'base_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Base URL',
                            'max_length': 200,
                        },
                        'client_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Client ID',
                            'max_length': 64,
                        },
                        'client_secret': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Client secret',
                            'max_length': 64,
                        },
                        'grant_type': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Grant type',
                            'max_length': 64,
                        },
                        'resource': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Resource',
                            'max_length': 64,
                        },
                        'registry_id': {
                            'type': 'integer',
                            'required': True,
                            'read_only': False,
                            'label': 'Registry ID',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'author_id': {
                            'type': 'integer',
                            'required': True,
                            'read_only': False,
                            'label': 'Author ID',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'organization_chart_level_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Organization chart level code',
                            'max_length': 64,
                        },
                        'institution_aoo_id': {
                            'type': 'integer',
                            'required': True,
                            'read_only': False,
                            'label': 'AOO ID',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'classification_title': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification title',
                            'max_length': 16,
                        },
                        'classification_class': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification class',
                            'max_length': 16,
                        },
                        'classification_subclass': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification subclass',
                            'max_length': 16,
                        },
                        'folder_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder code',
                            'max_length': 64,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('dedagroup-config-detail', args=(pk,))
        return reverse('dedagroup-config-list')


class HalleyConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_id': 1,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'login_url': 'https://example.local/halley/login/',
                'registration_url': 'https://example.local/halley/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano',
                'institution_aoo_id': 1,
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_id': 1,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'login_url': 'https://example.local/halley/login/',
                    'registration_url': 'https://example.local/halley/registration/',
                    'username': 'console',
                    'password': 's3cr3t',
                    'institution_name': 'Comune di Bugliano',
                    'institution_aoo_id': 1,
                    'institution_office_ids': [],
                    'classification_category': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_id': 1,
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': HalleyConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'login_url': 'https://example.local/halley/login/',
                'registration_url': 'https://example.local/halley/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano',
                'institution_aoo_id': 1,
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_id': 1,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'login_url': 'https://example.local/halley/login/',
                    'registration_url': 'https://example.local/halley/registration/',
                    'username': 'console',
                    'password': 's3cr3t',
                    'institution_name': 'Comune di Bugliano',
                    'institution_aoo_id': 1,
                    'institution_office_ids': [],
                    'classification_category': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_id': 1,
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'login_url': 'https://example.local/halley/login/',
                'registration_url': 'https://example.local/halley/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano',
                'institution_aoo_id': 1,
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_id': 1,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'login_url': 'https://example.local/halley/login/',
                'registration_url': 'https://example.local/halley/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano',
                'institution_aoo_id': 1,
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_id': 1,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'Id'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {'type': 'boolean', 'required': True, 'read_only': False, 'label': 'Active'},
                        'login_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Login URL',
                            'max_length': 200,
                        },
                        'registration_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Registration URL',
                            'max_length': 200,
                        },
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Password',
                            'max_length': 64,
                        },
                        'institution_name': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Institution name',
                            'max_length': 1024,
                        },
                        'institution_aoo_id': {
                            'type': 'integer',
                            'required': True,
                            'read_only': False,
                            'label': 'AOO ID',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'institution_office_ids': {
                            'type': 'list',
                            'required': False,
                            'read_only': False,
                            'label': 'Institution office IDs',
                            'child': {
                                'type': 'integer',
                                'required': True,
                                'read_only': False,
                                'label': 'Institution office ids',
                                'min_value': 0,
                                'max_value': 2147483647,
                            },
                        },
                        'classification_category': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Classification category',
                            'max_length': 16,
                        },
                        'classification_class': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Classification class',
                            'max_length': 16,
                        },
                        'classification_subclass': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification subclass',
                            'max_length': 16,
                        },
                        'folder_id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder ID',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('halley-config-detail', args=(pk,))
        return reverse('halley-config-list')


class HalleyCloudConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'mailbox': 'example@example.com',
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_name': 'Fascicolo test',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'registration_url': 'https://example.local/halley_cloud/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'mailbox': 'example@example.com',
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_name': 'Fascicolo test',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'registration_url': 'https://example.local/halley_cloud/registration/',
                    'username': 'console',
                    'password': 's3cr3t',
                    'mailbox': 'example@example.com',
                    'institution_office_ids': [],
                    'classification_category': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_name': 'Fascicolo test',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': HalleyCloudConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'registration_url': 'https://example.local/halley_cloud/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'mailbox': 'example@example.com',
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_name': 'Fascicolo test',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'registration_url': 'https://example.local/halley_cloud/registration/',
                    'username': 'console',
                    'password': 's3cr3t',
                    'mailbox': 'example@example.com',
                    'institution_office_ids': [],
                    'classification_category': '7',
                    'classification_class': '2',
                    'classification_subclass': '0',
                    'folder_name': 'Fascicolo test',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'registration_url': 'https://example.local/halley_cloud/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'mailbox': 'example@example.com',
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_name': 'Fascicolo test',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'registration_url': 'https://example.local/halley_cloud/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'mailbox': 'example@example.com',
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_name': 'Fascicolo test',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Cloud Config List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Active',
                        },
                        'registration_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Registration URL',
                            'max_length': 200,
                        },
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Password',
                            'max_length': 64,
                        },
                        'mailbox': {
                            'type': 'email',
                            'required': False,
                            'read_only': False,
                            'label': 'Mailbox',
                            'max_length': 254,
                        },
                        'institution_office_ids': {
                            'type': 'list',
                            'required': False,
                            'read_only': False,
                            'label': 'Institution office IDs',
                            'help_text': 'The list of office ids is provided to the institution by the protocol '
                            'manager, if omitted the default office will be used',
                            'child': {
                                'type': 'integer',
                                'required': True,
                                'read_only': False,
                                'label': 'Institution office ids',
                                'min_value': 0,
                                'max_value': 2147483647,
                            },
                        },
                        'classification_category': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification category',
                            'help_text': 'The list of classifications is provided to the institution by the protocol '
                            'manager, if omitted the default classification will be used',
                            'max_length': 16,
                        },
                        'classification_class': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification class',
                            'max_length': 16,
                        },
                        'classification_subclass': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification subclass',
                            'max_length': 16,
                        },
                        'folder_name': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder name',
                            'help_text': 'The list of folder names is provided to the institution by the protocol '
                            'manager, if omitted the default folder will be used',
                            'max_length': 64,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('halley-cloud-config-detail', args=(pk,))
        return reverse('halley-cloud-config-list')


class PitreConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'base_url': 'https://base.example.local',
                        'certificate': 's3cr3t',
                        'username': 'sdc',
                        'code_role': 'rda_sdc',
                        'code_application': 'sdc',
                        'code_adm': 'ABC',
                        'code_node_classification': '1.23',
                        'transmission_reason': 'TRASM ONLINE',
                        'transmission_type': 'T',
                        'receiver_codes': ['123'],
                        'recipient_code': 'rda_recipient',
                        'code_register': 'ABC',
                        'means_of_sending': 'SERVIZI ONLINE',
                        'code_rf': 'ABC',
                        'note': 'Outbound document note',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        PitreConfig.objects.create(**get_pitre_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'base_url': 'https://base.example.local',
                'certificate': 's3cr3t',
                'username': 'sdc',
                'code_role': 'rda_sdc',
                'code_application': 'sdc',
                'code_adm': 'ABC',
                'code_node_classification': '1.23',
                'transmission_reason': 'TRASM ONLINE',
                'transmission_type': 'T',
                'receiver_codes': ['123'],
                'recipient_code': 'rda_recipient',
                'code_register': 'ABC',
                'means_of_sending': 'SERVIZI ONLINE',
                'code_rf': 'ABC',
                'note': 'Outbound document note',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'base_url': 'https://base.example.local',
                    'certificate': 's3cr3t',
                    'username': 'sdc',
                    'code_role': 'rda_sdc',
                    'code_application': 'sdc',
                    'code_adm': 'ABC',
                    'code_node_classification': '1.23',
                    'transmission_reason': 'TRASM ONLINE',
                    'transmission_type': 'T',
                    'receiver_codes': ['123'],
                    'recipient_code': 'rda_recipient',
                    'code_register': 'ABC',
                    'means_of_sending': 'SERVIZI ONLINE',
                    'code_rf': 'ABC',
                    'note': 'Outbound document note',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': PitreConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'base_url': 'https://base.example.local',
                'certificate': 's3cr3t',
                'username': 'sdc',
                'code_role': 'rda_sdc',
                'code_application': 'sdc',
                'code_adm': 'ABC',
                'code_node_classification': '1.23',
                'transmission_reason': 'TRASM ONLINE',
                'transmission_type': 'T',
                'receiver_codes': ['123'],
                'recipient_code': 'rda_recipient',
                'code_register': 'ABC',
                'means_of_sending': 'SERVIZI ONLINE',
                'code_rf': 'ABC',
                'note': 'Outbound document note',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'base_url': 'https://base.example.local',
                    'certificate': 's3cr3t',
                    'username': 'sdc',
                    'code_role': 'rda_sdc',
                    'code_application': 'sdc',
                    'code_adm': 'ABC',
                    'code_node_classification': '1.23',
                    'transmission_reason': 'TRASM ONLINE',
                    'transmission_type': 'T',
                    'receiver_codes': ['123'],
                    'recipient_code': 'rda_recipient',
                    'code_register': 'ABC',
                    'means_of_sending': 'SERVIZI ONLINE',
                    'code_rf': 'ABC',
                    'note': 'Outbound document note',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'base_url': 'https://base.example.local',
                'certificate': 's3cr3t',
                'username': 'sdc',
                'code_role': 'rda_sdc',
                'code_application': 'sdc',
                'code_adm': 'ABC',
                'code_node_classification': '1.23',
                'transmission_reason': 'TRASM ONLINE',
                'transmission_type': 'T',
                'receiver_codes': ['123'],
                'recipient_code': 'rda_recipient',
                'code_register': 'ABC',
                'means_of_sending': 'SERVIZI ONLINE',
                'code_rf': 'ABC',
                'note': 'Outbound document note',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = PitreConfig.objects.create(**get_pitre_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'base_url': 'https://base.example.local',
                'certificate': 's3cr3t',
                'username': 'sdc',
                'code_role': 'rda_sdc',
                'code_application': 'sdc',
                'code_adm': 'ABC',
                'code_node_classification': '1.23',
                'transmission_reason': 'TRASM ONLINE',
                'transmission_type': 'T',
                'receiver_codes': ['123'],
                'recipient_code': 'rda_recipient',
                'code_register': 'ABC',
                'means_of_sending': 'SERVIZI ONLINE',
                'code_rf': 'ABC',
                'note': 'Outbound document note',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = PitreConfig.objects.create(**get_pitre_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Pitre Config List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Active',
                        },
                        'base_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Base URL',
                            'max_length': 200,
                        },
                        'certificate': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Certificate',
                        },
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 32,
                        },
                        'code_role': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Code role',
                            'max_length': 32,
                        },
                        'code_application': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Code application',
                            'max_length': 32,
                        },
                        'code_adm': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Code ADM',
                            'max_length': 32,
                        },
                        'code_node_classification': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Code node classification',
                            'max_length': 8,
                        },
                        'transmission_reason': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Transmission reason',
                            'max_length': 128,
                        },
                        'transmission_type': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Transmission_type',
                            'max_length': 128,
                        },
                        'receiver_codes': {
                            'type': 'list',
                            'required': True,
                            'read_only': False,
                            'label': 'Transmission receiver codes',
                            'help_text': 'List of role codes to transmit the creation of the project or document to',
                            'child': {
                                'type': 'string',
                                'required': True,
                                'read_only': False,
                                'label': 'Transmission receiver code',
                                'max_length': 32,
                            },
                        },
                        'recipient_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Recipient code',
                            'help_text': 'Recipient role code for inbound documents, sender for outbound documents. '
                            'If not set the role used for integration will be used',
                            'max_length': 32,
                        },
                        'code_register': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Code register',
                            'max_length': 32,
                        },
                        'means_of_sending': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Means of sending',
                            'max_length': 128,
                        },
                        'code_rf': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Code RF',
                            'max_length': 8,
                        },
                        'note': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Note',
                            'help_text': 'Outbound document note',
                            'max_length': 256,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('pitre-config-detail', args=(pk,))
        return reverse('pitre-config-list')


class InsielConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'login_url': 'https://base.example.local/token',
                        'registry_url': 'https://base.example.local/protocollo',
                        'file_transfer_url': 'https://base.example.local/filetransfer',
                        'client_id': 'sdc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'user_code': 'sdc',
                        'user_password': 's3cr3t',
                        'operating_office_code': '1',
                        'office_code': 'ABC',
                        'office_registry': 'ABC',
                        'internal_offices_codes': [],
                        'internal_offices_types': [],
                        'register_code': '  1',
                        'prog_doc': None,
                        'prog_movi': None,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        InsielConfig.objects.create(**get_insiel_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'login_url': 'https://base.example.local/token',
                'registry_url': 'https://base.example.local/protocollo',
                'file_transfer_url': 'https://base.example.local/filetransfer',
                'client_id': 'sdc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'user_code': 'sdc',
                'user_password': 's3cr3t',
                'operating_office_code': '1',
                'office_code': 'ABC',
                'office_registry': 'ABC',
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'register_code': '  1',
                'prog_doc': None,
                'prog_movi': None,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'login_url': 'https://base.example.local/token',
                    'registry_url': 'https://base.example.local/protocollo',
                    'file_transfer_url': 'https://base.example.local/filetransfer',
                    'client_id': 'sdc',
                    'client_secret': 's3cr3t',
                    'grant_type': 'client_credentials',
                    'user_code': 'sdc',
                    'user_password': 's3cr3t',
                    'operating_office_code': '1',
                    'office_code': 'ABC',
                    'office_registry': 'ABC',
                    'internal_offices_codes': [],
                    'internal_offices_types': [],
                    'register_code': '  1',
                    'prog_doc': None,
                    'prog_movi': None,
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': InsielConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'login_url': 'https://base.example.local/token',
                'registry_url': 'https://base.example.local/protocollo',
                'file_transfer_url': 'https://base.example.local/filetransfer',
                'client_id': 'sdc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'user_code': 'sdc',
                'user_password': 's3cr3t',
                'operating_office_code': '1',
                'office_code': 'ABC',
                'office_registry': 'ABC',
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'register_code': '  1',
                'prog_doc': None,
                'prog_movi': None,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'login_url': 'https://base.example.local/token',
                    'registry_url': 'https://base.example.local/protocollo',
                    'file_transfer_url': 'https://base.example.local/filetransfer',
                    'client_id': 'sdc',
                    'client_secret': 's3cr3t',
                    'grant_type': 'client_credentials',
                    'user_code': 'sdc',
                    'user_password': 's3cr3t',
                    'operating_office_code': '1',
                    'office_code': 'ABC',
                    'office_registry': 'ABC',
                    'internal_offices_codes': [],
                    'internal_offices_types': [],
                    'register_code': '  1',
                    'prog_doc': None,
                    'prog_movi': None,
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'login_url': 'https://base.example.local/token',
                'registry_url': 'https://base.example.local/protocollo',
                'file_transfer_url': 'https://base.example.local/filetransfer',
                'client_id': 'sdc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'user_code': 'sdc',
                'user_password': 's3cr3t',
                'operating_office_code': '1',
                'office_code': 'ABC',
                'office_registry': 'ABC',
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'register_code': '  1',
                'prog_doc': None,
                'prog_movi': None,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InsielConfig.objects.create(**get_insiel_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'login_url': 'https://base.example.local/token',
                'registry_url': 'https://base.example.local/protocollo',
                'file_transfer_url': 'https://base.example.local/filetransfer',
                'client_id': 'sdc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'user_code': 'sdc',
                'user_password': 's3cr3t',
                'operating_office_code': '1',
                'office_code': 'ABC',
                'office_registry': 'ABC',
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'register_code': '  1',
                'prog_doc': None,
                'prog_movi': None,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InsielConfig.objects.create(**get_insiel_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Insiel Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'Id'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {'type': 'boolean', 'required': True, 'read_only': False, 'label': 'Active'},
                        'login_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Login URL',
                            'max_length': 200,
                        },
                        'registry_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Protocol Web service URL',
                            'max_length': 200,
                        },
                        'file_transfer_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'File transfer Web service URL',
                            'max_length': 200,
                        },
                        'client_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Client id',
                            'max_length': 32,
                        },
                        'client_secret': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Client secret',
                            'max_length': 32,
                        },
                        'grant_type': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Grant type',
                            'max_length': 32,
                        },
                        'user_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': "User's code",
                            'max_length': 32,
                        },
                        'user_password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': "User's password",
                            'max_length': 32,
                        },
                        'operating_office_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': "Operating office's code",
                            'max_length': 32,
                        },
                        'office_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': "Office's code",
                            'max_length': 32,
                        },
                        'office_registry': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': "Office's registry",
                            'max_length': 32,
                        },
                        'internal_offices_codes': {
                            'type': 'list',
                            'required': False,
                            'read_only': False,
                            'label': "Internal office's codes",
                            'child': {
                                'type': 'string',
                                'required': True,
                                'read_only': False,
                                'label': "Internal office's code",
                                'max_length': 32,
                            },
                        },
                        'internal_offices_types': {
                            'type': 'list',
                            'required': False,
                            'read_only': False,
                            'label': "Internal office's types",
                            'child': {
                                'type': 'string',
                                'required': True,
                                'read_only': False,
                                'label': "Internal office's type",
                                'max_length': 32,
                            },
                        },
                        'register_code': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Register code',
                            'help_text': "Right aligned levels: ex: '1  2  3'. This field is required "
                            'if prog_doc and prog_movi are not defined.',
                            'max_length': 32,
                        },
                        'prog_doc': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Register prog doc',
                            'help_text': 'This field is required if prog_movi is defined',
                            'max_length': 32,
                        },
                        'prog_movi': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Register prog movi',
                            'help_text': 'This field is required if prog_doc is defined',
                            'max_length': 32,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('insiel-config-detail', args=(pk,))
        return reverse('insiel-config-list')


class InforConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'auth_username': None,
                        'auth_password': None,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        InforConfig.objects.create(**get_infor_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'auth_username': None,
                'auth_password': None,
                'username': 'sdc',
                'denomination': 'sdc',
                'email': 'user@example.com',
                'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                'registry_url': 'https://base.example.local/protocollo',
                'incoming_document_type': 'xx',
                'incoming_intermediary': 'WEB',
                'incoming_sorting': 'ABC',
                'incoming_classification': '16.02',
                'incoming_folder': '2012/6',
                'response_internal_sender': 'sdc',
                'response_document_type': 'xx',
                'response_intermediary': 'WEB',
                'response_sorting': 'ABC',
                'response_classification': '16.02',
                'response_folder': '2012/6',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'auth_username': 'username',
                    'auth_password': 'password',
                    'username': 'sdc',
                    'denomination': 'sdc',
                    'email': 'user@example.com',
                    'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                    'registry_url': 'https://base.example.local/protocollo',
                    'incoming_document_type': 'xx',
                    'incoming_intermediary': 'WEB',
                    'incoming_sorting': 'ABC',
                    'incoming_classification': '16.02',
                    'incoming_folder': '2012/6',
                    'response_internal_sender': 'sdc',
                    'response_document_type': 'xx',
                    'response_intermediary': 'WEB',
                    'response_sorting': 'ABC',
                    'response_classification': '16.02',
                    'response_folder': '2012/6',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': InforConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'auth_username': 'username',
                'auth_password': 'password',
                'username': 'sdc',
                'denomination': 'sdc',
                'email': 'user@example.com',
                'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                'registry_url': 'https://base.example.local/protocollo',
                'incoming_document_type': 'xx',
                'incoming_intermediary': 'WEB',
                'incoming_sorting': 'ABC',
                'incoming_classification': '16.02',
                'incoming_folder': '2012/6',
                'response_internal_sender': 'sdc',
                'response_document_type': 'xx',
                'response_intermediary': 'WEB',
                'response_sorting': 'ABC',
                'response_classification': '16.02',
                'response_folder': '2012/6',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'auth_username': 'username',
                    'auth_password': 'password',
                    'username': 'sdc',
                    'denomination': 'sdc',
                    'email': 'user@example.com',
                    'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                    'registry_url': 'https://base.example.local/protocollo',
                    'incoming_document_type': 'xx',
                    'incoming_intermediary': 'WEB',
                    'incoming_sorting': 'ABC',
                    'incoming_classification': '16.02',
                    'incoming_folder': '2012/6',
                    'response_internal_sender': 'sdc',
                    'response_document_type': 'xx',
                    'response_intermediary': 'WEB',
                    'response_sorting': 'ABC',
                    'response_classification': '16.02',
                    'response_folder': '2012/6',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'auth_username': 'username',
                'auth_password': 'password',
                'username': 'sdc',
                'denomination': 'sdc',
                'email': 'user@example.com',
                'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                'registry_url': 'https://base.example.local/protocollo',
                'incoming_document_type': 'xx',
                'incoming_intermediary': 'WEB',
                'incoming_sorting': 'ABC',
                'incoming_classification': '16.02',
                'incoming_folder': '2012/6',
                'response_internal_sender': 'sdc',
                'response_document_type': 'xx',
                'response_intermediary': 'WEB',
                'response_sorting': 'ABC',
                'response_classification': '16.02',
                'response_folder': '2012/6',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'auth_username': None,
                'auth_password': None,
                'username': 'sdc',
                'denomination': 'sdc',
                'email': 'user@example.com',
                'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                'registry_url': 'https://base.example.local/protocollo',
                'incoming_document_type': 'xx',
                'incoming_intermediary': 'WEB',
                'incoming_sorting': 'ABC',
                'incoming_classification': '16.02',
                'incoming_folder': '2012/6',
                'response_internal_sender': 'sdc',
                'response_document_type': 'xx',
                'response_intermediary': 'WEB',
                'response_sorting': 'ABC',
                'response_classification': '16.02',
                'response_folder': '2012/6',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InforConfig.objects.create(**get_infor_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Infor Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'Id'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {'type': 'boolean', 'required': True, 'read_only': False, 'label': 'Active'},
                        'auth_username': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Auth username',
                            'max_length': 64,
                        },
                        'auth_password': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Auth password',
                            'max_length': 64,
                        },
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'denomination': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Denomination',
                            'max_length': 128,
                        },
                        'email': {
                            'type': 'email',
                            'required': True,
                            'read_only': False,
                            'label': 'Email',
                            'max_length': 254,
                        },
                        'wsdl_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Protocol WSDL URL',
                            'max_length': 200,
                        },
                        'registry_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Protocol Web service URL',
                            'max_length': 200,
                        },
                        'incoming_document_type': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Incoming document type',
                            'max_length': 64,
                        },
                        'incoming_intermediary': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Incoming intermediary',
                            'max_length': 64,
                        },
                        'incoming_sorting': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Incoming sorting',
                            'max_length': 64,
                        },
                        'incoming_classification': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Incoming classification',
                            'max_length': 16,
                        },
                        'incoming_folder': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Incoming folder',
                            'max_length': 64,
                        },
                        'response_internal_sender': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response internal sender',
                            'max_length': 254,
                        },
                        'response_document_type': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response document type',
                            'max_length': 64,
                        },
                        'response_intermediary': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response intermediary',
                            'max_length': 64,
                        },
                        'response_sorting': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response sorting',
                            'max_length': 64,
                        },
                        'response_classification': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response classification',
                            'max_length': 16,
                        },
                        'response_folder': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Response folder',
                            'max_length': 64,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('infor-config-detail', args=(pk,))
        return reverse('infor-config-list')


class D3ConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        D3Config.objects.create(**get_d3_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/',
                'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                'main_source_category': 'EXAMPLE',
                'register_attachments': True,
                'attachment_source_category': 'EXAMPLE2',
                'document_subtype': 'Allgmein - generico',
                'classification': '15.02.02',
                'folder_name': '1234',
                'competent_service': 'Sekretariat - Segreteria',
                'visibility': 'Sekretariat - Segreteria: S',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'username': 'sdc',
                    'password': 's3cr3t',
                    'registry_url': 'https://base.example.local/',
                    'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                    'main_source_category': 'EXAMPLE',
                    'register_attachments': True,
                    'attachment_source_category': 'EXAMPLE2',
                    'document_subtype': 'Allgmein - generico',
                    'classification': '15.02.02',
                    'folder_name': '1234',
                    'competent_service': 'Sekretariat - Segreteria',
                    'visibility': 'Sekretariat - Segreteria: S',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': D3Config.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/',
                'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                'main_source_category': 'EXAMPLE',
                'register_attachments': True,
                'attachment_source_category': 'EXAMPLE2',
                'document_subtype': 'Allgmein - generico',
                'classification': '15.02.02',
                'folder_name': '1234',
                'competent_service': 'Sekretariat - Segreteria',
                'visibility': 'Sekretariat - Segreteria: S',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'username': 'sdc',
                    'password': 's3cr3t',
                    'registry_url': 'https://base.example.local/',
                    'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                    'main_source_category': 'EXAMPLE',
                    'register_attachments': True,
                    'attachment_source_category': 'EXAMPLE2',
                    'document_subtype': 'Allgmein - generico',
                    'classification': '15.02.02',
                    'folder_name': '1234',
                    'competent_service': 'Sekretariat - Segreteria',
                    'visibility': 'Sekretariat - Segreteria: S',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/',
                'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                'main_source_category': 'EXAMPLE',
                'register_attachments': True,
                'attachment_source_category': 'EXAMPLE2',
                'document_subtype': 'Allgmein - generico',
                'classification': '15.02.02',
                'folder_name': '1234',
                'competent_service': 'Sekretariat - Segreteria',
                'visibility': 'Sekretariat - Segreteria: S',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/',
                'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                'main_source_category': 'EXAMPLE',
                'register_attachments': True,
                'attachment_source_category': 'EXAMPLE2',
                'document_subtype': 'Allgmein - generico',
                'classification': '15.02.02',
                'folder_name': '1234',
                'competent_service': 'Sekretariat - Segreteria',
                'visibility': 'Sekretariat - Segreteria: S',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'D3 Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'Id'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {'type': 'boolean', 'required': True, 'read_only': False, 'label': 'Active'},
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Password',
                            'max_length': 32,
                        },
                        'registry_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Registry URL',
                            'max_length': 200,
                        },
                        'repository_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Repository ID',
                            'max_length': 64,
                        },
                        'main_source_category': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Main Source Category',
                            'max_length': 64,
                        },
                        'register_attachments': {
                            'type': 'boolean',
                            'required': True,
                            'read_only': False,
                            'label': 'Include attachments in registration',
                        },
                        'attachment_source_category': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Attachment Source Category',
                            'max_length': 64,
                        },
                        'document_subtype': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Document Subtype',
                            'max_length': 128,
                        },
                        'classification': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification',
                            'max_length': 128,
                        },
                        'folder_name': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder name',
                            'help_text': 'The list of folder names is provided to the institution by the '
                            'protocol manager, if omitted the default folder will be used',
                            'max_length': 64,
                        },
                        'competent_service': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Competent service',
                            'max_length': 254,
                        },
                        'visibility': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Visibility',
                            'max_length': 254,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('d3-config-detail', args=(pk,))
        return reverse('d3-config-list')


class HypersicConfigAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'is_active': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        response = self.client.get(self._url(), {'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        response = self.client.get(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/protocollo',
                'office_ids': [123],
                'classification': '6-3-0',
                'folder_number': '2',
                'folder_year': '2021',
                'annotation': 'something',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Test configuration',
                    'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'is_active': True,
                    'username': 'sdc',
                    'password': 's3cr3t',
                    'registry_url': 'https://base.example.local/protocollo',
                    'office_ids': [123],
                    'classification': '6-3-0',
                    'folder_number': '2',
                    'folder_year': '2021',
                    'annotation': 'something',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': HypersicConfig.objects.get().id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/protocollo',
                'office_ids': [123],
                'classification': '6-3-0',
                'folder_number': '2',
                'folder_year': '2021',
                'annotation': 'something',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        Service.objects.create(**get_service_data(tenant=tenant, sdc_id='684819aa-7cbe-4c21-8ef3-47167156e13e'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=config.pk),
                {
                    'description': 'Update test configuration',
                    'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                    'is_active': False,
                    'username': 'sdc',
                    'password': 's3cr3t',
                    'registry_url': 'https://base.example.local/protocollo',
                    'office_ids': [123],
                    'classification': '6-3-0',
                    'folder_number': '2',
                    'folder_year': '2021',
                    'annotation': 'something',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Update test configuration',
                'service_sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e',
                'is_active': False,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/protocollo',
                'office_ids': [123],
                'classification': '6-3-0',
                'folder_number': '2',
                'folder_year': '2021',
                'annotation': 'something',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=config.pk),
                {'is_active': False},
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/protocollo',
                'office_ids': [123],
                'classification': '6-3-0',
                'folder_number': '2',
                'folder_year': '2021',
                'annotation': 'something',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        response = self.client.delete(self._url(pk=config.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Hypersic Config List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'Id'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'service_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Service sdc id',
                        },
                        'is_active': {'type': 'boolean', 'required': True, 'read_only': False, 'label': 'Active'},
                        'username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Username',
                            'max_length': 64,
                        },
                        'password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'Password',
                            'max_length': 64,
                        },
                        'registry_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'Protocol Web service URL',
                            'max_length': 200,
                        },
                        'office_ids': {
                            'type': 'list',
                            'required': False,
                            'read_only': False,
                            'label': 'Office ids',
                            'help_text': 'The list of office ids is provided to the institution by the '
                            'protocol manager',
                            'child': {
                                'type': 'integer',
                                'required': True,
                                'read_only': False,
                                'label': 'Office ids',
                                'min_value': 0,
                                'max_value': 2147483647,
                            },
                        },
                        'classification': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Classification',
                            'max_length': 16,
                        },
                        'folder_number': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder number',
                            'max_length': 16,
                        },
                        'folder_year': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Folder year',
                            'max_length': 16,
                        },
                        'annotation': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Registration annotation',
                            'max_length': 128,
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('hypersic-config-detail', args=(pk,))
        return reverse('hypersic-config-list')


class TinnServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'url': 'https://example.local/tinn/',
                                'codente': 'COD-111',
                                'username': 'WSDOCAREA',
                                'password': 's3cr3t',
                                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                                'institution_code': 'COD-222',
                                'institution_email': 'bugliano-tributi@example.local',
                                'institution_ou': 'TRI',
                                'institution_aoo_code': 'COD-333',
                                'classification_institution_code': 'COD-444',
                                'classification_aoo_code': 'COD-555',
                                'classification_hierarchy': '7.2.0',
                                'folder_number': '42',
                                'folder_year': '2021',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'url': 'https://example.local/tinn/',
                            'codente': 'COD-111',
                            'username': 'WSDOCAREA',
                            'password': 's3cr3t',
                            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                            'institution_code': 'COD-222',
                            'institution_email': 'bugliano-tributi@example.local',
                            'institution_ou': 'TRI',
                            'institution_aoo_code': 'COD-333',
                            'classification_institution_code': 'COD-444',
                            'classification_aoo_code': 'COD-555',
                            'classification_hierarchy': '7.2.0',
                            'folder_number': '42',
                            'folder_year': '2021',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': TinnConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        TinnConfig.objects.create(**get_tinn_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        TinnConfig.objects.create(**get_tinn_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Tinn Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'codente': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codente',
                                        'max_length': 64,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution name',
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution code',
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution email',
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution OU',
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'AOO code',
                                        'max_length': 32,
                                    },
                                    'classification_institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution code for classification',
                                        'max_length': 32,
                                    },
                                    'classification_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'AOO code for classification',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Classification hierarchy',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder number',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder year',
                                        'max_length': 4,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('tinn-service-detail', args=(sdc_id,))
        return reverse('tinn-service-list')


class SicrawebWSProtocolloDMServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'url': 'https://example.local/sicraweb-wsprotocollodm/',
                                'connection_string': 'test_conn_string',
                                'institution_code': 'COD-111',
                                'institution_ou': 'TRI',
                                'institution_aoo_code': 'COD-222',
                                'sending_mode': 'PEC',
                                'internal_sender': 'ANA',
                                'update_records': 'S',
                                'classification_hierarchy': '7.2.0',
                                'folder_number': '42',
                                'folder_year': '2021',
                                'connection_user': 'opencontent',
                                'connection_user_role': 'user',
                                'standard_subject': 'subject',
                                'document_type': 'servizio online',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_code': 'COD-111',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'internal_sender': 'ANA',
                        'update_records': 'S',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'connection_user': 'opencontent',
                        'connection_user_role': 'user',
                        'standard_subject': 'subject',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'url': 'https://example.local/sicraweb-wsprotocollodm/',
                            'connection_string': 'test_conn_string',
                            'institution_code': 'COD-111',
                            'institution_ou': 'TRI',
                            'institution_aoo_code': 'COD-222',
                            'sending_mode': 'PEC',
                            'internal_sender': 'ANA',
                            'update_records': 'S',
                            'classification_hierarchy': '7.2.0',
                            'folder_number': '42',
                            'folder_year': '2021',
                            'connection_user': 'opencontent',
                            'connection_user_role': 'user',
                            'standard_subject': 'subject',
                            'document_type': 'servizio online',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': SicrawebWSProtocolloDMConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_code': 'COD-111',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'internal_sender': 'ANA',
                        'update_records': 'S',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'connection_user': 'opencontent',
                        'connection_user_role': 'user',
                        'standard_subject': 'subject',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Sicraweb Ws Protocollo Dm Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'connection_string': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Connection string',
                                        'max_length': 64,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution code',
                                        'max_length': 32,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Institution OU',
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'AOO code',
                                        'max_length': 32,
                                    },
                                    'sending_mode': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sending mode',
                                        'max_length': 32,
                                    },
                                    'internal_sender': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Internal sender',
                                        'max_length': 32,
                                    },
                                    'update_records': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Update records',
                                        'help_text': 'One of: N, S or F',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification hierarchy',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder number',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder year',
                                        'max_length': 4,
                                    },
                                    'connection_user': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Connection user',
                                        'max_length': 32,
                                    },
                                    'connection_user_role': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Connection user role',
                                        'max_length': 32,
                                    },
                                    'standard_subject': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Standard subject',
                                        'max_length': 32,
                                    },
                                    'document_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Document type',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('sicraweb-wsprotocollodm-service-detail', args=(sdc_id,))
        return reverse('sicraweb-wsprotocollodm-service-list')


class SiscomServiceAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_queryset(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url())

        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SiscomConfig.objects.create(
                **get_siscom_config_data(service=service),
            )
        service.application_states.set([state])

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': service.id,
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': ['example_state'],
                        'configs': [
                            {
                                'id': config.id,
                                'description': 'Test configuration',
                                'is_active': True,
                                'url': 'https://example.local/siscom/',
                                'license_code': '1008',
                                'operator_denomination': 'John Doe',
                                'classification_category': '0',
                                'classification_class': '0',
                                'folder_id': '0',
                                'mail_type': '0',
                                'office_id': '7',
                                'created_at': '2021-05-25T10:20:30.123456Z',
                                'modified_at': '2021-05-25T10:20:30.123456Z',
                            },
                        ],
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.get(self._url(), {'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SiscomConfig.objects.create(
                **get_siscom_config_data(service=service),
            )
        service.application_states.set([state])

        response = self.client.get(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'operator_denomination': 'John Doe',
                        'classification_category': '0',
                        'classification_class': '0',
                        'folder_id': '0',
                        'mail_type': '0',
                        'office_id': '7',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data())

        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Posizione TARI',
                    'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                    'application_states': ['example_state'],
                    'configs': [
                        {
                            'description': 'Test configuration',
                            'is_active': True,
                            'url': 'https://example.local/siscom/',
                            'license_code': '1008',
                            'operator_denomination': 'John Doe',
                            'classification_category': '0',
                            'classification_class': '0',
                            'folder_id': '0',
                            'mail_type': '0',
                            'office_id': '7',
                        },
                    ],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Service.objects.get().id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': SiscomConfig.objects.get().id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'operator_denomination': 'John Doe',
                        'classification_category': '0',
                        'classification_class': '0',
                        'folder_id': '0',
                        'mail_type': '0',
                        'office_id': '7',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create_optional_application_states(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.post(
            self._url(),
            {
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        service.application_states.set([state])
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                {
                    'description': 'Permesso raccolta funghi',
                    'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                    'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                    'application_states': [],
                    'configs': [],
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': service.id,
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'application_states': [],
                'configs': [],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_update_required_application_states(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        Tenant.objects.create(**get_tenant_data(slug='other-slug', sdc_id='17d18a29-996d-4850-b0e3-36df4a80768b'))

        response = self.client.put(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {
                'description': 'Permesso raccolta funghi',
                'tenant_sdc_id': '17d18a29-996d-4850-b0e3-36df4a80768b',
                'sdc_id': 'f8e005a9-1362-4401-a3db-edde97e43605',
                'configs': [],
            },
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'application_states': ['This field is required.']})

    def test_partial_update_not_allowed(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.patch(
            self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
            {},
        )

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        response = self.client.delete(self._url(sdc_id='5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Siscom Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Tenant SDC ID',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Application states',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configs',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Description',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Active',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'license_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codice licenza',
                                        'max_length': 16,
                                    },
                                    'operator_denomination': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Operator denomination',
                                        'max_length': 128,
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification category',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification class',
                                        'max_length': 16,
                                    },
                                    'folder_id': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder ID',
                                        'max_length': 16,
                                    },
                                    'mail_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Mail type',
                                        'max_length': 16,
                                    },
                                    'office_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Office id',
                                        'max_length': 16,
                                        'help_text': 'The office id is provided to '
                                        'the institution by the protocol manager',
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Created at',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modified at',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, sdc_id=None):
        if sdc_id:
            return reverse('siscom-service-detail', args=(sdc_id,))
        return reverse('siscom-service-list')


class RegistrationAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'application_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        with freeze_time(NOW):
            registration = Registration.objects.create(**get_registration_data())

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': registration.id,
                        'number': '1234',
                        'date': '2021-01-02T10:20:30.123456Z',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                        'application_state_name': 'example_state',
                        'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
                        'is_completed': True,
                        'is_updated': True,
                        'registration_data': {},
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        Registration.objects.create(**get_registration_data())

        response = self.client.get(self._url(), {'application_sdc_id': '2c9f1c1a-c907-43ac-9bd4-37257616a48d'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        with freeze_time(NOW):
            registration = Registration.objects.create(**get_registration_data())

        response = self.client.get(self._url(pk=registration.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': registration.id,
                'number': '1234',
                'date': '2021-01-02T10:20:30.123456Z',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                'application_state_name': 'example_state',
                'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
                'is_completed': True,
                'is_updated': True,
                'registration_data': {},
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Registration List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('registration-detail', args=(pk,))
        return reverse('registration-list')


class PreRegistrationAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'application_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        with freeze_time(NOW):
            pre_registration = PreRegistration.objects.create(**get_pre_registration_data())

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': pre_registration.id,
                        'number': '1234',
                        'folder': '123',
                        'date': '2021-01-02T10:20:30.123456Z',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                        'application_state_name': 'example_state',
                        'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
                        'pre_registration_data': {},
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        PreRegistration.objects.create(**get_pre_registration_data())

        response = self.client.get(self._url(), {'application_sdc_id': '2c9f1c1a-c907-43ac-9bd4-37257616a48d'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        with freeze_time(NOW):
            pre_registration = PreRegistration.objects.create(**get_pre_registration_data())

        response = self.client.get(self._url(pk=pre_registration.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': pre_registration.id,
                'number': '1234',
                'folder': '123',
                'date': '2021-01-02T10:20:30.123456Z',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                'application_state_name': 'example_state',
                'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
                'pre_registration_data': {},
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Pre Registration List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('pre-registration-detail', args=(pk,))
        return reverse('pre-registration-list')


class DeadLetterAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'tenant_sdc_id': 'invalid'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        with freeze_time(NOW):
            dead_letter = DeadLetter.objects.create(**get_dead_letter_data())

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': dead_letter.id,
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                        'state': 'new',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        DeadLetter.objects.create(**get_dead_letter_data())

        response = self.client.get(self._url(), {'tenant_sdc_id': '45bf8f48-e31d-4169-a989-55de3c3d82e2'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        with freeze_time(NOW):
            dead_letter = DeadLetter.objects.create(**get_dead_letter_data())

        response = self.client.get(self._url(pk=dead_letter.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': dead_letter.id,
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                'state': 'new',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_destroy(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())

        response = self.client.delete(self._url(pk=dead_letter.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_soft_delete(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())

        self.client.delete(self._url(pk=dead_letter.pk))

        self.assertEqual(DeadLetter.objects.count(), 0)
        self.assertEqual(DeadLetter._base_manager.count(), 1)

        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {'count': 0, 'next': None, 'previous': None, 'results': []})

        response = self.client.get(self._url(pk=dead_letter.pk))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retry_nonexistent_object(self):
        stub = TestRetryProducer()

        with stub_retry_producer_class(stub):
            response = self.client.post(self._retry_url(pk=1))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        stub.assert_send_not_called()

    def test_retry_non_retryable_object(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data(state=DeadLetter.State.QUEUED))
        stub = TestRetryProducer()

        with stub_retry_producer_class(stub):
            response = self.client.post(self._retry_url(pk=dead_letter.pk))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        stub.assert_send_not_called()

    def test_retry_success(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        stub = TestRetryProducer()

        with stub_retry_producer_class(stub):
            response = self.client.post(self._retry_url(pk=dead_letter.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        stub.assert_send('applications_test', b'{"key": "value"}')

    def test_retry_failure(self):
        dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        stub = TestRetryProducer(send_error=Exception('xyz failed'))

        with stub_retry_producer_class(stub):
            response = self.client.post(self._retry_url(pk=dead_letter.pk))

        self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        stub.assert_send('applications_test', b'{"key": "value"}')

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Dead Letter List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('dead-letter-detail', args=(pk,))
        return reverse('dead-letter-list')

    @staticmethod
    def _retry_url(*, pk):
        return reverse('dead-letter-retry', args=(pk,))


class TenantAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_nonexistent_object(self):
        response = self.client.get(self._url(pk=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_payload(self):
        response = self.client.post(self._url(), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_filter(self):
        response = self.client.get(self._url(), {'sdc_id': '123'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**get_tenant_data())

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': tenant.id,
                        'description': 'Comune Di Bugliano',
                        'slug': 'comune-di-bugliano',
                        'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_base_url': 'https://example.local/comune-di-bugliano/',
                        'sdc_username': 'johndoe',
                        'sdc_password': 's3cr3t',
                        'sdc_institution_code': 'SDC',
                        'sdc_aoo_code': 'P_SDC',
                        'latest_registration_number': 42,
                        'latest_registration_number_issued_at': '2021-01-02T11:20:30.123456+01:00',
                        'register_after_date': '2000-01-01T11:20:30.123456+01:00',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
            },
        )

    def test_list_filter(self):
        Tenant.objects.create(**get_tenant_data())

        response = self.client.get(self._url(), {'sdc_id': '684819aa-7cbe-4c21-8ef3-47167156e13e'})

        self.assertEqual(
            response.json(),
            {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
            },
        )

    def test_retrieve(self):
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**get_tenant_data())

        response = self.client.get(self._url(pk=tenant.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': tenant.id,
                'description': 'Comune Di Bugliano',
                'slug': 'comune-di-bugliano',
                'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_base_url': 'https://example.local/comune-di-bugliano/',
                'sdc_username': 'johndoe',
                'sdc_password': 's3cr3t',
                'sdc_institution_code': 'SDC',
                'sdc_aoo_code': 'P_SDC',
                'latest_registration_number': 42,
                'latest_registration_number_issued_at': '2021-01-02T11:20:30.123456+01:00',
                'register_after_date': '2000-01-01T11:20:30.123456+01:00',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_create(self):
        with freeze_time(NOW):
            response = self.client.post(
                self._url(),
                {
                    'description': 'Comune Di Bugliano',
                    'slug': 'comune-di-bugliano',
                    'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_base_url': 'https://example.local/comune-di-bugliano/',
                    'sdc_username': 'johndoe',
                    'sdc_password': 's3cr3t',
                    'sdc_institution_code': 'SDC',
                    'sdc_aoo_code': 'P_SDC',
                    'latest_registration_number': 42,
                    'latest_registration_number_issued_at': '2021-01-02T10:20:30.123456Z',
                    'register_after_date': '2021-01-02T10:20:30.123456Z',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json(),
            {
                'id': Tenant.objects.get().id,
                'description': 'Comune Di Bugliano',
                'slug': 'comune-di-bugliano',
                'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_base_url': 'https://example.local/comune-di-bugliano/',
                'sdc_username': 'johndoe',
                'sdc_password': 's3cr3t',
                'sdc_institution_code': 'SDC',
                'sdc_aoo_code': 'P_SDC',
                'latest_registration_number': 42,
                'latest_registration_number_issued_at': '2021-01-02T11:20:30.123456+01:00',
                'register_after_date': '2021-01-02T11:20:30.123456+01:00',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_update(self):
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**get_tenant_data())

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.put(
                self._url(pk=tenant.pk),
                {
                    'description': 'Updated Comune Di Bugliano',
                    'slug': 'updated-comune-di-bugliano',
                    'sdc_id': '1929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                    'sdc_base_url': 'https://example.local/comune-di-bugliano/updated',
                    'sdc_username': 'updated-johndoe',
                    'sdc_password': 'updated-s3cr3t',
                    'sdc_institution_code': 'UPDATED SDC',
                    'sdc_aoo_code': 'UPDATED P_SDC',
                    'latest_registration_number': 1,
                    'latest_registration_number_issued_at': '2021-02-02T10:20:30.123456Z',
                    'register_after_date': '2000-02-02T10:20:30.123456Z',
                },
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': tenant.id,
                'description': 'Updated Comune Di Bugliano',
                'slug': 'updated-comune-di-bugliano',
                'sdc_id': '1929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_base_url': 'https://example.local/comune-di-bugliano/updated',
                'sdc_username': 'updated-johndoe',
                'sdc_password': 'updated-s3cr3t',
                'sdc_institution_code': 'UPDATED SDC',
                'sdc_aoo_code': 'UPDATED P_SDC',
                'latest_registration_number': 1,
                'latest_registration_number_issued_at': '2021-02-02T11:20:30.123456+01:00',
                'register_after_date': '2000-02-02T11:20:30.123456+01:00',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_partial_update(self):
        with freeze_time(NOW):
            tenant = Tenant.objects.create(**get_tenant_data())

        with freeze_time(NOW + timedelta(hours=1)):
            response = self.client.patch(
                self._url(pk=tenant.pk),
                {'description': 'Updated Comune Di Bugliano'},
            )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'id': tenant.id,
                'description': 'Updated Comune Di Bugliano',
                'slug': 'comune-di-bugliano',
                'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_base_url': 'https://example.local/comune-di-bugliano/',
                'sdc_username': 'johndoe',
                'sdc_password': 's3cr3t',
                'sdc_institution_code': 'SDC',
                'sdc_aoo_code': 'P_SDC',
                'latest_registration_number': 42,
                'latest_registration_number_issued_at': '2021-01-02T11:20:30.123456+01:00',
                'register_after_date': '2000-01-01T11:20:30.123456+01:00',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T11:20:30.123456Z',
            },
        )

    def test_destroy(self):
        tenant = Tenant.objects.create(**get_tenant_data())

        response = self.client.delete(self._url(pk=tenant.pk))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_metadata(self):
        response = self.client.options(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Tenant List',
                'description': '',
                'renders': [
                    'application/json',
                ],
                'parses': [
                    'application/json',
                ],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'Id',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Description',
                            'max_length': 128,
                        },
                        'slug': {
                            'type': 'slug',
                            'required': True,
                            'read_only': False,
                            'label': 'Slug',
                            'max_length': 64,
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC ID',
                        },
                        'sdc_base_url': {
                            'type': 'url',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC base URL',
                            'max_length': 200,
                        },
                        'sdc_username': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC username',
                            'max_length': 64,
                        },
                        'sdc_password': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC password',
                            'max_length': 64,
                        },
                        'sdc_institution_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC institution code',
                            'max_length': 32,
                        },
                        'sdc_aoo_code': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'SDC AOO code',
                            'max_length': 32,
                        },
                        'latest_registration_number': {
                            'type': 'integer',
                            'required': False,
                            'read_only': False,
                            'label': 'Latest registration number',
                            'min_value': 0,
                            'max_value': 2147483647,
                        },
                        'latest_registration_number_issued_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': False,
                            'label': 'Latest registration number issued at',
                        },
                        'register_after_date': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': False,
                            'label': 'Register after date',
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Created at',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modified at',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url(*, pk=None):
        if pk:
            return reverse('tenant-detail', args=(pk,))
        return reverse('tenant-list')


class PitreSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'pitre-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('pitre-service-schema')


class MaggioliSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'maggioli-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('maggioli-service-schema')


class DatagraphSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'datagraph-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('datagraph-service-schema')


class DedagroupSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'dedagroup-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('dedagroup-service-schema')


class InsielSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'insiel-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('insiel-service-schema')


class HalleySchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'halley-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('halley-service-schema')


class HalleyCloudSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'halley-cloud-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('halley-cloud-service-schema')


class InforSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'infor-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('infor-service-schema')


class D3SchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'd3-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('d3-service-schema')


class HypersicSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'hypersic-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('hypersic-service-schema')


class TinnSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'tinn-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('tinn-service-schema')


class SicrawebWSProtocolloDMSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'sicraweb-wsprotocollodm-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('sicraweb-wsprotocollodm-service-schema')


class SiscomSchemaAPITestCase(APITestCase):
    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()

    def test_unauthorized(self):
        self.client.credentials()
        response = self.client.get(self._url())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list(self):
        path = settings.BASE_DIR / 'apps' / 'applications' / 'schemas' / 'siscom-form-schema.json'
        with open(path, 'r') as f:
            reference = json.load(f)

        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response.json())

    @staticmethod
    def _url():
        return reverse('siscom-service-schema')
