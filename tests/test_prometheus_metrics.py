from django.test import SimpleTestCase
from django.urls import reverse
from rest_framework import status


class PrometheusMetricsTestCase(SimpleTestCase):
    def test_metrics(self):
        response = self.client.get(self._url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(
            response.content.startswith(b'# HELP python_gc_objects_collected_total Objects collected during gc\n'),
        )

    @staticmethod
    def _url():
        return reverse('prometheus-django-metrics')
