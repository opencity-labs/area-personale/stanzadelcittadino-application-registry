from datetime import datetime, timezone
from uuid import UUID

from django.test import TestCase
from freezegun import freeze_time

from apps.applications.models import (
    ApplicationState,
    D3Config,
    DatagraphConfig,
    DeadLetter,
    DedagroupConfig,
    HalleyCloudConfig,
    HalleyConfig,
    HypersicConfig,
    InforConfig,
    MaggioliConfig,
    Registration,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)
from apps.applications.serializers import (
    ApplicationStateSerializer,
    D3ServiceSerializer,
    DatagraphServiceSerializer,
    DeadLetterSerializer,
    DedagroupServiceSerializer,
    HalleyCloudServiceSerializer,
    HalleyServiceSerializer,
    HypersicServiceSerializer,
    InforServiceSerializer,
    MaggioliConfigSerializer,
    MaggioliServiceSerializer,
    NestedD3ConfigSerializer,
    NestedDatagraphConfigSerializer,
    NestedDedagroupConfigSerializer,
    NestedHalleyCloudConfigSerializer,
    NestedHalleyConfigSerializer,
    NestedHypersicConfigSerializer,
    NestedInforConfigSerializer,
    NestedMaggioliConfigSerializer,
    NestedSicrawebWSProtocolloDMConfigSerializer,
    NestedSiscomConfigSerializer,
    NestedTinnConfigSerializer,
    RegistrationSerializer,
    ServiceSerializer,
    SicrawebWSProtocolloDMServiceSerializer,
    SiscomServiceSerializer,
    TinnServiceSerializer,
)

from ._helpers import (
    get_application_state_data,
    get_d3_config_data,
    get_datagraph_config_data,
    get_dead_letter_data,
    get_dedagroup_config_data,
    get_halley_cloud_config_data,
    get_halley_config_data,
    get_hypersic_config_data,
    get_infor_config_data,
    get_maggioli_config_data,
    get_registration_data,
    get_service_data,
    get_sicraweb_wsprotocollodm_config_data,
    get_siscom_config_data,
    get_tenant_data,
    get_tinn_config_data,
)

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class ServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
        service.application_states.set([state])

        serializer = ServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
                'application_states': ['example_state'],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
                'application_states': ['example_state'],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'provider': Service.Provider.DATAGRAPH,
                'application_states': [state],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'provider', 'application_states'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'provider', 'application_states'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'provider'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = ServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = ServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = ServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_invalid_provider(self):
        serializer = ServiceSerializer(data={'provider': 'not-a-provider'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'provider'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = ServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'provider': 'datagraph',
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedDatagraphConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        serializer = NestedDatagraphConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/datagraph/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_format': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'url': 'https://example.local/datagraph/',
            'codente': 'COD-111',
            'username': 'WSDOCAREA',
            'password': 's3cr3t',
            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
            'institution_code': 'COD-222',
            'institution_email': 'bugliano-tributi@example.local',
            'institution_ou': 'TRI',
            'institution_aoo_code': 'COD-333',
            'classification_institution_code': 'COD-444',
            'classification_aoo_code': 'COD-555',
            'classification_hierarchy': '7.2.0',
            'folder_number': '42',
            'folder_year': '2021',
            'document_format': 'servizio online',
        }

        serializer = NestedDatagraphConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedDatagraphConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'url',
                'codente',
                'username',
                'password',
                'institution_name',
                'institution_code',
                'institution_email',
                'institution_ou',
                'institution_aoo_code',
                'document_format',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedDatagraphConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nullable_fields(self):
        serializer = NestedDatagraphConfigSerializer(
            data={
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class DatagraphServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        service.application_states.set([state])

        serializer = DatagraphServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_format': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DatagraphServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'document_format': 'servizio online',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'datagraph_configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'document_format': 'servizio online',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = DatagraphServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = DatagraphServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = DatagraphServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = DatagraphServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'url': 'https://example.local/datagraph/',
            'codente': 'COD-111',
            'username': 'WSDOCAREA',
            'password': 's3cr3t',
            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
            'institution_code': 'COD-222',
            'institution_email': 'bugliano-tributi@example.local',
            'institution_ou': 'TRI',
            'institution_aoo_code': 'COD-333',
            'document_format': 'servizio online',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = DatagraphServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DatagraphServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'document_format': 'servizio online',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.DATAGRAPH)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.datagraph_configs.all(), [DatagraphConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = DatagraphServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.datagraph_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        serializer = DatagraphServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'url': 'https://example.local/datagraph/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'document_format': 'servizio online',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.datagraph_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))

        serializer = DatagraphServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.datagraph_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = DatagraphConfig.objects.create(**get_datagraph_config_data(service=other_service))

        serializer = DatagraphServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(DatagraphConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(DatagraphConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DatagraphServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedMaggioliConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = NestedMaggioliConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        serializer = NestedMaggioliConfigSerializer(
            data={
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': MaggioliConfig.SendingMode.PEC,
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
            },
        )

    def test_required_fields(self):
        serializer = NestedMaggioliConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'url',
                'connection_string',
                'institution_name',
                'institution_code',
                'institution_email',
                'institution_ou',
                'institution_aoo_code',
                'document_type',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedMaggioliConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_invalid_sending_mode(self):
        serializer = NestedMaggioliConfigSerializer(data={'sending_mode': 'not-a-sending-mode'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sending_mode'})

    def test_nullable_fields(self):
        serializer = NestedMaggioliConfigSerializer(
            data={
                'sending_mode': None,
                'sending_method': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'sending_mode': None,
                'sending_method': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class MaggioliServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        service.application_states.set([state])

        serializer = MaggioliServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'sending_method': 'COD-333',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = MaggioliServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'document_type': 'servizio online',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'maggioli_configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'document_type': 'servizio online',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = MaggioliServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = MaggioliServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = MaggioliServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = MaggioliServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'url': 'https://example.local/maggioli/',
            'connection_string': 'test_conn_string',
            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
            'institution_code': 'COD-111',
            'institution_email': 'bugliano-tributi@example.local',
            'institution_ou': 'TRI',
            'institution_aoo_code': 'COD-222',
            'document_type': 'servizio online',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = MaggioliServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = MaggioliServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'document_type': 'servizio online',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.MAGGIOLI)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.maggioli_configs.all(), [MaggioliConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = MaggioliServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.maggioli_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = MaggioliServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'url': 'https://example.local/maggioli/',
                        'connection_string': 'test_conn_string',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-111',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'document_type': 'servizio online',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.maggioli_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = MaggioliServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.maggioli_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=other_service))

        serializer = MaggioliServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(MaggioliConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(MaggioliConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = MaggioliServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedHalleyConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        serializer = NestedHalleyConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'login_url': 'https://example.local/halley/login/',
                'registration_url': 'https://example.local/halley/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano',
                'institution_aoo_id': 1,
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_id': 1,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'login_url': 'https://example.local/halley/login/',
            'registration_url': 'https://example.local/halley/registration/',
            'username': 'console',
            'password': 's3cr3t',
            'institution_name': 'Comune di Bugliano',
            'institution_aoo_id': 1,
            'institution_office_ids': [],
            'classification_category': '7',
            'classification_class': '2',
            'classification_subclass': '0',
            'folder_id': 1,
        }

        serializer = NestedHalleyConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedHalleyConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'login_url',
                'registration_url',
                'username',
                'password',
                'institution_name',
                'institution_aoo_id',
                'classification_category',
                'classification_class',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedHalleyConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_allow_empty_institution_office_ids(self):
        serializer = NestedHalleyConfigSerializer(data={'institution_office_ids': []}, partial=True)

        self.assertTrue(serializer.is_valid())

    def test_invalid_institution_office_ids(self):
        serializer = NestedHalleyConfigSerializer(data={'institution_office_ids': [1, 2]}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'institution_office_ids'})

    def test_nullable_fields(self):
        serializer = NestedHalleyConfigSerializer(
            data={
                'classification_subclass': None,
                'folder_id': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'classification_subclass': None,
                'folder_id': None,
            },
        )


class HalleyServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        service.application_states.set([state])

        serializer = HalleyServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_id': 1,
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'halley_configs': [
                    {
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = HalleyServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = HalleyServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = HalleyServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = HalleyServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'login_url': 'https://example.local/halley/login/',
            'registration_url': 'https://example.local/halley/registration/',
            'username': 'console',
            'password': 's3cr3t',
            'institution_name': 'Comune di Bugliano',
            'institution_aoo_id': 1,
            'institution_office_ids': [],
            'classification_category': '7',
            'classification_class': '2',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = HalleyServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.HALLEY)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.halley_configs.all(), [HalleyConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = HalleyServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.halley_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))

        serializer = HalleyServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'login_url': 'https://example.local/halley/login/',
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano',
                        'institution_aoo_id': 1,
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.halley_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyConfig.objects.create(**get_halley_config_data(service=service))

        serializer = HalleyServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.halley_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyConfig.objects.create(**get_halley_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = HalleyConfig.objects.create(**get_halley_config_data(service=other_service))

        serializer = HalleyServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(HalleyConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(HalleyConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedHalleyCloudConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        serializer = NestedHalleyCloudConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'registration_url': 'https://example.local/halley_cloud/registration/',
                'username': 'console',
                'password': 's3cr3t',
                'mailbox': 'example@example.com',
                'institution_office_ids': [],
                'classification_category': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_name': 'Fascicolo test',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'registration_url': 'https://example.local/halley_cloud/registration/',
            'username': 'console',
            'password': 's3cr3t',
            'mailbox': 'example@example.com',
            'institution_office_ids': [],
            'classification_category': '7',
            'classification_class': '2',
            'classification_subclass': '0',
            'folder_name': 'Fascicolo test',
        }

        serializer = NestedHalleyCloudConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedHalleyCloudConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'registration_url',
                'username',
                'password',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedHalleyCloudConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_allow_empty_institution_office_ids(self):
        serializer = NestedHalleyCloudConfigSerializer(data={'institution_office_ids': []}, partial=True)

        self.assertTrue(serializer.is_valid())

    def test_invalid_institution_office_ids(self):
        serializer = NestedHalleyCloudConfigSerializer(data={'institution_office_ids': [1, 2]}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'institution_office_ids'})

    def test_nullable_fields(self):
        serializer = NestedHalleyCloudConfigSerializer(
            data={
                'mailbox': None,
                'classification_category': None,
                'classification_class': None,
                'classification_subclass': None,
                'folder_name': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'mailbox': None,
                'classification_category': None,
                'classification_class': None,
                'classification_subclass': None,
                'folder_name': None,
            },
        )


class HalleyCloudServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        service.application_states.set([state])

        serializer = HalleyCloudServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'mailbox': 'example@example.com',
                        'institution_office_ids': [],
                        'classification_category': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_name': 'Fascicolo test',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyCloudServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_office_ids': [],
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'halley_cloud_configs': [
                    {
                        'is_active': True,
                        'registration_url': 'https://example.local/halley_cloud/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_office_ids': [],
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = HalleyCloudServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = HalleyCloudServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = HalleyCloudServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = HalleyCloudServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'registration_url': 'https://example.local/halley/registration/',
            'username': 'console',
            'password': 's3cr3t',
            'institution_office_ids': [],
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = HalleyCloudServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyCloudServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_office_ids': [],
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.HALLEY_CLOUD)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.halley_cloud_configs.all(), [HalleyCloudConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = HalleyCloudServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.halley_cloud_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        serializer = HalleyCloudServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'registration_url': 'https://example.local/halley/registration/',
                        'username': 'console',
                        'password': 's3cr3t',
                        'institution_office_ids': [],
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.halley_cloud_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))

        serializer = HalleyCloudServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.halley_cloud_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=other_service))

        serializer = HalleyCloudServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(HalleyCloudConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(HalleyCloudConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HalleyCloudServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedDedagroupConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        serializer = NestedDedagroupConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'token_url': 'https://token.example.local',
                'base_url': 'https://base.example.local',
                'client_id': 'abc',
                'client_secret': 's3cr3t',
                'grant_type': 'client_credentials',
                'resource': 'http://base.example.local',
                'registry_id': 3,
                'author_id': 123456,
                'organization_chart_level_code': 'abcde12345',
                'institution_aoo_id': 123,
                'classification_title': '7',
                'classification_class': '2',
                'classification_subclass': '0',
                'folder_code': '2022.1.1',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'token_url': 'https://token.example.local',
            'base_url': 'https://base.example.local',
            'client_id': 'abc',
            'client_secret': 's3cr3t',
            'grant_type': 'client_credentials',
            'resource': 'http://base.example.local',
            'registry_id': 3,
            'author_id': 123456,
            'organization_chart_level_code': 'abcde12345',
            'institution_aoo_id': 123,
            'classification_title': '7',
            'classification_class': '2',
            'classification_subclass': '0',
            'folder_code': '2022.1.1',
        }

        serializer = NestedDedagroupConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedDedagroupConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'token_url',
                'base_url',
                'client_id',
                'client_secret',
                'grant_type',
                'resource',
                'registry_id',
                'author_id',
                'organization_chart_level_code',
                'institution_aoo_id',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedDedagroupConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nullable_fields(self):
        serializer = NestedDedagroupConfigSerializer(
            data={
                'classification_title': None,
                'classification_class': None,
                'classification_subclass': None,
                'folder_code': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'classification_title': None,
                'classification_class': None,
                'classification_subclass': None,
                'folder_code': None,
            },
        )


class DedagroupServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        service.application_states.set([state])

        serializer = DedagroupServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                        'classification_title': '7',
                        'classification_class': '2',
                        'classification_subclass': '0',
                        'folder_code': '2022.1.1',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DedagroupServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'dedagroup_configs': [
                    {
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = DedagroupServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = DedagroupServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = DedagroupServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = DedagroupServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'token_url': 'https://token.example.local',
            'base_url': 'https://base.example.local',
            'client_id': 'abc',
            'client_secret': 's3cr3t',
            'grant_type': 'client_credentials',
            'resource': 'http://base.example.local',
            'registry_id': 3,
            'author_id': 123456,
            'organization_chart_level_code': 'abcde12345',
            'institution_aoo_id': 123,
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = DedagroupServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DedagroupServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.DEDAGROUP)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.dedagroup_configs.all(), [DedagroupConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = DedagroupServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.dedagroup_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        serializer = DedagroupServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'token_url': 'https://token.example.local',
                        'base_url': 'https://base.example.local',
                        'client_id': 'abc',
                        'client_secret': 's3cr3t',
                        'grant_type': 'client_credentials',
                        'resource': 'http://base.example.local',
                        'registry_id': 3,
                        'author_id': 123456,
                        'organization_chart_level_code': 'abcde12345',
                        'institution_aoo_id': 123,
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.dedagroup_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))

        serializer = DedagroupServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.dedagroup_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = DedagroupConfig.objects.create(**get_dedagroup_config_data(service=other_service))

        serializer = DedagroupServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(DedagroupConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(DedagroupConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = DedagroupServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class ApplicationStateSerializerTestCase(TestCase):
    def test_serialization(self):
        with freeze_time(NOW):
            state = ApplicationState.objects.create(**get_application_state_data())

        serializer = ApplicationStateSerializer(state)

        self.assertEqual(
            serializer.data,
            {
                'id': state.id,
                'name': 'example_state',
                'direction': 'inbound',
                'integrations': False,
                'default': True,
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )


class MaggioliConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = MaggioliConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))

        serializer = MaggioliConfigSerializer(
            data={
                'description': 'Test configuration',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Test configuration',
                'service': service,
                'is_active': True,
                'url': 'https://example.local/maggioli/',
                'connection_string': 'test_conn_string',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-111',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': MaggioliConfig.SendingMode.PEC,
                'sending_method': 'COD-333',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'document_type': 'servizio online',
            },
        )

    def test_required_fields(self):
        serializer = MaggioliConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'service_sdc_id',
                'is_active',
                'url',
                'connection_string',
                'institution_name',
                'institution_code',
                'institution_email',
                'institution_ou',
                'institution_aoo_code',
                'document_type',
            },
        )

    def test_readonly_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = MaggioliConfigSerializer(
            config,  # We need this to prevent UniqueTogetherValidator from raising errors.
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = MaggioliConfigSerializer(
            data={
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'service_sdc_id'})

    def test_invalid_sending_mode(self):
        serializer = MaggioliConfigSerializer(data={'sending_mode': 'not-a-sending-mode'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sending_mode'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=service, is_active=False))

        serializer = MaggioliConfigSerializer(
            data={
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': False,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'service': service,
                'is_active': False,
            },
        )

        serializer = MaggioliConfigSerializer(
            data={
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'is_active': True,
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            serializer.errors,
            {'non_field_errors': ['An active configuration already exists for the selected service.']},
        )

    def test_nullable_fields(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))

        serializer = MaggioliConfigSerializer(
            config,  # We need this to prevent UniqueTogetherValidator from raising errors.
            data={
                'sending_mode': None,
                'sending_method': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'sending_mode': None,
                'sending_method': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class NestedInforConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = InforConfig.objects.create(**get_infor_config_data(service=service))

        serializer = NestedInforConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'auth_username': None,
                'auth_password': None,
                'username': 'sdc',
                'denomination': 'sdc',
                'email': 'user@example.com',
                'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                'registry_url': 'https://base.example.local/protocollo',
                'incoming_document_type': 'xx',
                'incoming_intermediary': 'WEB',
                'incoming_sorting': 'ABC',
                'incoming_classification': '16.02',
                'incoming_folder': '2012/6',
                'response_internal_sender': 'sdc',
                'response_document_type': 'xx',
                'response_intermediary': 'WEB',
                'response_sorting': 'ABC',
                'response_classification': '16.02',
                'response_folder': '2012/6',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'username': 'sdc',
            'denomination': 'sdc',
            'email': 'user@example.com',
            'wsdl_url': 'https://base.example.local/protocollo?wsdl',
            'registry_url': 'https://base.example.local/protocollo',
            'incoming_document_type': 'xx',
            'incoming_intermediary': 'WEB',
            'incoming_sorting': 'ABC',
            'incoming_classification': '16.02',
            'incoming_folder': '2012/6',
            'response_internal_sender': 'sdc',
            'response_document_type': 'xx',
            'response_intermediary': 'WEB',
            'response_sorting': 'ABC',
            'response_classification': '16.02',
            'response_folder': '2012/6',
        }

        serializer = NestedInforConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedInforConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'username',
                'denomination',
                'email',
                'wsdl_url',
                'registry_url',
                'incoming_document_type',
                'incoming_intermediary',
                'incoming_sorting',
                'incoming_classification',
                'incoming_folder',
                'response_internal_sender',
                'response_document_type',
                'response_intermediary',
                'response_sorting',
                'response_classification',
                'response_folder',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedInforConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})


class InforServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = InforConfig.objects.create(**get_infor_config_data(service=service))
        service.application_states.set([state])

        serializer = InforServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'auth_username': None,
                        'auth_password': None,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = InforServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'infor_configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = InforServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = InforServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = InforServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = InforServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'username': 'sdc',
            'denomination': 'sdc',
            'email': 'user@example.com',
            'wsdl_url': 'https://base.example.local/protocollo?wsdl',
            'registry_url': 'https://base.example.local/protocollo',
            'incoming_document_type': 'xx',
            'incoming_intermediary': 'WEB',
            'incoming_sorting': 'ABC',
            'incoming_classification': '16.02',
            'incoming_folder': '2012/6',
            'response_internal_sender': 'sdc',
            'response_document_type': 'xx',
            'response_intermediary': 'WEB',
            'response_sorting': 'ABC',
            'response_classification': '16.02',
            'response_folder': '2012/6',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = InforServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = InforServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.INFOR)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.infor_configs.all(), [InforConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = InforServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.infor_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InforConfig.objects.create(**get_infor_config_data(service=service))

        serializer = InforServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'username': 'sdc',
                        'denomination': 'sdc',
                        'email': 'user@example.com',
                        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
                        'registry_url': 'https://base.example.local/protocollo',
                        'incoming_document_type': 'xx',
                        'incoming_intermediary': 'WEB',
                        'incoming_sorting': 'ABC',
                        'incoming_classification': '16.02',
                        'incoming_folder': '2012/6',
                        'response_internal_sender': 'sdc',
                        'response_document_type': 'xx',
                        'response_intermediary': 'WEB',
                        'response_sorting': 'ABC',
                        'response_classification': '16.02',
                        'response_folder': '2012/6',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.infor_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        InforConfig.objects.create(**get_infor_config_data(service=service))

        serializer = InforServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.infor_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = InforConfig.objects.create(**get_infor_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = InforConfig.objects.create(**get_infor_config_data(service=other_service))

        serializer = InforServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(InforConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(InforConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = InforServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedD3ConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = D3Config.objects.create(**get_d3_config_data(service=service))

        serializer = NestedD3ConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/',
                'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                'main_source_category': 'EXAMPLE',
                'register_attachments': True,
                'attachment_source_category': 'EXAMPLE2',
                'document_subtype': 'Allgmein - generico',
                'classification': '15.02.02',
                'folder_name': '1234',
                'competent_service': 'Sekretariat - Segreteria',
                'visibility': 'Sekretariat - Segreteria: S',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'username': 'sdc',
            'password': 's3cr3t',
            'registry_url': 'https://base.example.local/',
            'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
            'main_source_category': 'EXAMPLE',
            'register_attachments': True,
            'attachment_source_category': 'EXAMPLE2',
            'document_subtype': 'Allgmein - generico',
            'classification': '15.02.02',
            'folder_name': '1234',
            'competent_service': 'Sekretariat - Segreteria',
            'visibility': 'Sekretariat - Segreteria: S',
        }

        serializer = NestedD3ConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedD3ConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'username',
                'password',
                'registry_url',
                'repository_id',
                'main_source_category',
                'register_attachments',
                'competent_service',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedD3ConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})


class D3ServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = D3Config.objects.create(**get_d3_config_data(service=service))
        service.application_states.set([state])

        serializer = D3ServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = D3ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'd3_configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = D3ServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = D3ServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = D3ServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = D3ServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'username': 'sdc',
            'password': 's3cr3t',
            'registry_url': 'https://base.example.local/',
            'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
            'main_source_category': 'EXAMPLE',
            'register_attachments': True,
            'attachment_source_category': 'EXAMPLE2',
            'document_subtype': 'Allgmein - generico',
            'classification': '15.02.02',
            'folder_name': '1234',
            'competent_service': 'Sekretariat - Segreteria',
            'visibility': 'Sekretariat - Segreteria: S',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = D3ServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = D3ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.D3)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.d3_configs.all(), [D3Config.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = D3ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.d3_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))

        serializer = D3ServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/',
                        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
                        'main_source_category': 'EXAMPLE',
                        'register_attachments': True,
                        'attachment_source_category': 'EXAMPLE2',
                        'document_subtype': 'Allgmein - generico',
                        'classification': '15.02.02',
                        'folder_name': '1234',
                        'competent_service': 'Sekretariat - Segreteria',
                        'visibility': 'Sekretariat - Segreteria: S',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.d3_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        D3Config.objects.create(**get_d3_config_data(service=service))

        serializer = D3ServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.d3_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = D3Config.objects.create(**get_d3_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = D3Config.objects.create(**get_d3_config_data(service=other_service))

        serializer = D3ServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(D3Config.objects.filter(pk=config.pk).exists())
        self.assertTrue(D3Config.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = D3ServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedHypersicConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        serializer = NestedHypersicConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'username': 'sdc',
                'password': 's3cr3t',
                'registry_url': 'https://base.example.local/protocollo',
                'office_ids': [123],
                'classification': '6-3-0',
                'folder_number': '2',
                'folder_year': '2021',
                'annotation': 'something',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'username': 'sdc',
            'password': 's3cr3t',
            'registry_url': 'https://base.example.local/protocollo',
            'office_ids': [123],
            'classification': '6-3-0',
            'folder_number': '2',
            'folder_year': '2021',
            'annotation': 'something',
        }

        serializer = NestedHypersicConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedHypersicConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'username',
                'password',
                'registry_url',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedHypersicConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})


class HypersicServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        service.application_states.set([state])

        serializer = HypersicServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HypersicServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'hypersic_configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = HypersicServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = HypersicServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = HypersicServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = HypersicServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'username': 'sdc',
            'password': 's3cr3t',
            'registry_url': 'https://base.example.local/protocollo',
            'office_ids': [123],
            'classification': '6-3-0',
            'folder_number': '2',
            'folder_year': '2021',
            'annotation': 'something',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = HypersicServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HypersicServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.HYPERSIC)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.hypersic_configs.all(), [HypersicConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = HypersicServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.hypersic_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        serializer = HypersicServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'username': 'sdc',
                        'password': 's3cr3t',
                        'registry_url': 'https://base.example.local/protocollo',
                        'office_ids': [123],
                        'classification': '6-3-0',
                        'folder_number': '2',
                        'folder_year': '2021',
                        'annotation': 'something',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.hypersic_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        HypersicConfig.objects.create(**get_hypersic_config_data(service=service))

        serializer = HypersicServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.hypersic_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = HypersicConfig.objects.create(**get_hypersic_config_data(service=other_service))

        serializer = HypersicServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(HypersicConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(HypersicConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = HypersicServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedTinnConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))

        serializer = NestedTinnConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/tinn/',
                'codente': 'COD-111',
                'username': 'WSDOCAREA',
                'password': 's3cr3t',
                'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                'institution_code': 'COD-222',
                'institution_email': 'bugliano-tributi@example.local',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-333',
                'classification_institution_code': 'COD-444',
                'classification_aoo_code': 'COD-555',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'url': 'https://example.local/tinn/',
            'codente': 'COD-111',
            'username': 'WSDOCAREA',
            'password': 's3cr3t',
            'institution_name': 'Comune di Bugliano - Ufficio Tributi',
            'institution_code': 'COD-222',
            'institution_email': 'bugliano-tributi@example.local',
            'institution_ou': 'TRI',
            'institution_aoo_code': 'COD-333',
            'classification_institution_code': 'COD-444',
            'classification_aoo_code': 'COD-555',
            'classification_hierarchy': '7.2.0',
            'folder_number': '42',
            'folder_year': '2021',
        }

        serializer = NestedTinnConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedTinnConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'url',
                'codente',
                'username',
                'password',
                'classification_hierarchy',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedTinnConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nullable_fields(self):
        serializer = NestedTinnConfigSerializer(
            data={
                'institution_name': None,
                'institution_code': None,
                'institution_email': None,
                'institution_ou': None,
                'institution_aoo_code': None,
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'folder_number': None,
                'folder_year': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'institution_name': None,
                'institution_code': None,
                'institution_email': None,
                'institution_ou': None,
                'institution_aoo_code': None,
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class TinnServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        service.application_states.set([state])

        serializer = TinnServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
                        'institution_code': 'COD-222',
                        'institution_email': 'bugliano-tributi@example.local',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-333',
                        'classification_institution_code': 'COD-444',
                        'classification_aoo_code': 'COD-555',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = TinnServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'classification_hierarchy': '7.2.0',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'tinn_configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'classification_hierarchy': '7.2.0',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = TinnServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = TinnServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = TinnServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = TinnServiceSerializer(data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'}, partial=True)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'url': 'https://example.local/tinn/',
            'codente': 'COD-111',
            'username': 'WSDOCAREA',
            'password': 's3cr3t',
            'classification_hierarchy': '7.2.0',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = TinnServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = TinnServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'classification_hierarchy': '7.2.0',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.TINN)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.tinn_configs.all(), [TinnConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = TinnServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.tinn_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))

        serializer = TinnServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'url': 'https://example.local/tinn/',
                        'codente': 'COD-111',
                        'username': 'WSDOCAREA',
                        'password': 's3cr3t',
                        'classification_hierarchy': '7.2.0',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.tinn_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        TinnConfig.objects.create(**get_tinn_config_data(service=service))

        serializer = TinnServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.tinn_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = TinnConfig.objects.create(**get_tinn_config_data(service=other_service))

        serializer = TinnServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(TinnConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(TinnConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = TinnServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedSicrawebWSProtocolloDMConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )

        serializer = NestedSicrawebWSProtocolloDMConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/sicraweb-wsprotocollodm/',
                'connection_string': 'test_conn_string',
                'institution_code': 'COD-111',
                'institution_ou': 'TRI',
                'institution_aoo_code': 'COD-222',
                'sending_mode': 'PEC',
                'internal_sender': 'ANA',
                'update_records': 'S',
                'classification_hierarchy': '7.2.0',
                'folder_number': '42',
                'folder_year': '2021',
                'connection_user': 'opencontent',
                'connection_user_role': 'user',
                'standard_subject': 'subject',
                'document_type': 'servizio online',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'url': 'https://example.local/sicraweb-wsprotocollodm/',
            'connection_string': 'test_conn_string',
            'institution_code': 'COD-111',
            'institution_ou': 'TRI',
            'institution_aoo_code': 'COD-222',
            'sending_mode': 'PEC',
            'internal_sender': 'ANA',
            'update_records': 'S',
            'classification_hierarchy': '7.2.0',
            'folder_number': '42',
            'folder_year': '2021',
            'connection_user': 'opencontent',
            'connection_user_role': 'user',
            'standard_subject': 'subject',
            'document_type': 'servizio online',
        }

        serializer = NestedSicrawebWSProtocolloDMConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedSicrawebWSProtocolloDMConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'url',
                'connection_string',
                'institution_aoo_code',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedSicrawebWSProtocolloDMConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nullable_fields(self):
        serializer = NestedSicrawebWSProtocolloDMConfigSerializer(
            data={
                'institution_code': None,
                'institution_ou': None,
                'sending_mode': None,
                'internal_sender': None,
                'update_records': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
                'connection_user': None,
                'connection_user_role': None,
                'standard_subject': None,
                'document_type': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'institution_code': None,
                'institution_ou': None,
                'sending_mode': None,
                'internal_sender': None,
                'update_records': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
                'connection_user': None,
                'connection_user_role': None,
                'standard_subject': None,
                'document_type': None,
            },
        )


class SicrawebWSProtocolloDMServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SicrawebWSProtocolloDMConfig.objects.create(
                **get_sicraweb_wsprotocollodm_config_data(service=service),
            )
        service.application_states.set([state])

        serializer = SicrawebWSProtocolloDMServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_code': 'COD-111',
                        'institution_ou': 'TRI',
                        'institution_aoo_code': 'COD-222',
                        'sending_mode': 'PEC',
                        'internal_sender': 'ANA',
                        'update_records': 'S',
                        'classification_hierarchy': '7.2.0',
                        'folder_number': '42',
                        'folder_year': '2021',
                        'connection_user': 'opencontent',
                        'connection_user_role': 'user',
                        'standard_subject': 'subject',
                        'document_type': 'servizio online',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_aoo_code': 'COD-222',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'sicraweb_wsprotocollodm_configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_aoo_code': 'COD-222',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = SicrawebWSProtocolloDMServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'url': 'https://example.local/sicraweb-wsprotocollodm/',
            'connection_string': 'test_conn_string',
            'institution_aoo_code': 'COD-222',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = SicrawebWSProtocolloDMServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_aoo_code': 'COD-222',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.SICRAWEB_WSPROTOCOLLODM)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(
            service.sicraweb_wsprotocollodm_configs.all(),
            [SicrawebWSProtocolloDMConfig.objects.get()],
        )

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.sicraweb_wsprotocollodm_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'url': 'https://example.local/sicraweb-wsprotocollodm/',
                        'connection_string': 'test_conn_string',
                        'institution_aoo_code': 'COD-222',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.sicraweb_wsprotocollodm_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SicrawebWSProtocolloDMConfig.objects.create(**get_sicraweb_wsprotocollodm_config_data(service=service))

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.sicraweb_wsprotocollodm_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=other_service),
        )

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(SicrawebWSProtocolloDMConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(SicrawebWSProtocolloDMConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SicrawebWSProtocolloDMServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class NestedSiscomConfigSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        with freeze_time(NOW):
            config = SiscomConfig.objects.create(
                **get_siscom_config_data(service=service),
            )

        serializer = NestedSiscomConfigSerializer(config)

        self.assertEqual(
            serializer.data,
            {
                'id': config.id,
                'description': 'Test configuration',
                'is_active': True,
                'url': 'https://example.local/siscom/',
                'license_code': '1008',
                'operator_denomination': 'John Doe',
                'classification_category': '0',
                'classification_class': '0',
                'folder_id': '0',
                'mail_type': '0',
                'office_id': '7',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        data = {
            'description': 'Test configuration',
            'is_active': True,
            'url': 'https://example.local/siscom/',
            'license_code': '1008',
            'operator_denomination': 'John Doe',
            'classification_category': '0',
            'classification_class': '0',
            'folder_id': '0',
            'mail_type': '0',
            'office_id': '7',
        }

        serializer = NestedSiscomConfigSerializer(data=data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, data)

    def test_required_fields(self):
        serializer = NestedSiscomConfigSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            set(serializer.errors),
            {
                'is_active',
                'url',
                'license_code',
                'office_id',
            },
        )

    def test_readonly_fields(self):
        serializer = NestedSiscomConfigSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nullable_fields(self):
        serializer = NestedSiscomConfigSerializer(
            data={
                'operator_denomination': None,
                'classification_category': None,
                'classification_class': None,
                'folder_id': None,
                'mail_type': None,
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'operator_denomination': None,
                'classification_category': None,
                'classification_class': None,
                'folder_id': None,
                'mail_type': None,
            },
        )


class SiscomServiceSerializerTestCase(TestCase):
    def test_serialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())
        with freeze_time(NOW):
            service = Service.objects.create(**get_service_data(tenant=tenant))
            config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))
        service.application_states.set([state])

        serializer = SiscomServiceSerializer(service)

        self.assertEqual(
            serializer.data,
            {
                'id': service.id,
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'id': config.id,
                        'description': 'Test configuration',
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'operator_denomination': 'John Doe',
                        'classification_category': '0',
                        'classification_class': '0',
                        'folder_id': '0',
                        'mail_type': '0',
                        'office_id': '7',
                        'created_at': '2021-05-25T10:20:30.123456Z',
                        'modified_at': '2021-05-25T10:20:30.123456Z',
                    },
                ],
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )

    def test_deserialization(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SiscomServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'office_id': '7',
                    },
                ],
            },
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            serializer.validated_data,
            {
                'description': 'Posizione TARI',
                'tenant': tenant,
                'sdc_id': UUID('5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'),
                'application_states': [state],
                'siscom_configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'office_id': '7',
                    },
                ],
            },
        )

    def test_required_fields(self):
        for kwargs, errors in [
            ({}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': False}, {'tenant_sdc_id', 'sdc_id', 'application_states', 'configs'}),
            ({'default_application_states': True}, {'tenant_sdc_id', 'sdc_id', 'configs'}),
        ]:
            with self.subTest(kwargs=kwargs):
                serializer = SiscomServiceSerializer(data={}, **kwargs)

                self.assertFalse(serializer.is_valid())
                self.assertEqual(set(serializer.errors), errors)

    def test_readonly_fields(self):
        serializer = SiscomServiceSerializer(
            data={
                'created_at': '2020-01-01T11:22:33.654321Z',
                'modified_at': '2020-01-01T11:22:33.654321Z',
            },
            partial=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data, {})

    def test_nonexistent_fk(self):
        serializer = SiscomServiceSerializer(
            data={
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'application_states': ['example_state'],
            },
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'tenant_sdc_id', 'application_states'})

    def test_uniqueness(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        Service.objects.create(**get_service_data(tenant=tenant))

        serializer = SiscomServiceSerializer(
            data={'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            partial=True,
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors), {'sdc_id'})

    def test_configs_validation(self):
        Tenant.objects.create(**get_tenant_data())
        config = {
            'url': 'https://example.local/siscom/',
            'license_code': '1008',
            'office_id': '7',
        }

        for is_active1, is_active2, is_valid, errors in [
            (True, True, False, {'configs': ['Only one active configuration is allowed.']}),
            (True, False, True, {}),
            (False, True, True, {}),
            (False, False, True, {}),
        ]:
            with self.subTest(is_active1=is_active1, is_active2=is_active2):
                serializer = SiscomServiceSerializer(
                    data={
                        'description': 'Posizione TARI',
                        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                        'application_states': [],
                        'configs': [
                            {'is_active': is_active1, **config},
                            {'is_active': is_active2, **config},
                        ],
                    },
                )

                self.assertIs(serializer.is_valid(), is_valid)
                self.assertEqual(serializer.errors, errors)

    def test_create(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SiscomServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': ['example_state'],
                'configs': [
                    {
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'office_id': '7',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertEqual(service, Service.objects.get())
        self.assertEqual(service.tenant, tenant)
        self.assertEqual(service.provider, Service.Provider.SISCOM)
        self.assertQuerySetEqual(service.application_states.all(), [state])
        self.assertQuerySetEqual(service.siscom_configs.all(), [SiscomConfig.objects.get()])

    def test_create_no_configs(self):
        Tenant.objects.create(**get_tenant_data())

        serializer = SiscomServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        service = serializer.save()

        self.assertQuerySetEqual(service.siscom_configs.all(), [])

    def test_update(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(**get_siscom_config_data(service=service))

        serializer = SiscomServiceSerializer(
            instance=service,
            data={
                'description': 'Update test service',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [
                    {
                        'description': 'Update test configuration',
                        'is_active': True,
                        'url': 'https://example.local/siscom/',
                        'license_code': '1008',
                        'office_id': '7',
                    },
                ],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.description, 'Update test service')
        updated_config = updated_service.siscom_configs.get()
        self.assertNotEqual(updated_config, config)
        self.assertEqual(updated_config.description, 'Update test configuration')

    def test_update_no_configs(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        SiscomConfig.objects.create(**get_siscom_config_data(service=service))

        serializer = SiscomServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        updated_service = serializer.save()

        self.assertEqual(updated_service, Service.objects.get())
        self.assertEqual(updated_service, service)
        self.assertEqual(updated_service.siscom_configs.count(), 0)

    def test_update_associated_configs_only(self):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        config = SiscomConfig.objects.create(
            **get_siscom_config_data(service=service),
        )
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='ce6e4771-69f5-4568-bba6-9a3c1a04e989'),
        )
        other_config = SiscomConfig.objects.create(**get_siscom_config_data(service=other_service))

        serializer = SiscomServiceSerializer(
            instance=service,
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_states': [],
                'configs': [],
            },
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        self.assertFalse(SiscomConfig.objects.filter(pk=config.pk).exists())
        self.assertTrue(SiscomConfig.objects.filter(pk=other_config.pk).exists())

    def test_default_application_states(self):
        Tenant.objects.create(**get_tenant_data())
        ApplicationState.objects.create(**get_application_state_data(name='other_state', code='9999', default=False))
        state = ApplicationState.objects.create(**get_application_state_data())

        serializer = SiscomServiceSerializer(
            data={
                'description': 'Posizione TARI',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'configs': [],
            },
            default_application_states=True,
        )

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['application_states'], [state])


class RegistrationSerializerTestCase(TestCase):
    def test_serialization(self):
        with freeze_time(NOW):
            registration = Registration.objects.create(**get_registration_data())

        serializer = RegistrationSerializer(registration)

        self.assertEqual(
            serializer.data,
            {
                'id': registration.id,
                'number': '1234',
                'date': '2021-01-02T10:20:30.123456Z',
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                'application_state_name': 'example_state',
                'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
                'is_completed': True,
                'is_updated': True,
                'registration_data': {},
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )


class DeadLetterSerializerTestCase(TestCase):
    def test_serialization(self):
        with freeze_time(NOW):
            dead_letter = DeadLetter.objects.create(**get_dead_letter_data())

        serializer = DeadLetterSerializer(dead_letter)

        self.assertEqual(
            serializer.data,
            {
                'id': dead_letter.id,
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
                'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
                'state': 'new',
                'created_at': '2021-05-25T10:20:30.123456Z',
                'modified_at': '2021-05-25T10:20:30.123456Z',
            },
        )
