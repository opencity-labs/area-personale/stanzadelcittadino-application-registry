from django.test import TestCase

from apps.applications.filters import (
    ApplicationStateFilterSet,
    D3ConfigFilterSet,
    DeadLetterFilterSet,
    HalleyCloudConfigFilterSet,
    HypersicConfigFilterSet,
    InforConfigFilterSet,
    MaggioliConfigFilterSet,
    RegistrationFilterSet,
    ServiceFilterSet,
    SicrawebWSProtocolloDMConfigFilterSet,
    SiscomConfigFilterSet,
    TinnConfigFilterSet,
)
from apps.applications.models import (
    ApplicationState,
    D3Config,
    DeadLetter,
    HalleyCloudConfig,
    HypersicConfig,
    InforConfig,
    MaggioliConfig,
    Registration,
    Service,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    Tenant,
    TinnConfig,
)

from ._helpers import (
    get_application_state_data,
    get_d3_config_data,
    get_dead_letter_data,
    get_halley_cloud_config_data,
    get_hypersic_config_data,
    get_infor_config_data,
    get_maggioli_config_data,
    get_registration_data,
    get_service_data,
    get_sicraweb_wsprotocollodm_config_data,
    get_siscom_config_data,
    get_tenant_data,
    get_tinn_config_data,
)


class ServiceFilterSetTestCase(TestCase):
    queryset = Service.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        cls.service = Service.objects.create(**get_service_data(tenant=tenant))
        other_tenant = Tenant.objects.create(
            **get_tenant_data(slug='other-slug', sdc_id='b6705efb-7db2-4a75-9522-d04c1d20ad1f'),
        )
        Service.objects.create(**get_service_data(tenant=other_tenant, sdc_id='68b03c6e-675d-4bde-8ad7-b6897be39cd0'))

    def test_filter_by_invalid_tenant_sdc_id(self):
        f = ServiceFilterSet({'tenant_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_tenant_sdc_id(self):
        f = ServiceFilterSet({'tenant_sdc_id': 'ad89b99d-a175-4edb-a56a-913403847451'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_tenant_sdc_id_exact(self):
        f = ServiceFilterSet({'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.service])


class ApplicationStateFilterSetTestCase(TestCase):
    queryset = ApplicationState.objects.all()

    @classmethod
    def setUpTestData(cls):
        cls.state = ApplicationState.objects.create(**get_application_state_data())
        ApplicationState.objects.create(
            **get_application_state_data(
                name='other_state',
                code='9999',
                direction=ApplicationState.Direction.OUTBOUND,
                default=False,
            ),
        )

    def test_filter_by_nonexistent_name(self):
        f = ApplicationStateFilterSet({'name': 'unknown'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_direction(self):
        f = ApplicationStateFilterSet({'direction': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_invalid_default(self):
        f = ApplicationStateFilterSet({'default': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_name_exact(self):
        f = ApplicationStateFilterSet({'name': 'example_state'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.state])

    def test_filter_by_direction_exact(self):
        f = ApplicationStateFilterSet({'direction': 'inbound'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.state])

    def test_filter_by_default_exact(self):
        f = ApplicationStateFilterSet({'default': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.state])

    def test_filters_combined_by_logical_and(self):
        f = ApplicationStateFilterSet({'direction': 'inbound', 'default': 'false'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class HalleyCloudConfigFilterSetTestCase(TestCase):
    queryset = HalleyCloudConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        HalleyCloudConfig.objects.create(**get_halley_cloud_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = HalleyCloudConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = HalleyCloudConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = HalleyCloudConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = HalleyCloudConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = HalleyCloudConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = HalleyCloudConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class MaggioliConfigFilterSetTestCase(TestCase):
    queryset = MaggioliConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = MaggioliConfig.objects.create(**get_maggioli_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        MaggioliConfig.objects.create(**get_maggioli_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = MaggioliConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = MaggioliConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = MaggioliConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = MaggioliConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = MaggioliConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = MaggioliConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class InforConfigFilterSetTestCase(TestCase):
    queryset = InforConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = InforConfig.objects.create(**get_infor_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        InforConfig.objects.create(**get_infor_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = InforConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = InforConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = InforConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = InforConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = InforConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = InforConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class D3ConfigFilterSetTestCase(TestCase):
    queryset = D3Config.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = D3Config.objects.create(**get_d3_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        D3Config.objects.create(**get_d3_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = D3ConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = D3ConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = D3ConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = D3ConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = D3ConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = D3ConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class HypersicConfigFilterSetTestCase(TestCase):
    queryset = HypersicConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = HypersicConfig.objects.create(**get_hypersic_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        HypersicConfig.objects.create(**get_hypersic_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = HypersicConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = HypersicConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = HypersicConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = HypersicConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = HypersicConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = HypersicConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class TinnConfigFilterSetTestCase(TestCase):
    queryset = TinnConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = TinnConfig.objects.create(**get_tinn_config_data(service=service))
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        TinnConfig.objects.create(**get_tinn_config_data(service=other_service, is_active=False))

    def test_filter_by_invalid_service_sdc_id(self):
        f = TinnConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = TinnConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = TinnConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = TinnConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = TinnConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = TinnConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class SicrawebWSProtocolloDMConfigFilterSetTestCase(TestCase):
    queryset = SicrawebWSProtocolloDMConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=service),
        )
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        SicrawebWSProtocolloDMConfig.objects.create(
            **get_sicraweb_wsprotocollodm_config_data(service=other_service, is_active=False),
        )

    def test_filter_by_invalid_service_sdc_id(self):
        f = SicrawebWSProtocolloDMConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = SicrawebWSProtocolloDMConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = SicrawebWSProtocolloDMConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = SicrawebWSProtocolloDMConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = SicrawebWSProtocolloDMConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = SicrawebWSProtocolloDMConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class SiscomConfigFilterSetTestCase(TestCase):
    queryset = SiscomConfig.objects.all()

    @classmethod
    def setUpTestData(cls):
        tenant = Tenant.objects.create(**get_tenant_data())
        service = Service.objects.create(**get_service_data(tenant=tenant))
        cls.config = SiscomConfig.objects.create(
            **get_siscom_config_data(service=service),
        )
        other_service = Service.objects.create(
            **get_service_data(tenant=tenant, sdc_id='696556d7-0fcb-480d-8091-362729938b1f'),
        )
        SiscomConfig.objects.create(
            **get_siscom_config_data(service=other_service, is_active=False),
        )

    def test_filter_by_invalid_service_sdc_id(self):
        f = SiscomConfigFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = SiscomConfigFilterSet(
            {'service_sdc_id': 'c822d571-b2cc-4403-b5b6-e03eb76299f3'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_is_active(self):
        f = SiscomConfigFilterSet({'is_active': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_service_sdc_id_exact(self):
        f = SiscomConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filter_by_is_active_exact(self):
        f = SiscomConfigFilterSet({'is_active': 'true'}, queryset=self.queryset.all())
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.config])

    def test_filters_combined_by_logical_and(self):
        f = SiscomConfigFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca', 'is_active': 'false'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])


class RegistrationFilterSetTestCase(TestCase):
    queryset = Registration.objects.all()

    @classmethod
    def setUpTestData(cls):
        cls.registration = Registration.objects.create(**get_registration_data())
        Registration.objects.create(**get_registration_data(application_sdc_id='f07f1a73-db4b-4458-ba15-6467158c364c'))

    def test_filter_by_invalid_application_sdc_id(self):
        f = RegistrationFilterSet({'application_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_application_sdc_id(self):
        f = RegistrationFilterSet(
            {'application_sdc_id': 'e52d8098-76eb-467c-ad5b-8ffda3ac6697'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_application_sdc_id_exact(self):
        f = RegistrationFilterSet(
            {'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.registration])


class DeadLetterFilterSetTestCase(TestCase):
    queryset = DeadLetter._base_manager.all()

    @classmethod
    def setUpTestData(cls):
        cls.dead_letter = DeadLetter.objects.create(**get_dead_letter_data())
        DeadLetter.objects.create(
            **get_dead_letter_data(
                tenant_sdc_id='0b6e54ed-0b2a-4285-99a0-aefc42c7bf80',
                service_sdc_id='31205a96-e4cb-4d7f-9fac-e98b45061a04',
            ),
        )

    def test_filter_by_invalid_tenant_sdc_id(self):
        f = DeadLetterFilterSet({'tenant_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_tenant_sdc_id(self):
        f = DeadLetterFilterSet(
            {'tenant_sdc_id': '816bcb05-e9f7-4149-bf0c-6a3248dc29d5'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_invalid_service_sdc_id(self):
        f = DeadLetterFilterSet({'service_sdc_id': 'invalid'}, queryset=self.queryset.all())
        self.assertFalse(f.is_valid())

    def test_filter_by_nonexistent_service_sdc_id(self):
        f = DeadLetterFilterSet(
            {'service_sdc_id': 'ba16a318-1ace-4166-b57a-dff767f7cc85'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])

    def test_filter_by_tenant_sdc_id_exact(self):
        f = DeadLetterFilterSet(
            {'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.dead_letter])

    def test_filter_by_service_sdc_id_exact(self):
        f = DeadLetterFilterSet(
            {'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca'},
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [self.dead_letter])

    def test_filters_combined_by_logical_and(self):
        f = DeadLetterFilterSet(
            {
                'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
                'service_sdc_id': 'afd541b9-0484-405f-8f58-06aa7594cfed',
            },
            queryset=self.queryset.all(),
        )
        self.assertTrue(f.is_valid())
        self.assertQuerySetEqual(f.qs, [])
