import re
from dataclasses import fields
from uuid import UUID

from django.test import SimpleTestCase

from apps.workers import dime


class DIMETestCase(SimpleTestCase):
    def test_record(self):
        mediatype = 'application/octet-stream'
        data = b'\x01\x02\x03'
        id = '22f490f5-e84a-47ef-8d8b-ce47dac9dadf'  # noqa
        record = dime.Record(type=mediatype, data=data, id=id)
        self.assertEqual(
            [field.name for field in fields(record)],
            ['type', 'data', 'id', 'btype', 'btype_length', 'data_length', 'bid', 'bid_length'],
        )
        self.assertEqual(record.type, mediatype)
        self.assertEqual(record.data, data)
        self.assertEqual(record.id, id)
        self.assertEqual(record.btype, b'application/octet-stream')
        self.assertEqual(record.btype_length, 24)
        self.assertEqual(record.data_length, 3)
        self.assertEqual(record.bid, b'22f490f5-e84a-47ef-8d8b-ce47dac9dadf')
        self.assertEqual(record.bid_length, 36)

    def test_record_default_id(self):
        record = dime.Record(type='application/octet-stream', data=b'\x01\x02\x03')
        pattern = r'^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$'
        self.assertIsNotNone(re.match(pattern, record.id))
        try:
            UUID(record.id)
        except Exception:
            self.fail()

    def test_envelope(self):
        data = b'\x01\x02\x03'
        envelope = dime.Envelope(data=data)
        self.assertEqual(
            [field.name for field in fields(envelope)],
            ['type', 'data', 'id', 'btype', 'btype_length', 'data_length', 'bid', 'bid_length'],
        )
        self.assertEqual(envelope.type, 'http://schemas.xmlsoap.org/soap/envelope/')
        self.assertEqual(envelope.data, data)
        self.assertEqual(envelope.id, '')
        self.assertEqual(envelope.btype, b'http://schemas.xmlsoap.org/soap/envelope/')
        self.assertEqual(envelope.btype_length, 41)
        self.assertEqual(envelope.data_length, 3)
        self.assertEqual(envelope.bid, b'')
        self.assertEqual(envelope.bid_length, 0)

    def test_message_as_bytes(self):
        envelope = dime.Envelope(data=b'\x01\x02\x03')
        record = dime.Record(
            type='application/octet-stream',
            data=b'\x04\x05\x06\x07\x08',
            id='22f490f5-e84a-47ef-8d8b-ce47dac9dadf',
        )
        message = dime.Message(envelope, record)
        self.assertEqual(
            message.as_bytes().hex(),
            '0c2000000000002900000003687474703a2f2f736368656d61732e786d6c736f61702e6f72672f736f61702f656e76656c6f70652'
            'f000000010203000a100000002400180000000532326634393066352d653834612d343765662d386438622d636534376461633964'
            '6164666170706c69636174696f6e2f6f637465742d73747265616d0405060708000000',
        )
