from contextlib import contextmanager
from datetime import datetime, timezone
from unittest import mock

import requests
import responses
from django.test import SimpleTestCase
from freezegun import freeze_time
from requests.exceptions import ConnectionError, RequestException, SSLError, Timeout  # noqa
from zeep.cache import InMemoryCache

from apps.workers.datagraph import soap
from apps.workers.errors import SOAPError, TransportError, ValidationError

from ._helpers import BASE_URL, INSERIMENTO_RESULT_XML, PROTOCOLLAZIONE_RESULT_XML, DatagraphSOAPTestClient, TestToken

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class CredentialsTestCase(SimpleTestCase):
    def test_fields(self):
        credentials = soap.Credentials(codente='cod', username='user01', password='s3cr3t')
        self.assertEqual(credentials.codente, 'cod')
        self.assertEqual(credentials.username, 'user01')
        self.assertEqual(credentials.password, 's3cr3t')


@contextmanager
def _stub_soap_client_class(stub):
    original, soap.Client.soap_client_class = soap.Client.soap_client_class, stub
    try:
        yield
    finally:
        soap.Client.soap_client_class = original


def _create_client():
    return soap.Client(
        credentials=soap.Credentials(codente='cod', username='user01', password='s3cr3t'),
        url=BASE_URL,
    )


class WSDLCacheTestCaseMixin:
    def test_shared_wsdl_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, f'{BASE_URL}/?wsdl', body=b'abc', status=200)

            for _ in range(2):
                client = _create_client()
                try:
                    self.invoke(client)
                except SOAPError:
                    pass

            self.assertEqual(len(rsps.calls), 1)
            InMemoryCache._cache = {}


class SOAPOperationTestCaseMixin:
    def test_soap_client_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = DatagraphSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                self.invoke(client)
                self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)

    def test_soap_client_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = DatagraphSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                self.invoke(client)

            stub.assert_timeout(840)

    def test_http_client_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            client = _create_client()

            with mock.patch.object(requests, 'post', wraps=requests.post) as mocked_post:
                with _stub_soap_client_class(DatagraphSOAPTestClient()):
                    self.invoke(client)

                self.assertEqual(mocked_post.call_args.kwargs['timeout'], 840)

    def test_token_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error):
                client = _create_client()

                with _stub_soap_client_class(DatagraphSOAPTestClient(token_error=error)):
                    with self.assertRaises(TransportError):
                        self.invoke(client)

    def test_token_response_body_failure(self):
        for dst in [None, '']:
            with self.subTest(dst=dst):
                client = _create_client()

                with _stub_soap_client_class(DatagraphSOAPTestClient(token=TestToken(dst))):
                    with self.assertRaises(ValidationError):
                        self.invoke(client)

    def test_token_unexpected_failure(self):
        client = _create_client()

        with _stub_soap_client_class(DatagraphSOAPTestClient(token_error=Exception)):
            with self.assertRaises(SOAPError):
                self.invoke(client)

    def test_token_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = DatagraphSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                self.invoke(client)
                self.invoke(client)

            stub.assert_login('cod', 'user01', 's3cr3t')

    def test_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, BASE_URL, body=error())
                client = _create_client()

                with _stub_soap_client_class(DatagraphSOAPTestClient()):
                    with self.assertRaises(TransportError):
                        self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_response_status_failure(self):
        for code in [401, 500]:
            with self.subTest(code=code), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, BASE_URL, body=b'abc', status=code)
                client = _create_client()

                with _stub_soap_client_class(DatagraphSOAPTestClient()):
                    with self.assertRaises(TransportError):
                        self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_response_body_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=b'<root/>', status=200)
            client = _create_client()

            with _stub_soap_client_class(DatagraphSOAPTestClient()):
                with self.assertRaises(ValidationError):
                    self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)

    def test_unexpected_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=Exception())
            client = _create_client()

            with _stub_soap_client_class(DatagraphSOAPTestClient()):
                with self.assertRaises(SOAPError):
                    self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)


class ClientInserimentoTestCase(WSDLCacheTestCaseMixin, SOAPOperationTestCaseMixin, SimpleTestCase):
    result_xml = INSERIMENTO_RESULT_XML

    def invoke(self, client):
        return client.inserimento('application/pdf', b'abc')

    def test_success(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = DatagraphSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                result = self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)
            stub.assert_login('cod', 'user01', 's3cr3t')
            stub.assert_create_message(
                stub.service,
                'Inserimento',
                strDST='xyz',
                strUserName='user01',
            )

            self.assertEqual(result.doc_id, '1170840885')

            self.assertEqual(len(rsps.calls), 1)
            self.assertEqual(rsps.calls[0].request.headers['Content-Type'], 'application/dime')
            self.assertEqual(rsps.calls[0].request.headers['SOAPAction'], 'http://tempuri.org/Inserimento')
            self.assertEqual(
                rsps.calls[0].request.body.hex()[:128],
                '0c200000000000290000002e687474703a2f2f736368656d61732e786d6c736f61702e6f72672f736f61702f656e76656c6f7'
                '0652f0000003c3f786d6c207665',
            )


class ClientProtocollazioneTestCase(WSDLCacheTestCaseMixin, SOAPOperationTestCaseMixin, SimpleTestCase):
    result_xml = PROTOCOLLAZIONE_RESULT_XML

    def invoke(self, client):
        return client.protocollazione(b'abc')

    def test_success(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = DatagraphSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                result = self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)
            stub.assert_login('cod', 'user01', 's3cr3t')
            stub.assert_create_message(
                stub.service,
                'Protocollazione',
                strDST='xyz',
                strUserName='user01',
            )

            self.assertEqual(result.number, '376')
            self.assertEqual(result.year, '2021')
            self.assertEqual(result.date, '25/05/2021')

            self.assertEqual(len(rsps.calls), 1)
            self.assertEqual(rsps.calls[0].request.headers['Content-Type'], 'application/dime')
            self.assertEqual(rsps.calls[0].request.headers['SOAPAction'], 'http://tempuri.org/Protocollazione')
            self.assertEqual(
                rsps.calls[0].request.body.hex()[:128],
                '0c200000000000290000002e687474703a2f2f736368656d61732e786d6c736f61702e6f72672f736f61702f656e76656c6f7'
                '0652f0000003c3f786d6c207665',
            )


class ClientGetTypesTestCase(WSDLCacheTestCaseMixin, SimpleTestCase):
    def invoke(self, client):
        return client.get_types()

    def test_soap_client_cache(self):
        stub = DatagraphSOAPTestClient()
        client = _create_client()

        with _stub_soap_client_class(stub):
            self.invoke(client)
            self.invoke(client)

        stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)

    def test_soap_client_timeout(self):
        stub = DatagraphSOAPTestClient()
        client = _create_client()

        with _stub_soap_client_class(stub):
            self.invoke(client)

        stub.assert_timeout(840)

    def test_unexpected_failure(self):
        client = _create_client()

        with _stub_soap_client_class(DatagraphSOAPTestClient(get_type_error=Exception)):
            with self.assertRaises(SOAPError):
                self.invoke(client)

    def test_success(self):
        stub = DatagraphSOAPTestClient()
        client = _create_client()

        with _stub_soap_client_class(stub):
            ret = self.invoke(client)

        stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)
        stub.assert_get_type_calls([((name,), {}) for name in soap.TYPE_NAMES])

        for name in soap.TYPE_NAMES:
            type_ = getattr(ret, name)
            self.assertEqual(type_.name, name)


class ResultTestCaseMixin:
    def test_invalid_xml(self):
        with self.assertRaises(ValidationError):
            self.create_result('invalid-xml')

    def test_error_code_failure(self):
        for errcode in [
            '',
            '<lngErrNumber></lngErrNumber>',
            '<lngErrNumber>-1</lngErrNumber>',
        ]:
            with self.subTest(errcode=errcode):
                xml = self.result_xml.replace('<lngErrNumber>0</lngErrNumber>', errcode)
                with self.assertRaises(ValidationError):
                    self.create_result(xml)


class InserimentoResultTestCase(ResultTestCaseMixin, SimpleTestCase):
    result_xml = INSERIMENTO_RESULT_XML

    def create_result(self, xml=None):
        xml = xml or self.result_xml
        return soap.InserimentoResult(xml.encode('utf8'))

    def test_doc_id_failure(self):
        for doc_id in [
            '',
            '<lngDocID></lngDocID>',
        ]:
            with self.subTest(doc_id=doc_id):
                xml = self.result_xml.replace('<lngDocID>1170840885</lngDocID>', doc_id)
                with self.assertRaises(ValidationError):
                    self.create_result(xml)

    @freeze_time(NOW)
    def test_success(self):
        result = self.create_result()
        self.assertEqual(result.doc_id, '1170840885')
        self.assertEqual(result.created_at, NOW)


class ProtocollazioneResultTestCase(ResultTestCaseMixin, SimpleTestCase):
    result_xml = PROTOCOLLAZIONE_RESULT_XML

    def create_result(self, xml=None):
        xml = xml or self.result_xml
        return soap.ProtocollazioneResult(xml.encode('utf8'))

    def test_number_failure(self):
        for number in [
            '',
            '<lngNumPG></lngNumPG>',
        ]:
            with self.subTest(number=number):
                xml = self.result_xml.replace('<lngNumPG>376</lngNumPG>', number)
                with self.assertRaises(ValidationError):
                    self.create_result(xml)

    def test_year_failure(self):
        for year in [
            '',
            '<lngAnnoPG></lngAnnoPG>',
        ]:
            with self.subTest(year=year):
                xml = self.result_xml.replace('<lngAnnoPG>2021</lngAnnoPG>', year)
                with self.assertRaises(ValidationError):
                    self.create_result(xml)

    def test_date_failure(self):
        for date in [
            '',
            '<strDataPG></strDataPG>',
        ]:
            with self.subTest(date=date):
                xml = self.result_xml.replace('<strDataPG>25/05/2021</strDataPG>', date)
                with self.assertRaises(ValidationError):
                    self.create_result(xml)

    @freeze_time(NOW)
    def test_success(self):
        result = self.create_result()
        self.assertEqual(result.number, '376')
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.date, '25/05/2021')
        self.assertEqual(result.created_at, NOW)
