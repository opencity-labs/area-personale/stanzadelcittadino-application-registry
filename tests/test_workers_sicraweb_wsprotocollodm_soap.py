from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.sicraweb_wsprotocollodm.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(
                SimpleNamespace(
                    IdDocumento=None,
                    AnnoProtocollo=None,
                    NumeroProtocollo=None,
                    DataProtocollo='0001-01-01T00:00:00.000+0100',
                    Messaggio='Error',
                    Errore='Error',
                ),
            )

    @freeze_time(NOW)
    def test_success(self):
        result = InsertResult(
            SimpleNamespace(
                IdDocumento='123456789',
                AnnoProtocollo='2021',
                NumeroProtocollo='12345',
                DataProtocollo='2023-10-04T10:58:20.441+0200',
                Messaggio='Success',
                Registri=None,
                Allegati={
                    'item': [
                        {
                            'Serial': '9449151',
                            'IDBase': '9449151',
                            'Versione': '0',
                        },
                        {
                            'Serial': '9449150',
                            'IDBase': '9449150',
                            'Versione': '0',
                        },
                    ],
                },
                Errore='',
            ),
        )
        self.assertEqual(result.number, '12345')
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.document_id, '123456789')
        self.assertEqual(result.created_at, NOW)
