from django.contrib.admin.sites import AdminSite
from django.test import TestCase

from apps.applications.admin import (
    D3ConfigAdmin,
    DatagraphConfigAdmin,
    HalleyCloudConfigAdmin,
    HypersicConfigAdmin,
    InforConfigAdmin,
    InsielConfigAdmin,
    MaggioliConfigAdmin,
    PitreConfigAdmin,
    SicrawebWSProtocolloDMConfigAdmin,
    SiscomConfigAdmin,
    TinnConfigAdmin,
)
from apps.applications.models import (
    D3Config,
    DatagraphConfig,
    HalleyCloudConfig,
    HypersicConfig,
    InforConfig,
    InsielConfig,
    MaggioliConfig,
    PitreConfig,
    SicrawebWSProtocolloDMConfig,
    SiscomConfig,
    TinnConfig,
)


class HalleyCloudConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = HalleyCloudConfigAdmin(HalleyCloudConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'institution_office_ids': [],
                'classification_category': '',
                'classification_class': '',
                'classification_subclass': '',
                'mailbox': '',
                'folder_name': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'institution_office_ids': [],
                'classification_category': None,
                'classification_class': None,
                'classification_subclass': None,
                'mailbox': None,
                'folder_name': None,
            },
        )


class DatagraphConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = DatagraphConfigAdmin(DatagraphConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'classification_institution_code': '',
                'classification_aoo_code': '',
                'classification_hierarchy': '',
                'folder_number': '',
                'folder_year': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class MaggioliConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = MaggioliConfigAdmin(MaggioliConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'sending_mode': '',
                'sending_method': '',
                'classification_hierarchy': '',
                'folder_number': '',
                'folder_year': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'sending_mode': None,
                'sending_method': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class PitreConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = PitreConfigAdmin(PitreConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'code_rf': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'code_rf': None,
                'means_of_sending': '',
                'recipient_code': None,
                'transmission_reason': '',
                'transmission_type': None,
                'note': '',
            },
        )


class InsielConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = InsielConfigAdmin(InsielConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'operating_office_code': '',
                'register_code': '1',
                'prog_doc': '',
                'prog_movi': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'operating_office_code': None,
                'register_code': '1',
                'prog_doc': None,
                'prog_movi': None,
            },
        )

        form = form_class(
            data={
                'description': '',
                'operating_office_code': '',
                'register_code': '',
                'prog_doc': '1',
                'prog_movi': '1',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'internal_offices_codes': [],
                'internal_offices_types': [],
                'operating_office_code': None,
                'register_code': None,
                'prog_doc': '1',
                'prog_movi': '1',
            },
        )

        form = form_class(
            data={
                'description': '',
                'operating_office_code': '',
                'register_code': '',
                'prog_doc': '',
                'prog_movi': '',
            },
        )

        self.assertFalse(form.is_valid())


class InforConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = InforConfigAdmin(InforConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={},
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
            },
        )


class D3ConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = D3ConfigAdmin(D3Config, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'attachment_source_category': '',
                'document_subtype': '',
                'classification': '',
                'folder_name': '',
                'visibility': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'register_attachments': False,
                'attachment_source_category': None,
                'document_subtype': None,
                'classification': None,
                'folder_name': None,
                'visibility': None,
            },
        )


class HypersicConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = HypersicConfigAdmin(HypersicConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'classification': '',
                'folder_number': '',
                'folder_year': '',
                'annotation': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'classification': None,
                'folder_number': None,
                'folder_year': None,
                'annotation': None,
            },
        )


class TinnConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = TinnConfigAdmin(TinnConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'institution_name': '',
                'institution_code': '',
                'institution_email': '',
                'institution_ou': '',
                'institution_aoo_code': '',
                'classification_institution_code': '',
                'classification_aoo_code': '',
                'folder_number': '',
                'folder_year': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'institution_name': None,
                'institution_code': None,
                'institution_email': None,
                'institution_ou': None,
                'institution_aoo_code': None,
                'classification_institution_code': None,
                'classification_aoo_code': None,
                'folder_number': None,
                'folder_year': None,
            },
        )


class SicrawebWSProtocolloDMConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = SicrawebWSProtocolloDMConfigAdmin(SicrawebWSProtocolloDMConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'institution_code': '',
                'institution_ou': '',
                'sending_mode': '',
                'internal_sender': '',
                'update_records': '',
                'classification_hierarchy': '',
                'folder_number': '',
                'folder_year': '',
                'connection_user': '',
                'connection_user_role': '',
                'standard_subject': '',
                'document_type': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'institution_code': None,
                'institution_ou': None,
                'sending_mode': None,
                'internal_sender': None,
                'update_records': None,
                'classification_hierarchy': None,
                'folder_number': None,
                'folder_year': None,
                'connection_user': None,
                'connection_user_role': None,
                'standard_subject': None,
                'document_type': None,
            },
        )


class SiscomConfigAdminTestCase(TestCase):
    def test_optional_fields(self):
        modeladmin = SiscomConfigAdmin(SiscomConfig, AdminSite())
        form_class = modeladmin.get_form(None)

        form = form_class(
            data={
                'description': '',
                'operator_denomination': '',
                'classification_category': '',
                'classification_class': '',
                'folder_id': '',
                'mail_type': '',
            },
        )

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.cleaned_data,
            {
                'description': '',
                'is_active': False,
                'operator_denomination': None,
                'classification_category': None,
                'classification_class': None,
                'folder_id': None,
                'mail_type': None,
            },
        )
