from collections import namedtuple
from contextlib import contextmanager
from datetime import datetime, timezone
from itertools import count
from types import SimpleNamespace

from lxml import etree
from requests.models import Response

from apps.applications.models import ApplicationState, DeadLetter, MaggioliConfig, RegistrationEvent, Service
from apps.timezone_utils import make_aware
from apps.workers.d3.rest import InsertResult as D3InsertResult
from apps.workers.datagraph.soap import InserimentoResult, ProtocollazioneResult
from apps.workers.dedagroup.rest import ProtocollazioneResult as DedagroupProtocollazioneResult
from apps.workers.halley.soap import InsertResult
from apps.workers.halley_cloud.soap import InsertResult as HCInsertResult
from apps.workers.hypersic.soap import InsertResult as HypersicInsertResult
from apps.workers.infor.soap import InsertResult as InforInsertResult
from apps.workers.maggioli.soap import InsertDocumentoResult, RegistraProtocolloResult
from apps.workers.sicraweb_wsprotocollodm.soap import InsertResult as SicrawebWSProtocolloDMInsertResult
from apps.workers.siscom.soap import InsertResult as SiscomInsertResult
from apps.workers.tinn.soap import InsertResult as TinnInsertResult

BASE_URL = 'https://example.local'

INSERIMENTO_RESULT_XML = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <!-- soap:Header tag omitted -->
    <soap:Body>
        <InserimentoResponse xmlns="http://tempuri.org/">
            <InserimentoResult>
                <lngDocID>1170840885</lngDocID>
                <lngErrNumber>0</lngErrNumber>
                <strErrString />
            </InserimentoResult>
        </InserimentoResponse>
    </soap:Body>
</soap:Envelope>"""

PROTOCOLLAZIONE_RESULT_XML = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <!-- soap:Header tag omitted -->
    <soap:Body>
        <ProtocollazioneResponse xmlns="http://tempuri.org/">
            <ProtocollazioneResult>
                <lngNumPG>376</lngNumPG>
                <lngAnnoPG>2021</lngAnnoPG>
                <strDataPG>25/05/2021</strDataPG>
                <lngErrNumber>0</lngErrNumber>
                <strErrString />
            </ProtocollazioneResult>
        </ProtocollazioneResponse>
    </soap:Body>
</soap:Envelope>"""

INSERT_DOCUMENTO_RESULT_XML = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>
    <soapenv:Body>
        <ns1:insertDocumentoResponse
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xmlns:ns1="urn:ProWSApi"
        >
            <insertDocumentoReturn href="#id0"/>
        </ns1:insertDocumentoResponse>
        <multiRef
            id="id0"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="ns2:InserimentoRet"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
            xmlns:ns2="http://impl.webservices.protocollo.pubblici.saga.it"
        >
            <lngDocID href="#id1"/>
            <lngErrNumber href="#id2"/>
            <strErrString xsi:type="xsd:string" xsi:nil="true"/>
        </multiRef>
        <multiRef
            id="id2"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="xsd:long"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        >0</multiRef>
        <multiRef
            id="id1"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="xsd:long"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        >516187</multiRef>
    </soapenv:Body>
</soapenv:Envelope>"""

REGISTRA_PROTOCOLLO_RESULT_XML = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>
    <soapenv:Body>
        <ns1:registraProtocolloResponse
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xmlns:ns1="urn:ProWSApi"
        >
            <registraProtocolloReturn href="#id0"/>
        </ns1:registraProtocolloResponse>
        <multiRef
            id="id0"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="ns2:ProtocollazioneRet"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
            xmlns:ns2="http://impl.webservices.protocollo.pubblici.saga.it"
        >
            <lngNumPG href="#id1"/>
            <lngAnnoPG href="#id2"/>
            <lngErrNumber href="#id3"/>
            <strErrString xsi:type="xsd:string"></strErrString>
        </multiRef>
        <multiRef
            id="id1"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="xsd:long"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        >84</multiRef>
        <multiRef
            id="id2"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="xsd:long"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        >2021</multiRef>
        <multiRef
            id="id3"
            soapenc:root="0"
            soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
            xsi:type="xsd:long"
            xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        >0</multiRef>
    </soapenv:Body>
</soapenv:Envelope>"""

RETRY_DATA_VALUE = (
    '{"id": "b8a44017-6db1-4abb-af6c-c95f34f08af6", "user": "43f619df-3ba2-4d9d-bcdd-270484eac44d", '
    '"tenant": "f0620f93-c2a8-4ebd-8e64-67ef125177e5", '
    '"service_name": "example service", "service_id": "74dcab79-790e-459e-bbe6-d5972b298b42", '
    '"event_id": "b751510d-c5e4-4fe9-a8cf-f517f90e68b5", "event_version": "1.0", '
    '"compiled_modules": [{"url": "https://example.local/inbound-document/", "originalName": "inbound_document.pdf", '
    '"description": "Inbound document", "protocol_required": true}], '
    '"attachments": [{"url": "https://example.local/inbound-attachment/", '
    '"originalName": "inbound_attachment.doc", "description": "Inbound attachment", "protocol_required": true}], '
    '"data": {"applicant.data.completename.data.name": "Mario", "applicant.data.completename.data.surname": "Rossi", '
    '"applicant.data.fiscal_code.data.fiscal_code": "MRARSS80A01F205W", '
    '"applicant.data.address.data.address": "Corso Buenos Aires", "applicant.data.address.data.house_number": "42/A", '
    '"applicant.data.address.data.postal_code": "20124", "applicant.data.address.data.municipality": "Milano", '
    '"applicant.data.address.data.county": "MI", "applicant.data.email_address": "rossi@example.local"}, '
    '"submitted_at": "2021-05-18T15:07:16+02:00", "flow_changed_at": "2021-05-18T16:17:18+02:00", '
    '"status_name": "example_state", "outcome_file": {"url": "https://example.local/outbound-document/", '
    '"originalName": "outbound_document.pdf", "description": "Outbound document", "protocol_required": true}, '
    '"outcome_attachments": [{"url": "https://example.local/outbound-attachment/", '
    '"originalName": "outbound_attachment.doc", "description": "Outbound attachment", "protocol_required": true}], '
    '"integrations": [{"outbound": {"url": "https://example.local/ignored/", "originalName": "ignored.pdf", '
    '"description": "Ignored", "protocol_required": true, "attachments": []}, "inbound": null}, '
    '{"outbound": {"url": "https://example.local/outbound-integration-document/", '
    '"originalName": "outbound_integration_document.pdf", "description": "Outbound integration document", '
    '"protocol_required": true, "attachments": [{"url": "https://example.local/outbound-integration-attachment/", '
    '"originalName": "outbound_integration_attachment.doc", "description": "Outbound integration attachment", '
    '"protocol_required": true}]}, "inbound": {"url": "https://example.local/inbound-integration-document/", '
    '"originalName": "inbound_integration_document.pdf", "description": "Inbound integration document", '
    '"protocol_required": true, "attachments": [{"url": "https://example.local/inbound-integration-attachment/", '
    '"originalName": "inbound_integration_attachment.doc", "description": "Inbound integration attachment", '
    '"protocol_required": true}]}}], '
    '"subject": "Test", "__registry_metadata": {"attempts": 6, "retry_at": "2021-05-25T13:20:30.123456+00:00", '
    '"retry_topic": "retry_180_test"}}'
).encode()

DUNDER_CALL = '__call__'
LOGIN = 'Login'
CREATE_MESSAGE = 'create_message'
GET_TYPE = 'get_type'

GET_DOCUMENT = 'get_document'
UPDATE_APPLICATION = 'update_application'

INSERIMENTO = 'inserimento'
PROTOCOLLAZIONE = 'protocollazione'
GET_TYPES = 'get_types'

INSERT_DOCUMENTO = 'insert_documento'
REGISTRA_PROTOCOLLO = 'registra_protocollo'

UPLOAD = 'upload'
INSERT = 'insert'
COLLATE = 'collate'
REGISTER = 'register'

FASCICOLA_DOCUMENTO = 'fascicola_documento'

SUBSCRIBE = 'subscribe'
MOVE = 'move'
SEND = 'send'
FLUSH = 'flush'
COMMIT = 'commit'
CLOSE = 'close'

INCR_RETRIES = 'incr_retries'
SET_LATENCY = 'set_latency'
INCR_DEAD_LETTERED = 'incr_dead_lettered'


def get_application_data(**kwargs):
    return {
        'id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'user': '43f619df-3ba2-4d9d-bcdd-270484eac44d',
        'tenant': 'f0620f93-c2a8-4ebd-8e64-67ef125177e5',
        'service_name': 'example service',
        'service_id': '74dcab79-790e-459e-bbe6-d5972b298b42',
        'event_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
        'event_version': '1.0',
        'compiled_modules': [
            {
                'url': 'https://example.local/inbound-document/',
                'originalName': 'inbound_document.pdf',
                'description': 'Inbound document',
                'protocol_required': True,
            },
        ],
        'attachments': [
            {
                'url': 'https://example.local/inbound-attachment/',
                'originalName': 'inbound_attachment.doc',
                'description': 'Inbound attachment',
                'protocol_required': True,
            },
        ],
        'data': {
            'applicant.data.completename.data.name': 'Mario',
            'applicant.data.completename.data.surname': 'Rossi',
            'applicant.data.fiscal_code.data.fiscal_code': 'MRARSS80A01F205W',
            'applicant.data.address.data.address': 'Corso Buenos Aires',
            'applicant.data.address.data.house_number': '42/A',
            'applicant.data.address.data.postal_code': '20124',
            'applicant.data.address.data.municipality': 'Milano',
            'applicant.data.address.data.county': 'MI',
            'applicant.data.email_address': 'rossi@example.local',
        },
        'submitted_at': '2021-05-18T15:07:16+02:00',
        'flow_changed_at': '2021-05-18T16:17:18+02:00',
        'status_name': 'example_state',
        'outcome_file': {
            'url': 'https://example.local/outbound-document/',
            'originalName': 'outbound_document.pdf',
            'description': 'Outbound document',
            'protocol_required': True,
        },
        'outcome_attachments': [
            {
                'url': 'https://example.local/outbound-attachment/',
                'originalName': 'outbound_attachment.doc',
                'description': 'Outbound attachment',
                'protocol_required': True,
            },
        ],
        'integrations': [
            {
                'outbound': {
                    'url': 'https://example.local/ignored/',
                    'originalName': 'ignored.pdf',
                    'description': 'Ignored',
                    'protocol_required': True,
                    'attachments': [],
                },
                'inbound': None,
            },
            {
                'outbound': {
                    'url': 'https://example.local/outbound-integration-document/',
                    'originalName': 'outbound_integration_document.pdf',
                    'description': 'Outbound integration document',
                    'protocol_required': True,
                    'attachments': [
                        {
                            'url': 'https://example.local/outbound-integration-attachment/',
                            'originalName': 'outbound_integration_attachment.doc',
                            'description': 'Outbound integration attachment',
                            'protocol_required': True,
                        },
                    ],
                },
                'inbound': {
                    'url': 'https://example.local/inbound-integration-document/',
                    'originalName': 'inbound_integration_document.pdf',
                    'description': 'Inbound integration document',
                    'protocol_required': True,
                    'attachments': [
                        {
                            'url': 'https://example.local/inbound-integration-attachment/',
                            'originalName': 'inbound_integration_attachment.doc',
                            'description': 'Inbound integration attachment',
                            'protocol_required': True,
                        },
                    ],
                },
            },
        ],
        'subject': 'Test',
        '__registry_metadata': {
            'attempts': 5,
            'retry_at': '2021-11-30T09:46:08.403371+00:00',
            'retry_topic': 'retry_30_test',
        },
        **kwargs,
    }


def get_document_data(**kwargs):
    return {
        'app_id': 'stanzadelcittadino-application-registry:1.12.3',
        'event_created_at': '2024-03-08T14:47:30+01:00',
        'event_id': 'e39db95d-c11a-4026-aedf-894da52ab34f',
        'title': 'Richiesta: Test',
        'id': '73564084-b505-4ef4-81c4-beb2489d002c',
        'event_version': 1,
        'registration_data': {
            'transmission_type': 'Inbound',
            'date': '2022-09-19T18:00:00+02:00',
            'document_number': '123',
        },
        'folder': {'title': 'example service Mario Rossi MRARSS80A01F205W', 'id': None},
        'type': 'application-request',
        'remote_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'remote_collection': {'id': '74dcab79-790e-459e-bbe6-d5972b298b42', 'type': 'service'},
        'short_description': 'Test',
        'description': 'Test',
        'main_document': {
            'name': 'inbound_document.pdf',
            'description': 'Inbound document',
            'mime_type': 'application/pdf',
            'url': 'https://example.local/inbound-document/',
            'filename': 'inbound_document.pdf',
            'md5': None,
        },
        'attachments': [
            {
                'name': 'inbound_attachment.doc',
                'description': 'Inbound attachment',
                'mime_type': 'application/msword',
                'url': 'https://example.local/inbound-attachment/',
                'filename': 'inbound_attachment.doc',
                'md5': None,
            },
        ],
        'tenant_id': 'f0620f93-c2a8-4ebd-8e64-67ef125177e5',
        'owner_id': '43f619df-3ba2-4d9d-bcdd-270484eac44d',
        'created_at': '2024-03-08T14:47:30+01:00',
        'updated_at': '2024-03-08T14:47:30+01:00',
        'author': [
            {
                'type': 'human',
                'tax_identification_number': 'MRARSS80A01F205W',
                'name': 'Mario',
                'family_name': 'Rossi',
                'street_name': 'Corso Buenos Aires',
                'building_number': '42/A',
                'postal_code': '20124',
                'email': 'rossi@example.local',
                'role': 'sender',
            },
        ],
        'source_type': 'user',
        'recipient_type': 'tenant',
        'topics': None,
        'image_gallery': None,
        'related_public_services': None,
        'normative_requirements': None,
        'related_documents': None,
        'life_events': None,
        'business_events': None,
        'allowed_readers': None,
        **kwargs,
    }


def get_tenant_data(**kwargs):
    return {
        'description': 'Comune Di Bugliano',
        'slug': 'comune-di-bugliano',
        'sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
        'sdc_base_url': 'https://example.local/comune-di-bugliano/',
        'sdc_username': 'johndoe',
        'sdc_password': 's3cr3t',
        'sdc_institution_code': 'SDC',
        'sdc_aoo_code': 'P_SDC',
        'latest_registration_number': 42,
        'latest_registration_number_issued_at': datetime(2021, 1, 2, 10, 20, 30, 123456, tzinfo=timezone.utc),
        'register_after_date': datetime(2000, 1, 1, 10, 20, 30, 123456, tzinfo=timezone.utc),
        **kwargs,
    }


def get_service_data(**kwargs):
    return {
        'description': 'Posizione TARI',
        'sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
        'provider': Service.Provider.DATAGRAPH,
        **kwargs,
    }


def get_application_state_data(**kwargs):
    return {
        'name': 'example_state',
        'code': '12345',
        'direction': ApplicationState.Direction.INBOUND,
        'integrations': False,
        'default': True,
        'update_sdc': True,
        **kwargs,
    }


def get_datagraph_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'url': 'https://example.local/datagraph/',
        'codente': 'COD-111',
        'username': 'WSDOCAREA',
        'password': 's3cr3t',
        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
        'institution_code': 'COD-222',
        'institution_email': 'bugliano-tributi@example.local',
        'institution_ou': 'TRI',
        'institution_aoo_code': 'COD-333',
        'classification_institution_code': 'COD-444',
        'classification_aoo_code': 'COD-555',
        'classification_hierarchy': '7.2.0',
        'folder_number': '42',
        'folder_year': '2021',
        'document_format': 'servizio online',
        **kwargs,
    }


def get_maggioli_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'url': 'https://example.local/maggioli/',
        'connection_string': 'test_conn_string',
        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
        'institution_code': 'COD-111',
        'institution_email': 'bugliano-tributi@example.local',
        'institution_ou': 'TRI',
        'institution_aoo_code': 'COD-222',
        'sending_mode': MaggioliConfig.SendingMode.PEC,
        'sending_method': 'COD-333',
        'classification_hierarchy': '7.2.0',
        'folder_number': '42',
        'folder_year': '2021',
        'document_type': 'servizio online',
        **kwargs,
    }


def get_halley_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'login_url': 'https://example.local/halley/login/',
        'registration_url': 'https://example.local/halley/registration/',
        'username': 'console',
        'password': 's3cr3t',
        'institution_name': 'Comune di Bugliano',
        'institution_aoo_id': 1,
        'institution_office_ids': [],
        'classification_category': '7',
        'classification_class': '2',
        'classification_subclass': '0',
        'folder_id': 1,
        **kwargs,
    }


def get_halley_cloud_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'registration_url': 'https://example.local/halley_cloud/registration/',
        'username': 'console',
        'password': 's3cr3t',
        'mailbox': 'example@example.com',
        'institution_office_ids': [],
        'classification_category': '7',
        'classification_class': '2',
        'classification_subclass': '0',
        'folder_name': 'Fascicolo test',
        **kwargs,
    }


def get_dedagroup_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'token_url': 'https://token.example.local',
        'base_url': 'https://base.example.local',
        'client_id': 'abc',
        'client_secret': 's3cr3t',
        'grant_type': 'client_credentials',
        'resource': 'http://base.example.local',
        'registry_id': 3,
        'author_id': 123456,
        'organization_chart_level_code': 'abcde12345',
        'institution_aoo_id': 123,
        'classification_title': '7',
        'classification_class': '2',
        'classification_subclass': '0',
        'folder_code': '2022.1.1',
        **kwargs,
    }


def get_pitre_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'base_url': 'https://base.example.local',
        'certificate': 's3cr3t',
        'username': 'sdc',
        'code_role': 'rda_sdc',
        'code_application': 'sdc',
        'code_adm': 'ABC',
        'code_node_classification': '1.23',
        'transmission_reason': 'TRASM ONLINE',
        'transmission_type': 'T',
        'receiver_codes': ['123'],
        'recipient_code': 'rda_recipient',
        'code_register': 'ABC',
        'means_of_sending': 'SERVIZI ONLINE',
        'code_rf': 'ABC',
        'note': 'Outbound document note',
        **kwargs,
    }


def get_insiel_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'login_url': 'https://base.example.local/token',
        'registry_url': 'https://base.example.local/protocollo',
        'file_transfer_url': 'https://base.example.local/filetransfer',
        'client_id': 'sdc',
        'client_secret': 's3cr3t',
        'grant_type': 'client_credentials',
        'user_code': 'sdc',
        'user_password': 's3cr3t',
        'operating_office_code': '1',
        'office_code': 'ABC',
        'office_registry': 'ABC',
        'internal_offices_codes': [],
        'register_code': '  1',
        'prog_doc': None,
        'prog_movi': None,
        **kwargs,
    }


def get_infor_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'username': 'sdc',
        'denomination': 'sdc',
        'email': 'user@example.com',
        'wsdl_url': 'https://base.example.local/protocollo?wsdl',
        'registry_url': 'https://base.example.local/protocollo',
        'incoming_document_type': 'xx',
        'incoming_intermediary': 'WEB',
        'incoming_sorting': 'ABC',
        'incoming_classification': '16.02',
        'incoming_folder': '2012/6',
        'response_internal_sender': 'sdc',
        'response_document_type': 'xx',
        'response_intermediary': 'WEB',
        'response_sorting': 'ABC',
        'response_classification': '16.02',
        'response_folder': '2012/6',
        **kwargs,
    }


def get_d3_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'username': 'sdc',
        'password': 's3cr3t',
        'registry_url': 'https://base.example.local/',
        'repository_id': '0b07a6e1-dee9-4f54-889a-fbf459143c26',
        'main_source_category': 'EXAMPLE',
        'register_attachments': True,
        'attachment_source_category': 'EXAMPLE2',
        'document_subtype': 'Allgmein - generico',
        'classification': '15.02.02',
        'folder_name': '1234',
        'competent_service': 'Sekretariat - Segreteria',
        'visibility': 'Sekretariat - Segreteria: S',
        **kwargs,
    }


def get_hypersic_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'username': 'sdc',
        'password': 's3cr3t',
        'registry_url': 'https://base.example.local/protocollo',
        'office_ids': [123],
        'classification': '6-3-0',
        'folder_number': '2',
        'folder_year': '2021',
        'annotation': 'something',
        **kwargs,
    }


def get_tinn_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'url': 'https://example.local/tinn/',
        'codente': 'COD-111',
        'username': 'WSDOCAREA',
        'password': 's3cr3t',
        'institution_name': 'Comune di Bugliano - Ufficio Tributi',
        'institution_code': 'COD-222',
        'institution_email': 'bugliano-tributi@example.local',
        'institution_ou': 'TRI',
        'institution_aoo_code': 'COD-333',
        'classification_institution_code': 'COD-444',
        'classification_aoo_code': 'COD-555',
        'classification_hierarchy': '7.2.0',
        'folder_number': '42',
        'folder_year': '2021',
        **kwargs,
    }


def get_sicraweb_wsprotocollodm_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'url': 'https://example.local/sicraweb-wsprotocollodm/',
        'connection_string': 'test_conn_string',
        'institution_code': 'COD-111',
        'institution_ou': 'TRI',
        'institution_aoo_code': 'COD-222',
        'sending_mode': 'PEC',
        'internal_sender': 'ANA',
        'update_records': 'S',
        'classification_hierarchy': '7.2.0',
        'folder_number': '42',
        'folder_year': '2021',
        'connection_user': 'opencontent',
        'connection_user_role': 'user',
        'standard_subject': 'subject',
        'document_type': 'servizio online',
        **kwargs,
    }


def get_siscom_config_data(**kwargs):
    return {
        'description': 'Test configuration',
        'is_active': True,
        'url': 'https://example.local/siscom/',
        'license_code': '1008',
        'operator_denomination': 'John Doe',
        'classification_category': '0',
        'classification_class': '0',
        'folder_id': '0',
        'mail_type': '0',
        'office_id': '7',
        **kwargs,
    }


def get_registration_event_data(**kwargs):
    return {
        'provider': Service.Provider.DATAGRAPH,
        'config_id': 1,
        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
        'state': RegistrationEvent.State.PROCESSING,
        'data': {'key': 'value'},
        'traceback': 'a lengthy error',
        'retry_traceback': 'another lengthy error',
        **kwargs,
    }


def get_registration_data(**kwargs):
    return {
        'number': '1234',
        'date': datetime(2021, 1, 2, 10, 20, 30, 123456, tzinfo=timezone.utc),
        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'application_state_name': 'example_state',
        'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
        'is_completed': True,
        'is_updated': True,
        'registration_data': {},
        **kwargs,
    }


def get_pre_registration_data(**kwargs):
    return {
        'number': '1234',
        'folder': '123',
        'date': datetime(2021, 1, 2, 10, 20, 30, 123456, tzinfo=timezone.utc),
        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'application_state_name': 'example_state',
        'event_sdc_id': 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5',
        'pre_registration_data': {},
        **kwargs,
    }


def get_dead_letter_data(**kwargs):
    return {
        'tenant_sdc_id': '3929c9d4-d50f-4a3d-8d1a-d9dda7cfdafc',
        'service_sdc_id': '5b0ebe19-51a3-4dc9-8fa8-0af3555a4dca',
        'application_sdc_id': 'b8a44017-6db1-4abb-af6c-c95f34f08af6',
        'state': DeadLetter.State.NEW,
        'data': {'key': 'value'},
        'is_deleted': False,
        **kwargs,
    }


@contextmanager
def stub_retry_producer_class(stub):
    original, DeadLetter.retry_producer_class = DeadLetter.retry_producer_class, stub
    try:
        yield
    finally:
        DeadLetter.retry_producer_class = original


class TestToken:
    def __init__(self, dst):
        self.strDST = dst


class TestType:
    def __init__(self, name):
        self.name = name


class DatagraphSOAPTestClient:
    def __init__(self, token=None, token_error=None, get_type_error=None):
        self._token = token or TestToken('xyz')
        self._token_error = token_error
        self._get_type_error = get_type_error
        self._calls = {
            DUNDER_CALL: [],
            LOGIN: [],
            CREATE_MESSAGE: [],
            GET_TYPE: [],
        }

    @property
    def service(self):
        return self

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def Login(self, *args, **kwargs):  # noqa
        self._store_call(LOGIN, args, kwargs)
        if self._token_error:
            raise self._token_error
        return self._token

    def create_message(self, *args, **kwargs):
        self._store_call(CREATE_MESSAGE, args, kwargs)
        return etree.Element('root')

    def get_type(self, *args, **kwargs):
        self._store_call(GET_TYPE, args, kwargs)
        if self._get_type_error:
            raise self._get_type_error
        return TestType(name=args[0])

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_login(self, *args, **kwargs):
        assert self._calls[LOGIN] == [(args, kwargs)]

    def assert_create_message(self, *args, **kwargs):
        assert self._calls[CREATE_MESSAGE] == [(args, kwargs)]

    def assert_get_type_calls(self, expected):
        assert self._calls[GET_TYPE] == expected

    def assert_timeout(self, seconds):
        calls = self._calls[DUNDER_CALL]
        assert len(calls) == 1
        _, kwargs = self._calls[DUNDER_CALL][0]
        transport = kwargs['transport']
        assert transport.load_timeout == seconds
        assert transport.operation_timeout == seconds


class MaggioliSOAPTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            CREATE_MESSAGE: [],
        }

    @property
    def service(self):
        return self

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def create_message(self, *args, **kwargs):
        self._store_call(CREATE_MESSAGE, args, kwargs)
        return etree.Element('root')

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_create_message(self, *args, **kwargs):
        assert self._calls[CREATE_MESSAGE] == [(args, kwargs)]

    def assert_timeout(self, seconds):
        calls = self._calls[DUNDER_CALL]
        assert len(calls) == 1
        _, kwargs = self._calls[DUNDER_CALL][0]
        transport = kwargs['transport']
        assert transport.load_timeout == seconds
        assert transport.operation_timeout == seconds


class SDCTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            GET_DOCUMENT: [],
            UPDATE_APPLICATION: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def get_document(self, *args, **kwargs):
        self._store_call(GET_DOCUMENT, args, kwargs)
        return b'abc'

    def update_application(self, *args, **kwargs):
        self._store_call(UPDATE_APPLICATION, args, kwargs)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_get_document(self, *args, **kwargs):
        assert self._calls[GET_DOCUMENT] == [(args, kwargs)]

    def assert_update_application(self, *args, **kwargs):
        assert self._calls[UPDATE_APPLICATION] == [(args, kwargs)]


class DatagraphTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERIMENTO: [],
            PROTOCOLLAZIONE: [],
            GET_TYPES: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def inserimento(self, *args, **kwargs):
        self._store_call(INSERIMENTO, args, kwargs)
        return InserimentoResult(INSERIMENTO_RESULT_XML.encode('utf8'))

    def protocollazione(self, *args, **kwargs):
        self._store_call(PROTOCOLLAZIONE, args, kwargs)
        return ProtocollazioneResult(PROTOCOLLAZIONE_RESULT_XML.encode('utf8'))

    def get_types(self, *args, **kwargs):
        self._store_call(GET_TYPES, args, kwargs)
        return SimpleNamespace(Documento=TestType('doc'))

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_inserimento(self, *args, **kwargs):
        assert self._calls[INSERIMENTO] == [(args, kwargs)]

    def assert_protocollazione(self, *args, **kwargs):
        assert self._calls[PROTOCOLLAZIONE] == [(args, kwargs)]

    def assert_get_types(self, *args, **kwargs):
        assert self._calls[GET_TYPES] == [(args, kwargs)]


class MaggioliTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT_DOCUMENTO: [],
            REGISTRA_PROTOCOLLO: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert_documento(self, *args, **kwargs):
        self._store_call(INSERT_DOCUMENTO, args, kwargs)
        return InsertDocumentoResult(INSERT_DOCUMENTO_RESULT_XML.encode('utf8'))

    def registra_protocollo(self, *args, **kwargs):
        self._store_call(REGISTRA_PROTOCOLLO, args, kwargs)
        return RegistraProtocolloResult(REGISTRA_PROTOCOLLO_RESULT_XML.encode('utf8'))

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert_documento(self, *args, **kwargs):
        assert self._calls[INSERT_DOCUMENTO] == [(args, kwargs)]

    def assert_registra_protocollo(self, *args, **kwargs):
        assert self._calls[REGISTRA_PROTOCOLLO] == [(args, kwargs)]


class HalleyTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return InsertResult(SimpleNamespace(Numero=84, Esito=0))

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class HalleyCloudTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return HCInsertResult(SimpleNamespace(anno=2021, numero=376, coderrore=0))

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class DedagroupTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            PROTOCOLLAZIONE: [],
            FASCICOLA_DOCUMENTO: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def protocollazione(self, *args, **kwargs):
        self._store_call(PROTOCOLLAZIONE, args, kwargs)
        return DedagroupProtocollazioneResult({'resultType': 1, 'result': {'numeroProtocollo': 84}})

    def fascicola_documento(self, *args, **kwargs):
        self._store_call(FASCICOLA_DOCUMENTO, args, kwargs)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_protocollazione(self, *args, **kwargs):
        assert self._calls[PROTOCOLLAZIONE] == [(args, kwargs)]

    def assert_fascicola_documento(self, *args, **kwargs):
        assert self._calls[FASCICOLA_DOCUMENTO] == [(args, kwargs)]

    def assert_fascicola_documento_not_called(self):
        assert self._calls[FASCICOLA_DOCUMENTO] == []


class InforTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return InforInsertResult(SimpleNamespace(esito='OK', segnatura=SimpleNamespace(numero=8790, anno=2021)))

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class D3TestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            UPLOAD: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def upload(self, *args, **kwargs):
        self._store_call(UPLOAD, args, kwargs)
        response = Response()
        response.status_code = 201
        response.headers = {
            'Location': '/dms/r/7ff67ade-4bbe-5aea-b370-19904efbd7cb/blob/chunk/'
            '2023-03-21_temp_master_file_test99_sgv_ea86e5db-5e27-',
        }
        return D3InsertResult(response)

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        response = Response()
        response.status_code = 200
        response._content = (
            b'{"items": [{"sourceProperties": '
            b'[{"key": "23", "value": "0000825"}, {"key": "24", "value": "2023-01-01"}]}]}'
        )
        return D3InsertResult(response)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_upload(self, *args, **kwargs):
        assert self._calls[UPLOAD] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class HypersicTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return HypersicInsertResult(
            dict(  # noqa
                _value_1=dict(  # noqa
                    _value_1=[
                        dict(  # noqa
                            protocollo=dict(  # noqa
                                codice=123,
                                numero_protocollo=456,
                                data_protocollo='10/10/2021',
                            ),
                        ),
                    ],
                ),
            ),
        )

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class TinnTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERIMENTO: [],
            PROTOCOLLAZIONE: [],
        }

    @property
    def service(self):
        return self

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def inserimento(self, *args, **kwargs):
        self._store_call(INSERIMENTO, args, kwargs)
        return TinnInsertResult(SimpleNamespace(IngDocID=17330, IngErrNumber=0, strErrString=None))

    def protocollazione(self, *args, **kwargs):
        self._store_call(PROTOCOLLAZIONE, args, kwargs)
        return TinnInsertResult(
            SimpleNamespace(
                IngNumPG=38,
                IngAnnoPG=2023,
                StrDataPG='03/08/2023',
                StrSezione='PROT',
                IngErrNumber=0,
                strErrString=None,
            ),
        )

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_inserimento(self, *args, **kwargs):
        assert self._calls[INSERIMENTO] == [(args, kwargs)]

    def assert_protocollazione(self, *args, **kwargs):
        assert self._calls[PROTOCOLLAZIONE] == [(args, kwargs)]

    def assert_timeout(self, seconds):
        calls = self._calls[DUNDER_CALL]
        assert len(calls) == 1
        _, kwargs = self._calls[DUNDER_CALL][0]
        transport = kwargs['transport']
        assert transport.load_timeout == seconds
        assert transport.operation_timeout == seconds


class SicrawebWSProtocolloDMTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return SicrawebWSProtocolloDMInsertResult(
            SimpleNamespace(
                IdDocumento='123456789',
                AnnoProtocollo='2021',
                NumeroProtocollo='12345',
                DataProtocollo='2023-10-04T10:58:20.441+0200',
                Messaggio='Success',
                Registri=None,
                Allegati={
                    'item': [
                        {
                            'Serial': '9449151',
                            'IDBase': '9449151',
                            'Versione': '0',
                        },
                        {
                            'Serial': '9449150',
                            'IDBase': '9449150',
                            'Versione': '0',
                        },
                    ],
                },
                Errore='',
            ),
        )

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]


class SiscomTestClient:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            INSERT: [],
            UPLOAD: [],
            COLLATE: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def insert(self, *args, **kwargs):
        self._store_call(INSERT, args, kwargs)
        return SiscomInsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'RegisterNewProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                ProtoID='1856',
                                ProtoData='14/11/2023',
                                ProtoAnno='2023',
                                ProtoNum='91',
                            ),
                        },
                    ],
                },
            },
            service='RegisterNewProto',
        )

    def upload(self, *args, **kwargs):
        self._store_call(UPLOAD, args, kwargs)
        return SiscomInsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'AppendFileToProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                IDDocFile='1738',
                            ),
                        },
                    ],
                },
            },
            service='AppendFileToProto',
        )

    def collate(self, *args, **kwargs):
        self._store_call(COLLATE, args, kwargs)
        return SiscomInsertResult(
            {
                'schema': None,
                '_value_1': {
                    '_value_1': [
                        {
                            'SetFascDigitaleToProto': SimpleNamespace(
                                CodErrore=None,
                                DescErrore=None,
                                CodRis='OK',
                                DescRis='OPERAZIONE EFFETTUATA',
                                IDDocFascDigit='168',
                            ),
                        },
                    ],
                },
            },
            service='SetFascDigitaleToProto',
        )

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_insert(self, *args, **kwargs):
        assert self._calls[INSERT] == [(args, kwargs)]

    def assert_upload(self, *args, **kwargs):
        assert self._calls[UPLOAD] == [(args, kwargs)]

    def assert_collate(self, *args, **kwargs):
        assert self._calls[COLLATE] == [(args, kwargs)]


class TestInboundTemplate:
    def __init__(self, config):
        pass

    def render(self, application):
        root = etree.Element('Segnatura')
        etree.SubElement(root, 'inbound')
        return etree.tostring(root)


class TestOutboundTemplate:
    def __init__(self, config):
        pass

    def render(self, application):
        root = etree.Element('Segnatura')
        etree.SubElement(root, 'outbound')
        return etree.tostring(root)


class TestRetryProducer:
    def __init__(self, send_error=None):
        self._send_error = send_error
        self._calls = {
            DUNDER_CALL: [],
            SEND: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def send(self, *args, **kwargs):
        self._store_call(SEND, args, kwargs)
        if self._send_error:
            raise self._send_error

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_send(self, *args, **kwargs):
        assert self._calls[SEND] == [(args, kwargs)]

    def assert_send_not_called(self):
        assert self._calls[SEND] == []


class TestKafkaConsumer:
    def __init__(self, records=None, iter_error=None):
        self._records = records or []
        self._iter_error = iter_error
        self._calls = {
            DUNDER_CALL: [],
            SUBSCRIBE: [],
            COMMIT: [],
            CLOSE: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def __iter__(self):
        if self._iter_error:
            raise self._iter_error
        return iter(self._records)

    def subscribe(self, *args, **kwargs):
        self._store_call(SUBSCRIBE, args, kwargs)

    def end_offsets(self, partitions):
        return dict.fromkeys(partitions, 42)

    def commit(self, *args, **kwargs):
        self._store_call(COMMIT, args, kwargs)

    def close(self, *args, **kwargs):
        self._store_call(CLOSE, args, kwargs)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_subscribe(self, *args, **kwargs):
        assert self._calls[SUBSCRIBE] == [(args, kwargs)]

    def assert_commit(self, *args, **kwargs):
        assert self._calls[COMMIT] == [(args, kwargs)]

    def assert_close(self, *args, **kwargs):
        assert self._calls[CLOSE] == [(args, kwargs)]


class TestKafkaProducer:
    def __init__(self):
        self._calls = {
            DUNDER_CALL: [],
            SEND: [],
            FLUSH: [],
            CLOSE: [],
        }

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def send(self, *args, **kwargs):
        self._store_call(SEND, args, kwargs)

    def flush(self, *args, **kwargs):
        self._store_call(FLUSH, args, kwargs)

    def close(self, *args, **kwargs):
        self._store_call(CLOSE, args, kwargs)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_send(self, *args, **kwargs):
        assert self._calls[SEND] == [(args, kwargs)]

    def assert_flush(self, *args, **kwargs):
        assert self._calls[FLUSH] == [(args, kwargs)]

    def assert_close(self, *args, **kwargs):
        assert self._calls[CLOSE] == [(args, kwargs)]


TestKafkaRecord = namedtuple('TestKafkaRecord', ['topic', 'partition', 'offset', 'value'])


class TestDeferredApplication:
    _move_call_counter = count()

    def __init__(self, move_error=None):
        self._move_error = move_error
        self._calls = {
            DUNDER_CALL: [],
            MOVE: [],
        }
        self.move_call_order = None

    def __call__(self, *args, **kwargs):
        self._store_call(DUNDER_CALL, args, kwargs)
        return self

    def move(self, *args, **kwargs):
        self._store_call(MOVE, args, kwargs)
        self.move_call_order = next(self._move_call_counter)
        if self._move_error:
            raise self._move_error

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_call(self, *args, **kwargs):
        assert self._calls[DUNDER_CALL] == [(args, kwargs)]

    def assert_not_called(self):
        assert self._calls[DUNDER_CALL] == []

    def assert_move(self, *args, **kwargs):
        assert self._calls[MOVE] == [(args, kwargs)]


class TestApplicationRelay:
    def __init__(self, next_ready_at=None, move_error=None):
        self._next_ready_at = next_ready_at or make_aware(datetime.max, timezone.utc)
        self._move_error = move_error
        self._calls = {
            SEND: [],
            COMMIT: [],
        }
        self.send_commit_sequence = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def move_ready_applications(self, topic):
        if self._move_error:
            raise self._move_error
        return self._next_ready_at

    def send(self, *args, **kwargs):
        self._store_call(SEND, args, kwargs)
        self.send_commit_sequence.append(SEND)

    def commit(self, *args, **kwargs):
        self._store_call(COMMIT, args, kwargs)
        self.send_commit_sequence.append(COMMIT)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_send(self, *args, **kwargs):
        assert self._calls[SEND] == [(args, kwargs)]

    def assert_send_not_called(self):
        assert self._calls[SEND] == []

    def assert_commit(self, *args, **kwargs):
        assert self._calls[COMMIT] == [(args, kwargs)]

    def assert_commit_not_called(self):
        assert self._calls[COMMIT] == []


class TestClock:
    @staticmethod
    def sleep(seconds):
        pass


class GravelGatewayTestClient:
    def __init__(self):
        self._calls = {
            INCR_RETRIES: [],
            SET_LATENCY: [],
            INCR_DEAD_LETTERED: [],
        }

    def incr_retries(self, *args, **kwargs):
        self._store_call(INCR_RETRIES, args, kwargs)

    def set_latency(self, *args, **kwargs):
        self._store_call(SET_LATENCY, args, kwargs)

    def incr_dead_lettered(self, *args, **kwargs):
        self._store_call(INCR_DEAD_LETTERED, args, kwargs)

    def _store_call(self, key, args, kwargs):
        call = (args, kwargs)
        self._calls[key].append(call)

    def assert_incr_retries(self, *args, **kwargs):
        assert self._calls[INCR_RETRIES] == [(args, kwargs)]

    def assert_incr_retries_not_called(self):
        assert self._calls[INCR_RETRIES] == []

    def assert_set_latency(self, *args, **kwargs):
        assert self._calls[SET_LATENCY] == [(args, kwargs)]

    def assert_set_latency_not_called(self):
        assert self._calls[SET_LATENCY] == []

    def assert_incr_dead_lettered(self, *args, **kwargs):
        assert self._calls[INCR_DEAD_LETTERED] == [(args, kwargs)]

    def assert_incr_dead_lettered_not_called(self):
        assert self._calls[INCR_DEAD_LETTERED] == []
