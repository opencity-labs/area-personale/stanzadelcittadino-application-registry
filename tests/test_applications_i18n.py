import os

from django.core.management import call_command
from django.urls import reverse
from django.utils import translation
from rest_framework import status
from rest_framework.test import APITestCase


class DatagraphServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Datagraph Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'codente': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codente',
                                        'max_length': 64,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Denominazione dell'amministrazione",
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Indirizzo email dell'amministrazione",
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Unità organizzativa dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Codice AOO dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'classification_institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione per la classificazione",
                                        'max_length': 32,
                                    },
                                    'classification_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice AOO per la classificazione',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice titolario',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Numero del fascicolo',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Anno del fascicolo',
                                        'max_length': 4,
                                    },
                                    'document_format': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Formato del documento',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('datagraph-service-list')


class MaggioliServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Maggioli Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'connection_string': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Stringa di connessione',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Denominazione dell'amministrazione",
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Indirizzo email dell'amministrazione",
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Unità organizzativa dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Codice AOO dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'sending_mode': {
                                        'type': 'choice',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Modalità invio',
                                        'choices': [
                                            {'value': 'Cartaceo', 'display_name': 'Cartaceo'},
                                            {'value': 'PEC', 'display_name': 'PEC'},
                                            {'value': 'Interoperabile', 'display_name': 'Interoperabile'},
                                            {'value': 'InterPRO', 'display_name': 'InterPRO'},
                                        ],
                                    },
                                    'sending_method': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Mezzo invio',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice titolario',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Numero del fascicolo',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Anno del fascicolo',
                                        'max_length': 4,
                                    },
                                    'document_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Tipo del documento',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('maggioli-service-list')


class HalleyServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'login_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL di login',
                                        'max_length': 200,
                                    },
                                    'registration_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL di protocollo',
                                        'max_length': 200,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Denominazione dell'amministrazione",
                                        'max_length': 1024,
                                    },
                                    'institution_aoo_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': "ID dell'AOO dell'amministrazione",
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'institution_office_ids': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "ID degli uffici dell'amministrazione",
                                        'child': {
                                            'type': 'integer',
                                            'required': True,
                                            'read_only': False,
                                            'label': "ID degli uffici dell'amministrazione",
                                            'min_value': 0,
                                            'max_value': 2147483647,
                                        },
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Categoria della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Classe della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sottoclasse della classificazione',
                                        'max_length': 16,
                                    },
                                    'folder_id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'ID del fascicolo',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('halley-service-list')


class HalleyCloudServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Halley Cloud Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'registration_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL di protocollo',
                                        'max_length': 200,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'mailbox': {
                                        'type': 'email',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Mailbox',
                                        'max_length': 254,
                                    },
                                    'institution_office_ids': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "ID degli uffici dell'amministrazione",
                                        'help_text': 'The list of office ids is provided to the institution by the '
                                        'protocol manager, if omitted the default office will be used',
                                        'child': {
                                            'type': 'integer',
                                            'required': True,
                                            'read_only': False,
                                            'label': "ID degli uffici dell'amministrazione",
                                            'min_value': 0,
                                            'max_value': 2147483647,
                                        },
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Categoria della classificazione',
                                        'help_text': 'The list of classifications is provided to the institution by '
                                        'the protocol manager, if omitted the default classification '
                                        'will be used',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classe della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sottoclasse della classificazione',
                                        'max_length': 16,
                                    },
                                    'folder_name': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder name',
                                        'help_text': 'The list of folder names is provided to the institution by the '
                                        'protocol manager, if omitted the default folder will be used',
                                        'max_length': 64,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('halley-cloud-service-list')


class DedagroupServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Dedagroup Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'token_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL del token',
                                        'max_length': 200,
                                    },
                                    'base_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL base',
                                        'max_length': 200,
                                    },
                                    'client_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'ID del client',
                                        'max_length': 64,
                                    },
                                    'client_secret': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Segreto del client',
                                        'max_length': 64,
                                    },
                                    'grant_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Tipo di grant',
                                        'max_length': 64,
                                    },
                                    'resource': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Risorsa',
                                        'max_length': 64,
                                    },
                                    'registry_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'ID del registro',
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'author_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': "ID dell'autore",
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'organization_chart_level_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codice livello organigramma',
                                        'max_length': 64,
                                    },
                                    'institution_aoo_id': {
                                        'type': 'integer',
                                        'required': True,
                                        'read_only': False,
                                        'label': "ID dell'AOO dell'amministrazione",
                                        'min_value': 0,
                                        'max_value': 2147483647,
                                    },
                                    'classification_title': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Titolo della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classe della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_subclass': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Sottoclasse della classificazione',
                                        'max_length': 16,
                                    },
                                    'folder_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice del fascicolo',
                                        'max_length': 64,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('dedagroup-service-list')


class InsielServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Insiel Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {'type': 'string', 'required': True, 'read_only': False, 'label': 'ID SDC'},
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'login_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Login URL',
                                        'max_length': 200,
                                    },
                                    'registry_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Protocol Web service URL',
                                        'max_length': 200,
                                    },
                                    'file_transfer_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'File transfer Web service URL',
                                        'max_length': 200,
                                    },
                                    'client_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client id',
                                        'max_length': 32,
                                    },
                                    'client_secret': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Client secret',
                                        'max_length': 32,
                                    },
                                    'grant_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Grant type',
                                        'max_length': 32,
                                    },
                                    'user_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "User's code",
                                        'max_length': 32,
                                    },
                                    'user_password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "User's password",
                                        'max_length': 32,
                                    },
                                    'operating_office_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Operating office's code",
                                        'max_length': 32,
                                    },
                                    'office_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Office's code",
                                        'max_length': 32,
                                    },
                                    'office_registry': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Office's registry",
                                        'max_length': 32,
                                    },
                                    'internal_offices_codes': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Internal office's codes",
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': "Internal office's code",
                                            'max_length': 32,
                                        },
                                    },
                                    'internal_offices_types': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Internal office's types",
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': "Internal office's type",
                                            'max_length': 32,
                                        },
                                    },
                                    'register_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register code',
                                        'help_text': "Right aligned levels: ex: '1  2  3'. This field is required "
                                        'if prog_doc and prog_movi are not defined.',
                                        'max_length': 32,
                                    },
                                    'prog_doc': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register prog doc',
                                        'help_text': 'This field is required if prog_movi is defined',
                                        'max_length': 32,
                                    },
                                    'prog_movi': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Register prog movi',
                                        'help_text': 'This field is required if prog_doc is defined',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {'type': 'datetime', 'required': False, 'read_only': True, 'label': 'Creato il'},
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('insiel-service-list')


class PitreServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Pitre Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {'type': 'string', 'required': True, 'read_only': False, 'label': 'ID SDC'},
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {'type': 'integer', 'required': False, 'read_only': True, 'label': 'ID'},
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'base_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL base',
                                        'max_length': 200,
                                    },
                                    'certificate': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Certificate',
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 32,
                                    },
                                    'code_role': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code role',
                                        'max_length': 32,
                                    },
                                    'code_application': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code application',
                                        'max_length': 32,
                                    },
                                    'code_adm': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code ADM',
                                        'max_length': 32,
                                    },
                                    'code_node_classification': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code node classification',
                                        'max_length': 8,
                                    },
                                    'transmission_reason': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Transmission reason',
                                        'max_length': 128,
                                    },
                                    'transmission_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Transmission_type',
                                        'max_length': 128,
                                    },
                                    'receiver_codes': {
                                        'type': 'list',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Transmission receiver codes',
                                        'help_text': 'List of role codes to transmit the creation of the project '
                                        'or document to',
                                        'child': {
                                            'type': 'string',
                                            'required': True,
                                            'read_only': False,
                                            'label': 'Transmission receiver code',
                                            'max_length': 32,
                                        },
                                    },
                                    'recipient_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Recipient code',
                                        'help_text': 'Recipient role code for inbound documents, sender for outbound '
                                        'documents. If not set the role used for integration will be used',
                                        'max_length': 32,
                                    },
                                    'code_register': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Code register',
                                        'max_length': 32,
                                    },
                                    'means_of_sending': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Means of sending',
                                        'max_length': 128,
                                    },
                                    'code_rf': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Code RF',
                                        'max_length': 8,
                                    },
                                    'note': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Note',
                                        'help_text': 'Outbound document note',
                                        'max_length': 256,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {'type': 'datetime', 'required': False, 'read_only': True, 'label': 'Creato il'},
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('pitre-service-list')


class InforServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Infor Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'auth_username': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Auth username',
                                        'max_length': 64,
                                    },
                                    'auth_password': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Auth password',
                                        'max_length': 64,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 64,
                                    },
                                    'denomination': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Denomination',
                                        'max_length': 128,
                                    },
                                    'email': {
                                        'type': 'email',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Email',
                                        'max_length': 254,
                                    },
                                    'wsdl_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Protocol WSDL URL',
                                        'max_length': 200,
                                    },
                                    'registry_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Protocol Web service URL',
                                        'max_length': 200,
                                    },
                                    'incoming_document_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Incoming document type',
                                        'max_length': 64,
                                    },
                                    'incoming_intermediary': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Incoming intermediary',
                                        'max_length': 64,
                                    },
                                    'incoming_sorting': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Incoming sorting',
                                        'max_length': 64,
                                    },
                                    'incoming_classification': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Incoming classification',
                                        'max_length': 16,
                                    },
                                    'incoming_folder': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Incoming folder',
                                        'max_length': 64,
                                    },
                                    'response_internal_sender': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response internal sender',
                                        'max_length': 254,
                                    },
                                    'response_document_type': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response document type',
                                        'max_length': 64,
                                    },
                                    'response_intermediary': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response intermediary',
                                        'max_length': 64,
                                    },
                                    'response_sorting': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response sorting',
                                        'max_length': 64,
                                    },
                                    'response_classification': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response classification',
                                        'max_length': 16,
                                    },
                                    'response_folder': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Response folder',
                                        'max_length': 64,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('infor-service-list')


class D3ServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'D3 Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 32,
                                    },
                                    'registry_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Registry URL',
                                        'max_length': 200,
                                    },
                                    'repository_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Repository ID',
                                        'max_length': 64,
                                    },
                                    'main_source_category': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Main Source Category',
                                        'max_length': 64,
                                    },
                                    'register_attachments': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Include attachments in registration',
                                    },
                                    'attachment_source_category': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Attachment Source Category',
                                        'max_length': 64,
                                    },
                                    'document_subtype': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Document Subtype',
                                        'max_length': 128,
                                    },
                                    'classification': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification',
                                        'max_length': 128,
                                    },
                                    'folder_name': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder name',
                                        'help_text': 'The list of folder names is provided to the institution by the '
                                        'protocol manager, if omitted the default folder will be used',
                                        'max_length': 64,
                                    },
                                    'competent_service': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Competent service',
                                        'max_length': 254,
                                    },
                                    'visibility': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Visibility',
                                        'max_length': 254,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('d3-service-list')


class HypersicServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Hypersic Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Username',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'registry_url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Protocol Web service URL',
                                        'max_length': 200,
                                    },
                                    'office_ids': {
                                        'type': 'list',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Office ids',
                                        'help_text': 'The list of office ids is provided to the institution by '
                                        'the protocol manager',
                                        'child': {
                                            'type': 'integer',
                                            'required': True,
                                            'read_only': False,
                                            'label': 'Office ids',
                                            'min_value': 0,
                                            'max_value': 2147483647,
                                        },
                                    },
                                    'classification': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classification',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder number',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Folder year',
                                        'max_length': 16,
                                    },
                                    'annotation': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Registration annotation',
                                        'max_length': 128,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('hypersic-service-list')


class TinnServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Tinn Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'codente': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codente',
                                        'max_length': 64,
                                    },
                                    'username': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Nome utente',
                                        'max_length': 64,
                                    },
                                    'password': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Password',
                                        'max_length': 64,
                                    },
                                    'institution_name': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Denominazione dell'amministrazione",
                                        'max_length': 1024,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_email': {
                                        'type': 'email',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Indirizzo email dell'amministrazione",
                                        'max_length': 254,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Unità organizzativa dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Codice AOO dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'classification_institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione per la classificazione",
                                        'max_length': 32,
                                    },
                                    'classification_aoo_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice AOO per la classificazione',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codice titolario',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Numero del fascicolo',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Anno del fascicolo',
                                        'max_length': 4,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('tinn-service-list')


class SicrawebWSProtocolloDMServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Sicraweb Ws Protocollo Dm Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'connection_string': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Stringa di connessione',
                                        'max_length': 64,
                                    },
                                    'institution_code': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Codice dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_ou': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': "Unità organizzativa dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'institution_aoo_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': "Codice AOO dell'amministrazione",
                                        'max_length': 32,
                                    },
                                    'sending_mode': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Modalità invio',
                                        'max_length': 32,
                                    },
                                    'internal_sender': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Internal sender',
                                        'max_length': 32,
                                    },
                                    'update_records': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Update records',
                                        'help_text': 'One of: N, S or F',
                                        'max_length': 32,
                                    },
                                    'classification_hierarchy': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Codice titolario',
                                        'max_length': 16,
                                    },
                                    'folder_number': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Numero del fascicolo',
                                        'max_length': 16,
                                    },
                                    'folder_year': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Anno del fascicolo',
                                        'max_length': 4,
                                    },
                                    'connection_user': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Connection user',
                                        'max_length': 32,
                                    },
                                    'connection_user_role': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Connection user role',
                                        'max_length': 32,
                                    },
                                    'standard_subject': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Standard subject',
                                        'max_length': 32,
                                    },
                                    'document_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Tipo del documento',
                                        'max_length': 32,
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('sicraweb-wsprotocollodm-service-list')


class SiscomServiceI18NAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command('compilemessages', ignore=['.*'], verbosity=0)

    def setUp(self):
        api_key = 'xyz'
        os.environ['REGISTRY_API_KEY'] = api_key
        self.client.credentials(HTTP_X_REGISTRY_API_KEY=api_key)

    def tearDown(self):
        del os.environ['REGISTRY_API_KEY']
        self.client.credentials()
        translation.deactivate()

    def test_metadata_it(self):
        response = self.client.options(self._url(), HTTP_ACCEPT_LANGUAGE='it')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            {
                'name': 'Siscom Service List',
                'description': '',
                'renders': ['application/json'],
                'parses': ['application/json'],
                'actions': {
                    'POST': {
                        'id': {
                            'type': 'integer',
                            'required': False,
                            'read_only': True,
                            'label': 'ID',
                        },
                        'description': {
                            'type': 'string',
                            'required': False,
                            'read_only': False,
                            'label': 'Descrizione',
                            'max_length': 128,
                        },
                        'tenant_sdc_id': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC del tenant',
                        },
                        'sdc_id': {
                            'type': 'string',
                            'required': True,
                            'read_only': False,
                            'label': 'ID SDC',
                        },
                        'application_states': {
                            'type': 'field',
                            'required': False,
                            'read_only': False,
                            'label': 'Stati della pratica',
                        },
                        'configs': {
                            'type': 'field',
                            'required': True,
                            'read_only': False,
                            'label': 'Configurazioni',
                            'child': {
                                'type': 'nested object',
                                'required': True,
                                'read_only': False,
                                'children': {
                                    'id': {
                                        'type': 'integer',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'ID',
                                    },
                                    'description': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Descrizione',
                                        'max_length': 128,
                                    },
                                    'is_active': {
                                        'type': 'boolean',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Attiva',
                                    },
                                    'url': {
                                        'type': 'url',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'URL',
                                        'max_length': 200,
                                    },
                                    'license_code': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Codice licenza',
                                        'max_length': 16,
                                    },
                                    'operator_denomination': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Operator denomination',
                                        'max_length': 128,
                                    },
                                    'classification_category': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Categoria della classificazione',
                                        'max_length': 16,
                                    },
                                    'classification_class': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Classe della classificazione',
                                        'max_length': 16,
                                    },
                                    'folder_id': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'ID del fascicolo',
                                        'max_length': 16,
                                    },
                                    'mail_type': {
                                        'type': 'string',
                                        'required': False,
                                        'read_only': False,
                                        'label': 'Mail type',
                                        'max_length': 16,
                                    },
                                    'office_id': {
                                        'type': 'string',
                                        'required': True,
                                        'read_only': False,
                                        'label': 'Office id',
                                        'max_length': 16,
                                        'help_text': 'The office id is provided to '
                                        'the institution by the protocol manager',
                                    },
                                    'created_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Creato il',
                                    },
                                    'modified_at': {
                                        'type': 'datetime',
                                        'required': False,
                                        'read_only': True,
                                        'label': 'Modificato il',
                                    },
                                },
                            },
                        },
                        'created_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Creato il',
                        },
                        'modified_at': {
                            'type': 'datetime',
                            'required': False,
                            'read_only': True,
                            'label': 'Modificato il',
                        },
                    },
                },
            },
        )

    @staticmethod
    def _url():
        return reverse('siscom-service-list')
