from datetime import datetime, timezone

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.dedagroup.rest import ProtocollazioneResult
from apps.workers.errors import RESTValidationError

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class ProtocollazioneResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(RESTValidationError):
            ProtocollazioneResult({'resultType': 123, 'resultDescription': 'error'})

    @freeze_time(NOW)
    def test_success(self):
        result = ProtocollazioneResult(
            {
                'resultType': 1,
                'result': {
                    'numeroProtocollo': 376,
                    'dataRegistrazione': '2021-05-25T10:20:30.123',
                    'allegati': [
                        {'id': 123},
                        {'id': 124},
                    ],
                    'idPratica': 567,
                },
            },
        )
        self.assertEqual(result.number, '376')
        self.assertEqual(result.date, '2021-05-25T10:20:30.123')
        self.assertEqual(result.remote_document_ids, [123, 124])
        self.assertEqual(result.remote_application_id, 567)
        self.assertEqual(result.created_at, NOW)
