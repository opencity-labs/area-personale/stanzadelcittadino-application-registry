from contextlib import contextmanager
from datetime import datetime, timezone
from unittest import mock

import requests
import responses
from django.test import SimpleTestCase
from freezegun import freeze_time
from lxml import etree
from requests.exceptions import ConnectionError, RequestException, SSLError, Timeout  # noqa
from zeep.cache import InMemoryCache

from apps.workers.errors import SOAPError, TransportError, ValidationError
from apps.workers.maggioli import soap

from ._helpers import BASE_URL, INSERT_DOCUMENTO_RESULT_XML, REGISTRA_PROTOCOLLO_RESULT_XML, MaggioliSOAPTestClient

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


@contextmanager
def _stub_soap_client_class(stub):
    original, soap.Client.soap_client_class = soap.Client.soap_client_class, stub
    try:
        yield
    finally:
        soap.Client.soap_client_class = original


def _create_client():
    return soap.Client(connection_string='test_conn_string', url=BASE_URL)


class WSDLCacheTestCaseMixin:
    def test_shared_wsdl_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, f'{BASE_URL}/?wsdl', body=b'abc', status=200)

            for _ in range(2):
                client = _create_client()
                try:
                    self.invoke(client)
                except SOAPError:
                    pass

            self.assertEqual(len(rsps.calls), 1)
            InMemoryCache._cache = {}


class SOAPOperationTestCaseMixin:
    def test_soap_client_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = MaggioliSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                self.invoke(client)
                self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)

    def test_soap_client_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = MaggioliSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                self.invoke(client)

            stub.assert_timeout(60)

    def test_http_client_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            client = _create_client()

            with mock.patch.object(requests, 'post', wraps=requests.post) as mocked_post:
                with _stub_soap_client_class(MaggioliSOAPTestClient()):
                    self.invoke(client)

                self.assertEqual(mocked_post.call_args.kwargs['timeout'], 60)

    def test_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, BASE_URL, body=error())
                client = _create_client()

                with _stub_soap_client_class(MaggioliSOAPTestClient()):
                    with self.assertRaises(TransportError):
                        self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_response_status_failure(self):
        for code in [401, 500]:
            with self.subTest(code=code), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, BASE_URL, body=b'abc', status=code)
                client = _create_client()

                with _stub_soap_client_class(MaggioliSOAPTestClient()):
                    with self.assertRaises(TransportError):
                        self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_response_body_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=b'<root/>', status=200)
            client = _create_client()

            with _stub_soap_client_class(MaggioliSOAPTestClient()):
                with self.assertRaises(ValidationError):
                    self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)

    def test_unexpected_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=Exception())
            client = _create_client()

            with _stub_soap_client_class(MaggioliSOAPTestClient()):
                with self.assertRaises(SOAPError):
                    self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)


class ClientInsertDocumentoTestCase(WSDLCacheTestCaseMixin, SOAPOperationTestCaseMixin, SimpleTestCase):
    result_xml = INSERT_DOCUMENTO_RESULT_XML

    def invoke(self, client):
        return client.insert_documento('foobar.pdf', 'application/pdf', b'abc')

    def test_success(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = MaggioliSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                result = self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)
            stub.assert_create_message(
                stub.service,
                'insertDocumento',
                connectionString='test_conn_string',
                documentName='foobar.pdf',
                documentDescription='Documento ricevuto da Stanza del cittadino',
            )

            self.assertEqual(result.doc_id, '516187')

            self.assertEqual(len(rsps.calls), 1)
            self.assertEqual(rsps.calls[0].request.headers['Content-Type'], 'application/dime')
            self.assertEqual(rsps.calls[0].request.headers['SOAPAction'], '""')
            self.assertEqual(
                rsps.calls[0].request.body.hex()[:128],
                '0c200000000000290000002e687474703a2f2f736368656d61732e786d6c736f61702e6f72672f736f61702f656e76656c6f7'
                '0652f0000003c3f786d6c207665',
            )


class ClientRegistraProtocolloTestCase(WSDLCacheTestCaseMixin, SOAPOperationTestCaseMixin, SimpleTestCase):
    result_xml = REGISTRA_PROTOCOLLO_RESULT_XML

    def invoke(self, client):
        return client.registra_protocollo(b'abc')

    def test_success(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, BASE_URL, body=self.result_xml.encode('utf8'), status=200)
            stub = MaggioliSOAPTestClient()
            client = _create_client()

            with _stub_soap_client_class(stub):
                result = self.invoke(client)

            stub.assert_call(wsdl='https://example.local?wsdl', transport=mock.ANY)
            stub.assert_create_message(
                stub.service,
                'registraProtocollo',
                connectionString='test_conn_string',
            )

            self.assertEqual(result.number, '84')
            self.assertEqual(result.year, '2021')

            self.assertEqual(len(rsps.calls), 1)
            self.assertEqual(rsps.calls[0].request.headers['Content-Type'], 'application/dime')
            self.assertEqual(rsps.calls[0].request.headers['SOAPAction'], '""')
            self.assertEqual(
                rsps.calls[0].request.body.hex()[:128],
                '0c200000000000290000002e687474703a2f2f736368656d61732e786d6c736f61702e6f72672f736f61702f656e76656c6f7'
                '0652f0000003c3f786d6c207665',
            )


class ResultTestCaseMixin:
    XMLNS_PREFIX_MAP = {'soap': 'http://schemas.xmlsoap.org/soap/envelope/'}

    def test_invalid_xml(self):
        with self.assertRaises(ValidationError):
            self.create_result('invalid-xml')

    def test_error_code_failure(self):
        errcode_xpath = './soap:Body/multiRef[@id="id0"]/lngErrNumber'

        for xpath in [
            errcode_xpath,
            self.errcode_multiref_xpath,
        ]:
            with self.subTest(xpath=xpath):
                root = self.get_root()
                self.remove(root, xpath)
                with self.assertRaises(ValidationError):
                    self.create_result(self.to_string(root))

        for errcode in [
            '',
            '-1',
        ]:
            with self.subTest(errcode=errcode):
                root = self.get_root()
                self.set_text(root, self.errcode_multiref_xpath, errcode)
                with self.assertRaises(ValidationError):
                    self.create_result(self.to_string(root))

    def get_root(self):
        return etree.fromstring(self.result_xml.encode('utf8'))

    def remove(self, root, xpath):
        element = root.xpath(xpath, namespaces=self.XMLNS_PREFIX_MAP)[0]
        element.getparent().remove(element)

    def set_text(self, root, xpath, text):
        element = root.xpath(xpath, namespaces=self.XMLNS_PREFIX_MAP)[0]
        element.text = text

    @staticmethod
    def to_string(element):
        return etree.tostring(element, encoding='UTF-8', xml_declaration=True).decode('utf8')


class InsertDocumentoResultTestCase(ResultTestCaseMixin, SimpleTestCase):
    result_xml = INSERT_DOCUMENTO_RESULT_XML
    errcode_multiref_xpath = './soap:Body/multiRef[@id="id2"]'

    def create_result(self, xml=None):
        xml = xml or self.result_xml
        return soap.InsertDocumentoResult(xml.encode('utf8'))

    def test_doc_id_failure(self):
        doc_id_xpath = './soap:Body/multiRef[@id="id0"]/lngDocID'
        doc_id_multiref_xpath = './soap:Body/multiRef[@id="id1"]'

        for xpath in [
            doc_id_xpath,
            doc_id_multiref_xpath,
        ]:
            with self.subTest(xpath=xpath):
                root = self.get_root()
                self.remove(root, xpath)
                with self.assertRaises(ValidationError):
                    self.create_result(self.to_string(root))

        with self.subTest(doc_id=''):
            root = self.get_root()
            self.set_text(root, doc_id_multiref_xpath, '')
            with self.assertRaises(ValidationError):
                self.create_result(self.to_string(root))

    @freeze_time(NOW)
    def test_success(self):
        result = self.create_result()
        self.assertEqual(result.doc_id, '516187')
        self.assertEqual(result.created_at, NOW)


class RegistraProtocolloResultTestCase(ResultTestCaseMixin, SimpleTestCase):
    result_xml = REGISTRA_PROTOCOLLO_RESULT_XML
    errcode_multiref_xpath = './soap:Body/multiRef[@id="id3"]'

    def create_result(self, xml=None):
        xml = xml or self.result_xml
        return soap.RegistraProtocolloResult(xml.encode('utf8'))

    def test_number_failure(self):
        number_xpath = './soap:Body/multiRef[@id="id0"]/lngNumPG'
        number_multiref_xpath = './soap:Body/multiRef[@id="id1"]'

        for xpath in [
            number_xpath,
            number_multiref_xpath,
        ]:
            with self.subTest(xpath=xpath):
                root = self.get_root()
                self.remove(root, xpath)
                with self.assertRaises(ValidationError):
                    self.create_result(self.to_string(root))

        with self.subTest(number=''):
            root = self.get_root()
            self.set_text(root, number_multiref_xpath, '')
            with self.assertRaises(ValidationError):
                self.create_result(self.to_string(root))

    def test_year_failure(self):
        year_xpath = './soap:Body/multiRef[@id="id0"]/lngAnnoPG'
        year_multiref_xpath = './soap:Body/multiRef[@id="id2"]'

        for xpath in [
            year_xpath,
            year_multiref_xpath,
        ]:
            with self.subTest(xpath=xpath):
                root = self.get_root()
                self.remove(root, xpath)
                with self.assertRaises(ValidationError):
                    self.create_result(self.to_string(root))

        with self.subTest(year=''):
            root = self.get_root()
            self.set_text(root, year_multiref_xpath, '')
            with self.assertRaises(ValidationError):
                self.create_result(self.to_string(root))

    @freeze_time(NOW)
    def test_success(self):
        result = self.create_result()
        self.assertEqual(result.number, '84')
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.created_at, NOW)
