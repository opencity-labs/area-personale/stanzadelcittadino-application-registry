import yaml
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class SchemaAPITestCase(APITestCase):
    def test_schema(self):
        path = settings.BASE_DIR / 'tests' / 'test_schema.yaml'
        with open(path, 'r') as f:
            reference = yaml.safe_load(f)

        response = self.client.get(self._url())
        response_content = yaml.safe_load(response.content.decode())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reference, response_content)

    @staticmethod
    def _url():
        return reverse('schema')
