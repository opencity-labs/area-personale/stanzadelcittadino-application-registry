from copy import deepcopy
from dataclasses import fields
from datetime import datetime, timedelta, timezone
from types import SimpleNamespace
from unittest import mock

import requests
import responses
from dateutil import tz
from django.db import transaction
from django.test import SimpleTestCase, TestCase
from freezegun import freeze_time
from requests.exceptions import ConnectionError, RequestException, SSLError, Timeout  # noqa
from responses.matchers import json_params_matcher

from apps.applications.models import ApplicationState
from apps.applications.sdc.client import Client, Credentials
from apps.applications.sdc.entities import Application
from apps.applications.sdc.errors import SDCError, TransportError, ValidationError

from ._helpers import BASE_URL, RETRY_DATA_VALUE, get_application_data, get_application_state_data

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


def _create_client():
    return Client(credentials=Credentials(username='user01', password='s3cr3t'), base_url=BASE_URL)


class ApplicationTestCase(TestCase):
    APPLICATION = 'application'
    METADATA = 'metadata'
    APPLICANT = 'applicant'
    COMPILED_MODULE = 'compiled_module'
    ATTACHMENTS = 'attachments'
    OUTCOME_FILE = 'outcome_file'
    OUTCOME_ATTACHMENTS = 'outcome_attachments'
    INTEGRATIONS = 'integrations'
    OUTBOUND_INTEGRATION_FILE = 'outbound_integration_file'
    OUTBOUND_INTEGRATION_ATTACHMENTS = 'outbound_integration_attachments'
    INBOUND_INTEGRATION_FILE = 'inbound_integration_file'
    INBOUND_INTEGRATION_ATTACHMENTS = 'inbound_integration_attachments'
    INBOUND_WITHDRAW_FILE = 'inbound_withdraw_file'

    def test_field_names(self):
        application_fields = [
            'raw',
            'metadata',
            'id',
            'tenant_id',
            'service_name',
            'service_id',
            'user',
            'event_id',
            'event_version',
            'status_name',
            'submitted_at',
            'subject',
            'flow_changed_at',
            'applicant',
            'compiled_module',
            'attachments',
            'outcome_file',
            'outcome_attachments',
            'outbound_integration_file',
            'outbound_integration_attachments',
            'inbound_integration_file',
            'inbound_integration_attachments',
            'inbound_withdraw_file',
        ]
        metadata_fields = [
            'attempts',
            'retry_at',
            'retry_topic',
        ]
        applicant_fields = [
            'name',
            'surname',
            'fiscal_code',
            'address',
            'house_number',
            'house_number_digits',
            'house_number_letters',
            'postal_code',
            'municipality',
            'county',
            'email',
        ]
        document_fields = [
            'url',
            'original_name',
            'description',
            'mimetype',
            'attributes',
        ]

        application = Application(get_application_data())

        with self.subTest(self.APPLICATION):
            self.assertEqual([field.name for field in fields(application)], application_fields)
        with self.subTest(self.METADATA):
            self.assertEqual([field.name for field in fields(application.metadata)], metadata_fields)
        with self.subTest(self.APPLICANT):
            self.assertEqual([field.name for field in fields(application.applicant)], applicant_fields)
        with self.subTest(self.COMPILED_MODULE):
            self.assertEqual([field.name for field in fields(application.compiled_module)], document_fields)
        with self.subTest(self.ATTACHMENTS):
            self.assertEqual([field.name for field in fields(application.attachments[0])], document_fields)
        with self.subTest(self.OUTCOME_FILE):
            self.assertEqual([field.name for field in fields(application.outcome_file)], document_fields)
        with self.subTest(self.OUTCOME_ATTACHMENTS):
            self.assertEqual([field.name for field in fields(application.outcome_attachments[0])], document_fields)
        with self.subTest(self.OUTBOUND_INTEGRATION_FILE):
            self.assertEqual([field.name for field in fields(application.outbound_integration_file)], document_fields)
        with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS):
            self.assertEqual(
                [field.name for field in fields(application.outbound_integration_attachments[0])],
                document_fields,
            )
        with self.subTest(self.INBOUND_INTEGRATION_FILE):
            self.assertEqual([field.name for field in fields(application.inbound_integration_file)], document_fields)
        with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS):
            self.assertEqual(
                [field.name for field in fields(application.inbound_integration_attachments[0])],
                document_fields,
            )
        with self.subTest(self.INBOUND_WITHDRAW_FILE):
            self.assertEqual([field.name for field in fields(application.inbound_withdraw_file)], document_fields)

    def test_field_values(self):
        data = get_application_data()
        application = Application(data)

        with self.subTest(self.APPLICATION):
            self.assertEqual(application.raw, data)
            self.assertEqual(application.id, 'b8a44017-6db1-4abb-af6c-c95f34f08af6')
            self.assertEqual(application.tenant_id, 'f0620f93-c2a8-4ebd-8e64-67ef125177e5')
            self.assertEqual(application.service_name, 'example service')
            self.assertEqual(application.service_id, '74dcab79-790e-459e-bbe6-d5972b298b42')
            self.assertEqual(application.user, '43f619df-3ba2-4d9d-bcdd-270484eac44d')
            self.assertEqual(application.event_id, 'b751510d-c5e4-4fe9-a8cf-f517f90e68b5')
            self.assertEqual(application.event_version, '1.0')
            self.assertEqual(application.status_name, 'example_state')
            self.assertEqual(
                application.submitted_at,
                datetime(2021, 5, 18, 15, 7, 16, tzinfo=tz.gettz('Europe/Rome')),
            )
            self.assertEqual(
                application.flow_changed_at,
                datetime(2021, 5, 18, 16, 17, 18, tzinfo=tz.gettz('Europe/Rome')),
            )

        metadata = application.metadata
        with self.subTest(self.METADATA):
            self.assertEqual(metadata.attempts, 5)
            self.assertEqual(metadata.retry_at, '2021-11-30T09:46:08.403371+00:00')
            self.assertEqual(metadata.retry_topic, 'retry_30_test')

        applicant = application.applicant
        with self.subTest(self.APPLICANT):
            self.assertEqual(applicant.name, 'Mario')
            self.assertEqual(applicant.surname, 'Rossi')
            self.assertEqual(applicant.fiscal_code, 'MRARSS80A01F205W')
            self.assertEqual(applicant.address, 'Corso Buenos Aires')
            self.assertEqual(applicant.house_number, '42/A')
            self.assertEqual(applicant.house_number_digits, '42')
            self.assertEqual(applicant.house_number_letters, 'A')
            self.assertEqual(applicant.postal_code, '20124')
            self.assertEqual(applicant.municipality, 'Milano')
            self.assertEqual(applicant.county, 'MI')
            self.assertEqual(applicant.email, 'rossi@example.local')

        compiled_module = application.compiled_module
        with self.subTest(self.COMPILED_MODULE):
            self.assertEqual(compiled_module.url, 'https://example.local/inbound-document/')
            self.assertEqual(compiled_module.original_name, 'inbound_document.pdf')
            self.assertEqual(compiled_module.description, 'Inbound document')
            self.assertEqual(compiled_module.mimetype, 'application/pdf')
            self.assertIsNone(compiled_module.attributes)

        [attachment] = application.attachments
        with self.subTest(self.ATTACHMENTS):
            self.assertEqual(attachment.url, 'https://example.local/inbound-attachment/')
            self.assertEqual(attachment.original_name, 'inbound_attachment.doc')
            self.assertEqual(attachment.description, 'Inbound attachment')
            self.assertEqual(attachment.mimetype, 'application/msword')
            self.assertIsNone(attachment.attributes)

        outcome_file = application.outcome_file
        with self.subTest(self.OUTCOME_FILE):
            self.assertEqual(outcome_file.url, 'https://example.local/outbound-document/')
            self.assertEqual(outcome_file.original_name, 'outbound_document.pdf')
            self.assertEqual(outcome_file.description, 'Outbound document')
            self.assertEqual(outcome_file.mimetype, 'application/pdf')
            self.assertIsNone(outcome_file.attributes)

        [outcome_attachment] = application.outcome_attachments
        with self.subTest(self.OUTCOME_ATTACHMENTS):
            self.assertEqual(outcome_attachment.url, 'https://example.local/outbound-attachment/')
            self.assertEqual(outcome_attachment.original_name, 'outbound_attachment.doc')
            self.assertEqual(outcome_attachment.description, 'Outbound attachment')
            self.assertEqual(outcome_attachment.mimetype, 'application/msword')
            self.assertIsNone(outcome_attachment.attributes)

        outbound_integration_file = application.outbound_integration_file
        with self.subTest(self.OUTBOUND_INTEGRATION_FILE):
            self.assertEqual(outbound_integration_file.url, 'https://example.local/outbound-integration-document/')
            self.assertEqual(outbound_integration_file.original_name, 'outbound_integration_document.pdf')
            self.assertEqual(outbound_integration_file.description, 'Outbound integration document')
            self.assertEqual(outbound_integration_file.mimetype, 'application/pdf')
            self.assertIsNone(outbound_integration_file.attributes)

        [outbound_integration_attachment] = application.outbound_integration_attachments
        with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS):
            self.assertEqual(
                outbound_integration_attachment.url,
                'https://example.local/outbound-integration-attachment/',
            )
            self.assertEqual(outbound_integration_attachment.original_name, 'outbound_integration_attachment.doc')
            self.assertEqual(outbound_integration_attachment.description, 'Outbound integration attachment')
            self.assertEqual(outbound_integration_attachment.mimetype, 'application/msword')
            self.assertIsNone(outbound_integration_attachment.attributes)

        inbound_integration_file = application.inbound_integration_file
        with self.subTest(self.INBOUND_INTEGRATION_FILE):
            self.assertEqual(inbound_integration_file.url, 'https://example.local/inbound-integration-document/')
            self.assertEqual(inbound_integration_file.original_name, 'inbound_integration_document.pdf')
            self.assertEqual(inbound_integration_file.description, 'Inbound integration document')
            self.assertEqual(inbound_integration_file.mimetype, 'application/pdf')
            self.assertIsNone(inbound_integration_file.attributes)

        [inbound_integration_attachment] = application.inbound_integration_attachments
        with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS):
            self.assertEqual(
                inbound_integration_attachment.url,
                'https://example.local/inbound-integration-attachment/',
            )
            self.assertEqual(inbound_integration_attachment.original_name, 'inbound_integration_attachment.doc')
            self.assertEqual(inbound_integration_attachment.description, 'Inbound integration attachment')
            self.assertEqual(inbound_integration_attachment.mimetype, 'application/msword')
            self.assertIsNone(inbound_integration_attachment.attributes)

        inbound_withdraw_file = application.inbound_withdraw_file
        with self.subTest(self.INBOUND_WITHDRAW_FILE):
            self.assertEqual(
                inbound_withdraw_file.url,
                'https://example.local/inbound-attachment/',
            )
            self.assertEqual(inbound_withdraw_file.original_name, 'inbound_attachment.doc')
            self.assertEqual(inbound_withdraw_file.description, 'Inbound attachment')
            self.assertEqual(inbound_withdraw_file.mimetype, 'application/msword')
            self.assertIsNone(inbound_withdraw_file.attributes)

    def test_required_fields(self):
        application_fields = [
            'id',
            'tenant',
            'service_name',
            'service_id',
            'user',
            'event_id',
            'event_version',
            'status_name',
            'submitted_at',
            'data',
            'compiled_modules',
            'attachments',
            'outcome_attachments',
        ]
        metadata_fields = [
            'attempts',
            'retry_at',
            'retry_topic',
        ]
        document_fields = ['url', 'originalName', 'description']
        integration_fields = ['outbound', 'inbound']

        for key in application_fields:
            with self.subTest(self.APPLICATION, key=key):
                data = get_application_data()
                data.pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

        for key in metadata_fields:
            with self.subTest(self.METADATA, key=key):
                data = get_application_data()
                data['__registry_metadata'].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

        for key in document_fields:
            with self.subTest(self.COMPILED_MODULE, key=key):
                data = get_application_data()
                data['compiled_modules'][0].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.ATTACHMENTS, key=key):
                data = get_application_data()
                data['attachments'][0].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.OUTCOME_FILE, key=key):
                data = get_application_data()
                data['outcome_file'].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.OUTCOME_ATTACHMENTS, key=key):
                data = get_application_data()
                data['outcome_attachments'][0].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.OUTBOUND_INTEGRATION_FILE, key=key):
                data = get_application_data()
                data['integrations'][-1]['outbound'].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS, key=key):
                data = get_application_data()
                data['integrations'][-1]['outbound']['attachments'][0].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.INBOUND_INTEGRATION_FILE, key=key):
                data = get_application_data()
                data['integrations'][-1]['inbound'].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS, key=key):
                data = get_application_data()
                data['integrations'][-1]['inbound']['attachments'][0].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.INBOUND_WITHDRAW_FILE, key=key):
                data = get_application_data()
                data['attachments'][-1].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

        for key in integration_fields:
            with self.subTest(self.INTEGRATIONS, key=key):
                data = get_application_data()
                data['integrations'][-1].pop(key)
                with self.assertRaises(ValidationError):
                    Application(data)

            with self.subTest(self.INTEGRATIONS, key=f'{key}.attachments'):
                data = get_application_data()
                data['integrations'][-1][key].pop('attachments')
                with self.assertRaises(ValidationError):
                    Application(data)

    def test_empty_compiled_modules_list(self):
        data = get_application_data()
        data['compiled_modules'] = []
        application = Application(data)
        self.assertIsNone(application.compiled_module)

    def test_multiple_items_in_compiled_modules_list(self):
        data = get_application_data()
        data['compiled_modules'] = [
            {
                'url': 'https://example.local/inbound-document-1/',
                'originalName': 'inbound_document_1.pdf',
                'description': 'Inbound document 1',
                'protocol_required': True,
                'created_at': '2021-08-10T11:20:30+02:00',
            },
            {
                'url': 'https://example.local/inbound-document-2/',
                'originalName': 'inbound_document_2.pdf',
                'description': 'Inbound document 2',
                'protocol_required': True,
                'created_at': '2021-08-10T12:20:30+02:00',  # latest
            },
            {
                'url': 'https://example.local/inbound-document-3/',
                'originalName': 'inbound_document_3.pdf',
                'description': 'Inbound document 3',
                'protocol_required': True,
                'created_at': '2021-08-10T10:20:30+02:00',
            },
        ]
        compiled_module = Application(data).compiled_module
        self.assertEqual(compiled_module.url, 'https://example.local/inbound-document-2/')

    def test_multiple_items_in_compiled_modules_list_with_undefined_order(self):
        for compiled_module in [
            {
                'url': 'https://example.local/inbound-document-1/',
                'originalName': 'inbound_document_1.pdf',
                'description': 'Inbound document 1',
                'protocol_required': True,
            },
            {
                'url': 'https://example.local/inbound-document-1/',
                'originalName': 'inbound_document_1.pdf',
                'description': 'Inbound document 2',
                'protocol_required': True,
                'created_at': None,
            },
            {
                'url': 'https://example.local/inbound-document-1/',
                'originalName': 'inbound_document_1.pdf',
                'description': 'Inbound document 3',
                'protocol_required': True,
                'created_at': 'invalid',
            },
        ]:
            with self.subTest(compiled_module=compiled_module):
                data = get_application_data()
                data['compiled_modules'] = [
                    compiled_module,
                    {
                        'url': 'https://example.local/inbound-document-2/',
                        'originalName': 'inbound_document_2.pdf',
                        'description': 'Inbound document 2',
                        'protocol_required': True,
                        'created_at': '2021-08-10T12:20:30+02:00',
                    },
                ]
                with self.assertRaises(ValidationError):
                    Application(data)

    def test_empty_integrations_list(self):
        data = get_application_data()
        data['integrations'] = []
        application = Application(data)
        self.assertIsNone(application.outbound_integration_file)
        self.assertEqual(application.outbound_integration_attachments, [])
        self.assertIsNone(application.inbound_integration_file)
        self.assertEqual(application.inbound_integration_attachments, [])

    def test_optional_fields(self):
        with self.subTest(self.APPLICATION, key='flow_changed_at'):
            data = get_application_data()
            data.pop('flow_changed_at')
            application = Application(data)
            self.assertIsNone(application.flow_changed_at)

        with self.subTest(self.APPLICATION, key='outcome_file'):
            data = get_application_data()
            data.pop('outcome_file')
            application = Application(data)
            self.assertIsNone(application.outcome_file)

        with self.subTest(self.APPLICATION, key='integrations'):
            data = get_application_data()
            data.pop('integrations')
            application = Application(data)
            self.assertIsNone(application.outbound_integration_file)
            self.assertEqual(application.outbound_integration_attachments, [])
            self.assertIsNone(application.inbound_integration_file)
            self.assertEqual(application.inbound_integration_attachments, [])

        with self.subTest(self.APPLICATION, key='__registry_metadata'):
            data = get_application_data()
            data.pop('__registry_metadata')
            metadata = Application(data).metadata
            self.assertEqual(metadata.attempts, 0)
            self.assertIsNone(metadata.retry_at)
            self.assertIsNone(metadata.retry_topic)

        for key, attr in [
            ('applicant.data.completename.data.name', 'name'),
            ('applicant.data.completename.data.surname', 'surname'),
            ('applicant.data.fiscal_code.data.fiscal_code', 'fiscal_code'),
            ('applicant.data.address.data.address', 'address'),
            ('applicant.data.address.data.house_number', 'house_number'),
            ('applicant.data.address.data.house_number', 'house_number_digits'),
            ('applicant.data.address.data.house_number', 'house_number_letters'),
            ('applicant.data.address.data.postal_code', 'postal_code'),
            ('applicant.data.address.data.municipality', 'municipality'),
            ('applicant.data.address.data.county', 'county'),
            ('applicant.data.email_address', 'email'),
        ]:
            with self.subTest(self.APPLICANT, key=key, attr=attr):
                data = get_application_data()
                data['data'].pop(key)
                applicant = Application(data).applicant
                self.assertIsNone(getattr(applicant, attr))

        with self.subTest(self.COMPILED_MODULE, key='protocol_required'):
            data = get_application_data()
            data['compiled_modules'][0].pop('protocol_required')
            compiled_module = Application(data).compiled_module
            self.assertIsNotNone(compiled_module)

        with self.subTest(self.ATTACHMENTS, key='protocol_required'):
            data = get_application_data()
            data['attachments'][0].pop('protocol_required')
            attachments = Application(data).attachments
            self.assertEqual(len(attachments), 1)

        with self.subTest(self.OUTCOME_FILE, key='protocol_required'):
            data = get_application_data()
            data['outcome_file'].pop('protocol_required')
            outcome_file = Application(data).outcome_file
            self.assertIsNotNone(outcome_file)

        with self.subTest(self.OUTCOME_ATTACHMENTS, key='protocol_required'):
            data = get_application_data()
            data['outcome_attachments'][0].pop('protocol_required')
            outcome_attachments = Application(data).outcome_attachments
            self.assertEqual(len(outcome_attachments), 1)

        with self.subTest(self.OUTBOUND_INTEGRATION_FILE, key='protocol_required'):
            data = get_application_data()
            data['integrations'][-1]['outbound'].pop('protocol_required')
            outbound_integration_file = Application(data).outbound_integration_file
            self.assertIsNotNone(outbound_integration_file)

        with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS, key='protocol_required'):
            data = get_application_data()
            data['integrations'][-1]['outbound']['attachments'][0].pop('protocol_required')
            outbound_integration_attachments = Application(data).outbound_integration_attachments
            self.assertEqual(len(outbound_integration_attachments), 1)

        with self.subTest(self.INBOUND_INTEGRATION_FILE, key='protocol_required'):
            data = get_application_data()
            data['integrations'][-1]['inbound'].pop('protocol_required')
            inbound_integration_file = Application(data).inbound_integration_file
            self.assertIsNotNone(inbound_integration_file)

        with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS, key='protocol_required'):
            data = get_application_data()
            data['integrations'][-1]['inbound']['attachments'][0].pop('protocol_required')
            inbound_integration_attachments = Application(data).inbound_integration_attachments
            self.assertEqual(len(inbound_integration_attachments), 1)

        with self.subTest(self.INBOUND_WITHDRAW_FILE, key='protocol_required'):
            data = get_application_data()
            data['attachments'][-1].pop('protocol_required')
            inbound_withdraw_file = Application(data).inbound_withdraw_file
            self.assertIsNotNone(inbound_withdraw_file)

    def test_nullable_fields(self):
        with self.subTest(self.APPLICATION, key='event_id'):
            data = get_application_data()
            data['event_id'] = None
            application = Application(data)
            self.assertEqual(application.event_id, '00000000-0000-0000-0000-000000000000')

        with self.subTest(self.APPLICATION, key='submitted_at'), freeze_time(NOW):
            data = get_application_data()
            data['submitted_at'] = None
            application = Application(data)
            self.assertEqual(application.submitted_at, NOW)
            self.assertEqual(application.submitted_at.utcoffset(), timedelta(hours=2))

        with self.subTest(self.INTEGRATIONS, key='inbound'):
            data = get_application_data()
            data['integrations'][-1]['inbound'] = None
            application = Application(data)
            self.assertIsNone(application.inbound_integration_file)
            self.assertEqual(application.inbound_integration_attachments, [])

    def test_exclusions_via_protocol_required_field(self):
        with self.subTest(self.COMPILED_MODULE):
            data = get_application_data()
            data['compiled_modules'][0]['protocol_required'] = False
            application = Application(data)
            self.assertIsNone(application.compiled_module)

        with self.subTest(self.ATTACHMENTS):
            data = get_application_data()
            data['attachments'][0]['protocol_required'] = False
            application = Application(data)
            self.assertEqual(application.attachments, [])

        with self.subTest(self.OUTCOME_FILE):
            data = get_application_data()
            data['outcome_file']['protocol_required'] = False
            application = Application(data)
            self.assertIsNone(application.outcome_file)

        with self.subTest(self.OUTCOME_ATTACHMENTS):
            data = get_application_data()
            data['outcome_attachments'][0]['protocol_required'] = False
            application = Application(data)
            self.assertEqual(application.outcome_attachments, [])

        with self.subTest(self.OUTBOUND_INTEGRATION_FILE):
            data = get_application_data()
            data['integrations'][-1]['outbound']['protocol_required'] = False
            application = Application(data)
            self.assertIsNone(application.outbound_integration_file)

        with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS):
            data = get_application_data()
            data['integrations'][-1]['outbound']['attachments'][0]['protocol_required'] = False
            application = Application(data)
            self.assertEqual(application.outbound_integration_attachments, [])

        with self.subTest(self.INBOUND_INTEGRATION_FILE):
            data = get_application_data()
            data['integrations'][-1]['inbound']['protocol_required'] = False
            application = Application(data)
            self.assertIsNone(application.inbound_integration_file)

        with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS):
            data = get_application_data()
            data['integrations'][-1]['inbound']['attachments'][0]['protocol_required'] = False
            application = Application(data)
            self.assertEqual(application.inbound_integration_attachments, [])

        with self.subTest(self.INBOUND_WITHDRAW_FILE):
            data = get_application_data()
            data['attachments'][-1]['protocol_required'] = False
            application = Application(data)
            self.assertIsNone(application.inbound_withdraw_file)

    def test_raw_deepcopy(self):
        data = get_application_data()
        application = Application(data)
        data['data']['foo'] = 'bar'
        self.assertNotIn('foo', application.raw['data'])

    def test_timezone_normalizations(self):
        for datestr in (
            '2021-05-18T15:07:16-03:00',
            '2021-05-18T15:07:16+00:00',
            '2021-05-18T15:07:16+02:00',
            '2021-05-18T15:07:16+03:00',
        ):
            with self.subTest(submitted_at=datestr):
                data = get_application_data()
                data['submitted_at'] = datestr
                application = Application(data)
                self.assertEqual(application.submitted_at.utcoffset(), timedelta(hours=1))

            with self.subTest(flow_changed_at=datestr):
                data = get_application_data()
                data['flow_changed_at'] = datestr
                application = Application(data)
                self.assertEqual(application.flow_changed_at.utcoffset(), timedelta(hours=1))

    def test_status_name_lowercase_coercion(self):
        data = get_application_data()
        data['status_name'] = 'EXAMPLE_STATE'
        application = Application(data)
        self.assertEqual(application.status_name, 'example_state')

    def test_applicant_is_null(self):
        data = get_application_data()
        data['data'] = None
        applicant = Application(data).applicant
        for attr in [
            'name',
            'surname',
            'fiscal_code',
            'address',
            'house_number',
            'house_number_digits',
            'house_number_letters',
            'postal_code',
            'municipality',
            'county',
            'email',
        ]:
            self.assertIsNone(getattr(applicant, attr))

    def test_applicant_empty_string_values(self):
        for key, attr in [
            ('applicant.data.completename.data.name', 'name'),
            ('applicant.data.completename.data.surname', 'surname'),
            ('applicant.data.fiscal_code.data.fiscal_code', 'fiscal_code'),
            ('applicant.data.address.data.address', 'address'),
            ('applicant.data.address.data.house_number', 'house_number'),
            ('applicant.data.address.data.house_number', 'house_number_digits'),
            ('applicant.data.address.data.house_number', 'house_number_letters'),
            ('applicant.data.address.data.postal_code', 'postal_code'),
            ('applicant.data.address.data.municipality', 'municipality'),
            ('applicant.data.address.data.county', 'county'),
            ('applicant.data.email_address', 'email'),
        ]:
            with self.subTest(key=key, attr=attr):
                data = get_application_data()
                data['data'][key] = ''
                applicant = Application(data).applicant
                self.assertIsNone(getattr(applicant, attr))

    def test_applicant_house_number_split(self):
        for raw, parsed in [
            ('42', ('42', None)),
            ('42A', ('42', 'A')),
            ('42 A', ('42', 'A')),
            ('42/A', ('42', 'A')),
            ('42AB', ('42', 'AB')),
            ('A', (None, 'A')),
            ('42,./! A', ('42', 'A')),
            ('A,./! 42', ('42', 'A')),
            ('', (None, None)),
            ('A1B2', ('12', 'AB')),
            ('1A2B', ('12', 'AB')),
        ]:
            with self.subTest(raw=raw):
                data = get_application_data()
                data['data']['applicant.data.address.data.house_number'] = raw
                applicant = Application(data).applicant
                digits, letters = parsed
                self.assertEqual(applicant.house_number_digits, digits)
                self.assertEqual(applicant.house_number_letters, letters)

    def test_document_mimetype_fallback(self):
        with self.subTest(self.COMPILED_MODULE):
            data = get_application_data()
            data['compiled_modules'][0]['originalName'] = 'file.unknown'
            compiled_module = Application(data).compiled_module
            self.assertEqual(compiled_module.mimetype, 'application/octet-stream')

        with self.subTest(self.ATTACHMENTS):
            data = get_application_data()
            data['attachments'][0]['originalName'] = 'file.unknown'
            [attachment] = Application(data).attachments
            self.assertEqual(attachment.mimetype, 'application/octet-stream')

        with self.subTest(self.OUTCOME_FILE):
            data = get_application_data()
            data['outcome_file']['originalName'] = 'file.unknown'
            outcome_file = Application(data).outcome_file
            self.assertEqual(outcome_file.mimetype, 'application/octet-stream')

        with self.subTest(self.OUTCOME_ATTACHMENTS):
            data = get_application_data()
            data['outcome_attachments'][0]['originalName'] = 'file.unknown'
            [outcome_attachment] = Application(data).outcome_attachments
            self.assertEqual(outcome_attachment.mimetype, 'application/octet-stream')

        with self.subTest(self.OUTBOUND_INTEGRATION_FILE):
            data = get_application_data()
            data['integrations'][-1]['outbound']['originalName'] = 'file.unknown'
            outbound_integration_file = Application(data).outbound_integration_file
            self.assertEqual(outbound_integration_file.mimetype, 'application/octet-stream')

        with self.subTest(self.OUTBOUND_INTEGRATION_ATTACHMENTS):
            data = get_application_data()
            data['integrations'][-1]['outbound']['attachments'][0]['originalName'] = 'file.unknown'
            [outbound_integration_attachment] = Application(data).outbound_integration_attachments
            self.assertEqual(outbound_integration_attachment.mimetype, 'application/octet-stream')

        with self.subTest(self.INBOUND_INTEGRATION_FILE):
            data = get_application_data()
            data['integrations'][-1]['inbound']['originalName'] = 'file.unknown'
            inbound_integration_file = Application(data).inbound_integration_file
            self.assertEqual(inbound_integration_file.mimetype, 'application/octet-stream')

        with self.subTest(self.INBOUND_INTEGRATION_ATTACHMENTS):
            data = get_application_data()
            data['integrations'][-1]['inbound']['attachments'][0]['originalName'] = 'file.unknown'
            [inbound_integration_attachment] = Application(data).inbound_integration_attachments
            self.assertEqual(inbound_integration_attachment.mimetype, 'application/octet-stream')

        with self.subTest(self.INBOUND_WITHDRAW_FILE):
            data = get_application_data()
            data['attachments'][-1]['originalName'] = 'file.unknown'
            inbound_withdraw_file = Application(data).inbound_withdraw_file
            self.assertEqual(inbound_withdraw_file.mimetype, 'application/octet-stream')

    def test_direction_inbound(self):
        ApplicationState.objects.create(**get_application_state_data())
        application = Application(get_application_data())
        self.assertEqual(application.direction, ApplicationState.Direction.INBOUND)
        self.assertTrue(application.is_inbound)
        self.assertFalse(application.is_outbound)

    def test_direction_outbound(self):
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        application = Application(get_application_data())
        self.assertEqual(application.direction, ApplicationState.Direction.OUTBOUND)
        self.assertTrue(application.is_outbound)
        self.assertFalse(application.is_inbound)

    def test_current_documents_inbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data())
        application = Application(get_application_data())
        compiled_module, attachment = application.current_documents
        self.assertEqual(compiled_module.url, 'https://example.local/inbound-document/')
        self.assertEqual(compiled_module.original_name, 'inbound_document.pdf')
        self.assertEqual(compiled_module.description, 'Inbound document')
        self.assertEqual(compiled_module.mimetype, 'application/pdf')
        self.assertIsNone(compiled_module.attributes)
        self.assertEqual(attachment.url, 'https://example.local/inbound-attachment/')
        self.assertEqual(attachment.original_name, 'inbound_attachment.doc')
        self.assertEqual(attachment.description, 'Inbound attachment')
        self.assertEqual(attachment.mimetype, 'application/msword')
        self.assertIsNone(attachment.attributes)

    def test_current_documents_outbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        application = Application(get_application_data())
        outcome_file, outcome_attachment = application.current_documents
        self.assertEqual(outcome_file.url, 'https://example.local/outbound-document/')
        self.assertEqual(outcome_file.original_name, 'outbound_document.pdf')
        self.assertEqual(outcome_file.description, 'Outbound document')
        self.assertEqual(outcome_file.mimetype, 'application/pdf')
        self.assertIsNone(outcome_file.attributes)
        self.assertEqual(outcome_attachment.url, 'https://example.local/outbound-attachment/')
        self.assertEqual(outcome_attachment.original_name, 'outbound_attachment.doc')
        self.assertEqual(outcome_attachment.description, 'Outbound attachment')
        self.assertEqual(outcome_attachment.mimetype, 'application/msword')
        self.assertIsNone(outcome_attachment.attributes)

    def test_current_documents_inbound_integration(self):
        ApplicationState.objects.create(**get_application_state_data(integrations=True))
        application = Application(get_application_data())
        inbound_integration_file, inbound_integration_attachment = application.current_documents
        self.assertEqual(inbound_integration_file.url, 'https://example.local/inbound-integration-document/')
        self.assertEqual(inbound_integration_file.original_name, 'inbound_integration_document.pdf')
        self.assertEqual(inbound_integration_file.description, 'Inbound integration document')
        self.assertEqual(inbound_integration_file.mimetype, 'application/pdf')
        self.assertIsNone(inbound_integration_file.attributes)
        self.assertEqual(inbound_integration_attachment.url, 'https://example.local/inbound-integration-attachment/')
        self.assertEqual(inbound_integration_attachment.original_name, 'inbound_integration_attachment.doc')
        self.assertEqual(inbound_integration_attachment.description, 'Inbound integration attachment')
        self.assertEqual(inbound_integration_attachment.mimetype, 'application/msword')
        self.assertIsNone(inbound_integration_attachment.attributes)

    def test_current_documents_outbound_integration(self):
        ApplicationState.objects.create(
            **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND, integrations=True),
        )
        application = Application(get_application_data())
        outbound_integration_file, outbound_integration_attachment = application.current_documents
        self.assertEqual(outbound_integration_file.url, 'https://example.local/outbound-integration-document/')
        self.assertEqual(outbound_integration_file.original_name, 'outbound_integration_document.pdf')
        self.assertEqual(outbound_integration_file.description, 'Outbound integration document')
        self.assertEqual(outbound_integration_file.mimetype, 'application/pdf')
        self.assertIsNone(outbound_integration_file.attributes)
        self.assertEqual(outbound_integration_attachment.url, 'https://example.local/outbound-integration-attachment/')
        self.assertEqual(outbound_integration_attachment.original_name, 'outbound_integration_attachment.doc')
        self.assertEqual(outbound_integration_attachment.description, 'Outbound integration attachment')
        self.assertEqual(outbound_integration_attachment.mimetype, 'application/msword')
        self.assertIsNone(outbound_integration_attachment.attributes)

    def test_current_document_inbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data())
        application = Application(get_application_data())
        compiled_module = application.current_document
        self.assertEqual(compiled_module.url, 'https://example.local/inbound-document/')
        self.assertEqual(compiled_module.original_name, 'inbound_document.pdf')
        self.assertEqual(compiled_module.description, 'Inbound document')
        self.assertEqual(compiled_module.mimetype, 'application/pdf')
        self.assertIsNone(compiled_module.attributes)

    def test_current_document_outbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        application = Application(get_application_data())
        outcome_file = application.current_document
        self.assertEqual(outcome_file.url, 'https://example.local/outbound-document/')
        self.assertEqual(outcome_file.original_name, 'outbound_document.pdf')
        self.assertEqual(outcome_file.description, 'Outbound document')
        self.assertEqual(outcome_file.mimetype, 'application/pdf')
        self.assertIsNone(outcome_file.attributes)

    def test_current_document_inbound_integration(self):
        ApplicationState.objects.create(**get_application_state_data(integrations=True))
        application = Application(get_application_data())
        inbound_integration_file = application.current_document
        self.assertEqual(inbound_integration_file.url, 'https://example.local/inbound-integration-document/')
        self.assertEqual(inbound_integration_file.original_name, 'inbound_integration_document.pdf')
        self.assertEqual(inbound_integration_file.description, 'Inbound integration document')
        self.assertEqual(inbound_integration_file.mimetype, 'application/pdf')
        self.assertIsNone(inbound_integration_file.attributes)

    def test_current_document_outbound_integration(self):
        ApplicationState.objects.create(
            **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND, integrations=True),
        )
        application = Application(get_application_data())
        outbound_integration_file = application.current_document
        self.assertEqual(outbound_integration_file.url, 'https://example.local/outbound-integration-document/')
        self.assertEqual(outbound_integration_file.original_name, 'outbound_integration_document.pdf')
        self.assertEqual(outbound_integration_file.description, 'Outbound integration document')
        self.assertEqual(outbound_integration_file.mimetype, 'application/pdf')
        self.assertIsNone(outbound_integration_file.attributes)

    def test_current_attachments_inbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data())
        application = Application(get_application_data())
        [attachment] = application.current_attachments
        self.assertEqual(attachment.url, 'https://example.local/inbound-attachment/')
        self.assertEqual(attachment.original_name, 'inbound_attachment.doc')
        self.assertEqual(attachment.description, 'Inbound attachment')
        self.assertEqual(attachment.mimetype, 'application/msword')
        self.assertIsNone(attachment.attributes)

    def test_current_attachments_outbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        application = Application(get_application_data())
        [outcome_attachment] = application.current_attachments
        self.assertEqual(outcome_attachment.url, 'https://example.local/outbound-attachment/')
        self.assertEqual(outcome_attachment.original_name, 'outbound_attachment.doc')
        self.assertEqual(outcome_attachment.description, 'Outbound attachment')
        self.assertEqual(outcome_attachment.mimetype, 'application/msword')
        self.assertIsNone(outcome_attachment.attributes)

    def test_current_attachments_inbound_integration(self):
        ApplicationState.objects.create(**get_application_state_data(integrations=True))
        application = Application(get_application_data())
        [inbound_integration_attachment] = application.current_attachments
        self.assertEqual(inbound_integration_attachment.url, 'https://example.local/inbound-integration-attachment/')
        self.assertEqual(inbound_integration_attachment.original_name, 'inbound_integration_attachment.doc')
        self.assertEqual(inbound_integration_attachment.description, 'Inbound integration attachment')
        self.assertEqual(inbound_integration_attachment.mimetype, 'application/msword')
        self.assertIsNone(inbound_integration_attachment.attributes)

    def test_current_documents_inbound_withdraw(self):
        ApplicationState.objects.create(**get_application_state_data(name='status_withdraw'))
        application = Application(get_application_data())
        application.status_name = 'status_withdraw'
        [inbound_withdraw_file] = application.current_documents
        self.assertEqual(inbound_withdraw_file.url, 'https://example.local/inbound-attachment/')
        self.assertEqual(inbound_withdraw_file.original_name, 'inbound_attachment.doc')
        self.assertEqual(inbound_withdraw_file.description, 'Inbound attachment')
        self.assertEqual(inbound_withdraw_file.mimetype, 'application/msword')
        self.assertIsNone(inbound_withdraw_file.attributes)

    def test_current_attachments_outbound_integration(self):
        ApplicationState.objects.create(
            **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND, integrations=True),
        )
        application = Application(get_application_data())
        [outbound_integration_attachment] = application.current_attachments
        self.assertEqual(outbound_integration_attachment.url, 'https://example.local/outbound-integration-attachment/')
        self.assertEqual(outbound_integration_attachment.original_name, 'outbound_integration_attachment.doc')
        self.assertEqual(outbound_integration_attachment.description, 'Outbound integration attachment')
        self.assertEqual(outbound_integration_attachment.mimetype, 'application/msword')
        self.assertIsNone(outbound_integration_attachment.attributes)

    def test_is_registration_required(self):
        for required in [True, False]:
            with self.subTest(self.COMPILED_MODULE, required=required), transaction.atomic():
                ApplicationState.objects.create(**get_application_state_data())
                data = get_application_data()
                data['compiled_modules'][0]['protocol_required'] = required
                application = Application(data)
                self.assertIs(application.is_registration_required, required)
                transaction.set_rollback(True)

            with self.subTest(self.OUTCOME_FILE, required=required), transaction.atomic():
                ApplicationState.objects.create(
                    **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND),
                )
                data = get_application_data()
                data['outcome_file']['protocol_required'] = required
                application = Application(data)
                self.assertIs(application.is_registration_required, required)
                transaction.set_rollback(True)

            with self.subTest(self.OUTBOUND_INTEGRATION_FILE, required=required), transaction.atomic():
                ApplicationState.objects.create(
                    **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND, integrations=True),
                )
                data = get_application_data()
                data['integrations'][-1]['outbound']['protocol_required'] = required
                application = Application(data)
                self.assertIs(application.is_registration_required, required)
                transaction.set_rollback(True)

            with self.subTest(self.INBOUND_INTEGRATION_FILE, required=required), transaction.atomic():
                ApplicationState.objects.create(**get_application_state_data(integrations=True))
                data = get_application_data()
                data['integrations'][-1]['inbound']['protocol_required'] = required
                application = Application(data)
                self.assertIs(application.is_registration_required, required)
                transaction.set_rollback(True)

    def test_registration_date(self):
        application = Application(get_application_data())
        self.assertEqual(application.registration_date, application.flow_changed_at)

    def test_registration_date_fallback(self):
        data = get_application_data()
        data.pop('flow_changed_at')
        application = Application(data)
        self.assertEqual(application.registration_date, application.submitted_at)

    def test_build_registration_data_inbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data())
        application = Application(get_application_data())
        application.compiled_module.attributes = SimpleNamespace(remote_id='123')
        reg_number, reg_dt = '376', datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
        data = application.build_registration_data(reg_number, reg_dt)
        self.assertEqual(
            data,
            {
                'protocol_folder_code': None,
                'protocol_document_id': '123',
                'protocol_number': '376',
                'protocolled_at': '2021-05-25T10:20:30.123456+00:00',
            },
        )

    def test_build_registration_data_outbound_no_integration(self):
        ApplicationState.objects.create(**get_application_state_data(direction=ApplicationState.Direction.OUTBOUND))
        application = Application(get_application_data())
        application.outcome_file.attributes = SimpleNamespace(remote_id='456')
        reg_number, reg_dt = '376', datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
        data = application.build_registration_data(reg_number, reg_dt)
        self.assertEqual(
            data,
            {
                'outcome_protocol_document_id': '456',
                'outcome_protocol_number': '376',
                'outcome_protocolled_at': '2021-05-25T10:20:30.123456+00:00',
            },
        )

    def test_build_registration_data_inbound_integration(self):
        ApplicationState.objects.create(**get_application_state_data(integrations=True))
        application = Application(get_application_data())
        application.inbound_integration_file.attributes = SimpleNamespace(remote_id='789')
        reg_number, reg_dt = '376', datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
        data = application.build_registration_data(reg_number, reg_dt)
        self.assertEqual(
            data,
            {
                'integration_inbound_protocol_document_id': '789',
                'integration_inbound_protocol_number': '376',
                'integration_inbound_protocolled_at': '2021-05-25T10:20:30.123456+00:00',
            },
        )

    def test_build_registration_data_outbound_integration(self):
        ApplicationState.objects.create(
            **get_application_state_data(direction=ApplicationState.Direction.OUTBOUND, integrations=True),
        )
        application = Application(get_application_data())
        application.outbound_integration_file.attributes = SimpleNamespace(remote_id='012')
        reg_number, reg_dt = '376', datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
        data = application.build_registration_data(reg_number, reg_dt)
        self.assertEqual(
            data,
            {
                'integration_outbound_protocol_document_id': '012',
                'integration_outbound_protocol_number': '376',
                'integration_outbound_protocolled_at': '2021-05-25T10:20:30.123456+00:00',
            },
        )

    def test_build_registration_data_with_no_sdc_update(self):
        ApplicationState.objects.create(**get_application_state_data(update_sdc=False))
        application = Application(get_application_data())
        application.compiled_module.attributes = SimpleNamespace(remote_id='123')
        reg_number, reg_dt = '376', datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)
        data = application.build_registration_data(reg_number, reg_dt)
        self.assertEqual(data, {})

    def test_retry_data(self):
        application = Application(get_application_data())
        with freeze_time(NOW):
            topic, value = application.retry_data
        self.assertEqual(topic, 'retry_180_test')
        self.assertEqual(value, RETRY_DATA_VALUE)

    def test_retry_data_deepcopy(self):
        application = Application(get_application_data())
        raw, metadata = deepcopy(application.raw), deepcopy(application.metadata)
        _ = application.retry_data
        self.assertEqual(application.raw, raw)
        self.assertEqual(application.metadata, metadata)

    def test_is_first_attempt(self):
        with self.subTest(ret=True):
            data = get_application_data()
            data.pop('__registry_metadata')
            application = Application(data)
            self.assertTrue(application.is_first_attempt)

        with self.subTest(ret=False):
            application = Application(get_application_data())
            self.assertFalse(application.is_first_attempt)

    def test_is_last_attempt(self):
        with self.subTest(ret=True):
            application = Application(get_application_data())
            for _ in range(9):
                application.metadata.incr_attempts()
            self.assertTrue(application.is_last_attempt)

        with self.subTest(ret=False):
            application = Application(get_application_data())
            self.assertFalse(application.is_last_attempt)

    def test_time_since_submission(self):
        for submitted_at, delta in [
            ('0001-01-01T00:00:00+00:00', timedelta(days=737934, seconds=37230, microseconds=123456)),
            ('2021-05-24T10:20:29+00:00', timedelta(days=1, seconds=1, microseconds=123456)),
            ('2021-05-24T12:20:29+02:00', timedelta(days=1, seconds=1, microseconds=123456)),
            ('9999-12-31T22:59:59+00:00', timedelta(days=-2914125, seconds=40831, microseconds=123456)),
        ]:
            with self.subTest(submitted_at=submitted_at):
                with freeze_time(NOW):
                    data = get_application_data()
                    data['submitted_at'] = submitted_at
                    application = Application(data)
                    self.assertEqual(application.time_since_submission, delta)

    def test_dead_letter_data(self):
        application = Application(get_application_data())
        expected = get_application_data()
        expected.pop('__registry_metadata')
        self.assertEqual(application.dead_letter_data, expected)

    def test_dead_letter_data_deepcopy(self):
        application = Application(get_application_data())
        raw = deepcopy(application.raw)
        _ = application.dead_letter_data
        self.assertEqual(application.raw, raw)

    def test_metadata_incr_attempts(self):
        data = get_application_data()
        data.pop('__registry_metadata')
        metadata = Application(data).metadata
        for attempts, retry_at, retry_topic in [
            # now + 10min
            (1, '2021-05-25T10:30:30.123456+00:00', 'retry_10_test'),
            (2, '2021-05-25T10:30:30.123456+00:00', 'retry_10_test'),
            # now + 30min
            (3, '2021-05-25T10:50:30.123456+00:00', 'retry_30_test'),
            (4, '2021-05-25T10:50:30.123456+00:00', 'retry_30_test'),
            (5, '2021-05-25T10:50:30.123456+00:00', 'retry_30_test'),
            # now + 180min
            (6, '2021-05-25T13:20:30.123456+00:00', 'retry_180_test'),
            (7, '2021-05-25T13:20:30.123456+00:00', 'retry_180_test'),
            (8, '2021-05-25T13:20:30.123456+00:00', 'retry_180_test'),
            (9, '2021-05-25T13:20:30.123456+00:00', 'retry_180_test'),
            # now + 720min
            (10, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
            (11, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
            (12, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
            (13, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
            (14, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
            (15, '2021-05-25T22:20:30.123456+00:00', 'retry_720_test'),
        ]:
            with self.subTest(attempts=attempts):
                with freeze_time(NOW):
                    metadata.incr_attempts()
                self.assertEqual(metadata.attempts, attempts)
                self.assertEqual(metadata.retry_at, retry_at)
                self.assertEqual(metadata.retry_topic, retry_topic)

    def test_metadata_serialize(self):
        with self.subTest('default metadata'):
            data = get_application_data()
            data.pop('__registry_metadata')
            metadata = Application(data).metadata
            self.assertEqual(
                metadata.serialize(),
                {
                    'attempts': 0,
                    'retry_at': None,
                    'retry_topic': None,
                },
            )

        with self.subTest('existing metadata'):
            data = get_application_data()
            metadata = Application(data).metadata
            self.assertEqual(
                metadata.serialize(),
                {
                    'attempts': 5,
                    'retry_at': '2021-11-30T09:46:08.403371+00:00',
                    'retry_topic': 'retry_30_test',
                },
            )

    def test_applicant_address_with_house_number(self):
        with self.subTest('both address and house_number are strings'):
            data = get_application_data()
            applicant = Application(data).applicant
            self.assertEqual(applicant.address_with_house_number, 'Corso Buenos Aires 42/A')

        with self.subTest('null address'):
            data = get_application_data()
            data['data'].pop('applicant.data.address.data.address')
            applicant = Application(data).applicant
            self.assertIsNone(applicant.address_with_house_number)

        with self.subTest('null house_number'):
            data = get_application_data()
            data['data'].pop('applicant.data.address.data.house_number')
            applicant = Application(data).applicant
            self.assertEqual(applicant.address_with_house_number, 'Corso Buenos Aires')

    def test_applicant_full_name(self):
        with self.subTest('name and surname present'):
            data = get_application_data()
            applicant = Application(data).applicant
            self.assertEqual(applicant.full_name, 'Mario Rossi')

        with self.subTest('name and surname absent'):
            data = get_application_data()
            data['data'].pop('applicant.data.completename.data.name')
            data['data'].pop('applicant.data.completename.data.surname')
            applicant = Application(data).applicant
            self.assertEqual(applicant.full_name, '')


class CredentialsTestCase(SimpleTestCase):
    def test_fields(self):
        credentials = Credentials(username='user01', password='s3cr3t')
        self.assertEqual(credentials.username, 'user01')
        self.assertEqual(credentials.password, 's3cr3t')

    def test_required_values(self):
        for key in ['username', 'password']:
            with self.subTest(key=key):
                data = {'username': 'user01', 'password': 's3cr3t'}
                data.pop(key)
                with self.assertRaises(TypeError):
                    Credentials(**data)

    def test_to_sdc(self):
        credentials = Credentials(username='user01', password='s3cr3t')
        self.assertEqual(credentials.to_sdc(), {'username': 'user01', 'password': 's3cr3t'})


class TokenTestCaseMixin:
    auth_url = f'{BASE_URL}/auth'

    def test_token_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, body=error())
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_token_response_status_failure(self):
        for code in [401, 500]:
            with self.subTest(code=code), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=code)
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 1)

    def test_token_response_body_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'not-a-token': 'xyz'}, status=200)
            client = _create_client()

            with self.assertRaises(ValidationError):
                self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)

    def test_token_unexpected_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, body=Exception())
            client = _create_client()

            with self.assertRaises(SDCError):
                self.invoke(client)

            self.assertEqual(len(rsps.calls), 1)

    def test_token_cache(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            client = _create_client()

            for _ in range(2):
                try:
                    self.invoke(client)
                except TransportError:
                    pass

            rsps.assert_call_count(self.auth_url, 1)

    def test_token_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            client = _create_client()

            with mock.patch.object(requests, 'post', wraps=requests.post) as mocked_post:
                try:
                    self.invoke(client)
                except TransportError:
                    pass

                self.assertEqual(mocked_post.call_args.args, (self.auth_url,))
                self.assertEqual(mocked_post.call_args.kwargs['timeout'], 30)


class ClientGetDocumentTestCase(TokenTestCaseMixin, SimpleTestCase):
    document_url = f'{BASE_URL}/documents/1'

    def invoke(self, client):
        return client.get_document(self.document_url)

    def test_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
                rsps.add(responses.GET, self.document_url, body=error())
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 2)

    def test_response_status_failure(self):
        for code in [401, 500]:
            with self.subTest(code=code), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
                rsps.add(responses.GET, self.document_url, body=b'abc', status=code)
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 2)

    def test_unexpected_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            rsps.add(responses.GET, self.document_url, body=Exception())
            client = _create_client()

            with self.assertRaises(SDCError):
                self.invoke(client)

            self.assertEqual(len(rsps.calls), 2)

    def test_success(self):
        with responses.RequestsMock() as rsps:
            token = 'xyz'
            rsps.add(
                responses.POST,
                self.auth_url,
                json={'token': token},
                status=200,
                match=[json_params_matcher({'username': 'user01', 'password': 's3cr3t'})],
            )

            body = b'abc'
            rsps.add(responses.GET, self.document_url, body=body, status=200)
            client = _create_client()

            document = self.invoke(client)

            self.assertEqual(document, body)
            self.assertEqual(len(rsps.calls), 2)
            self.assertEqual(rsps.calls[1].request.headers['Authorization'], f'Bearer {token}')

    def test_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            rsps.add(responses.GET, self.document_url, body=b'abc', status=200)
            client = _create_client()

            with mock.patch.object(requests, 'get', wraps=requests.get) as mocked_get:
                self.invoke(client)

                self.assertEqual(mocked_get.call_args.kwargs['timeout'], 30)


class ClientUpdateInboundApplicationTestCase(TokenTestCaseMixin, SimpleTestCase):
    application_url = f'{BASE_URL}/applications/1'

    def invoke(self, client):
        data = {'protocol_number': '376', 'protocolled_at': '2021-05-25T10:20:30.123456+00:00'}
        return client.update_application(1, data)

    def test_transport_failure(self):
        for error in [ConnectionError, RequestException, SSLError, Timeout]:
            with self.subTest(error=error), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
                rsps.add(responses.PATCH, self.application_url, body=error())
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 2)

    def test_response_status_failure(self):
        for code in [401, 500]:
            with self.subTest(code=code), responses.RequestsMock() as rsps:
                rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
                rsps.add(responses.PATCH, self.application_url, body=b'abc', status=code)
                client = _create_client()

                with self.assertRaises(TransportError):
                    self.invoke(client)

                self.assertEqual(len(rsps.calls), 2)

    def test_unexpected_failure(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            rsps.add(responses.PATCH, self.application_url, body=Exception())
            client = _create_client()

            with self.assertRaises(SDCError):
                self.invoke(client)

            self.assertEqual(len(rsps.calls), 2)

    def test_success(self):
        with responses.RequestsMock() as rsps:
            token = 'xyz'
            rsps.add(
                responses.POST,
                self.auth_url,
                json={'token': token},
                status=200,
                match=[json_params_matcher({'username': 'user01', 'password': 's3cr3t'})],
            )

            rsps.add(
                responses.PATCH,
                self.application_url,
                body=b'abc',
                status=200,
                match=[
                    json_params_matcher(
                        {
                            'protocol_number': '376',
                            'protocolled_at': '2021-05-25T10:20:30.123456+00:00',
                        },
                    ),
                ],
            )
            client = _create_client()

            ret = self.invoke(client)

            self.assertIsNone(ret)
            self.assertEqual(len(rsps.calls), 2)
            self.assertEqual(rsps.calls[1].request.headers['Authorization'], f'Bearer {token}')

    def test_timeout(self):
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, self.auth_url, json={'token': 'xyz'}, status=200)
            rsps.add(responses.PATCH, self.application_url, body=b'abc', status=200)
            client = _create_client()

            with mock.patch.object(requests, 'patch', wraps=requests.patch) as mocked_patch:
                self.invoke(client)

                self.assertEqual(mocked_patch.call_args.kwargs['timeout'], 30)
