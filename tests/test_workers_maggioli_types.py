from django.test import SimpleTestCase
from lxml import etree

from apps.workers.maggioli.types import OptionalType, RequiredType


class RequiredTypeTestCase(SimpleTestCase):
    def test_empty(self):
        rtype = RequiredType('Example')
        self.assertEqual(etree.tostring(rtype()), b'<Example/>')

    def test_contents_and_attributes(self):
        outer = RequiredType('Outer')
        inner = RequiredType('Inner')
        self.assertEqual(
            etree.tostring(outer(inner(), attr='value')),
            b'<Outer attr="value"><Inner/></Outer>',
        )

    def test_null_contents_and_null_attributes(self):
        rtype = RequiredType('Example')
        self.assertEqual(
            etree.tostring(rtype(None, attr=None)),
            b'<Example/>',
        )


class OptionalTypeTestCase(SimpleTestCase):
    def test_empty(self):
        otype = OptionalType('Example')
        self.assertIsNone(otype())

    def test_contents_and_attributes(self):
        outer = OptionalType('Outer')
        inner = RequiredType('Inner')
        self.assertEqual(
            etree.tostring(outer(inner(), attr='value')),
            b'<Outer attr="value"><Inner/></Outer>',
        )

    def test_null_contents_and_null_attributes(self):
        otype = OptionalType('Example')
        self.assertIsNone(otype(None, attr=None))

    def test_weak_attributes(self):
        otype = OptionalType('Example', weak_attrs=['attr'])
        self.assertIsNone(otype(attr='value'))
