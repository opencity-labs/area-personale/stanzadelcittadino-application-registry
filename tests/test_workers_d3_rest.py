from datetime import datetime, timezone

from django.test import SimpleTestCase
from freezegun import freeze_time
from requests.models import Response

from apps.workers.d3.rest import InsertResult
from apps.workers.errors import RESTValidationError

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(RESTValidationError):
            response = Response()
            response.status_code = 400
            response._content = b'{"error": "error"}'
            InsertResult(response)

    @freeze_time(NOW)
    def test_success_upload(self):
        response = Response()
        response.status_code = 201
        response.headers = {
            'Location': '/dms/r/7ff67ade-4bbe-5aea-b370-19904efbd7cb/blob/chunk/'
            '2023-03-21_temp_master_file_test99_sgv_ea86e5db-5e27-',
        }
        result = InsertResult(response)
        self.assertEqual(
            result.content_location_uri,
            '/dms/r/7ff67ade-4bbe-5aea-b370-19904efbd7cb/blob/chunk/'
            '2023-03-21_temp_master_file_test99_sgv_ea86e5db-5e27-',
        )
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_document(self):
        response = Response()
        response.status_code = 201
        response.headers = {
            'Location': '/dms/r/7ff67ade-4bbe-5aea-b370-19904efbd7cb/o2m/PO00009969'
            '?sourceid=%2fdms%2fr%2f7ff67ade-4bbe-5aea-b370-19904efbd7cb%2fsource',
        }
        result = InsertResult(response)
        self.assertEqual(result.document_id, 'PO00009969')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_registration(self):
        response = Response()
        response.status_code = 200
        response._content = (
            b'{"items": [{"sourceProperties": '
            b'[{"key": "23", "value": "0000825"}, {"key": "24", "value": "2023-01-01"}]}]}'
        )
        result = InsertResult(response)
        self.assertEqual(result.number, '0000825')
        self.assertEqual(result.year, '2023')
        self.assertEqual(result.created_at, NOW)
