from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.halley_cloud.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(SimpleNamespace(coderrore=123, deserrore='Error'))

    @freeze_time(NOW)
    def test_success(self):
        result = InsertResult(SimpleNamespace(anno=2021, numero=376, coderrore=0))
        self.assertEqual(result.number, '376')
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.created_at, NOW)
