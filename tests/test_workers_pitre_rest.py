from datetime import datetime, timezone

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import RESTValidationError
from apps.workers.pitre.rest import PitreResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class ProtocollazioneResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(RESTValidationError):
            PitreResult({'Code': 1, 'ErrorMessage': 'error'})

    @freeze_time(NOW)
    def test_success_folder(self):
        result = PitreResult(
            {
                'Code': 0,
                'Project': {
                    'Code': 376,
                    'Id': 1,
                },
            },
        )
        self.assertEqual(result.folder, '376')
        self.assertEqual(result.folder_id, '1')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_document(self):
        result = PitreResult(
            {
                'Code': 0,
                'Document': {
                    'Id': 1,
                },
            },
        )
        self.assertEqual(result.document, '1')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_sender(self):
        result = PitreResult(
            {
                'Code': 0,
                'Correspondents': [
                    {
                        'Id': 1,
                    },
                ],
            },
        )
        self.assertEqual(result.sender, {'Id': 1})
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_registration(self):
        result = PitreResult(
            {
                'Code': 0,
                'Document': {
                    'Id': 1,
                    'Signature': 123,
                },
            },
        )
        self.assertEqual(result.document, '1')
        self.assertEqual(result.number, '123')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_classification_scheme(self):
        result = PitreResult(
            {
                'Code': 0,
                'ClassificationScheme': {
                    'Id': 1,
                },
            },
        )
        self.assertEqual(result.classification_scheme_id, '1')
        self.assertEqual(result.created_at, NOW)
