from datetime import datetime, timezone

from django.test import SimpleTestCase
from freezegun import freeze_time
from lxml import etree

from apps.workers.errors import ValidationError
from apps.workers.hypersic.soap import CorrispondenteNotFoundError, InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(
                dict(  # noqa
                    _value_1=dict(  # noqa
                        _value_1=[
                            dict(  # noqa
                                ERRORE=dict(  # noqa
                                    DESCRIZIONE='Authentication Failed.',
                                ),
                            ),
                        ],
                    ),
                ),
            )

    def test_error_code_correspondent(self):
        error = """
        <diffgr:diffgram
            xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"
            xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"
            xmlns="http://services.apsystems.it/"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        />
        """
        with self.assertRaises(CorrispondenteNotFoundError):
            InsertResult(
                dict(  # noqa
                    _value_1=etree.fromstring(error.strip()),
                ),
            )

    @freeze_time(NOW)
    def test_success_get_correspondent(self):
        result = InsertResult(
            dict(  # noqa
                _value_1=dict(  # noqa
                    _value_1=[
                        dict(  # noqa
                            corrispondente=dict(  # noqa
                                codice=101,
                                descrizione='Ufficio SUAP',
                            ),
                        ),
                    ],
                ),
            ),
        )
        self.assertEqual(result.correspondent_code, 101)
        self.assertEqual(result.correspondent_description, 'Ufficio SUAP')

    @freeze_time(NOW)
    def test_success_insert_correspondent(self):
        result = InsertResult(
            dict(  # noqa
                _value_1=dict(  # noqa
                    _value_1=[
                        dict(  # noqa
                            corrispondente=dict(  # noqa
                                codice=101,
                            ),
                        ),
                    ],
                ),
            ),
        )
        self.assertEqual(result.correspondent_code, 101)

    @freeze_time(NOW)
    def test_success(self):
        result = InsertResult(
            dict(  # noqa
                _value_1=dict(  # noqa
                    _value_1=[
                        dict(  # noqa
                            protocollo=dict(  # noqa
                                codice=976167,
                                numero_protocollo=456,
                                data_protocollo='10/10/2021',
                            ),
                        ),
                    ],
                ),
            ),
        )
        self.assertEqual(result.registry_code, 976167)
        self.assertEqual(result.number, 456)
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.created_at, NOW)
