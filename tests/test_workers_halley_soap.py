from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.halley.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(SimpleNamespace(Esito=123, MessaggioEsito='error'))

    @freeze_time(NOW)
    def test_success(self):
        result = InsertResult(SimpleNamespace(Esito=0, Numero=376, Anno=2021, DataRegistrazione='2021-05-25'))
        self.assertEqual(result.number, '376')
        self.assertEqual(result.year, '2021')
        self.assertEqual(result.date, '2021-05-25')
        self.assertEqual(result.created_at, NOW)
