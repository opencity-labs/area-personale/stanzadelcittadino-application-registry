from datetime import datetime, timedelta, timezone

from django.test import SimpleTestCase, override_settings
from freezegun import freeze_time

from apps.applications.metrics import GravelGatewayClient

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class GravelGatewayClientTestCase(SimpleTestCase):
    @override_settings(GRAVEL_GATEWAY_HOST=None)
    def test_incr_retries_disabled(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        with freeze_time(NOW):
            client.incr_retries('tenant-slug')

        self.assertEqual(calls, [])

    def test_incr_retries_success(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        with freeze_time(NOW):
            client.incr_retries('tenant-slug')

        self.assertEqual(
            calls,
            [
                (
                    'http://gravel-test:4278/metrics/job/registration',
                    'POST',
                    5,
                    [
                        ('Content-Type', 'text/plain; version=0.0.4; charset=utf-8'),
                        ('Authorization', 'Basic s3cr3t'),
                    ],
                    b'# HELP registry_retries_total Registration retries\n'
                    b'# TYPE registry_retries_total counter\n'
                    b'registry_retries_total{clearmode="aggregate",tenant="tenant-slug"} 1.0\n'
                    b'# HELP registry_retries_created Registration retries\n'
                    b'# TYPE registry_retries_created gauge\n'
                    b'registry_retries_created{clearmode="aggregate",tenant="tenant-slug"} 1.621938030123456e+09\n',
                ),
            ],
        )

    def test_incr_retries_failure(self):
        def handler(*args):
            raise Exception

        client = GravelGatewayClient(handler=handler)

        try:
            client.incr_retries('tenant-slug')
        except Exception:
            self.fail('should not raise')

    @override_settings(GRAVEL_GATEWAY_HOST=None)
    def test_set_latency_disabled(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        client.set_latency(timedelta(minutes=5, seconds=15, microseconds=123456))

        self.assertEqual(calls, [])

    def test_set_latency_success(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        client.set_latency(timedelta(minutes=5, seconds=15, microseconds=123456))

        self.assertEqual(
            calls,
            [
                (
                    'http://gravel-test:4278/metrics/job/registration',
                    'POST',
                    5,
                    [
                        ('Content-Type', 'text/plain; version=0.0.4; charset=utf-8'),
                        ('Authorization', 'Basic s3cr3t'),
                    ],
                    b'# HELP registry_latency_seconds Registration latency\n'
                    b'# TYPE registry_latency_seconds gauge\n'
                    b'registry_latency_seconds{clearmode="replace"} 315.123456\n',
                ),
            ],
        )

    def test_set_latency_failure(self):
        def handler(*args):
            raise Exception

        client = GravelGatewayClient(handler=handler)

        try:
            client.set_latency(timedelta(minutes=5, seconds=15, microseconds=123456))
        except Exception:
            self.fail('should not raise')

    @override_settings(GRAVEL_GATEWAY_HOST=None)
    def test_incr_dead_lettered_disabled(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        with freeze_time(NOW):
            client.incr_dead_lettered('tenant-slug')

        self.assertEqual(calls, [])

    def test_incr_dead_lettered_success(self):
        calls = []

        def handler(*args):
            calls.append(args)
            return lambda: None

        client = GravelGatewayClient(handler=handler)

        with freeze_time(NOW):
            client.incr_dead_lettered('tenant-slug')

        self.assertEqual(
            calls,
            [
                (
                    'http://gravel-test:4278/metrics/job/registration',
                    'POST',
                    5,
                    [
                        ('Content-Type', 'text/plain; version=0.0.4; charset=utf-8'),
                        ('Authorization', 'Basic s3cr3t'),
                    ],
                    b'# HELP registry_dead_lettered_total Dead-lettered applications\n'
                    b'# TYPE registry_dead_lettered_total counter\n'
                    b'registry_dead_lettered_total{clearmode="aggregate",tenant="tenant-slug"} 1.0\n'
                    b'# HELP registry_dead_lettered_created Dead-lettered applications\n'
                    b'# TYPE registry_dead_lettered_created gauge\n'
                    b'registry_dead_lettered_created{clearmode="aggregate",tenant="tenant-slug"} 1.621938030123456e+09'
                    b'\n',
                ),
            ],
        )

    def test_incr_dead_lettered_failure(self):
        def handler(*args):
            raise Exception

        client = GravelGatewayClient(handler=handler)

        try:
            client.incr_dead_lettered('tenant-slug')
        except Exception:
            self.fail('should not raise')
