from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.insiel.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(SimpleNamespace(esito=False, errore='error'))

    @freeze_time(NOW)
    def test_success_insert_folder(self):
        result = InsertResult(
            SimpleNamespace(
                esito=True,
                pratica=SimpleNamespace(
                    codiceUfficio='ABC',
                    codiceRegistro='  1',
                    anno='2022',
                    numero='376',
                    subNumero='',
                    dataApertura=NOW,
                    progDoc='10280',
                    progMovi='1',
                ),
            ),
        )

        self.assertEqual(result.folder_number, '376')
        self.assertEqual(
            result.folder_data,
            {
                'codiceUfficio': 'ABC',
                'codiceRegistro': '  1',
                'anno': '2022',
                'numero': '376',
                'subNumero': '',
                'dataApertura': '2021-05-25',
                'progDoc': '10280',
                'progMovi': '1',
            },
        )
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_insert_document(self):
        result = InsertResult(
            SimpleNamespace(
                esito=True,
                registrazione=[
                    SimpleNamespace(
                        codiceUfficio='ABC',
                        codiceRegistro='ABC',
                        anno='2022',
                        numero='376',
                        verso='A',
                        data=NOW,
                        progDoc='10280',
                        progMovi='1',
                    ),
                ],
            ),
        )

        self.assertEqual(result.number, '376')
        self.assertEqual(result.date, '2021-05-25')
        self.assertEqual(
            result.document_data,
            {
                'codiceUfficio': 'ABC',
                'codiceRegistro': 'ABC',
                'anno': '2022',
                'numero': '376',
                'verso': 'A',
                'data': '2021-05-25',
                'progDoc': '10280',
                'progMovi': '1',
            },
        )
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_upload(self):
        result = InsertResult(SimpleNamespace(esito=True, UploadedFile=SimpleNamespace(idFile='15685373794947845002')))

        self.assertEqual(result.id_file, '15685373794947845002')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_success_predispised_applicant(self):
        result = InsertResult(SimpleNamespace(esito=True, anagrafica=SimpleNamespace(descAna='COGNOME NOME - COMUNE')))

        self.assertEqual(result.description, 'COGNOME NOME - COMUNE')
