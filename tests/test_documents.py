import json
import uuid
from dataclasses import fields
from datetime import datetime, timezone
from unittest import TestCase
from uuid import UUID

from apps.applications.models import ApplicationState
from apps.applications.sdc.entities import Application, Document, DocumentStatus, TransmissionType

from ._helpers import get_application_data, get_application_state_data

NOW = datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc)


class DocumentTestCase(TestCase):
    DOCUMENT = 'document'
    REGISTRATION_DATA = 'registration_data'
    FOLDER = 'folder'
    MAIN_DOCUMENT = 'main_document'
    ATTACHMENTS = 'attachments'
    AUTHOR = 'author'
    APPLICATION = 'application'
    APPLICANT = 'applicant'
    COMPILED_MODULE = 'compiled_module'
    OUTCOME_FILE = 'outcome_file'
    OUTCOME_ATTACHMENTS = 'outcome_attachments'
    INTEGRATIONS = 'integrations'
    OUTBOUND_INTEGRATION_FILE = 'outbound_integration_file'
    OUTBOUND_INTEGRATION_ATTACHMENTS = 'outbound_integration_attachments'
    INBOUND_INTEGRATION_FILE = 'inbound_integration_file'
    INBOUND_INTEGRATION_ATTACHMENTS = 'inbound_integration_attachments'
    INBOUND_WITHDRAW_FILE = 'inbound_withdraw_file'

    def test_field_names(self):
        document_fields = [
            'app_id',
            'event_created_at',
            'event_id',
            'title',
            'registry_search_key',
            'id',
            'event_version',
            'status',
            'registration_data',
            'folder',
            'type',
            'remote_id',
            'remote_collection',
            'short_description',
            'description',
            'main_document',
            'attachments',
            'tenant_id',
            'owner_id',
            'created_at',
            'updated_at',
            'author',
            'source_type',
            'recipient_type',
            'topics',
            'image_gallery',
            'related_public_services',
            'normative_requirements',
            'related_documents',
            'life_events',
            'business_events',
            'allowed_readers',
        ]
        registration_data_fields = [
            'transmission_type',
            'date',
            'document_number',
        ]
        folder_fields = [
            'title',
            'id',
        ]
        main_document_fields = [
            'name',
            'description',
            'mime_type',
            'url',
            'filename',
            'md5',
        ]
        attachment_fields = [
            'name',
            'description',
            'mime_type',
            'url',
            'filename',
            'md5',
        ]
        author_fields = [
            'type',
            'tax_identification_number',
            'name',
            'family_name',
            'street_name',
            'building_number',
            'postal_code',
            'email',
            'role',
        ]

        ApplicationState.objects.create(
            **get_application_state_data(name='status_complete', direction=ApplicationState.Direction.OUTBOUND),
        )
        document = Document(
            data=Application(get_application_data(status_name='status_complete')),
            document_id=uuid.uuid4(),
            number='123',
            date=datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc),
            param_folder=None,
            param_status=DocumentStatus.REGISTRATION_PENDING,
        )

        with self.subTest(self.DOCUMENT):
            self.assertEqual([field.name for field in fields(document)], document_fields)
        with self.subTest(self.REGISTRATION_DATA):
            self.assertEqual([field.name for field in fields(document.registration_data)], registration_data_fields)
        with self.subTest(self.FOLDER):
            self.assertEqual([field.name for field in fields(document.folder)], folder_fields)
        with self.subTest(self.MAIN_DOCUMENT):
            self.assertEqual([field.name for field in fields(document.main_document)], main_document_fields)
        with self.subTest(self.ATTACHMENTS):
            self.assertEqual([field.name for field in fields(document.attachments[0])], attachment_fields)
        with self.subTest(self.AUTHOR):
            self.assertEqual([field.name for field in fields(document.author[0])], author_fields)

    def test_creation(self):
        ApplicationState.objects.create(
            **get_application_state_data(
                name='status_submitted',
                code='12',
                direction=ApplicationState.Direction.INBOUND,
            ),
        )
        document = Document(
            document_id=uuid.uuid4(),
            data=Application(get_application_data(status_name='status_submitted')),
            number='123',
            date=datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc),
            param_folder=None,
            param_status=DocumentStatus.REGISTRATION_PENDING,
        )
        self.assertEqual(document.app_id.startswith('stanzadelcittadino-application-registry'), True)
        self.assertTrue(document.title.startswith('Richiesta:'), True)
        self.assertTrue(document.registry_search_key.startswith('Richiesta:'), True)
        self.assertEqual(document.event_version, 1)
        self.assertEqual(document.registration_data.transmission_type, TransmissionType.INBOUND)
        self.assertEqual(document.registration_data.date, datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc))
        self.assertEqual(document.registration_data.document_number, '123')
        self.assertEqual(document.folder.title, 'example service Mario Rossi MRARSS80A01F205W')
        self.assertEqual(document.type, 'application-request')
        self.assertEqual(document.remote_id, UUID('b8a44017-6db1-4abb-af6c-c95f34f08af6'))
        self.assertEqual(document.remote_collection.id, UUID('74dcab79-790e-459e-bbe6-d5972b298b42'))
        self.assertEqual(document.remote_collection.type, 'service')
        self.assertEqual(
            document.short_description,
            'Test Mario Rossi MRARSS80A01F205W - b8a44017-6db1-4abb-af6c-c95f34f08af6',
        )
        self.assertEqual(
            document.description,
            'Test Mario Rossi MRARSS80A01F205W - b8a44017-6db1-4abb-af6c-c95f34f08af6',
        )
        self.assertEqual(document.main_document.name, 'inbound_document.pdf')
        self.assertEqual(document.main_document.description, 'Inbound document')
        self.assertEqual(document.main_document.mime_type, 'application/pdf')
        self.assertEqual(document.main_document.url, 'https://example.local/inbound-document/')
        self.assertEqual(document.main_document.md5, None)
        self.assertEqual(document.main_document.filename, 'inbound_document.pdf')
        self.assertEqual(document.attachments[0].name, 'inbound_attachment.doc')
        self.assertEqual(document.attachments[0].description, 'Inbound attachment')
        self.assertEqual(document.attachments[0].mime_type, 'application/msword')
        self.assertEqual(document.attachments[0].url, 'https://example.local/inbound-attachment/')
        self.assertEqual(document.attachments[0].md5, None)
        self.assertEqual(document.attachments[0].filename, 'inbound_attachment.doc')
        self.assertEqual(document.tenant_id, UUID('f0620f93-c2a8-4ebd-8e64-67ef125177e5'))
        self.assertEqual(document.owner_id, UUID('43f619df-3ba2-4d9d-bcdd-270484eac44d'))
        self.assertEqual(document.author[0].type, 'human')
        self.assertEqual(document.author[0].tax_identification_number, 'MRARSS80A01F205W')
        self.assertEqual(document.author[0].name, 'Mario')
        self.assertEqual(document.author[0].family_name, 'Rossi')
        self.assertEqual(document.author[0].street_name, 'Corso Buenos Aires')
        self.assertEqual(document.author[0].building_number, '42/A')
        self.assertEqual(document.author[0].postal_code, '20124')
        self.assertEqual(document.author[0].email, 'rossi@example.local')
        self.assertEqual(document.author[0].role, 'sender')
        self.assertEqual(document.source_type, 'user')
        self.assertEqual(document.recipient_type, 'tenant')

    def test_short_description_length(self):
        ApplicationState.objects.create(**get_application_state_data(code='123456'))
        application = Application(
            get_application_data(
                subject='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
                'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco '
                'laboris nisi ut aliquip ex ea commodo consequat.',
            ),
        )
        document = Document(
            document_id=uuid.uuid4(),
            data=application,
            number='123',
            date=datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc),
            param_folder=None,
            param_status=DocumentStatus.REGISTRATION_PENDING,
        )
        self.assertLessEqual(len(document.short_description), 255)

    def test_deserialization(self):
        document_id = uuid.uuid4()
        document = Document(
            data=Application(get_application_data(status_name='status_submitted')),
            document_id=document_id,
            number='123',
            date=datetime(2022, 9, 19, 16, 0, 0, tzinfo=timezone.utc),
            param_folder=None,
            param_status=DocumentStatus.REGISTRATION_PENDING,
        )
        document = json.loads(document.json())
        self.assertEqual(UUID(document['id']), document_id)
        self.assertTrue(document['app_id'].startswith('stanzadelcittadino-application-registry'), True)
        self.assertTrue(document['title'].startswith('Richiesta:'), True)
        self.assertTrue(document['registry_search_key'].startswith('Richiesta:'), True)
        self.assertEqual(document['event_version'], 1)
        self.assertEqual(document['registration_data']['transmission_type'], TransmissionType.INBOUND.value)
        self.assertEqual(document['registration_data']['date'], '2022-09-19T18:00:00+02:00')
        self.assertEqual(document['registration_data']['document_number'], '123')
        self.assertEqual(document['folder']['title'], 'example service Mario Rossi MRARSS80A01F205W')
        self.assertEqual(document['type'], 'application-request')
        self.assertEqual(document['remote_id'], 'b8a44017-6db1-4abb-af6c-c95f34f08af6')
        self.assertEqual(document['remote_collection']['id'], '74dcab79-790e-459e-bbe6-d5972b298b42')
        self.assertEqual(document['remote_collection']['type'], 'service')
        self.assertEqual(
            document['short_description'],
            'Test Mario Rossi MRARSS80A01F205W - b8a44017-6db1-4abb-af6c-c95f34f08af6',
        )
        self.assertEqual(
            document['description'],
            'Test Mario Rossi MRARSS80A01F205W - b8a44017-6db1-4abb-af6c-c95f34f08af6',
        )
        self.assertEqual(document['main_document']['name'], 'inbound_document.pdf')
        self.assertEqual(document['main_document']['description'], 'Inbound document')
        self.assertEqual(document['main_document']['mime_type'], 'application/pdf')
        self.assertEqual(document['main_document']['url'], 'https://example.local/inbound-document/')
        self.assertEqual(document['main_document']['md5'], None)
        self.assertEqual(document['main_document']['filename'], 'inbound_document.pdf')
        self.assertEqual(document['attachments'][0]['name'], 'inbound_attachment.doc')
        self.assertEqual(document['attachments'][0]['description'], 'Inbound attachment')
        self.assertEqual(document['attachments'][0]['mime_type'], 'application/msword')
        self.assertEqual(document['attachments'][0]['url'], 'https://example.local/inbound-attachment/')
        self.assertEqual(document['attachments'][0]['md5'], None)
        self.assertEqual(document['attachments'][0]['filename'], 'inbound_attachment.doc')
        self.assertEqual(document['tenant_id'], 'f0620f93-c2a8-4ebd-8e64-67ef125177e5')
        self.assertEqual(document['owner_id'], '43f619df-3ba2-4d9d-bcdd-270484eac44d')
        self.assertEqual(document['author'][0]['type'], 'human')
        self.assertEqual(document['author'][0]['tax_identification_number'], 'MRARSS80A01F205W')
        self.assertEqual(document['author'][0]['name'], 'Mario')
        self.assertEqual(document['author'][0]['family_name'], 'Rossi')
        self.assertEqual(document['author'][0]['street_name'], 'Corso Buenos Aires')
        self.assertEqual(document['author'][0]['building_number'], '42/A')
        self.assertEqual(document['author'][0]['postal_code'], '20124')
        self.assertEqual(document['author'][0]['email'], 'rossi@example.local')
        self.assertEqual(document['author'][0]['role'], 'sender')
        self.assertEqual(document['source_type'], 'user')
        self.assertEqual(document['recipient_type'], 'tenant')
