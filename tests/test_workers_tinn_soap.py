from datetime import datetime, timezone
from types import SimpleNamespace

from django.test import SimpleTestCase
from freezegun import freeze_time

from apps.workers.errors import ValidationError
from apps.workers.tinn.soap import InsertResult

NOW = datetime(2021, 5, 25, 10, 20, 30, 123456, tzinfo=timezone.utc)


class InsertResultTestCase(SimpleTestCase):
    def test_error_code_failure(self):
        with self.assertRaises(ValidationError):
            InsertResult(
                SimpleNamespace(
                    IngErrNumber=-104,
                    strErrString='Errore in fase di protocollazione: Titolario inesistente',
                ),
            )

    @freeze_time(NOW)
    def test_inserimento_success(self):
        result = InsertResult(SimpleNamespace(IngDocID=17330, IngErrNumber=0, strErrString=None))
        self.assertEqual(result.document_id, '17330')
        self.assertEqual(result.created_at, NOW)

    @freeze_time(NOW)
    def test_protocollazione_success(self):
        result = InsertResult(
            SimpleNamespace(
                IngNumPG=38,
                IngAnnoPG=2023,
                StrDataPG='03/08/2023',
                StrSezione='PROT',
                IngErrNumber=0,
                strErrString=None,
            ),
        )
        self.assertEqual(result.number, '38')
        self.assertEqual(result.year, '2023')
        self.assertEqual(result.created_at, NOW)
