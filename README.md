# Application Registry

## Produzione

### REST API

- Per eseguire la build della Docker image, invocare il seguente
  comando dalla directory radice del repository:

        docker build -t application_registry .

- Per creare un utente superuser, eseguire:

        docker run -ti -e DJANGO_SETTINGS_MODULE=... -e DJANGO_SECRET_KEY=... -e DJANGO_DATABASE=... application_registry /app/venv/bin/python3.9 manage.py createsuperuser

- L'interfaccia admin è raggiungibile al path `/admin/` dell'URL base
  a cui risponde l'applicazione.

  Es. https://applications.example.com/admin/

### Variabili d'ambiente

A runtime, l'applicazione accede alle seguenti variabili d'ambiente:

  - `DJANGO_SETTINGS_MODULE`: import path del modulo di settings di
    Django. Il valore per l'ambiente di produzione è
    `application_registry.settings_production`.

  - `DJANGO_SECRET_KEY`: stringa utilizzata da Django a fini
    crittografici. Il valore deve essere unico e non predicibile.

    Es. jsvbgv5y*)awwr5%rki%+3(6y)m^fa3knkk&2z4*rbswd(g#=k

  - `DJANGO_DATABASE`: stringa contenente i dati di connessione al
    database, nel formato:

        <HOST>:<PORT>:<NAME>:<USER>:<PASSWORD>

    Es. db.example.local:5432:application_registry:johndoe:s3cr3t

  - `DJANGO_LOG_LEVEL`: livello di verbosità del logging applicativo, a
    scelta tra:

    - `DEBUG`
    - `INFO`
    - `WARNING`
    - `ERROR`
    - `CRITICAL`

    Default: `INFO`

    Tutto il logging viene normalmente inviato a `stdout`, così da
    poter essere collezionato dagli strumenti messi a disposizione dal
    container orchestrator.

  - `REGISTRY_API_KEY`: API key che i client devono includere nelle
    richieste, come valore dell'header `X-Registry-API-Key`, per fini
    di autorizzazione.

  - `ENABLE_SENTRY_EVENT_FILTER`: abilita/disabilita il filtro sugli eventi di Sentry (valori: `true` o `false`).

  - `KAFKA_BOOTSTRAP_SERVERS`: stringa contenente i dati di connessione a Kafka, nel formato:

        <HOST1>:<PORT1>,<HOST2>:<PORT2>,...

  - `GRAVEL_GATEWAY_HOST`: stringa contenente i dati di connessione a Gravel Gateway, nel formato:

        <HOST>:<PORT>

  - `SENTRY_DSN`: stringa contenente il DSN Sentry.

### Lambda

- Il deploy del codice di una funzione AWS Lambda si ottiene invocando
  il seguente comando dalla directory radice del repository:

        infra/common/scripts/deploy_lambda.sh <NAME>

  dove `NAME` è il nome assegnato alla funzione lato AWS.

  Es. infra/common/scripts/deploy_lambda.sh protocollo-dev

  Lo script ha i seguenti prerequisiti:

  - Python 3.9
  - `aws-cli` v2

- È possibile accedere ai log in realtime di una funzione Lambda per
  mezzo di `aws-cli`, come segue:

        aws logs tail /aws/lambda/<NAME> --follow --format short

## Endpoint

La documentazione è disponibile all'URL `/api/v1/_docs/`.

Gli endpoint autorizzano le richieste verificando l'header
`X-Registry-API-Key`, il cui valore deve corrispondere a quello della
variabile d'ambiente `REGISTRY_API_KEY` impostata lato server.
