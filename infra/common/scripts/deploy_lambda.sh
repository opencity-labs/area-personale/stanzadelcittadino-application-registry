#! /bin/bash
set -xeuo pipefail

if [ -z ${1+x} ]; then echo "Please provide the function name."; exit; fi

PIP_REQUIRE_VIRTUALENV=false
ARN="arn:aws:lambda:eu-west-1:814614828974:function:$1"

rm -rf ./dist deploy.zip
mkdir dist
python3.9 -m pip install \
    --quiet \
    -r requirements-lambda.txt \
    --target ./dist \
    --platform manylinux2014_x86_64 \
    --python-version 3.9 \
    --no-deps
cp -a application_registry ./dist
cp -a apps ./dist
cd dist
zip -q -r ../deploy.zip ./*
cd ..
aws lambda update-function-code \
    --function-name "$1" \
    --zip-file fileb://deploy.zip \
    --cli-connect-timeout=300
aws lambda tag-resource \
    --resource "$ARN" \
    --tags "hash=`git rev-parse HEAD`" \
    --cli-connect-timeout=300
rm -rf ./dist deploy.zip
