#! /bin/bash
set -euo pipefail

SHA=`git rev-parse HEAD`

pushd "`pwd`/../opencontent-deploy"
BRANCH=`git rev-parse --abbrev-ref HEAD`
git pull
sed -i "s/master:.*/master:$SHA/g" boat/sdc-prod-registry.yml
git add boat/sdc-prod-registry.yml
git commit -m "registry: deploy $SHA"
git show
read -rp "Are you sure you want to push to \"$BRANCH\"? (y/n): "
if [[ $REPLY =~ ^[Yy]$ ]]; then
    git push
    echo "Done."
else
    echo "Aborted."
fi
popd
