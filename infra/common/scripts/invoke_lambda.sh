#! /bin/bash
set -xeuo pipefail

if [ -z ${1+x} ]; then echo "Please provide the function name."; exit; fi

PAYLOAD="$(cat << EOF
{
    "id": "4a088f5e-03b0-4a28-8cc7-c2d86c73e266",
    "user": "43f619df-3ba2-4d9d-bcdd-270484eac44d",
    "user_name": "Lorenzo Salvadorini",
    "service": "test-fs",
    "tenant": "2eeb8374-c8f9-4260-9e94-22504e262eb1",
    "service_name": "example service",
    "service_id": "10cdba2b-2db2-4ae1-9682-c587e6bdb52a",
    "event_id": "b751510d-c5e4-4fe9-a8cf-f517f90e68b5",
    "event_version": "1.0",
    "data": {
        "test": false,
        "file": [
            {
                "id": "2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0",
                "name": "dog-e533c0b8-8373-4472-827b-e1a932406833.jpg",
                "url": "https://devsdc.opencontent.it/comune-di-bugliano/api/applications/4a088f5e-03b0-4a28-8cc7-c2d86c73e266/attachments/2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0",
                "originalName": "dog.jpg",
                "protocol_required": true,
                "description": "Allegato"
            }
        ],
        "applicant.data.email_address": "lorenzo@opencontent.it",
        "applicant.data.phone_number": "",
        "applicant.data.fiscal_code.data.fiscal_code": "SLVLNZ76P01g843v",
        "applicant.data.completename.data.name": "Lorenzo",
        "applicant.data.completename.data.surname": "Salvadorini"
    },
    "compiled_modules": [
        {
            "id": "d301da30-54e6-4863-b476-46d62183a788",
            "name": "Modulo Test FS 202105171146",
            "url": "https://devsdc.opencontent.it/comune-di-bugliano/api/applications/4a088f5e-03b0-4a28-8cc7-c2d86c73e266/attachments/d301da30-54e6-4863-b476-46d62183a788",
            "originalName": "60a23deecf36e.pdf",
            "protocol_required": true,
            "description": "Modulo Test FS compilato il 17-05-2021 11:46:15",
            "created_at": "2021-05-17T11:57:02+02:00"
        }
    ],
    "attachments": [
        {
            "id": "2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0",
            "name": "dog-e533c0b8-8373-4472-827b-e1a932406833.jpg",
            "url": "https://devsdc.opencontent.it/comune-di-bugliano/api/applications/4a088f5e-03b0-4a28-8cc7-c2d86c73e266/attachments/2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0",
            "originalName": "60a23b617968f121605579.jpg",
            "protocol_required": true,
            "description": "Allegato",
            "created_at": "2021-05-17T11:46:09+02:00"
        }
    ],
    "creation_time": 1621244696,
    "created_at": "2021-05-17T11:44:56+02:00",
    "submission_time": 1621244775,
    "submitted_at": "2021-05-17T11:46:15+02:00",
    "flow_changed_at": "2021-05-18T16:17:18+02:00",
    "latest_status_change_time": 1621245963,
    "latest_status_change_at": "2021-05-17T12:06:03+02:00",
    "protocol_folder_number": "4a088f5e-03b0-4a28-8cc7-c2d86c73e266",
    "protocol_number": "4a088f5e-03b0-4a28-8cc7-c2d86c73e266",
    "protocol_document_id": "4a088f5e-03b0-4a28-8cc7-c2d86c73e266",
    "protocol_numbers": [
        {"id": "2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0", "protocollo": "a-2f83be82-5113-47dd-9eb2-ddaf6fb8b6a0"},
        {"id": "d301da30-54e6-4863-b476-46d62183a788", "protocollo": "a-d301da30-54e6-4863-b476-46d62183a788"}
    ],
    "outcome_attachments": [],
    "payment_data": [],
    "status": "3000",
    "status_name": "status_submitted",
    "authentication": {"authentication_method": "anonymous"},
    "links": [
        {
            "action": "assign",
            "description": "Assign Application",
            "url": "https://devsdc.opencontent.it/comune-di-bugliano/api/applications/4a088f5e-03b0-4a28-8cc7-c2d86c73e266/transiction/assign"
        }
    ],
    "integrations": []
}
EOF
)"

aws lambda invoke \
    --function-name "$1" \
    --cli-binary-format raw-in-base64-out \
    --payload "$PAYLOAD" \
    /dev/null
