FROM python:3.9.10-slim

RUN apt update \
    && apt upgrade -y \
    && apt install -y \
        gcc \
        gettext \
        libpq-dev \
        wait-for-it

RUN mkdir -p /app/src
COPY . /app/src

RUN python3.9 -m venv /app/venv
RUN /app/venv/bin/python3.9 -m pip install -U --no-cache-dir pip setuptools==71.1.0 wheel
RUN /app/venv/bin/python3.9 -m pip install --no-cache-dir -r /app/src/requirements-web.txt

RUN apt remove -y \
        gcc \
    && apt autoremove -y \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash oc

WORKDIR /app/src
RUN /app/venv/bin/python3.9 manage.py collectstatic --noinput
RUN /app/venv/bin/python3.9 manage.py compilemessages

USER oc
CMD /app/venv/bin/python3.9 manage.py migrate \
    && /app/venv/bin/gunicorn \
    --workers 4 \
    --log-config infra/common/conf/gunicorn_logging.conf \
    --bind 0.0.0.0:8000 \
    application_registry.wsgi
